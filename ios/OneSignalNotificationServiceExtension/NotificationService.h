//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Leudy Martes on 2/4/22.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
