import React, {useState} from 'react';
import {MainContext as MainContextType, User} from '../interfaces';
import AsyncStorage from '@react-native-community/async-storage';
import {useEffect} from 'react';
import * as RNLocalize from 'react-native-localize';
import {MAIN_URL} from '../Utils/Urls';
import OneSignal from 'react-native-onesignal';

var Sound = require('react-native-sound');

Sound.setCategory('Playback');

const user = {
  _id: '',
  name: '',
  lastName: '',
  phone: '',
  email: '',
  avatar: '',
  StripeID: '',
  OnesignalID: '',
  myCategory: [],
  description: '',
  rating: '',
  verified: false,
  location: {},
  city: '',
};

const initialValues: MainContextType = {
  id: '',
  languaje: '',
  users: user,
  cards: [],
  getCard: () => {},
  setUsers: () => {},
  setId: () => {},
  setLanguaje: () => {},
  getLanguaje: () => {},
  whoosh: () => {},
  getUser: () => {},
};

export const MainContext = React.createContext(initialValues);

interface Props {}

const MainContextProvider: React.FC<Props> = ({children}) => {
  const [id, setId] = useState(initialValues.id);
  const [users, setUsers] = useState<User>(initialValues.users);
  const [languaje, setLanguaje] = useState(initialValues.languaje);
  const [cards, setCard] = useState(initialValues.cards);

  const getUser = async () => {
    const usuario = await AsyncStorage.getItem('user');
    const user = JSON.parse(usuario);
    setUsers(JSON.parse(user));
  };

  const initialConst = async () => {
    const id = await AsyncStorage.getItem('id');
    setId(JSON.parse(id));
    const savedDataJSON = await AsyncStorage.getItem('languageCode');
    setLanguaje(savedDataJSON);
  };

  const getCard = async () => {
    const deviceState = await OneSignal.getDeviceState();

    if (users.StripeID) {
      const response = await fetch(
        `${MAIN_URL}/get-card?customers=${users.StripeID}`,
      );
      const cards_response = await response.json();
      setCard(cards_response.data);
    }

    if (users._id) {
      fetch(`${MAIN_URL}/user-notification-id-save`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          OnesignalID: deviceState.userId,
          user_id: users._id,
        }),
      }).catch(err => console.log(err));
    }
  };

  const getLanguaje = async () => {
    const country = RNLocalize.getLocales();
    const currecy = RNLocalize.getCurrencies();
    AsyncStorage.setItem('countryCode', country[0].countryCode);
    AsyncStorage.setItem('localeCode', country[0].languageTag);
    AsyncStorage.setItem('languageCode', country[0].languageCode);
    AsyncStorage.setItem('currecy', currecy[0]);
  };

  useEffect(() => {
    getUser();
    getLanguaje();
    initialConst();
    getCard();
  }, []);

  var whoosh = new Sound('ding.mp3', Sound.MAIN_BUNDLE, error => {
    if (error) {
      console.log('failed to load the sound', error);
      return;
    }
  });

  return (
    <MainContext.Provider
      value={{
        id,
        setId,
        users,
        cards,
        setUsers,
        getCard,
        setLanguaje,
        languaje,
        getLanguaje,
        whoosh,
        getUser,
      }}>
      {children}
    </MainContext.Provider>
  );
};

export default MainContextProvider;
