import Icon_Vetec from '../Assets/images/icon.png';
import Google_Icon from '../Assets/images/google-logo.png';
import Email from '../Assets/images/email.png';
import Phone from '../Assets/images/man-using-mobile-phone.png';
import IconPlaceHoldedLigth from '../Assets/images/IconPlaceHoldedwhite.png';
import IconPlaceHoldedDark from '../Assets/images/iconPlaceholdedBlack.png';
import DefaultAvarat from '../Assets/images/defaultAvatar.jpg';
import FreeCredit from '../Assets/images/credit.png';
import FreeCredit_Inglish from '../Assets/images/credit_english.png';
import MapPin from '../Assets/images/map-pin.png';
import PlaceHoldedCard from '../Assets/images/placeHoldedCar.png';
import CardLight from '../Assets/images/cardLight.png';
import CardDark from '../Assets/images/cardDark.png';
import ChatsLight from '../Assets/images/chatlight.png';
import ChatsDark from '../Assets/images/chatdark.png';
import Footer from '../Assets/images/footer.png';
import Header from '../Assets/images/header.png';
import LogoCard from '../Assets/images/logos_card.png';
import VisaLogo from '../Assets/images/PaymentMethod/Visa.png';
import MasterCardLogo from '../Assets/images/PaymentMethod/Mastercard.png';
import AmericamLogo from '../Assets/images/PaymentMethod/Amex.png';
import DiscoverLogo from '../Assets/images/PaymentMethod/Discover.png';
import PaypalLogo from '../Assets/images/PaymentMethod/PayPal.png';
import ApplePayLogo from '../Assets/images/PaymentMethod/ApplePay.png';
import GooglePayLogo from '../Assets/images/PaymentMethod/GooglePay.png';
import WallPaper_White from '../Assets/images/wallpaper_light.jpg';
import WallPaper_Black from '../Assets/images/wallpaper_black.jpg';
import WahtsApp from '../Assets/images/Whatsapp.png';
import SMS from '../Assets/images/Message.png';
import EmailIcon from '../Assets/images/E-mail.png';
import FaceBook from '../Assets/images/Facebook.png';
import MapLigth from '../Assets/images/maps.png';
import MapDakk from '../Assets/images/mapdark.png';
import Recibo from '../Assets/images/recibo.png';
import ReciboDark from '../Assets/images/recibodark.png';
import HeaderUser from '../Assets/images/HeaderUser.png';
import Chat from '../Assets/images/Chats.png';
import Logo from '../Assets/images/logo.png';

export const image = {
  Logo,
  Chat,
  HeaderUser,
  Recibo,
  ReciboDark,
  MapLigth,
  MapDakk,
  Icon_Vetec,
  Google_Icon,
  Email,
  Phone,
  IconPlaceHoldedLigth,
  IconPlaceHoldedDark,
  DefaultAvarat,
  DefaultAvarat,
  FreeCredit,
  FreeCredit_Inglish,
  MapPin,
  PlaceHoldedCard,
  CardLight,
  CardDark,
  ChatsLight,
  ChatsDark,
  Footer,
  Header,
  LogoCard,
  VisaLogo,
  MasterCardLogo,
  AmericamLogo,
  DiscoverLogo,
  PaypalLogo,
  ApplePayLogo,
  GooglePayLogo,
  WallPaper_White,
  WallPaper_Black,
  WahtsApp,
  SMS,
  EmailIcon,
  FaceBook,
};
