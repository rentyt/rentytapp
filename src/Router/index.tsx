import React from 'react';
import {StatusBar, Platform} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {ColorSchemeProvider} from 'react-native-dynamic';
import {ApolloProvider} from '@apollo/client';
import linking from './config';
import client from './apollo-client';
import MainContextProvider from '../store/MainProvider';
import {RootStackParamList} from '../interfaces/navigation';
import {navigationRef} from '../helpers/RootNavigation';
import {StripeProvider} from '@stripe/stripe-react-native';
import {STRIPE_CLIENT} from '../Utils/Urls';
//Screens
import HomeScreen from '../Screens/Home';
import Details from '../Screens/Details';
import LoginScreen from '../Screens/Auth/Login';
import RegisterScreen from '../Screens/Auth/Register';
import Orders from '../Screens/Orders';
import Forgot from '../Screens/Auth/ForgotPassword';
import VerifyPone from '../Screens/VerifyPhone/Phone';
import SendCode from '../Screens/VerifyPhone/senCode';
import Feed from '../Screens/Feed';
import AddPaymentMethod from '../Screens/PaymentMethod/AddPaymentScreen';
import PaymentMethod from '../Screens/PaymentMethod';
import Result from '../Screens/Result';
import {ChatScreen} from '../Screens/Chats/ChatsScreen/ChatsScreen';
import Web from '../Screens/Web';
import MyAds from '../Screens/MyAds';
import MyOrders from '../Screens/MyOrder';
import DetailsOrders from '../Screens/MyOrder/Details/Details';
import MyRequest from '../Screens/MyRequest';
import MyRequestDetails from '../Screens/MyRequest/Details/Details';
import UserPorfile from '../Screens/UserProfile';
import Rating from '../Screens/Rating';
import OwnerCar from '../Screens/OwnerCarList';
import MyRating from '../Screens/MyRating';
import UpdateProfile from '../Screens/UpdateProfile';
import Share from '../Screens/Share';
import PromoCode from '../Screens/PromoCode';

const Stack = createStackNavigator<RootStackParamList>();

function MyStack({token}) {
  return (
    <Stack.Navigator
      initialRouteName={token ? 'Home' : 'Login'}
      screenOptions={{
        headerShown: false,
        animationEnabled: Platform.OS === 'ios',
      }}>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen
        name="Details"
        component={Details}
        options={{...TransitionPresets.ScaleFromCenterAndroid}}
      />
      <Stack.Screen name="Orders" component={Orders} />
      <Stack.Screen name="ChatScreen" component={ChatScreen} />
      <Stack.Screen name="AddPaymentMethod" component={AddPaymentMethod} />
      <Stack.Screen name="Feed" component={Feed} />
      <Stack.Screen name="PaymentMethod" component={PaymentMethod} />
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="VerifyPhone" component={VerifyPone} />
      <Stack.Screen name="SendCode" component={SendCode} />
      <Stack.Screen name="MyAds" component={MyAds} />
      <Stack.Screen name="MyOrders" component={MyOrders} />
      <Stack.Screen name="DetailsOrders" component={DetailsOrders} />
      <Stack.Screen name="MyRequest" component={MyRequest} />
      <Stack.Screen name="MyRequestDetails" component={MyRequestDetails} />
      <Stack.Screen name="UserPorfile" component={UserPorfile} />
      <Stack.Screen name="Rating" component={Rating} />
      <Stack.Screen name="OwnerCar" component={OwnerCar} />
      <Stack.Screen name="MyRating" component={MyRating} />
      <Stack.Screen name="UpdateProfile" component={UpdateProfile} />
      <Stack.Screen name="Share" component={Share} />
      <Stack.Screen name="PromoCode" component={PromoCode} />
      <Stack.Screen
        name="Result"
        component={Result}
        options={{gestureEnabled: false}}
      />
      <Stack.Screen
        name="Web"
        component={Web}
        options={{gestureEnabled: true}}
      />
      <Stack.Screen
        name="Forgot"
        component={Forgot}
        options={{...TransitionPresets.ModalPresentationIOS}}
      />
      <Stack.Screen
        name="Register"
        component={RegisterScreen}
        options={{...TransitionPresets.ModalPresentationIOS}}
      />
    </Stack.Navigator>
  );
}
export default function Router({token}) {
  return (
    <MainContextProvider>
      <StripeProvider
        publishableKey={STRIPE_CLIENT}
        urlScheme="rentytapp://app/result" // required for 3D Secure and bank
        merchantIdentifier="merchant.com.rentytapp.do" // required for Apple Pay
      >
        <ColorSchemeProvider>
          <ApolloProvider client={client}>
            <StatusBar translucent backgroundColor="transparent" />
            <NavigationContainer ref={navigationRef} linking={linking}>
              <MyStack token={token} />
            </NavigationContainer>
          </ApolloProvider>
        </ColorSchemeProvider>
      </StripeProvider>
    </MainContextProvider>
  );
}
