const config = {
  screens: {
    Home: {
      path: 'home/:id',
      parse: {
        id: id => `${id}`,
      },
    },
    Details: {
      path: 'details/:id',
      parse: {
        id: id => `${id}`,
      },
    },
  },
  Feed: 'feed',
};

const linking = {
  prefixes: ['rentytapp://app'],
  config,
};

export default linking;
