export const formaterPrice = (
  price: any,
  localeCode: string,
  currecy: string,
) => {
  const priceFormater = new Intl.NumberFormat(
    localeCode ? localeCode : 'es-DO',
    {
      style: 'currency',
      currency: currecy ? currecy : 'USD',
      minimumFractionDigits: 0,
    },
  ).format(price / 100);

  return priceFormater;
};
