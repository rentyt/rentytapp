import {Linking} from 'react-native';

export const OpenURLButton = async (url: any) => {
  Linking.openURL(url);
};
