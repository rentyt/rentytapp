import {Alert} from 'react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Toast from 'react-native-simple-toast';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export const AddStoreToFavorite = (
  userID: string,
  adsID: string,
  refetch: any,
  createFavoritoAds: any,
  t: any,
  navigation?: any,
) => {
  if (userID) {
    createFavoritoAds({
      variables: {adsID: adsID, userID: userID},
    }).then(async (res: any) => {
      if (res.data.createFavoritoAds.success) {
        ReactNativeHapticFeedback.trigger('notificationSuccess', options);
        refetch();
      } else {
        Toast.showWithGravity(
          t(`favourite${res.data.deletedFavourite.messages}`),
          Toast.LONG,
          Toast.TOP,
        );
      }
    });
  } else {
    Alert.alert(
      t('favourite:logintitle'),
      t('favourite:loginmessage'),
      [
        {
          text: t('favourite:login_btn'),
          onPress: () => navigation.navigate('Login'),
        },

        {
          text: t('favourite:register_btn'),
          onPress: () => navigation.navigate('Login'),
        },
      ],
      {cancelable: false},
    );
  }
};

export const DeleteStoreToFavorite = (
  adsID: string,
  refetch: any,
  deletedFavourite: any,
  t: any,
) => {
  deletedFavourite({variables: {id: adsID}}).then(async (res: any) => {
    if (res.data.deletedFavourite.success) {
      ReactNativeHapticFeedback.trigger('notificationError', options);
      refetch();
    } else {
      Toast.showWithGravity(
        t(`favourite${res.data.deletedFavourite.messages}`),
        Toast.LONG,
        Toast.TOP,
      );
    }
  });
};
