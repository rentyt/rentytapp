import {Platform} from 'react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export const deleteItemExtra = (
  items: any,
  itemsID: string,
  setItems: any,
  setarraysDataBaby: any,
  babyChairs: any[],
) => {
  var i = items.findIndex((x: any) => x.title === itemsID);
  if (i !== -1) {
    items.splice(i, 1);
    setarraysDataBaby(JSON.parse(JSON.stringify(babyChairs)));
    setItems(items.concat());
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
  }
};

export const AddItemExtra = (
  items: any,
  itemsID: string,
  item: any,
  setItems: any,
  setarraysDataBaby: any,
  babyChairs: any[],
) => {
  if (items.filter((e: any) => e.title === itemsID).length > 0) {
    deleteItemExtra(items, itemsID, setItems, setarraysDataBaby, babyChairs);
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
  } else {
    const inp = [...items, item];
    setItems(inp);
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
  }
};
