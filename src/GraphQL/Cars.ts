import gql from 'graphql-tag';

const GET_FEED_CAR = gql`
  query getAdsFeed($input: JSON, $page: Int, $limit: Int) {
    getAdsFeed(input: $input, page: $page, limit: $limit) {
      message
      success
      status
      data {
        _id
        car
        instantBooking
        inService
        minDays
        gestionCost
        anticipation
        offerts
        owner
        rating
        offertDay
        offertMon
        status
        city
        statusProcess
        OwnerData {
          _id
          name
          lastName
          email
          description
          city
          avatar
          phone
          created_at
          updated_at
          termAndConditions
          verifyPhone
          isAvalible
          StripeID
          PaypalID
          OnesignalID
          socialNetworks
          isSocial
          rating
          contactCode
          verified
          lastSeen
          totalOrder
          totalRating
          totalCar
        }
        images
        description
        location
        details
        equipemnet
        ExtraEquipment
        babyChairs
        preferences
        prices
        availability
        busyDays
        created_at
        updated_at
        popular
        visible
        inFavourites
        airPortPickup
      }
    }
  }
`;

export const queryCar = {
  GET_FEED_CAR,
};
