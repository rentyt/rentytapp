import gql from 'graphql-tag';

const GET_RATING = gql`
  query getRating($owner: ID, $limit: Int, $page: Int) {
    getRating(owner: $owner, limit: $limit, page: $page) {
      message
      success
      data {
        _id
        owner
        customer
        car
        order
        rating
        comment
        reply
        Customer {
          _id
          name
          lastName
          email
          description
          city
          avatar
        }
        Owner {
          _id
          name
          lastName
          email
          description
          city
          avatar
        }
        Car {
          _id
          car
          images
          airPortPickup
          instantBooking
        }
        created_at
        updated_at
      }
    }
  }
`;

export const queryRating = {GET_RATING};
