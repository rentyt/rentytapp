import {mutations} from './Mutation';
import {query} from './Query';
import {queryOrder} from './Order';
import {queryCar} from './Cars';
import {userQuery} from './user';
import {queryRating} from './Rating';

export {mutations, query, queryOrder, queryCar, userQuery, queryRating};
