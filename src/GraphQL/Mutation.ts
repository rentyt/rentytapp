import gql from 'graphql-tag';

const AUTENTICAR_USUARIO = gql`
  mutation LoginUser($email: String!, $password: String!, $input: deviceInfo) {
    LoginUser(email: $email, password: $password, input: $input) {
      success
      message
      data {
        token
        id
        verifyPhone
        user
      }
    }
  }
`;

const CREATE_USUARIO = gql`
  mutation crearUsuario($input: UsuarioInput!) {
    crearUsuario(input: $input) {
      success
      message
      data {
        _id
        name
        lastName
        email
        description
        city
        avatar
        phone
        created_at
        updated_at
        termAndConditions
        verifyPhone
        isAvalible
        StripeID
        PaypalID
        OnesignalID
        socialNetworks
        isSocial
        rating
        contactCode
        verified
        myCategory
        lastSeen
        totalOrder
        totalRating
        totalCar
        location
      }
    }
  }
`;

const UPDATE_USUARIO = gql`
  mutation updateUser($input: JSON) {
    updateUser(input: $input) {
      success
      message
      status
      data {
        _id
        name
        lastName
        email
        description
        city
        avatar
        phone
        created_at
        updated_at
        termAndConditions
        verifyPhone
        isAvalible
        StripeID
        PaypalID
        OnesignalID
        socialNetworks
        isSocial
        rating
        contactCode
        verified
        myCategory
        lastSeen
        location
      }
    }
  }
`;

const UPLOAD_FILE = gql`
  mutation singleUploadToAws($file: Upload) {
    singleUploadToAws(file: $file) {
      data
    }
  }
`;

const CREATED_FAVOURITE = gql`
  mutation createFavoritoAds($adsID: ID, $userID: ID) {
    createFavoritoAds(adsID: $adsID, userID: $userID) {
      messages
      success
      status
    }
  }
`;

const DELETE_FAVOURITE = gql`
  mutation deletedFavourite($id: ID) {
    deletedFavourite(id: $id) {
      messages
      success
      status
    }
  }
`;

const CREATED_LAST_VIEW = gql`
  mutation createLAstViewAds($adsID: ID, $userID: ID) {
    createLAstViewAds(adsID: $adsID, userID: $userID) {
      messages
      success
      status
    }
  }
`;

const READ_MESSAGES = gql`
  mutation readMessage($id: ID, $userID: ID) {
    readMessage(id: $id, userID: $userID) {
      messages
      success
      status
    }
  }
`;

const DELETE_MESSAGES = gql`
  mutation deleteConversation($id: ID) {
    deleteConversation(id: $id) {
      messages
      success
      status
    }
  }
`;

const CREATED_ADS = gql`
  mutation createdAds($input: AdsInput) {
    createdAds(input: $input) {
      message
      success
      data {
        car
      }
    }
  }
`;

const CREATED_ORDER = gql`
  mutation crearModificarNewOrden($input: OrderInput) {
    crearModificarNewOrden(input: $input) {
      message
      success
      data {
        _id
      }
    }
  }
`;

const DELETE_ADS = gql`
  mutation deletedAds($id: ID!) {
    deletedAds(id: $id) {
      messages
      success
      status
    }
  }
`;

const UPDATE_ADS = gql`
  mutation updateAds($input: AdsInput) {
    updateAds(input: $input) {
      messages
      success
      status
    }
  }
`;

const PROCESS_ORDER = gql`
  mutation NewOrdenProceed($ordenId: ID, $status: String, $IntegerValue: Int) {
    NewOrdenProceed(
      ordenId: $ordenId
      status: $status
      IntegerValue: $IntegerValue
    ) {
      messages
      success
      status
    }
  }
`;

const CREATE_RATING = gql`
  mutation crearRating($input: RatingInput) {
    crearRating(input: $input) {
      messages
      success
      status
    }
  }
`;

const UPDATE_RATING = gql`
  mutation updateRating($input: RatingInput) {
    updateRating(input: $input) {
      messages
      success
      status
    }
  }
`;

export const mutations = {
  AUTENTICAR_USUARIO,
  CREATE_USUARIO,
  UPLOAD_FILE,
  CREATED_FAVOURITE,
  DELETE_FAVOURITE,
  CREATED_LAST_VIEW,
  READ_MESSAGES,
  DELETE_MESSAGES,
  CREATED_ADS,
  CREATED_ORDER,
  DELETE_ADS,
  UPDATE_ADS,
  PROCESS_ORDER,
  CREATE_RATING,
  UPDATE_RATING,
  UPDATE_USUARIO,
};
