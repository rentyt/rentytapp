import gql from 'graphql-tag';

const GET_ALL_ADS = gql`
  query getAds($input: JSON, $page: Int, $limit: Int) {
    getAds(input: $input, page: $page, limit: $limit) {
      message
      success
      status
      data {
        _id
        car
        instantBooking
        inService
        minDays
        gestionCost
        anticipation
        offerts
        owner
        rating
        offertDay
        offertMon
        status
        statusProcess
        city
        OwnerData {
          _id
          name
          lastName
          email
          description
          city
          avatar
          phone
          created_at
          updated_at
          termAndConditions
          verifyPhone
          isAvalible
          StripeID
          PaypalID
          OnesignalID
          socialNetworks
          isSocial
          rating
          contactCode
          verified
          lastSeen
          totalOrder
          totalRating
          totalCar
        }
        images
        description
        location
        details
        equipemnet
        ExtraEquipment
        babyChairs
        preferences
        prices
        availability
        busyDays
        created_at
        updated_at
        popular
        visible
        inFavourites
        airPortPickup
      }
    }
  }
`;

const GET_ADS_ID = gql`
  query getAdsForID($id: ID) {
    getAdsForID(id: $id) {
      message
      success
      status
      data {
        _id
        car
        instantBooking
        inService
        minDays
        gestionCost
        anticipation
        offerts
        owner
        rating
        airPortPickup
        offertDay
        offertMon
        status
        statusProcess
        city
        OwnerData {
          _id
          name
          lastName
          email
          description
          city
          avatar
          phone
          created_at
          updated_at
          termAndConditions
          verifyPhone
          isAvalible
          StripeID
          PaypalID
          OnesignalID
          socialNetworks
          isSocial
          rating
          contactCode
          verified
          lastSeen
          totalOrder
          totalRating
          totalCar
        }
        images
        description
        location
        details
        equipemnet
        ExtraEquipment
        babyChairs
        preferences
        prices
        availability
        busyDays
        created_at
        updated_at
        popular
        visible
        inFavourites
      }
    }
  }
`;

const GET_ADS_FAVOURITES = gql`
  query getAdsFavourites($id: ID) {
    getAdsFavourites(id: $id) {
      success
      status
      message
      data {
        _id
        adsID
        userID
        created_at
        updated_at
        Ads {
          _id
          car
          rating
          airPortPickup
          instantBooking
          inService
          minDays
          gestionCost
          anticipation
          offerts
          owner
          offertDay
          offertMon
          status
          statusProcess
          city
          OwnerData {
            _id
            name
            lastName
            email
            description
            city
            avatar
            phone
            created_at
            updated_at
            termAndConditions
            verifyPhone
            isAvalible
            StripeID
            PaypalID
            OnesignalID
            socialNetworks
            isSocial
            rating
            contactCode
            verified
            lastSeen
            totalOrder
            totalRating
            totalCar
          }
          images
          description
          location
          details
          equipemnet
          ExtraEquipment
          babyChairs
          preferences
          prices
          availability
          busyDays
          created_at
          updated_at
          popular
          visible
          inFavourites
        }
      }
    }
  }
`;

const GET_ADS_LAST_VIEW = gql`
  query getAdsLastView($id: ID) {
    getAdsLastView(id: $id) {
      success
      status
      message
      data {
        _id
        adsID
        userID
        created_at
        updated_at
        Ads {
          _id
          car
          instantBooking
          inService
          rating
          airPortPickup
          minDays
          gestionCost
          anticipation
          offerts
          owner
          offertDay
          offertMon
          status
          statusProcess
          city
          OwnerData {
            _id
            name
            lastName
            email
            description
            city
            avatar
            phone
            created_at
            updated_at
            termAndConditions
            verifyPhone
            isAvalible
            StripeID
            PaypalID
            OnesignalID
            socialNetworks
            isSocial
            rating
            contactCode
            verified
          }
          images
          description
          location
          details
          equipemnet
          ExtraEquipment
          babyChairs
          preferences
          prices
          availability
          busyDays
          created_at
          updated_at
          popular
          visible
          inFavourites
        }
      }
    }
  }
`;

const GET_CONVERSATION_LIST = gql`
  query getConversationList($userID: ID) {
    getConversationList(userID: $userID) {
      success
      status
      message
      data {
        _id
        userID
        adsID
        owner
        participate
        userRoomID
        ownerRoomID
        ads
        user {
          _id
          name
          lastName
          email
          description
          city
          avatar
          phone
          created_at
          updated_at
          termAndConditions
          verifyPhone
          isAvalible
          StripeID
          PaypalID
          OnesignalID
          socialNetworks
          isSocial
          rating
          contactCode
          verified
          lastSeen
          totalOrder
          totalRating
          totalCar
        }
        Owner {
          _id
          name
          lastName
          email
          description
          city
          avatar
          phone
          created_at
          updated_at
          termAndConditions
          verifyPhone
          isAvalible
          StripeID
          PaypalID
          OnesignalID
          socialNetworks
          isSocial
          rating
          contactCode
          verified
          lastSeen
          totalOrder
          totalRating
          totalCar
        }
        messages
        created_at
        updated_at
      }
    }
  }
`;

const GET_CONVERSATION_ONE = gql`
  query getConversationOne($userID: ID, $adsID: ID) {
    getConversationOne(userID: $userID, adsID: $adsID) {
      success
      status
      message
      data {
        _id
        userID
        adsID
        owner
        ads
        participate
        userRoomID
        ownerRoomID
        user {
          _id
          name
          lastName
          email
          description
          city
          avatar
          phone
          created_at
          updated_at
          termAndConditions
          verifyPhone
          isAvalible
          StripeID
          PaypalID
          OnesignalID
          socialNetworks
          isSocial
          rating
          contactCode
          verified
          lastSeen
          totalOrder
          totalRating
          totalCar
        }
        Owner {
          _id
          name
          lastName
          email
          description
          city
          avatar
          phone
          created_at
          updated_at
          termAndConditions
          verifyPhone
          isAvalible
          StripeID
          PaypalID
          OnesignalID
          socialNetworks
          isSocial
          rating
          contactCode
          verified
          lastSeen
          totalOrder
          totalRating
          totalCar
        }
        messages
        created_at
        updated_at
      }
    }
  }
`;

const GET_CONVERSATION_COUNT = gql`
  query getConversationNoRead($id: ID, $userID: ID) {
    getConversationNoRead(id: $id, userID: $userID) {
      message
      success
      conversation {
        _id
        count
      }
    }
  }
`;

const GET_PROMO_CODE = gql`
  query getPromocode($input: JSON) {
    getPromocode(input: $input) {
      success
      messages
      data {
        _id
        clave
        descuento
        tipo
        usage
        expire
        exprirable
        user
        description
        private
        uniqueCar
        canjeado
        vencido
        car
        Car
        Usuario
      }
    }
  }
`;

export const query = {
  GET_ALL_ADS,
  GET_ADS_ID,
  GET_ADS_FAVOURITES,
  GET_ADS_LAST_VIEW,
  GET_CONVERSATION_LIST,
  GET_CONVERSATION_ONE,
  GET_CONVERSATION_COUNT,
  GET_PROMO_CODE,
};
