import React, {Component} from 'react';
import {Animated} from 'react-native';
import Icon from 'react-native-dynamic-vector-icons';

export interface IProps {
  type: any;
  name: string;
  color: any;
  size: number;
  style?: any;
}

class Icons extends React.Component<IProps> {
  render() {
    return (
      <Icon
        name={this.props.name}
        type={this.props.type}
        size={this.props.size}
        color={this.props.color}
        style={this.props.style}
      />
    );
  }
}

const AnimatedIcon = Animated.createAnimatedComponent(Icons);

export default class AnimatedIcons extends Component<IProps> {
  render() {
    return (
      <AnimatedIcon
        name={this.props.name}
        type={this.props.type}
        size={this.props.size}
        color={this.props.color}
        style={this.props.style}
      />
    );
  }
}
