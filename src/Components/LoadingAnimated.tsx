import React from 'react';
import {View} from 'react-native';
import {colors, stylesText} from '../Themes';
import {CustomText} from './CustomText';
import LottieView from 'lottie-react-native';

export default function LoadingAnimated(props: any) {
  const {name} = props;

  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <LottieView
        source={require('../Assets/animated/loading.json')}
        autoPlay
        loop
        style={{width: 100}}
      />
      <CustomText
        light={colors.black}
        dark={colors.white}
        style={[stylesText.secondaryText, {textAlign: 'center'}]}>
        {name}
      </CustomText>
    </View>
  );
}
