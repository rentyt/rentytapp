import React from 'react';
import {View, Platform, SafeAreaView, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../CustomText/index';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export interface IHeader {
  navigation: any;
  title: string;
  leftIcon: boolean;
  type?: string;
  name?: string;
  onPress?: () => void;
}

export default function Header(props) {
  const {navigation, title, onPress, leftIcon, type, name} = props;
  const styles = useDynamicValue(dynamicStyles);

  const ActAdd = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const borderColor = ActAdd[mode];

  const types = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  return (
    <SafeAreaView
      style={[
        styles.container,
        {
          borderBottomColor: borderColor,
        },
      ]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginHorizontal: 10,
          marginTop: 10,
          marginBottom: 15,
        }}>
        <TouchableOpacity
          onPress={() => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(types, optiones);
            navigation.goBack();
          }}
          style={styles.icon}>
          <Icon
            name="arrow-left"
            type="Feather"
            size={24}
            color={colors.rgb_153}
          />
        </TouchableOpacity>
        <CustomText
          light={colors.back_suave_dark}
          dark={colors.white}
          style={[stylesText.mainText, {fontWeight: 'bold'}]}>
          {title}
        </CustomText>
        {leftIcon ? (
          <TouchableOpacity style={styles.icon} onPress={onPress}>
            <Icon type={type} name={name} size={24} color={colors.rgb_153} />
          </TouchableOpacity>
        ) : (
          <View style={{width: 40, height: 40}} />
        )}
      </View>
    </SafeAreaView>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
    borderBottomWidth: 0.5,
    paddingTop: Platform.select({
      ios: dimensions.IsIphoneX() ? 0 : 10,
      android: dimensions.Height(5),
    }),
  },

  icon: {
    width: 40,
    height: 40,
    backgroundColor: new DynamicValue(
      colors.slight_grey,
      colors.back_suave_dark,
    ),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
