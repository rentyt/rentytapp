import React, {useState} from 'react';
import {
  View,
  TextInput,
  TouchableOpacity,
  Platform,
  Modal,
  Alert,
  Switch,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {colors, dimensions, stylesText} from '../../Themes';
import {CustomText} from '../CustomText';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Icon from 'react-native-dynamic-vector-icons';
import HeaderModal from '../HeaderModal';
import {Button} from '../Buttom';
import Textarea from 'react-native-textarea';
import {formaterPrice} from '../../Utils/formaterPrice';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function ProceOffer({
  porcentajeMes,
  porcentajeDias,
  numberMes,
  numberDias,
  isEnabled,
  toggleSwitch,
  setPorcentajeMes,
  setNumbereDias,
  setPorcentajeDias,
  setNumberMes,
  description,
  setdescription,
  currency,
  setCurrency,
  price,
  setprice,
  minime,
  setminime,
  airplane,
  toggleSwitchAir,
  airValue,
  setAirValue,
  edit,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const modo = useColorSchemeContext();
  const [visibleModal, setVisibleModal] = useState(false);
  const [type, setType] = useState('mon');

  const linecolor = {
    light: colors.black,
    dark: colors.white,
  };

  const bordercolor = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const lineColor = linecolor[modo];
  const borderColors = bordercolor[modo];

  const swichColor = {
    light: colors.white,
    dark: colors.white,
  };

  const swichColor1 = {
    light: '#767577',
    dark: colors.back_suave_dark,
  };

  const SwichColor = swichColor[modo];

  const SwichColor1 = swichColor1[modo];

  const moneda = [
    {
      currency: 'USD',
      localcode: 'es-DO',
    },
    {
      currency: 'DOP',
      localcode: 'es-DO',
    },

    {
      currency: 'EUR',
      localcode: 'es-DO',
    },
  ];

  const applyOffert = () => {
    if (type == 'mon') {
      if (porcentajeMes && numberMes) {
        setVisibleModal(false);
        //@ts-ignore
        ReactNativeHapticFeedback.trigger(types, optiones);
      } else {
        Alert.alert(t('addCar:alertOffertTitle'), t('addCar:alertOffertTitle'));
      }
    } else {
      if (porcentajeDias && numberDias) {
        setVisibleModal(false);
        //@ts-ignore
        ReactNativeHapticFeedback.trigger(types, optiones);
      } else {
        Alert.alert(t('addCar:alertOffertTitle'), t('addCar:alertOffertTitle'));
      }
    }
  };

  return (
    <View style={{marginTop: 50}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          marginHorizontal: 15,
        }}>
        {moneda.map((m, i) => {
          return (
            <TouchableOpacity
              key={i}
              style={[
                styles.moneda,
                {
                  borderColor:
                    currency.currency === m.currency
                      ? colors.main
                      : borderColors,
                },
              ]}
              onPress={() => {
                setCurrency(m);
                //@ts-ignore
                ReactNativeHapticFeedback.trigger(types, optiones);
              }}>
              <CustomText
                style={[stylesText.placeholderText, {fontWeight: '600'}]}
                light={
                  currency.currency === m.currency ? colors.main : colors.black
                }
                dark={
                  currency.currency === m.currency ? colors.main : colors.white
                }>
                {m.currency}
              </CustomText>
            </TouchableOpacity>
          );
        })}
      </View>

      <CustomText
        style={[
          stylesText.secondaryText,
          {
            width: dimensions.Width(80),
            marginHorizontal: 23,
            marginBottom: 0,
            marginTop: 20,
          },
        ]}
        light={colors.ERROR}
        dark={colors.ERROR}>
        {t('addCar:obligatorio')}
      </CustomText>

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'flex-start',
          width: '100%',
        }}>
        <View>
          <TextInput
            autoCorrect={false}
            value={price == 0 ? '' : String(price)}
            placeholder={t('addCar:price')}
            enablesReturnKeyAutomatically={true}
            keyboardType="numeric"
            numberOfLines={1}
            multiline={false}
            autoCapitalize="none"
            selectionColor={colors.green}
            placeholderTextColor={lineColor}
            onChangeText={value => setprice(Number(value))}
            style={[styles.inputText, {width: dimensions.Width(50)}]}
          />

          {price > 0 ? (
            <View style={{marginHorizontal: 25, marginTop: 10}}>
              <CustomText
                style={stylesText.TitleCard}
                light={colors.black}
                dark={colors.white}>
                {formaterPrice(
                  price * 100,
                  currency.localcode,
                  currency.currency,
                )}{' '}
                / {t('card:day')}
              </CustomText>
            </View>
          ) : null}
        </View>

        <View>
          <TextInput
            autoCorrect={false}
            placeholder={t('addCar:minime')}
            enablesReturnKeyAutomatically={true}
            keyboardType="numeric"
            numberOfLines={1}
            multiline={false}
            autoCapitalize="none"
            selectionColor={colors.green}
            placeholderTextColor={lineColor}
            value={minime == 0 ? '' : String(minime)}
            onChangeText={value => setminime(Number(value))}
            style={[styles.inputText, {width: dimensions.Width(35)}]}
          />
        </View>
      </View>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.icon}>
            <Icon type="Entypo" name="flash" size={24} color={colors.ERROR} />
          </View>
          <View style={{marginLeft: 10}}>
            <CustomText
              style={stylesText.TitleCard}
              light={colors.black}
              dark={colors.white}>
              {t('details:InstantDetail')}
            </CustomText>
            <CustomText
              style={[stylesText.secondaryText, {width: dimensions.Width(55)}]}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('addCar:InstantDescription')}
            </CustomText>
          </View>
        </View>
        <View style={{marginRight: 10}}>
          <Switch
            trackColor={{false: SwichColor1, true: colors.ERROR}}
            thumbColor={isEnabled ? SwichColor : SwichColor}
            ios_backgroundColor={colors.rgb_235}
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
      </View>

      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.icon}>
            <Icon
              type="MaterialCommunityIcons"
              name="airplane"
              size={24}
              color={colors.twitter_color}
            />
          </View>
          <View style={{marginLeft: 10}}>
            <CustomText
              style={stylesText.TitleCard}
              light={colors.black}
              dark={colors.white}>
              {t('addCar:airTitle')}
            </CustomText>
            <CustomText
              style={[stylesText.secondaryText, {width: dimensions.Width(55)}]}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('addCar:airTitleDescription')}
            </CustomText>
          </View>
        </View>
        <View style={{marginRight: 10}}>
          <Switch
            trackColor={{false: SwichColor1, true: colors.twitter_color}}
            thumbColor={airplane ? SwichColor : SwichColor}
            ios_backgroundColor={colors.rgb_235}
            onValueChange={toggleSwitchAir}
            value={airplane}
          />
        </View>
      </View>

      {airplane ? (
        <>
          <TextInput
            autoCorrect={false}
            placeholder={t('addCar:airPLaceholded')}
            enablesReturnKeyAutomatically={true}
            keyboardType="numeric"
            numberOfLines={1}
            multiline={false}
            autoCapitalize="none"
            selectionColor={colors.green}
            placeholderTextColor={lineColor}
            value={airValue == 0 ? '' : String(airValue)}
            onChangeText={value => setAirValue(Number(value))}
            style={[styles.inputText, {marginTop: 0, marginBottom: 5}]}
          />
          {airValue > 0 ? (
            <View style={{marginHorizontal: 25, marginTop: 10}}>
              <CustomText
                style={stylesText.TitleCard}
                light={colors.black}
                dark={colors.white}>
                {formaterPrice(
                  airValue * 100,
                  currency.localcode,
                  currency.currency,
                )}
              </CustomText>
            </View>
          ) : (
            <CustomText
              style={[
                stylesText.secondaryText,
                {
                  width: dimensions.Width(80),
                  marginHorizontal: 20,
                  marginBottom: 15,
                },
              ]}
              light={colors.ERROR}
              dark={colors.ERROR}>
              {t('addCar:airInfo')}
            </CustomText>
          )}
        </>
      ) : null}

      <View style={{marginHorizontal: 15, marginTop: 20}}>
        <CustomText
          style={[stylesText.TitleCard]}
          light={colors.black}
          dark={colors.white}>
          {t('addCar:titledescription')}{' '}
          <CustomText
            style={[stylesText.secondaryText]}
            light={colors.ERROR}
            dark={colors.ERROR}>
            {t('addCar:obligatorio')}
          </CustomText>
        </CustomText>

        <Textarea
          containerStyle={styles.textareaContainer}
          //@ts-ignore
          style={styles.textarea}
          onChangeText={value => setdescription(value)}
          defaultValue={description}
          maxLength={200}
          minLength={100}
          selectionColor={colors.green}
          placeholder={t('addCar:titledescription')}
          placeholderTextColor={'#c7c7c7'}
          underlineColorAndroid={'transparent'}
        />
      </View>
      <View style={{marginHorizontal: 15, marginTop: 20}}>
        <CustomText
          style={[stylesText.TitleCard]}
          light={colors.black}
          dark={colors.white}>
          {t('addCar:titleOfferDay')}
        </CustomText>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            width: 50,
            marginTop: 20,
          }}>
          <Icon
            type="MaterialCommunityIcons"
            name="ticket-percent-outline"
            size={24}
            color={colors.orange1}
            style={{marginTop: -30}}
          />

          <View style={styles.offert}>
            <View style={{width: dimensions.Width(65)}}>
              <CustomText
                numberOfLines={3}
                style={[stylesText.mainText]}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {t('details:ofdertName')} {numberDias} {t('details:offertdays')}
              </CustomText>
              <TouchableOpacity
                style={styles.btn}
                onPress={() => {
                  //@ts-ignore
                  ReactNativeHapticFeedback.trigger(types, optiones);
                  setVisibleModal(true);
                  setType('day');
                }}>
                <CustomText
                  numberOfLines={3}
                  style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
                  light={colors.white}
                  dark={colors.white}>
                  {numberDias && porcentajeDias
                    ? t('addCar:btnEditOffer')
                    : t('addCar:btnCreatedOffer')}
                </CustomText>
              </TouchableOpacity>
            </View>
            <View>
              <CustomText
                style={[stylesText.secondaryTextBold]}
                light={colors.orange1}
                dark={colors.orange1}>
                {porcentajeDias}%
              </CustomText>
            </View>
          </View>
        </View>
      </View>
      <View style={{marginHorizontal: 15, marginTop: 30}}>
        <CustomText
          style={[stylesText.TitleCard]}
          light={colors.black}
          dark={colors.white}>
          {t('addCar:titleOfferMes')}
        </CustomText>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            width: 50,
            marginTop: 20,
          }}>
          <Icon
            type="Feather"
            name="gift"
            size={24}
            color={colors.green}
            style={{marginTop: -30}}
          />

          <View style={styles.offert}>
            <View style={{width: dimensions.Width(65)}}>
              <CustomText
                numberOfLines={3}
                style={[stylesText.mainText]}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {t('details:anticipationMessage')} {numberMes}{' '}
                {t('details:mon')} {t('details:continuesAnte')}
              </CustomText>
              <TouchableOpacity
                style={styles.btn}
                onPress={() => {
                  //@ts-ignore
                  ReactNativeHapticFeedback.trigger(types, optiones);
                  setVisibleModal(true);
                  setType('mon');
                }}>
                <CustomText
                  numberOfLines={3}
                  style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
                  light={colors.white}
                  dark={colors.white}>
                  {numberMes && porcentajeMes
                    ? t('addCar:btnEditOffer')
                    : t('addCar:btnCreatedOffer')}
                </CustomText>
              </TouchableOpacity>
            </View>
            <View>
              <CustomText
                style={[stylesText.secondaryTextBold]}
                light={colors.green}
                dark={colors.green}>
                {porcentajeMes}%
              </CustomText>
            </View>
          </View>
        </View>
      </View>
      <Modal
        animationType="slide"
        visible={visibleModal}
        presentationStyle="formSheet"
        collapsable={true}
        statusBarTranslucent={true}
        onRequestClose={() => setVisibleModal(false)}>
        <HeaderModal
          onPress={() => setVisibleModal(false)}
          title={type === 'mon' ? t('addCar:offertMon') : t('addCar:offertDay')}
          children={
            <View style={styles.contentModal}>
              {type === 'mon' ? (
                <View>
                  <TextInput
                    value={numberMes === 0 ? '' : String(numberMes)}
                    autoCorrect={false}
                    placeholder={t('addCar:monplaceholded')}
                    enablesReturnKeyAutomatically={true}
                    keyboardType="numeric"
                    numberOfLines={1}
                    multiline={false}
                    autoCapitalize="none"
                    onFocus={() => {}}
                    selectionColor={colors.green}
                    placeholderTextColor={lineColor}
                    onChangeText={value => setNumberMes(Number(value))}
                    style={styles.inputText}
                  />
                  <TextInput
                    value={porcentajeMes === 0 ? '' : String(porcentajeMes)}
                    autoCorrect={false}
                    placeholder={t('addCar:monplaceholdedPorcentaje')}
                    enablesReturnKeyAutomatically={true}
                    keyboardType="numeric"
                    numberOfLines={1}
                    multiline={false}
                    autoCapitalize="none"
                    onFocus={() => {}}
                    selectionColor={colors.green}
                    placeholderTextColor={lineColor}
                    onChangeText={value => setPorcentajeMes(Number(value))}
                    style={styles.inputText}
                  />
                </View>
              ) : (
                <View>
                  <TextInput
                    value={numberDias === 0 ? '' : String(numberDias)}
                    autoCorrect={false}
                    placeholder={t('addCar:placeHoldeddias')}
                    enablesReturnKeyAutomatically={true}
                    keyboardType="numeric"
                    numberOfLines={1}
                    multiline={false}
                    autoCapitalize="none"
                    onFocus={() => {}}
                    selectionColor={colors.green}
                    placeholderTextColor={lineColor}
                    onChangeText={value => setNumbereDias(Number(value))}
                    style={styles.inputText}
                  />
                  <TextInput
                    value={porcentajeDias === 0 ? '' : String(porcentajeDias)}
                    autoCorrect={false}
                    placeholder={t('addCar:monplaceholdedPorcentaje')}
                    enablesReturnKeyAutomatically={true}
                    keyboardType="numeric"
                    numberOfLines={1}
                    multiline={false}
                    autoCapitalize="none"
                    onFocus={() => {}}
                    selectionColor={colors.green}
                    placeholderTextColor={lineColor}
                    onChangeText={value => setPorcentajeDias(Number(value))}
                    style={styles.inputText}
                  />
                </View>
              )}
            </View>
          }
        />
        <View style={styles.buttonContent}>
          <Button
            light={colors.white}
            dark={colors.white}
            loading={false}
            onPress={applyOffert}
            title={t('addCar:btnOffer')}
          />
        </View>
      </Modal>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  inputText: {
    width: dimensions.Width(94),
    paddingHorizontal: 10,
    color: new DynamicValue(colors.black, colors.white),
    fontSize: 16,
    fontFamily: 'Poppins',
    fontWeight: '400',
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    height: 55,
    marginHorizontal: 10,
    marginTop: 15,
    borderRadius: 10,
  },

  moneda: {
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginHorizontal: 10,
    borderRadius: 100,
  },

  offert: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: dimensions.Width(85),
    marginLeft: 7,
    marginVertical: 10,
  },

  btn: {
    backgroundColor: colors.green,
    width: 150,
    padding: 5,
    marginTop: 10,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  buttonContent: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(14),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 0,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 15}),
    zIndex: 100,
  },

  textareaContainer: {
    height: 'auto',
    minHeight: 120,
    padding: 10,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderRadius: 10,
    marginTop: 20,
  },
  textarea: {
    textAlignVertical: 'top', // hack android
    height: 'auto',
    minHeight: 120,
    fontSize: 16,
    color: new DynamicValue(colors.black, colors.white),
    fontFamily: 'Poppins',
  },

  icon: {
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginLeft: 10,
  },

  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    marginTop: 30,
    marginHorizontal: 15,
  },
});
