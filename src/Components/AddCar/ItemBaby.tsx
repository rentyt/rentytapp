import React from 'react';
import {View} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';
import {formaterPrice} from '../../Utils/formaterPrice';
import CheckBox from '@react-native-community/checkbox';
import Pagination from '../../Components/Pagination';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {deleteItemExtra, AddItemExtra} from '../../Utils/addItemExtra';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ItemExtra({
  item,
  index,
  title,
  iconType,
  iconName,
  BabySeats,
  setarraysDataBaby,
  setBabySeats,
  arrays,
  currency,
  ifExist,
  edit,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const main = {
    light: colors.green,
    dark: colors.green,
  };

  const mainColor = main[mode];

  const plusQuantity = (quantity, index, max) => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    if (quantity < max) {
      let newBundled = [...arrays];
      newBundled[index]['quantityAvalible'] = quantity + 1;
      setarraysDataBaby(newBundled);
      if (edit) {
        setBabySeats(newBundled);
      } else {
        setBabySeats(BabySeats.concat([]));
      }
    }
  };

  const minusQuantity = (quantity, index) => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    if (quantity !== 1) {
      let newBundled = [...arrays];
      newBundled[index]['quantityAvalible'] = quantity - 1;
      setarraysDataBaby(newBundled);
      if (edit) {
        setBabySeats(newBundled);
      } else {
        setBabySeats(BabySeats.concat([]));
      }
    }
  };

  const plusQuantityPrice = (quantity, index, max) => {
    const plus = currency.currency === 'DOP' ? 1000 : 100;
    ReactNativeHapticFeedback.trigger('selection', optiones);
    if (quantity < max) {
      let newBundled = [...arrays];
      newBundled[index]['priceExtra'] = quantity + plus;
      console.log({
        newBundled,
        BabySeats,
      });
      setarraysDataBaby(newBundled);
      if (edit) {
        setBabySeats(newBundled);
      } else {
        setBabySeats(BabySeats.concat([]));
      }
    }
  };

  const minusQuantityPrice = (quantity, index) => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    const plus = currency.currency === 'DOP' ? 1000 : 100;
    if (quantity > 0) {
      let newBundled = [...arrays];
      newBundled[index]['priceExtra'] = quantity - plus;
      setarraysDataBaby(newBundled);
      if (edit) {
        setBabySeats(newBundled);
      } else {
        setBabySeats(BabySeats.concat([]));
      }
    }
  };

  const maximo = currency.currency === 'DOP' ? 100000 : 10000;

  return (
    <View>
      <View style={[styles.items]}>
        <View>
          <Icon
            type={iconType}
            name={iconName}
            size={28}
            color={colors.green}
            style={{marginTop: ifExist ? -50 : 0}}
          />
        </View>
        <View
          style={{
            marginLeft: 10,
            justifyContent: 'space-between',
            flexDirection: 'row',
            width: dimensions.IsIphoneX()
              ? dimensions.Width(81)
              : dimensions.Width(80),
          }}>
          <View
            style={{
              width: dimensions.IsIphoneX()
                ? dimensions.Width(48)
                : dimensions.Width(40),
            }}>
            <CustomText
              style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
              numberOfLines={2}
              light={colors.black}
              dark={colors.white}>
              {t(`details:babyChairs:${title}`)}
            </CustomText>

            {ifExist ? (
              <View>
                <CustomText
                  style={[stylesText.secondaryText, {marginTop: 5}]}
                  numberOfLines={3}
                  light={colors.rgb_153}
                  dark={colors.rgb_153}>
                  {t('addCar:cantidadDisponnible')}
                </CustomText>
                <View
                  style={{marginTop: 10, marginLeft: -5, flexDirection: 'row'}}>
                  <Pagination
                    page={item.quantityAvalible}
                    onPressMas={() => {
                      plusQuantity(item.quantityAvalible, index, 4);
                    }}
                    onPressMenos={() => {
                      if (item.quantityAvalible == 1) {
                        deleteItemExtra(
                          BabySeats,
                          title,
                          setBabySeats,
                          setarraysDataBaby,
                          arrays,
                        );
                      } else {
                        minusQuantity(item.quantityAvalible, index);
                      }
                    }}
                    width={80}
                    widthBTN={25}
                    sizeIcon={16}
                    fontSize={14}
                  />
                  <View style={{marginHorizontal: 15}} />
                  <Pagination
                    page={`${formaterPrice(
                      item.priceExtra,
                      currency.localcode,
                      currency.currency,
                    )} / ${t('card:day')}`}
                    onPressMas={() => {
                      plusQuantityPrice(item.priceExtra, index, maximo);
                    }}
                    onPressMenos={() => {
                      if (item.priceExtra == 1) {
                        deleteItemExtra(
                          BabySeats,
                          title,
                          setBabySeats,
                          setarraysDataBaby,
                          arrays,
                        );
                      } else {
                        minusQuantityPrice(item.priceExtra, index);
                      }
                    }}
                    width={180}
                    widthBTN={25}
                    sizeIcon={16}
                    fontSize={14}
                  />
                </View>
              </View>
            ) : null}
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <CheckBox
              value={ifExist}
              disabled={false}
              onValueChange={value => {
                if (value) {
                  AddItemExtra(
                    BabySeats,
                    title,
                    item,
                    setBabySeats,
                    setarraysDataBaby,
                    arrays,
                  );
                } else {
                  deleteItemExtra(
                    BabySeats,
                    title,
                    setBabySeats,
                    setarraysDataBaby,
                    arrays,
                  );
                }
              }}
              onTintColor={mainColor}
              tintColors={{true: mainColor, false: colors.rgb_153}}
              onCheckColor={mainColor}
              onAnimationType="fill"
              offAnimationType="fill"
              style={{marginLeft: 10}}
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingBottom: 20,
    width: dimensions.Width(92),
    marginTop: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },
});
