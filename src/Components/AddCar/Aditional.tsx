export const Fuel = [
  {
    label: 'Gasolina',
    value: 0,
  },
  {
    label: 'Diesel',
    value: 1,
  },
  {
    label: 'GLP',
    value: 2,
  },
  {
    label: 'Eléctrico',
    value: 3,
  },
];

export const Transmission = [
  {
    label: 'Mecánico',
    value: 0,
  },
  {
    label: 'Automático',
    value: 1,
  },
  {
    label: 'Conducción autónoma',
    value: 2,
  },
];

export const Plazas = [
  {
    label: '1',
    value: 190,
  },
  {
    label: '2',
    value: 100,
  },
  {
    label: '3',
    value: 0,
  },
  {
    label: '4',
    value: 990,
  },
  {
    label: '5',
    value: 1,
  },

  {
    label: '6',
    value: 871,
  },

  {
    label: '7',
    value: 2,
  },

  {
    label: '8',
    value: 2654,
  },

  {
    label: '9',
    value: 3,
  },
  {
    label: '10',
    value: 3784,
  },
  {
    label: '11',
    value: 453,
  },

  {
    label: '12 +',
    value: 4,
  },
];

export const Equipaments = [
  {
    title: 'title-1',
    active: true,
  },
  {
    title: 'title-2',
    active: true,
  },
  {
    title: 'title-3',
    active: true,
  },
  {
    title: 'title-4',
    active: true,
  },
  {
    title: 'title-5',
    active: true,
  },
  {
    title: 'title-6',
    active: true,
  },

  {
    title: 'title-7',
    active: true,
  },
  {
    title: 'title-8',
    active: true,
  },
  {
    title: 'title-9',
    active: true,
  },
  {
    title: 'title-10',
    active: true,
  },
  {
    title: 'title-11',
    active: true,
  },
];

export const Preference = [
  {
    title: 'preferences-title-1',
    active: true,
    iconName: 'slash',
    iconType: 'Feather',
  },
  {
    title: 'preferences-title-2',
    active: true,
    iconName: 'slash',
    iconType: 'Feather',
  },
  {
    title: 'preferences-title-3',
    active: true,
    iconName: 'slash',
    iconType: 'Feather',
  },
];

export const Babyseat = [
  {
    title: 'title-0-12',
    description: 'description-0-12',
    quantity: 1,
    quantityAvalible: 1,
    priceExtra: 0,
    avalible: true,
    iconName: 'car-child-seat',
    iconType: 'MaterialCommunityIcons',
  },

  {
    title: 'title-1-4',
    description: 'description-0-12',
    quantity: 1,
    quantityAvalible: 1,
    priceExtra: 0,
    avalible: true,
    iconName: 'car-child-seat',
    iconType: 'MaterialCommunityIcons',
  },

  {
    title: 'title-4-10',
    description: 'description-0-12',
    quantity: 1,
    quantityAvalible: 1,
    priceExtra: 0,
    avalible: true,
    iconName: 'car-child-seat',
    iconType: 'MaterialCommunityIcons',
  },
];

export const ExtraEquipment = [
  {
    title: 'Portaequipajes',
    iconName: 'bag-carry-on',
    priceExtra: 0,
    iconType: 'MaterialCommunityIcons',
  },

  {
    title: 'Portabicicletas',
    iconName: 'bicycle-outline',
    priceExtra: 0,
    iconType: 'Ionicons',
  },
];

export const Quantity = [
  {
    label: '1',
    value: 1,
  },
  {
    label: '2',
    value: 1,
  },
  {
    label: '3',
    value: 3,
  },
  {
    label: '4',
    value: 4,
  },
  {
    label: '5',
    value: 5,
  },
];
