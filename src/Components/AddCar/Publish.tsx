import React, {useState} from 'react';
import {FlatList, View} from 'react-native';
import {colors, stylesText} from '../../Themes';
import {useTranslation} from 'react-i18next';
import {Babyseat, ExtraEquipment} from './Aditional';
import {CustomText} from '../CustomText';
import Item from './ItemBaby';
import Items from './ExtraEquipament';

export default function Publish({
  extraequipemnet,
  setExtraEquipemnet,
  babySeat,
  setBabySeat,
  currency,
  edit,
}) {
  const {t} = useTranslation();
  const InitialStateBaby = edit && babySeat.length > 0 ? babySeat : Babyseat;
  const InitialStateExtra =
    edit && extraequipemnet.length > 0 ? extraequipemnet : ExtraEquipment;

  const Bs = JSON.parse(JSON.stringify(InitialStateBaby));
  const Ee = JSON.parse(JSON.stringify(InitialStateExtra));
  const [arraysDataBaby, setarraysDataBaby] = useState(Bs);
  const [arraysEquipemnet, setarraysEquipemnet] = useState(Ee);

  const renderItem = ({item, index}) => {
    const adds =
      babySeat.filter((x: any) => x.title === item.title).length === 1;

    return (
      <Item
        item={item}
        index={index}
        title={item.title}
        iconType={item.iconType}
        iconName={item.iconName}
        BabySeats={babySeat}
        setBabySeats={setBabySeat}
        setarraysDataBaby={setarraysDataBaby}
        arrays={arraysDataBaby}
        currency={currency}
        ifExist={adds}
        edit={edit}
      />
    );
  };

  const renderItems = ({item, index}) => {
    const adds =
      extraequipemnet.filter((x: any) => x.title === item.title).length === 1;

    return (
      <Items
        item={item}
        iconType={item.iconType}
        iconName={item.iconName}
        currency={currency}
        extraequipemnet={extraequipemnet}
        setExtraEquipemnet={setExtraEquipemnet}
        setarraysEquipemnet={setarraysEquipemnet}
        arrays={arraysEquipemnet}
        ifExist={adds}
        index={index}
        edit={edit}
      />
    );
  };

  return (
    <View style={{alignSelf: 'center', marginTop: 40}}>
      <View style={{marginBottom: 30}}>
        <CustomText
          style={[stylesText.TitleCard]}
          light={colors.black}
          dark={colors.white}>
          {t('addCar:babyseat')}
        </CustomText>
      </View>

      <FlatList
        data={arraysDataBaby}
        renderItem={(item: any) => renderItem(item)}
        keyExtractor={(item: any) => item.title}
        scrollEnabled={false}
      />

      <View style={{marginBottom: 30}}>
        <CustomText
          style={[stylesText.TitleCard]}
          light={colors.black}
          dark={colors.white}>
          {t('addCar:titleExtrasEquipament')}
        </CustomText>
      </View>
      <FlatList
        data={arraysEquipemnet}
        renderItem={(item: any) => renderItems(item)}
        keyExtractor={(item: any) => item.title}
        scrollEnabled={false}
      />
    </View>
  );
}
