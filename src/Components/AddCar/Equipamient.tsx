import React from 'react';
import {View, Platform} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';
import CheckBox from '@react-native-community/checkbox';

export default function Equipemnet({
  item,
  SelectItems,
  idExist,
  deletedItemEquipemnet,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const border = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const borderColor = border[mode];

  const main = {
    light: colors.green,
    dark: colors.green,
  };

  const mainColor = main[mode];

  return (
    <View style={[styles.items, {borderBottomColor: borderColor}]}>
      <View style={{marginLeft: 10}}>
        <CustomText
          style={[stylesText.secondaryText, {fontWeight: '400'}]}
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}>
          <Icon
            type="Feather"
            name="chevrons-right"
            size={14}
            color={mainColor}
          />{' '}
          {t(`details:equipemnet:${item.title}`)}
        </CustomText>
      </View>
      <CheckBox
        value={idExist}
        disabled={false}
        onValueChange={value =>
          idExist ? deletedItemEquipemnet(item.title) : SelectItems(item)
        }
        onTintColor={mainColor}
        tintColors={{true: mainColor, false: colors.rgb_153}}
        onCheckColor={mainColor}
        onAnimationType="fill"
        offAnimationType="fill"
        style={{marginLeft: 10, marginRight: 10}}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomWidth: 1,
    paddingBottom: 10,
    width: dimensions.Width(100),
    marginTop: 15,
  },
});
