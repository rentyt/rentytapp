import React, {useState, useEffect} from 'react';
import {
  TouchableOpacity,
  View,
  Platform,
  Image,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {dimensions, colors, stylesText} from '../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';
import {launchImageLibrary} from 'react-native-image-picker';
import {useMutation} from '@apollo/client';
import {mutations} from '../../GraphQL';
import {Equipaments, Preference} from './Aditional';
import Items from './Equipamient';
import Preferencias from './Preferencia';
import CustomAnimationProgress from '../ProgressBar';
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';

const height = Dimensions.get('window').height;

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function PhotoDescription({
  imagen,
  setImagens,
  preference,
  serPreference,
  equipemnets,
  setEquipemnet,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const [singleUploadToAws] = useMutation(mutations.UPLOAD_FILE);
  const [imgID, setImgID] = useState(0);
  const [Loading, setLoading] = useState(false);

  useEffect(() => {
    if (Platform.OS === 'ios') {
      requestMultiple([PERMISSIONS.IOS.CAMERA]).then(statuses => {});
    } else {
      requestMultiple([PERMISSIONS.ANDROID.CAMERA]).then(statuses => {});
    }
  }, []);

  const mode = useColorSchemeContext();

  let equipemnet = JSON.parse(JSON.stringify(equipemnets));
  let preferences = JSON.parse(JSON.stringify(preference));
  let imagens = JSON.parse(JSON.stringify(imagen));

  const main = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const mainColor = main[mode];

  const SelectItemsEquipemnet = (items: any) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    setEquipemnet(equipemnet.concat(items));
  };

  const deletedItemEquipemnet = (title: string) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    var i = equipemnet.findIndex((x: any) => x.title === title);

    if (i !== -1) {
      equipemnet.splice(i, 1);
      setEquipemnet(equipemnet.concat());
    }
  };

  const SelectItemsPreference = (items: any) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    serPreference(preferences.concat(items));
  };

  const deletedItemPreference = (title: string) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    var i = preferences.findIndex((x: any) => x.title === title);
    if (i !== -1) {
      preferences.splice(i, 1);
      serPreference(preferences.concat());
    }
  };

  const renderItems = (item, i) => {
    const adds =
      equipemnet.filter((x: any) => x.title === item.title).length === 1;

    return (
      <Items
        item={item}
        key={i}
        SelectItems={SelectItemsEquipemnet}
        deletedItemEquipemnet={deletedItemEquipemnet}
        idExist={adds}
      />
    );
  };

  const renderItemsPreferencias = (item, i) => {
    const adds =
      preferences.filter((x: any) => x.title === item.title).length === 1;

    return (
      <Preferencias
        item={item}
        key={i}
        iconType={item.iconType}
        iconName={item.iconName}
        SelectItems={SelectItemsPreference}
        deletedItemPreference={deletedItemPreference}
        idExist={adds}
      />
    );
  };

  const getImageData = async (num: number) => {
    setImgID(num);
    setLoading(true);
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);

    const result = await launchImageLibrary({
      mediaType: 'photo',
      includeBase64: true,
      maxWidth: 512,
      maxHeight: 512,
    });

    if (result.didCancel) {
      setLoading(false);
      return null;
    } else {
      const file = 'data:image/jpeg;base64,' + result.assets[0].base64;

      singleUploadToAws({variables: {file: file}})
        .then(res => {
          const result = res.data.singleUploadToAws.data;
          if (result) {
            imagens[num].uri = result.Location;
            const ifExist = imagens.filter(x => x.uri === null).length === 1;
            if (!ifExist && imagens.length < 11)
              setImagens([...imagens, {uri: null}]);
            setLoading(false);
          }
        })
        .catch(() => {
          setLoading(false);
        });
    }
  };

  const deletedImagen = (num: number) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    if (num !== -1) {
      imagens.splice(num, 1);
      setImagens(imagens.concat());
    }
  };

  return (
    <View style={{marginTop: 20}}>
      <View style={{marginHorizontal: 10}}>
        <CustomAnimationProgress
          stopColor={mainColor}
          color2={colors.green}
          color1={colors.green}
          color3={colors.green}
          maxpercent={4}
          percent={imagens.length - 1}
          width={dimensions.Width(95)} //numeric only
          height={14}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 5,
          }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              {paddingBottom: 0, marginTop: 5},
            ]}>
            {t('addCar:up')}
          </CustomText>
          <CustomText
            light={imagens.length > 4 ? colors.green : colors.main}
            dark={imagens.length > 4 ? colors.green : colors.main}
            style={[
              stylesText.secondaryText,
              {paddingBottom: 0, marginTop: 5},
            ]}>
            {imagens.length - 1} {t('addCar:minimephoto')}
          </CustomText>
        </View>
      </View>
      <View style={styles.container}>
        {imagens.map((imgen, i) => {
          return (
            <>
              {imagens[i].uri ? (
                <View
                  style={[
                    styles.addCont,
                    {
                      height: i == 0 ? 150 : 100,
                      width:
                        i == 0
                          ? dimensions.Width(95)
                          : height > 860
                          ? dimensions.Width(28)
                          : dimensions.Width(27),
                    },
                  ]}>
                  <TouchableOpacity
                    style={i == 0 ? styles.trashLargue : styles.trash}
                    onPress={() => deletedImagen(i)}>
                    <Icon
                      name="trash"
                      type="Feather"
                      size={24}
                      color={colors.ERROR}
                    />
                  </TouchableOpacity>
                  <Image
                    source={imgen}
                    style={
                      i == 0 ? styles.addContImagenLargue : styles.addContImagen
                    }
                    resizeMode="cover"
                  />
                </View>
              ) : (
                <TouchableOpacity
                  style={[
                    styles.addCont,
                    {
                      height: i == 0 ? 150 : 100,
                      width:
                        i == 0
                          ? dimensions.Width(95)
                          : height > 860
                          ? dimensions.Width(28)
                          : dimensions.Width(27),
                    },
                  ]}
                  onPress={() => getImageData(i)}>
                  {Loading && imgID === i ? (
                    <ActivityIndicator color={colors.green} />
                  ) : (
                    <View
                      style={{justifyContent: 'center', alignItems: 'center'}}>
                      <Icon
                        name="image"
                        type="Feather"
                        size={24}
                        color={colors.green}
                      />
                      <CustomText
                        light={colors.green}
                        dark={colors.green}
                        style={[
                          stylesText.secondaryText,
                          {paddingBottom: 5, marginTop: 5, textAlign: 'center'},
                        ]}>
                        {t('addCar:addPhoto')}
                      </CustomText>
                    </View>
                  )}
                </TouchableOpacity>
              )}
            </>
          );
        })}
      </View>
      <View style={{marginHorizontal: 15, marginTop: 30}}>
        <CustomText
          style={[stylesText.TitleCard]}
          light={colors.black}
          dark={colors.white}>
          {t('addCar:titleExtras')}
        </CustomText>
        <CustomText
          light={equipemnet.length > 3 ? colors.green : colors.main}
          dark={equipemnet.length > 3 ? colors.green : colors.main}
          style={[stylesText.secondaryText, {paddingBottom: 0, marginTop: 5}]}>
          {t('addCar:extraMinime')}
        </CustomText>
      </View>
      <View style={{marginTop: 20}}>
        {Equipaments.map((item, i) => {
          return renderItems(item, i);
        })}
      </View>

      <View style={{marginHorizontal: 15, marginTop: 30}}>
        <CustomText
          style={[stylesText.TitleCard]}
          light={colors.black}
          dark={colors.white}>
          {t('addCar:titlePreference')}
        </CustomText>
      </View>
      <View style={{marginTop: 20}}>
        {Preference.map((item, i) => {
          return renderItemsPreferencias(item, i);
        })}
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.Width(100),
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  addCont: {
    padding: 10,
    margin: 11,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderWidth: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderColor: colors.green,
  },

  addContImagen: {
    width: '118%',
    height: 98,
    borderRadius: 10,
  },

  addContImagenLargue: {
    width: '105%',
    height: 148,
    borderRadius: 10,
  },

  trashLargue: {
    position: 'absolute',
    zIndex: 100,
    top: -20,
    right: -5,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    padding: 12,
    borderRadius: 100,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
  },

  trash: {
    position: 'absolute',
    zIndex: 100,
    top: -20,
    right: -5,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    padding: 8,
    borderRadius: 100,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
  },
});
