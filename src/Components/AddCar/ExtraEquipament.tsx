import React from 'react';
import {View} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';
import {formaterPrice} from '../../Utils/formaterPrice';
import CheckBox from '@react-native-community/checkbox';
import Pagination from '../../Components/Pagination';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {deleteItemExtra, AddItemExtra} from '../../Utils/addItemExtra';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ItemExtra({
  item,
  iconType,
  iconName,
  currency,
  extraequipemnet,
  setExtraEquipemnet,
  setarraysEquipemnet,
  arrays,
  ifExist,
  index,
  edit,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const main = {
    light: colors.green,
    dark: colors.green,
  };

  const mainColor = main[mode];

  const plusQuantityPrice = (quantity, index, max) => {
    const plus = currency.currency === 'DOP' ? 1000 : 100;
    ReactNativeHapticFeedback.trigger('selection', optiones);
    if (quantity < max) {
      let newBundled = [...arrays];
      newBundled[index]['priceExtra'] = quantity + plus;
      setarraysEquipemnet(newBundled);
      if (edit) {
        setExtraEquipemnet(newBundled);
      } else {
        setExtraEquipemnet(extraequipemnet.concat([]));
      }
    }
  };

  const minusQuantityPrice = (quantity, index) => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    const plus = currency.currency === 'DOP' ? 1000 : 100;
    if (quantity > 0) {
      let newBundled = [...arrays];
      newBundled[index]['priceExtra'] = quantity - plus;
      setarraysEquipemnet(newBundled);
      if (edit) {
        setExtraEquipemnet(newBundled);
      } else {
        setExtraEquipemnet(extraequipemnet.concat([]));
      }
    }
  };

  const maximo = currency.currency === 'DOP' ? 100000 : 10000;

  return (
    <View style={[styles.items]}>
      <View>
        <Icon
          type={iconType}
          name={iconName}
          size={28}
          color={colors.green}
          style={{marginTop: -30}}
        />
      </View>
      <View
        style={{
          marginLeft: 10,
          justifyContent: 'space-between',
          flexDirection: 'row',
          width: dimensions.IsIphoneX()
            ? dimensions.Width(81)
            : dimensions.Width(80),
        }}>
        <View>
          <CustomText
            style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
            numberOfLines={1}
            light={colors.black}
            dark={colors.white}>
            {t(`details:ExtraEquipment:${item.title}`)}
          </CustomText>
          <CustomText
            style={[stylesText.secondaryText, {marginTop: 5}]}
            numberOfLines={1}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {t(`details:ExtraEquipmentExtra`)}
          </CustomText>
          {ifExist ? (
            <View style={{marginTop: 10, marginLeft: -5}}>
              <Pagination
                page={`${formaterPrice(
                  item.priceExtra,
                  currency.localcode,
                  currency.currency,
                )} / ${t('card:day')}`}
                onPressMas={() => {
                  plusQuantityPrice(item.priceExtra, index, maximo);
                }}
                onPressMenos={() => {
                  if (item.priceExtra == 0) {
                    deleteItemExtra(
                      extraequipemnet,
                      item.title,
                      setExtraEquipemnet,
                      setarraysEquipemnet,
                      arrays,
                    );
                  } else {
                    minusQuantityPrice(item.priceExtra, index);
                  }
                }}
                width={150}
                widthBTN={25}
                sizeIcon={16}
                fontSize={14}
              />
            </View>
          ) : null}
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <CheckBox
            value={ifExist}
            disabled={false}
            onValueChange={value => {
              if (value) {
                AddItemExtra(
                  extraequipemnet,
                  item.title,
                  item,
                  setExtraEquipemnet,
                  setarraysEquipemnet,
                  arrays,
                );
              } else {
                deleteItemExtra(
                  extraequipemnet,
                  item.title,
                  setExtraEquipemnet,
                  setarraysEquipemnet,
                  arrays,
                );
              }
            }}
            onTintColor={mainColor}
            tintColors={{true: mainColor, false: colors.rgb_153}}
            onCheckColor={mainColor}
            onAnimationType="fill"
            offAnimationType="fill"
            style={{marginLeft: 10}}
          />
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingBottom: 20,
    width: dimensions.Width(92),
    marginTop: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },
});
