import React from 'react';
import {View, TextInput, TouchableWithoutFeedback} from 'react-native';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {colors, dimensions} from '../../Themes';
import DataYear from '../../data/year.json';
import {Fuel, Transmission, Plazas} from './Aditional';
import {Picker} from 'react-native-woodpicker';
import {CustomText} from '../../Components/CustomText';

export default function Info({
  items,
  pickedData,
  pickedModel,
  pickedYear,
  pickedFuel,
  pickedTrans,
  pickedPlazas,
  matricula,
  setPickedData,
  setPickedModel,
  setPickedYear,
  setPickedFuel,
  setPickedTrans,
  setPickedPlazas,
  setMatricula,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const modo = useColorSchemeContext();

  const linecolor = {
    light: colors.black,
    dark: colors.white,
  };

  const lineColor = linecolor[modo];

  return (
    <View style={{marginTop: 50}}>
      <Picker
        item={pickedData}
        items={items}
        onItemChange={setPickedData}
        title={t('addCar:selectMaktitle')}
        placeholder={t('addCar:placeholderMark')}
        doneButtonLabel={t('addCar:btnDone')}
        isNullable={false}
        mode="dropdown"
        containerStyle={styles.input}
        textInputStyle={styles.textInput}
      />

      {pickedData.value != 290 ? (
        <Picker
          item={pickedModel}
          items={pickedData.models}
          onItemChange={setPickedModel}
          title={t('addCar:placeholderModel')}
          placeholder={t('addCar:placeholderModel')}
          doneButtonLabel={t('addCar:btnDone')}
          isNullable={false}
          mode="dropdown"
          //@ts-ignore
          containerStyle={[styles.input, {marginTop: 20}]}
          textInputStyle={styles.textInput}
        />
      ) : null}

      {pickedModel.value != 290 ? (
        <Picker
          item={pickedYear}
          items={DataYear}
          onItemChange={setPickedYear}
          title={t('addCar:placeholderYear')}
          placeholder={t('addCar:placeholderYear')}
          doneButtonLabel={t('addCar:btnDone')}
          isNullable={false}
          mode="dropdown"
          //@ts-ignore
          containerStyle={[styles.input, {marginTop: 20}]}
          textInputStyle={styles.textInput}
        />
      ) : null}

      {pickedYear.value != 290 ? (
        <TextInput
          autoCorrect={false}
          placeholder={t('addCar:placa')}
          enablesReturnKeyAutomatically={true}
          autoCompleteType="name"
          numberOfLines={1}
          multiline={false}
          autoCapitalize="none"
          value={matricula}
          selectionColor={colors.green}
          placeholderTextColor={lineColor}
          onChangeText={setMatricula}
          style={styles.inputText}
        />
      ) : null}

      {matricula ? (
        <Picker
          item={pickedFuel}
          items={Fuel}
          onItemChange={setPickedFuel}
          title={t('addCar:placeholderFuel')}
          placeholder={t('addCar:placeholderFuel')}
          doneButtonLabel={t('addCar:btnDone')}
          isNullable={false}
          mode="dropdown"
          //@ts-ignore
          containerStyle={[styles.input, {marginTop: 20}]}
          textInputStyle={styles.textInput}
        />
      ) : null}

      {pickedFuel.value != 290 ? (
        <Picker
          item={pickedTrans}
          items={Transmission}
          onItemChange={setPickedTrans}
          title={t('addCar:placeholderTrans')}
          placeholder={t('addCar:placeholderTrans')}
          doneButtonLabel={t('addCar:btnDone')}
          isNullable={false}
          mode="dropdown"
          //@ts-ignore
          containerStyle={[styles.input, {marginTop: 20}]}
          textInputStyle={styles.textInput}
        />
      ) : null}

      {pickedTrans.value != 290 ? (
        <Picker
          item={pickedPlazas}
          items={Plazas}
          onItemChange={setPickedPlazas}
          title={t('addCar:placeholderPlazas')}
          placeholder={t('addCar:placeholderPlazas')}
          doneButtonLabel={t('addCar:btnDone')}
          isNullable={false}
          mode="dropdown"
          //@ts-ignore
          containerStyle={[styles.input, {marginTop: 20}]}
          textInputStyle={styles.textInput}
        />
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  input: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    height: 55,
    marginHorizontal: 20,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    color: new DynamicValue(colors.black, colors.white),
  },

  textInput: {
    color: new DynamicValue(colors.black, colors.white),
    fontFamily: 'Poppins',
    justifyContent: 'center',
    alignItems: 'center',
  },

  inputText: {
    flexDirection: 'row',
    width: dimensions.Width(90),
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    color: new DynamicValue(colors.black, colors.white),
    fontSize: 16,
    fontFamily: 'Poppins',
    fontWeight: '400',
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    height: 55,
    marginHorizontal: 20,
    marginTop: 20,
    borderRadius: 10,
  },
});
