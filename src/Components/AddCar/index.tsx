import React, {useState, useEffect, useContext} from 'react';
import {View, Modal, Platform, TouchableOpacity} from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import {colors, dimensions, stylesText} from '../../Themes';
import HeaderModal from '../../Components/HeaderModal';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';

import Info from './Info';
import Price from './PriceOffer';
import PhotoDescription from './PhotoDescription';
import Location from './Location';
import Publish from './Publish';
import Icon from 'react-native-dynamic-vector-icons';
import type {PickerItem} from 'react-native-woodpicker';
import {MAIN_URL} from '../../Utils/Urls';
import {MainContext} from '../../store/MainProvider';
import moment from 'moment';
import ModalLoading from '../ModalLoadingAnimated';
import {useMutation} from '@apollo/client';
import {mutations} from '../../GraphQL';
import Succeess from './Succeess';
import Toast from 'react-native-simple-toast';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function AddCar({
  visibleModal,
  setvisibleModal,
  edit,
  refetch,
  data,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const modo = useColorSchemeContext();
  const {users} = useContext(MainContext);

  const [createdAds] = useMutation(mutations.CREATED_ADS);
  const [updateAds] = useMutation(mutations.UPDATE_ADS);

  const initialState = {
    marker: edit
      ? {
          label: data.car.marker,
          value: 0,
          models: [],
        }
      : {
          label: t('addCar:selectMaktitle'),
          value: 290,
          models: [],
        },
    model: edit
      ? {
          label: data.car.model,
          value: 0,
          models: [],
        }
      : {
          label: t('addCar:placeholderModel'),
          value: 290,
          models: [],
        },

    year: edit
      ? {
          label: data.car.year,
          value: 0,
          models: [],
        }
      : {
          label: t('addCar:placeholderYear'),
          value: 290,
          models: [],
        },

    placa: edit ? data.details.enrollment.data : '',

    Fuels: edit
      ? {
          label: data.details.combustible.data,
          value: 0,
          models: [],
        }
      : {
          llabel: t('addCar:placeholderFuel'),
          value: 290,
          models: [],
        },
    Trans: edit
      ? {
          label: data.details.transmission.data,
          value: 0,
          models: [],
        }
      : {
          label: t('addCar:placeholderTrans'),
          value: 290,
          models: [],
        },
    Plazas: edit
      ? {
          label: data.details.plazas.data,
          value: 0,
          models: [],
        }
      : {
          label: t('addCar:placeholderPlazas'),
          value: 290,
          models: [],
        },

    OrfersMonNumber: edit && data.offertMon ? data.anticipation[0].date : 0,
    OrfersMonPorcent:
      edit && data.offertMon ? data.anticipation[0].discount : 0,

    OrfersDayNumber: edit && data.offertDay ? data.offerts[0].moreDays : 0,
    OrfersDayPorcent: edit && data.offertDay ? data.offerts[0].discount : 0,
    instantBooking: edit ? data.instantBooking : false,
    airplane: edit ? data.airPortPickup.available : false,
    airplaneCost: edit ? data.airPortPickup.cost / 100 : 0,
    description: edit ? data.description : '',
    price: edit ? data.prices.value / 100 : 0,
    currecy: edit
      ? {
          currency: data.prices.currency,
          localcode: data.prices.localcode,
        }
      : {
          currency: 'USD',
          localcode: 'es-DO',
        },
    minDays: edit ? data.minDays : 0,
    image: edit ? data.images.concat([{uri: null}]) : [{uri: null}],
    preferences: edit
      ? data.preferences
      : [
          {
            title: 'preferences-title-2',
            active: true,
            iconName: 'slash',
            iconType: 'Feather',
          },
        ],
    equipemnet: edit ? data.equipemnet : [],
    location: edit ? data.location : null,
    extraequipemnet: edit ? data.ExtraEquipment : [],
    babySeat: edit ? data.babyChairs : [],
    city: edit ? data.city : '',
  };

  const [currentPosition, setcurrentPosition] = useState(0);
  const [items, setItems] = useState([]);

  const [pickedData, setPickedData] = useState<PickerItem>(initialState.marker);
  const [pickedModel, setPickedModel] = useState<PickerItem>(
    initialState.model,
  );
  const [pickedYear, setPickedYear] = useState<PickerItem>(initialState.year);
  //@ts-ignore
  const [pickedFuel, setPickedFuel] = useState<PickerItem>(initialState.Fuels);
  const [pickedTrans, setPickedTrans] = useState<PickerItem>(
    initialState.Trans,
  );
  const [pickedPlazas, setPickedPlazas] = useState<PickerItem>(
    initialState.Plazas,
  );
  const [matricula, setMatricula] = useState(initialState.placa);

  const [porcentajeMes, setPorcentajeMes] = useState(
    initialState.OrfersMonPorcent,
  );
  const [porcentajeDias, setPorcentajeDias] = useState(
    initialState.OrfersDayPorcent,
  );
  const [numberMes, setNumberMes] = useState(initialState.OrfersMonNumber);
  const [numberDias, setNumbereDias] = useState(initialState.OrfersDayNumber);
  const [isEnabled, setisEnabled] = useState(initialState.instantBooking);
  const [airplane, setairplane] = useState(initialState.airplane);
  const [description, setdescription] = useState(initialState.description);
  const [price, setprice] = useState(initialState.price);
  const [minime, setminime] = useState(initialState.minDays);
  const [currency, setCurrency] = useState(initialState.currecy);
  const [imagens, setImagens] = useState(initialState.image);
  const [preferences, serPreference] = useState(initialState.preferences);
  const [equipemnet, setEquipemnet] = useState(initialState.equipemnet);
  const [location, setLocation] = useState(initialState.location);
  const [extraequipemnet, setExtraEquipemnet] = useState(
    initialState.extraequipemnet,
  );
  const [babySeat, setBabySeat] = useState(initialState.babySeat);
  const [airValue, setAirValue] = useState(initialState.airplaneCost);
  const [Loading, setLoading] = useState(false);
  const [city, setCity] = useState(initialState.city);

  const imagenFormat = imagens.filter(imgs => {
    return imgs.uri !== null;
  });

  const values = price * 100;
  const airvalue = airValue * 100;

  const toggleSwitch = () => {
    setisEnabled(!isEnabled);
  };

  const toggleSwitchAir = () => {
    setairplane(!airplane);
  };

  const getItems = async () => {
    const resp = await fetch(`${MAIN_URL}/get-mark`);
    const data = await resp.json();
    setItems(data.data);
  };

  useEffect(() => {
    getItems();
  }, []);
  const labels = [
    t('addCar:label:1'),
    t('addCar:label:2'),
    t('addCar:label:3'),
    t('addCar:label:4'),
    t('addCar:label:5'),
  ];

  const main = colors.main;

  const bgcolor = {
    light: colors.white,
    dark: colors.black,
  };

  const bgColor = bgcolor[modo];

  const linecolor = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const lineColor = linecolor[modo];

  const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: main,
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: colors.green,
    stepStrokeUnFinishedColor: lineColor,
    separatorFinishedColor: colors.green,
    separatorUnFinishedColor: lineColor,
    stepIndicatorFinishedColor: colors.green,
    stepIndicatorUnFinishedColor: bgColor,
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 12,
    currentStepIndicatorLabelFontSize: 12,
    stepIndicatorLabelCurrentColor: main,
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: lineColor,
    labelColor: '#999999',
    labelSize: 12,
    currentStepLabelColor: main,
    labelFontFamily: 'Poppins',
  };

  const isOkSteep1 = () => {
    if (currentPosition === 0 && pickedPlazas.value != 290) {
      return true;
    } else {
      return false;
    }
  };

  const isOkSteep2 = () => {
    if (currentPosition === 1 && price && description) {
      return true;
    } else {
      return false;
    }
  };

  const isOkSteep3 = () => {
    if (currentPosition === 2 && imagens.length > 4 && equipemnet.length > 3) {
      return true;
    } else {
      return false;
    }
  };

  const isOkSteep4 = () => {
    if (currentPosition === 3 && location && city) {
      return true;
    } else {
      return false;
    }
  };

  const isOkSteep5 = () => {
    if (currentPosition > 3) {
      return true;
    } else {
      return false;
    }
  };

  const isOk = () => {
    switch (currentPosition) {
      case 0:
        return isOkSteep1();
      case 1:
        return isOkSteep2();
      case 2:
        return isOkSteep3();
      case 3:
        return isOkSteep4();
      case 4:
        return isOkSteep5();
    }
  };

  const onPageChange = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (currentPosition > 4) {
      null;
    } else {
      if (isOk()) {
        setcurrentPosition(currentPosition + 1);
      }
    }
  };

  const onPageChangeBack = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (currentPosition === 0) {
      null;
    } else {
      setcurrentPosition(currentPosition - 1);
    }
  };

  const CancelProduct = () => {
    setvisibleModal(false);
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
  };

  const isAnticipation = () => {
    if (porcentajeMes && numberMes) {
      return [
        {
          date: numberMes,
          type: 'months',
          discount: porcentajeMes,
          typeDiscount: 'percentage',
        },
      ];
    } else {
      return [];
    }
  };

  const Orfers = () => {
    if (porcentajeDias && numberDias) {
      return [
        {
          _id: 1,
          moreDays: numberDias,
          discount: porcentajeDias,
          type: 'percentage',
        },
      ];
    } else {
      return [];
    }
  };

  const input = {
    _id: edit ? data._id : null,
    car: {
      marker: pickedData.label,
      model: pickedModel.label,
      year: pickedYear.label,
    },
    instantBooking: isEnabled,
    name: edit
      ? data.name
      : `${pickedData.label} ${pickedModel.label} ${pickedYear.label}`,
    inService: edit ? data.inService : true,
    visible: edit ? data.visible : true,
    minDays: minime === 0 ? 1 : minime,
    gestionCost: currency.currency === 'DOP' ? 20000 : 1200,
    anticipation: isAnticipation(),
    offerts: Orfers(),
    images: imagenFormat,
    description: description,
    owner: edit ? data.owner : users._id,
    location: location,
    city: city,
    details: {
      enrollment: {
        title: 'Placa',
        data: matricula.toUpperCase().trim(),
        iconName: 'card-text-outline',
        iconType: 'MaterialCommunityIcons',
      },
      combustible: {
        title: 'Combustible',
        data: pickedFuel.label,
        iconName: 'gas-station-outline',
        iconType: 'MaterialCommunityIcons',
      },
      transmission: {
        title: 'Transmission',
        data: pickedTrans.label,
        iconName: 'car-shift-pattern',
        iconType: 'MaterialCommunityIcons',
      },
      plazas: {
        title: 'Plazas',
        data: pickedPlazas.label,
        iconName: 'car-seat',
        iconType: 'MaterialCommunityIcons',
      },
      year: {
        title: 'Año',
        data: pickedYear.label,
        iconName: 'calendar',
        iconType: 'Feather',
      },
    },
    equipemnet: equipemnet,
    ExtraEquipment: extraequipemnet,
    babyChairs: babySeat,
    preferences: preferences,
    prices: {
      value: Number(values.toFixed(0)),
      currency: currency.currency,
      localcode: currency.localcode,
    },
    availability: {
      startDate: edit
        ? data.availability.startDate
        : moment().format('YYYY-MM-DD').toString(),
      endDate: edit
        ? data.availability.endDate
        : moment().format('YYYY-MM-DD').toString(),
    },
    busyDays: edit ? data.busyDays : {},
    status: 'In review',
    statusProcess: edit
      ? data.statusProcess.concat({
          status: 'In review',
          date: moment().format(),
        })
      : [
          {
            status: 'Draft',
            date: moment().format(),
          },

          {
            status: 'Publish',
            date: moment().format(),
          },

          {
            status: 'In review',
            date: moment().format(),
          },
        ],
    offertDay: edit ? data.offertDay : Orfers().length > 0 ? true : false,
    offertMon: edit
      ? data.offertMon
      : isAnticipation().length > 0
      ? true
      : false,
    airPortPickup: {
      available: airplane,
      cost: airvalue,
    },
  };

  const crearteAds = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setLoading(true);
    createdAds({variables: {input: {data: input}}})
      .then(res => {
        if (res.data.createdAds.success) {
          null;
        } else {
          Toast.show(t(`addCar:${res.data.createdAds.message}`), Toast.LONG, [
            'UIAlertController',
          ]);
          setLoading(false);
        }
      })
      .catch(e => {
        console.log(e);
        setLoading(false);
        Toast.show(t(`addCar:error_created_ads`), Toast.LONG, [
          'UIAlertController',
        ]);
      })
      .finally(() => {
        setLoading(false);
        setcurrentPosition(5);
      });
  };

  const UpdateAds = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setLoading(true);
    updateAds({variables: {input: {data: input}}})
      .then(res => {
        if (res.data.updateAds.success) {
          refetch();
        } else {
          Toast.show(res.data.updateAds.messages, Toast.LONG, [
            'UIAlertController',
          ]);
          setLoading(false);
        }
      })
      .catch(e => {
        console.log(e);
        setLoading(false);
        Toast.show('Sistem error', Toast.LONG, ['UIAlertController']);
      })
      .finally(() => {
        refetch();
        setLoading(false);
        setcurrentPosition(5);
      });
  };

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      presentationStyle="formSheet"
      collapsable={true}
      statusBarTranslucent={true}
      onRequestClose={() => setvisibleModal(false)}>
      <ModalLoading loading={Loading} setVisible={setLoading} />
      {currentPosition == 5 ? (
        <Succeess
          setvisibleModal={setvisibleModal}
          setcurrentPosition={setcurrentPosition}
          edit={edit}
        />
      ) : null}
      {currentPosition < 5 ? (
        <HeaderModal
          onPress={CancelProduct}
          title={t('home:addAd')}
          children={
            <View style={styles.contentModal}>
              <StepIndicator
                customStyles={customStyles}
                currentPosition={currentPosition}
                labels={labels}
                onPress={edit ? setcurrentPosition : null}
              />
              {currentPosition == 0 ? (
                <Info
                  pickedData={pickedData}
                  items={items}
                  pickedModel={pickedModel}
                  pickedYear={pickedYear}
                  pickedFuel={pickedFuel}
                  pickedTrans={pickedTrans}
                  pickedPlazas={pickedPlazas}
                  matricula={matricula}
                  setPickedData={setPickedData}
                  setPickedModel={setPickedModel}
                  setPickedYear={setPickedYear}
                  setPickedFuel={setPickedFuel}
                  setPickedTrans={setPickedTrans}
                  setPickedPlazas={setPickedPlazas}
                  setMatricula={setMatricula}
                />
              ) : null}
              {currentPosition == 1 ? (
                <Price
                  porcentajeMes={porcentajeMes}
                  porcentajeDias={porcentajeDias}
                  numberMes={numberMes}
                  numberDias={numberDias}
                  isEnabled={isEnabled}
                  toggleSwitch={toggleSwitch}
                  setPorcentajeMes={setPorcentajeMes}
                  setNumbereDias={setNumbereDias}
                  setPorcentajeDias={setPorcentajeDias}
                  setNumberMes={setNumberMes}
                  description={description}
                  setdescription={setdescription}
                  currency={currency}
                  setCurrency={setCurrency}
                  price={price}
                  setprice={setprice}
                  minime={minime}
                  setminime={setminime}
                  airplane={airplane}
                  toggleSwitchAir={toggleSwitchAir}
                  airValue={airValue}
                  setAirValue={setAirValue}
                  edit={edit}
                />
              ) : null}
              {currentPosition == 2 ? (
                <PhotoDescription
                  imagen={imagens}
                  setImagens={setImagens}
                  preference={preferences}
                  serPreference={serPreference}
                  equipemnets={equipemnet}
                  setEquipemnet={setEquipemnet}
                />
              ) : null}
              {currentPosition == 3 ? (
                <Location
                  location={location}
                  setLocation={setLocation}
                  setCity={setCity}
                />
              ) : null}
              {currentPosition == 4 ? (
                <Publish
                  extraequipemnet={extraequipemnet}
                  setExtraEquipemnet={setExtraEquipemnet}
                  babySeat={babySeat}
                  setBabySeat={setBabySeat}
                  currency={currency}
                  edit={edit}
                />
              ) : null}
            </View>
          }
        />
      ) : null}
      {currentPosition < 5 ? (
        <View style={styles.buttonContent}>
          {currentPosition == 0 ? (
            <View
              style={{
                width: 60,
                height: 60,
              }}
            />
          ) : (
            <TouchableOpacity
              onPress={onPageChangeBack}
              style={{
                width: 60,
                height: 60,
                backgroundColor: colors.main,
                borderRadius: 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <CustomText
                light={colors.white}
                dark={colors.white}
                style={[stylesText.secondaryTextBold, {textAlign: 'center'}]}>
                <Icon
                  type="Feather"
                  name="arrow-left"
                  size={34}
                  color={colors.white}
                />
              </CustomText>
            </TouchableOpacity>
          )}

          <TouchableOpacity
            onPress={() => {
              if (currentPosition === 4 && isOk()) {
                if (edit) {
                  UpdateAds();
                } else {
                  crearteAds();
                }
              } else {
                onPageChange();
              }
            }}
            style={{
              backgroundColor: isOk() ? colors.green : colors.rgb_153,
              paddingVertical: 15,
              paddingHorizontal: 50,
              borderRadius: 100,
            }}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              style={[stylesText.secondaryTextBold, {textAlign: 'center'}]}>
              {currentPosition === 4
                ? edit
                  ? t('addCar:edit')
                  : t('addCar:publish')
                : t('addCar:continue')}
            </CustomText>
          </TouchableOpacity>
        </View>
      ) : null}
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    minHeight: dimensions.Height(100),
    paddingBottom: dimensions.Height(30),
  },

  buttonContent: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(12),
    paddingHorizontal: 15,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    bottom: 0,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 15}),
    zIndex: 100,
  },
});
