import React from 'react';
import {Image, View, Platform} from 'react-native';
import {dimensions, colors, image, stylesText} from '../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {GOOGLE_API_KEY} from '../../Utils/Urls';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {customMaspStyles} from '../../Components/MapStyle';
import {CustomText} from '../CustomText';

export default function Location({location, setLocation, setCity}) {
  const styles = useDynamicValue(dynamicStyles);
  const stylos = useDynamicValue(customMaspStyles);
  const {t} = useTranslation();
  const mode = useColorSchemeContext();
  const Map = new DynamicValue(image.MapLigth, image.MapDakk);

  const source = useDynamicValue(Map);

  const BorderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_dark,
  };

  const borderColor = BorderColor[mode];
  const backgroundColor = backgroundColors[mode];

  const textStyle = {
    container: {
      flex: 1,
    },
    textInputContainer: {
      flexDirection: 'row',
    },
    textInput: {
      backgroundColor: backgroundColor,
      height: 50,
      color: colors.rgb_102,
      borderRadius: 10,
      paddingVertical: 5,
      paddingHorizontal: 10,
      fontSize: 18,
      flex: 1,
    },
    poweredContainer: {
      justifyContent: 'flex-end',
      alignItems: 'center',
      borderBottomRightRadius: 5,
      borderBottomLeftRadius: 5,
      borderColor: borderColor,
      borderTopWidth: 0.5,
    },
    powered: {},
    listView: {},
    row: {
      backgroundColor: 'transparent',
      color: colors.white,
      flexDirection: 'row',
    },
    separator: {
      height: 0,
      backgroundColor: borderColor,
    },
    description: {
      color: colors.rgb_153,
      fontSize: 18,
    },
    loader: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      height: 20,
    },
  };

  const onCity = location && location.name ? location.name : null;

  const city =
    location && location.vicinity
      ? location.vicinity
      : onCity
      ? onCity
      : 'No specified';

  return (
    <View style={{marginTop: 40, marginHorizontal: 15}}>
      <GooglePlacesAutocomplete
        onFail={error => console.log('error' + error)}
        placeholder={t('addCar:placeHolded')}
        fetchDetails={true}
        listEmptyComponent={null}
        textInputProps={{
          selectionColor: colors.main,
          placeholderTextColor: colors.rgb_153,
        }}
        styles={textStyle}
        onPress={(data, details = null) => {
          const names = details && details.name ? details.name : null;
          const Incity =
            details && details.vicinity
              ? details.vicinity
              : names
              ? names
              : 'No specified';
          setLocation(details);
          setCity(Incity);
        }}
        query={{
          key: GOOGLE_API_KEY,
          language: 'es',
        }}
        currentLocation={true}
        currentLocationLabel={t('addCar:current')}
      />

      <View>
        {location ? (
          <View style={styles.mapContent}>
            <MapView
              showsUserLocation={true}
              provider={PROVIDER_GOOGLE}
              customMapStyle={stylos}
              style={{
                height: dimensions.Height(20),
                width: '100%',
                borderRadius: 15,
              }}
              region={{
                latitude: location.geometry.location.lat,
                longitude: location.geometry.location.lng,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}>
              <Marker
                coordinate={{
                  latitude: location.geometry.location.lat,
                  longitude: location.geometry.location.lng,
                }}>
                <Image source={image.MapPin} style={{width: 50, height: 50}} />
              </Marker>
            </MapView>
          </View>
        ) : (
          <Image source={source} style={styles.mapImagen} resizeMode="cover" />
        )}
      </View>

      {location ? (
        <View style={styles.iconContent}>
          <View style={styles.icon}>
            <Icon
              type="Feather"
              name="map-pin"
              size={14}
              color={colors.orange}
            />
          </View>
          <CustomText
            numberOfLines={1}
            style={[
              stylesText.TitleCard,
              {marginLeft: 5, width: dimensions.Width(90)},
            ]}
            light={colors.black}
            dark={colors.white}>
            {city}
          </CustomText>
        </View>
      ) : null}

      <View style={styles.secure}>
        <View>
          <Icon
            name="navigation"
            type="Feather"
            size={24}
            color={colors.green}
          />
        </View>

        <View style={{marginLeft: 15}}>
          <CustomText
            style={[
              stylesText.secondaryText,
              {color: colors.green, fontWeight: '500'},
            ]}>
            {t('addCar:nolocationText')}
          </CustomText>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  mapImagen: {
    width: '100%',
    height: dimensions.Height(20),
    borderRadius: 15,
    marginTop: 15,
  },
  mapContent: {
    overflow: 'hidden',
    height: dimensions.Height(20),
    width: '100%',
    borderRadius: 20,
    marginTop: 15,
  },

  iconContent: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: 15,
  },

  icon: {
    width: 25,
    height: 25,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  secure: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 20,
    backgroundColor: 'rgba(41, 216, 131, 0.205)',
    marginTop: 50,
    borderRadius: 10,
    marginBottom: dimensions.Height(5),
  },
});
