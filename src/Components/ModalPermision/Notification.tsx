import React from 'react';
import {Modal, View, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import LottieView from 'lottie-react-native';
import {colors, stylesText, dimensions} from '../../Themes';
import {Button} from '../Buttom';
import {CustomText} from '../CustomText';
import OneSignal from 'react-native-onesignal';
import {
  checkNotifications,
  requestNotifications,
} from 'react-native-permissions';
import AsyncStorage from '@react-native-community/async-storage';

export default function Notification(props) {
  const {visibleModal, setModalVisible} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const RequestPer = async () => {
    const deviceState = await OneSignal.getDeviceState();
    requestNotifications(['alert', 'sound', 'badge', 'criticalAlert']).then(
      ({status}) => {
        if (status === 'denied') {
          setModalVisible(false);
        } else if (status === 'blocked') {
          setModalVisible(false);
        } else {
          if (deviceState.isSubscribed == false) {
            OneSignal.addTrigger('prompt_ios', 'true');
          }
          setModalVisible(false);
        }
      },
    );
  };
  const getStatus = async () => {
    const deviceState = await OneSignal.getDeviceState();
    checkNotifications()
      .then(({status}) => {
        if (status === 'denied') {
          RequestPer();
        } else if (status === 'blocked') {
          RequestPer();
        } else if (status === 'unavailable') {
          RequestPer();
        } else {
          if (deviceState.isSubscribed == false) {
            OneSignal.addTrigger('prompt_ios', 'true');
          }
          setModalVisible(false);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  const noActivate = async () => {
    await AsyncStorage.setItem('notification', 'no_activate');
    setModalVisible(false);
  };

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      collapsable={true}
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.container}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: dimensions.ScreenHeight,
          }}>
          <LottieView
            source={require('../../Assets/animated/Notification.json')}
            autoPlay
            loop
            style={{width: 150}}
          />
          <View style={{marginHorizontal: 30, marginTop: 30}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.titleText, {textAlign: 'center'}]}>
              Activa las notificaciones
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {textAlign: 'center', marginTop: 20},
              ]}>
              Mantente enterado del estado de tus reservas, activa las
              notificaciones y síguela en tiempo real.
            </CustomText>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={[
                  styles.buttonView,
                  {backgroundColor: colors.main},
                ]}
                onPress={() => getStatus()}
                title="Continuar"
              />
            </View>
            <View style={{paddingTop: 20}}>
              <TouchableOpacity onPress={() => noActivate()}>
                <CustomText
                  light={colors.main}
                  dark={colors.main}
                  style={[
                    stylesText.secondaryTextBold,
                    {textAlign: 'center', color: colors.main},
                  ]}>
                  Cancelar
                </CustomText>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});
