import React from 'react';
import {Modal, View, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import LottieView from 'lottie-react-native';
import {colors, stylesText, dimensions} from '../../Themes';
import {Button} from '../Buttom';
import {CustomText} from '../CustomText';
import source from '../../Assets/animated/Tracking.json';
import {requestTrackingPermission} from 'react-native-tracking-transparency';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Notification(props) {
  const {visibleModal, setModalVisible} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const requetPermision = async () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    const trackingStatus = await requestTrackingPermission();
    setModalVisible(false);
  };

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      collapsable={true}
      statusBarTranslucent={true}>
      <View style={styles.container}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: dimensions.ScreenHeight,
          }}>
          <LottieView
            source={source}
            autoPlay
            loop
            style={{
              width: 250,
            }}
          />
          <View style={{marginHorizontal: 30}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.titleText, {textAlign: 'center'}]}>
              Contenido y ofertas exclusivas
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {textAlign: 'center', marginTop: 20},
              ]}>
              ¡El seguimiento de apps nos ayuda a mejorar tu experiencia en
              Rentyt y a mostrar promociones y contenido personalizados! Puedes
              permitirlo en un momento.
            </CustomText>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={[
                  styles.buttonView,
                  {backgroundColor: colors.main},
                ]}
                onPress={() => requetPermision()}
                title="Siguiente"
              />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    width: dimensions.Width(90),
    borderRadius: 10,
    marginBottom: 30,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});
