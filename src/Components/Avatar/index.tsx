import React, {useState} from 'react';
import {
  ImageBackground,
  Image,
  TouchableOpacity,
  Platform,
  View,
} from 'react-native';
import {image, colors} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import ImageView from 'react-native-image-viewing';

export const Avatar = ({uri, height, width}) => {
  const [visible, setIsVisible] = useState(false);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const logoUri = new DynamicValue(
    image.IconPlaceHoldedLigth,
    image.IconPlaceHoldedDark,
  );

  const source = useDynamicValue(logoUri);

  const images = [{uri: uri}];

  const alto = height ? height : 60;
  const ancho = width ? width : 60;

  return (
    <>
      <ImageBackground
        source={source}
        imageStyle={{borderRadius: 100}}
        resizeMode="cover"
        style={{width: ancho, height: alto}}>
        <TouchableOpacity
          onPress={() => setIsVisible(true)}
          activeOpacity={100}>
          <View style={[styles.avatarBorder]}>
            <Image
              source={{uri: uri}}
              style={[
                styles.avatar,
                {
                  borderWidth: width > 50 ? 4 : 2,
                  resizeMode: 'cover',
                  width: ancho,
                  height: alto,
                },
              ]}
            />
          </View>
        </TouchableOpacity>
      </ImageBackground>
      <ImageView
        images={images}
        imageIndex={0}
        visible={visible}
        presentationStyle="overFullScreen"
        onRequestClose={() => setIsVisible(false)}
      />
    </>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  avatar: {
    borderRadius: 100,
    borderColor: new DynamicValue(colors.white, colors.back_dark),
  },

  avatarBorder: {
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
  },
});
