import React from 'react';
import {View, Modal} from 'react-native';
import PropTypes from 'prop-types';
import LottieView from 'lottie-react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import {colors} from '../Themes';

const Loader = ({loading = false, setVisible}) => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <Modal
      transparent
      animationType={'none'}
      visible={loading}
      collapsable={true}
      statusBarTranslucent={true}
      onRequestClose={() => setVisible(false)}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <LottieView
            source={require('../Assets/animated/LoadingModal.json')}
            autoPlay
            loop
            style={{width: 500}}
          />
        </View>
      </View>
    </Modal>
  );
};

Loader.propTypes = {
  loading: PropTypes.bool.isRequired,
  color: PropTypes.string,
  size: PropTypes.string,
  opacity: (props, propName, componentName: any) => {
    if (props[propName] < 0 || props[propName] > 1) {
      return new Error('Opacity prop value out of range');
    }
  },
};

const dynamicStyles = new DynamicStyleSheet({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  activityIndicatorWrapper: {
    width: '100%',
    borderRadius: 10,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Loader;
