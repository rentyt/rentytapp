import React from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {colors, dimensions, stylesText} from '../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../CustomText';
import {BlurView, VibrancyView} from '@react-native-community/blur';
import {useTranslation} from 'react-i18next';
import {Badget} from '../Badget';

const ContainerView = Platform.select({
  ios: VibrancyView,
  //@ts-ignore
  android: BlurView,
});

export interface Tabs {
  ruteName: string;
  onPressHome: () => void;
  onPressFavourites: () => void;
  onPressOrder: () => void;
  onPressAccount: () => void;
  onPressAdd: () => void;
  favouriteContu: () => number;
}

export default function ButtomBar(props: Tabs) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const modo = useColorSchemeContext();
  const {t} = useTranslation();

  const {
    ruteName,
    onPressHome,
    onPressOrder,
    onPressFavourites,
    onPressAccount,
    onPressAdd,
    favouriteContu,
  } = props;

  const inact = {
    light: colors.black,
    dark: colors.white,
  };

  const Act = {
    light: colors.main,
    dark: colors.main,
  };

  const ActAdd = {
    light: colors.green,
    dark: colors.green,
  };

  const BorderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const inactiveColor = inact[modo];
  const activeColor = Act[modo];
  const activeColorAdd = ActAdd[modo];
  const border = BorderColor[modo];

  let Home = ruteName == 'Home' ? activeColor : inactiveColor;
  let Order = ruteName == 'Chat' ? activeColor : inactiveColor;
  let Favourite = ruteName == 'Favourites' ? activeColor : inactiveColor;
  let Account = ruteName == 'Account' ? activeColor : inactiveColor;
  let Add = ruteName == 'Add' ? activeColorAdd : inactiveColor;

  const blur = {
    light: 'xlight',
    dark: 'extraDark',
  };
  const mode = useColorSchemeContext();
  const blurtype = blur[mode];

  return (
    <View style={[styles.container, {borderTopColor: border}]}>
      <View style={styles.tabConten}>
        <TouchableOpacity style={styles.items} onPress={onPressHome}>
          <Icon type="Feather" name="home" size={26} color={Home} />

          <CustomText
            style={[
              stylesText.placeholderText,
              {marginTop: 5, fontWeight: 'bold'},
            ]}
            light={Home}
            dark={Home}>
            {t(`home:HomeTitle`)}
          </CustomText>
        </TouchableOpacity>

        <TouchableOpacity style={styles.items} onPress={onPressFavourites}>
          <Badget
            number={favouriteContu()}
            left={33}
            bottom={Platform.select({ios: 34, android: 35})}
          />
          <Icon
            type="FontAwesome"
            name="bookmark-o"
            size={26}
            color={Favourite}
          />
          <CustomText
            style={[
              stylesText.placeholderText,
              {marginTop: 5, fontWeight: 'bold'},
            ]}
            light={Favourite}
            dark={Favourite}>
            {t(`home:Favourites`)}
          </CustomText>
        </TouchableOpacity>

        <TouchableOpacity style={styles.items} onPress={onPressAdd}>
          <Icon type="Feather" name="plus-circle" size={28} color={Add} />
          <CustomText
            style={[
              stylesText.placeholderText,
              {marginTop: 5, fontWeight: 'bold'},
            ]}
            light={Add}
            dark={Add}>
            {t(`home:addAd`)}
          </CustomText>
        </TouchableOpacity>

        <TouchableOpacity style={styles.items} onPress={onPressOrder}>
          <Icon type="Feather" name="message-square" size={26} color={Order} />
          <CustomText
            style={[
              stylesText.placeholderText,
              {marginTop: 5, fontWeight: 'bold'},
            ]}
            light={Order}
            dark={Order}>
            {t(`home:Chat`)}
          </CustomText>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.items]} onPress={onPressAccount}>
          <Icon type="Feather" name="user" size={26} color={Account} />
          <CustomText
            style={[
              stylesText.placeholderText,
              {marginTop: 5, fontWeight: 'bold'},
            ]}
            light={Account}
            dark={Account}>
            {t(`home:Account`)}
          </CustomText>
        </TouchableOpacity>
      </View>
      <ContainerView
        style={styles.absoluto}
        //@ts-ignore
        blurType={blurtype}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  absoluto: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

  container: {
    height: dimensions.IsIphoneX()
      ? dimensions.Height(10)
      : dimensions.Height(12),
    width: dimensions.Width(100),
    justifyContent: 'center',
    bottom: 0,
    position: 'absolute',
    overflow: 'hidden',
    zIndex: 100,
    borderTopWidth: 1,
  },

  tabConten: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginHorizontal: 20,
    zIndex: 100,
    marginBottom: dimensions.IsIphoneX() ? 20 : 5,
  },

  items: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});
