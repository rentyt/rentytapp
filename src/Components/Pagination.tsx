import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {colors, stylesText} from '../Themes';
import {CustomText} from './CustomText';

export default function Pagination(props: any) {
  const {page, onPressMas, onPressMenos, width, widthBTN, sizeIcon, fontSize} =
    props;
  const styles = useDynamicValue(dynamicStyles);

  const mode = useColorSchemeContext();

  const bg = {
    light: 'rgba(41, 216, 131, 0.205)',
    dark: 'rgba(41, 216, 131, 0.205)',
  };

  const bgColor = bg[mode];

  const main = {
    light: colors.green,
    dark: colors.green,
  };

  const mainColor = main[mode];

  return (
    <View style={[styles.cont, {width: width ? width : 200}]}>
      <TouchableOpacity
        onPress={onPressMenos}
        style={[
          styles.btn,
          {
            width: widthBTN ? widthBTN : 40,
            height: widthBTN ? widthBTN : 40,
            backgroundColor: bgColor,
          },
        ]}>
        <Icon
          name="minus"
          type="AntDesign"
          size={sizeIcon ? sizeIcon : 24}
          color={mainColor}
        />
      </TouchableOpacity>
      <CustomText
        light={colors.black}
        dark={colors.white}
        style={[
          stylesText.secondaryTextBold,
          {
            textAlign: 'center',
            fontSize: fontSize ? fontSize : 20,
          },
        ]}>
        {page}
      </CustomText>
      <TouchableOpacity
        onPress={onPressMas}
        style={[
          styles.btn,
          {
            width: widthBTN ? widthBTN : 40,
            height: widthBTN ? widthBTN : 40,
            backgroundColor: bgColor,
          },
        ]}>
        <Icon
          name="plus"
          type="AntDesign"
          size={sizeIcon ? sizeIcon : 24}
          color={mainColor}
        />
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  btn: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
