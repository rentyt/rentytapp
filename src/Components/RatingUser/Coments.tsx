import React, {useState, useContext} from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import {DynamicStyleSheet, useDynamicValue} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../Themes';
import {CustomText} from '../CustomText';
import moment from 'moment';
import Stars from '../star';
import Icon from 'react-native-dynamic-vector-icons';
import {Avatar} from '../Avatar';
import {MAIN_URL} from '../../Utils/Urls';
import {MainContext} from '../../store/MainProvider';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useTranslation} from 'react-i18next';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export const ComentsUser = (props: any) => {
  const {data} = props;
  const {t} = useTranslation();
  const [line, setline] = useState(3);
  const [lineReply, setlineReply] = useState(3);
  const [translated, settranslated] = useState(false);
  const [translatedReply, settranslatedReply] = useState(false);
  const [coment, setComent] = useState(data.comment);
  const [comentReply, setComentReply] = useState(
    data.reply ? data.reply.coment : '',
  );
  const styles = useDynamicValue(dynamicStyles);

  const {languaje} = useContext(MainContext);

  let consideracion = '';
  switch (data.rating.userRating) {
    case 0:
      consideracion = t('rating:status:Sinopinion');
      break;
    case 1:
      consideracion = t('rating:status:Mala');
      break;
    case 2:
      consideracion = t('rating:status:Regular');
      break;
    case 3:
      consideracion = t('rating:status:Buena');
      break;
    case 4:
      consideracion = t('rating:status:Muybuena');
      break;
    case 5:
      consideracion = t('rating:status:Excelente');
      break;
  }

  const trsnlateText = async (
    text: string,
    translateds: boolean,
    reply: boolean,
  ) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);

    if (reply) {
      settranslatedReply(translateds);
    } else {
      settranslated(translateds);
    }
    const datas = {
      text: text,
      target: languaje,
    };

    const resp = await fetch(`${MAIN_URL}/translate-text`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(datas),
    });

    const result = await resp.json();

    if (reply) {
      if (translatedReply) {
        setComentReply(data.reply.coment);
      } else {
        setComentReply(result.text);
      }
      setlineReply(25);
    } else {
      if (translated) {
        setComent(data.comment);
      } else {
        setComent(result.text);
      }
      setline(25);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.user}>
        <View>
          <Avatar uri={data.Car.images[0].uri} width={50} height={50} />
          <View style={{marginTop: -20}}>
            <Avatar uri={data.Customer.avatar} width={50} height={50} />
          </View>
        </View>
        <View style={{marginLeft: 10, width: dimensions.Width(70)}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Stars star={data.rating.userRating} />
            <CustomText
              numberOfLines={line}
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, {marginLeft: 10}]}>
              {data.rating.userRating} {t('rating:of5')} · {consideracion}
            </CustomText>
            <Icon
              name="verified"
              type="Octicons"
              size={12}
              color={colors.green}
              style={{marginLeft: 10}}
            />
          </View>
          <View>
            <CustomText
              numberOfLines={line}
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {paddingTop: 10, lineHeight: 18},
              ]}>
              {coment ? coment : t('rating:NoComent')}
            </CustomText>
          </View>

          {coment ? (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                style={{marginVertical: 10}}
                onPress={() => {
                  if (line > 3) {
                    setline(3);
                  } else {
                    setline(20);
                  }
                }}>
                <CustomText light={colors.green} dark={colors.green}>
                  {line > 3 ? t('rating:viewLess') : t('rating:viewMore')}
                </CustomText>
              </TouchableOpacity>
              <TouchableOpacity
                style={{marginVertical: 10, marginLeft: 20}}
                onPress={() => trsnlateText(data.comment, !translated, false)}>
                <CustomText light={colors.green} dark={colors.green}>
                  <Icon
                    type="MaterialCommunityIcons"
                    name="google-translate"
                    size={12}
                    color={colors.green}
                  />{' '}
                  {translated ? t('rating:NoTranslate') : t('rating:Translate')}
                </CustomText>
              </TouchableOpacity>
            </View>
          ) : null}

          {data.reply ? (
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                }}>
                <Avatar uri={data.Owner.avatar} width={25} height={25} />
                <View style={{marginLeft: 10}}>
                  <CustomText
                    light={colors.green}
                    dark={colors.green}
                    style={[stylesText.placeholderText, {fontWeight: '400'}]}>
                    {t('rating:response')}{' '}
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[stylesText.placeholderText, {fontWeight: '400'}]}>
                      {`  ${moment(data.reply.date).fromNow()}`}
                    </CustomText>
                  </CustomText>
                  <CustomText
                    numberOfLines={1}
                    light={colors.black}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryText,
                      {
                        lineHeight: 18,
                        marginTop: 5,
                        width: 'auto',
                        maxWidth: dimensions.Width(35),
                      },
                    ]}>
                    {data.Owner.name} {data.Owner.lastName}
                  </CustomText>
                </View>
              </View>
              <CustomText
                numberOfLines={lineReply}
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {paddingTop: 5, lineHeight: 18, marginLeft: 35},
                ]}>
                {comentReply}
              </CustomText>
              <View
                style={{
                  marginLeft: 35,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 10,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    if (lineReply > 3) {
                      setlineReply(3);
                    } else {
                      setlineReply(20);
                    }
                  }}>
                  <CustomText light={colors.green} dark={colors.green}>
                    {lineReply > 3
                      ? t('rating:viewLess')
                      : t('rating:viewMore')}
                  </CustomText>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{marginVertical: 8, marginLeft: 15}}
                  onPress={() =>
                    trsnlateText(data.reply.coment, !translatedReply, true)
                  }>
                  <CustomText light={colors.green} dark={colors.green}>
                    <Icon
                      type="MaterialCommunityIcons"
                      name="google-translate"
                      size={12}
                      color={colors.green}
                    />{' '}
                    {translatedReply
                      ? t('rating:NoTranslate')
                      : t('rating:Translate')}
                  </CustomText>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}

          <View style={{flexDirection: 'row'}}>
            <View>
              <CustomText
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryText,
                  {
                    paddingTop: 10,
                    lineHeight: 18,
                    width: 'auto',
                    maxWidth: dimensions.Width(35),
                  },
                ]}>
                {data.Customer.name} {data.Customer.lastName}
              </CustomText>
            </View>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                {paddingTop: 12, lineHeight: 18, marginHorizontal: 15},
              ]}>
              ·
            </CustomText>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryText,
                {
                  paddingTop: 10,
                  lineHeight: 18,
                  width: 'auto',
                  maxWidth: dimensions.Width(35),
                },
              ]}>
              {data.Car.car.marker} {data.Car.car.model}
            </CustomText>
          </View>

          <CustomText
            numberOfLines={1}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[stylesText.secondaryText, {paddingTop: 5}]}>
            {moment(data.created_at).fromNow()}
          </CustomText>
        </View>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    marginHorizontal: 15,
    marginVertical: 20,
    width: dimensions.Width(94),
    height: 'auto',
  },

  user: {
    flexDirection: 'row',
  },
});
