import React from 'react';
import {DynamicStyleSheet, useDynamicValue} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../Themes';
import {CustomText} from '../CustomText';
import {View} from 'react-native';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';

export default function Safe() {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  return (
    <View style={styles.container}>
      <Icon name="Safety" type="AntDesign" size={34} color={colors.green} />
      <View style={{width: dimensions.Width(70), marginLeft: 10}}>
        <CustomText
          style={stylesText.secondaryTextBold}
          light={colors.green}
          dark={colors.green}>
          {t('rating:safety:title')}
        </CustomText>
        <CustomText
          style={[stylesText.secondaryText, {lineHeight: 18}]}
          light={colors.green}
          dark={colors.green}>
          {t('rating:safety:description')}
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    marginHorizontal: 15,
    marginVertical: 20,
    width: dimensions.Width(94),
    borderRadius: 15,
    padding: 20,
    height: 'auto',
    backgroundColor: colors.greenTranp,
    flexDirection: 'row',
  },

  user: {
    flexDirection: 'row',
  },
});
