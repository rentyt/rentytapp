import React, {useState} from 'react';
import {View, TouchableOpacity, FlatList, Button, Image} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Star from '../star';
import {PercentageBar} from './PercentageBar';
import {colors, stylesText, image, dimensions} from '../../Themes';
import {CustomText} from '../CustomText';
import {ComentsUser} from './Coments';
import {RatingCalculatorUser} from '../../Utils/RatingCalculartorUser';
import {DesgloseRating} from '../../Utils/DesgloseRatingUSer';
import LottieView from 'lottie-react-native';
import Modal from 'react-native-modal';
import Safe from './safe';
import 'moment/locale/es';
import {useQuery} from '@apollo/client';
import {queryRating} from '../../GraphQL';
import {useTranslation} from 'react-i18next';
import LoadingAnimated from '../PlaceHolded/Details';

export default function RetingView({id}) {
  const [isModalVisible, setModalVisible] = useState(false);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(50);
  const {t} = useTranslation();

  const {data, loading, refetch} = useQuery(queryRating.GET_RATING, {
    variables: {owner: id},
  });

  const ratings = data && data.getRating ? data.getRating.data : [];

  setTimeout(() => {
    refetch();
  }, 1500);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const averageRating = RatingCalculatorUser(ratings);
  const styles = useDynamicValue(dynamicStyles);

  const value = DesgloseRating(ratings);
  const bar = value[1].length / ratings.length;
  const bar1 = value[2].length / ratings.length;
  const bar2 = value[3].length / ratings.length;
  const bar3 = value[4].length / ratings.length;
  const bar4 = value[5].length / ratings.length;

  const porcentaje4 = bar4 ? bar4 * 100 : 0;
  const porcentaje3 = bar3 ? bar3 * 100 : 0;
  const porcentaje2 = bar2 ? bar2 * 100 : 0;
  const porcentaje1 = bar1 ? bar1 * 100 : 0;
  const porcentaje = bar ? bar * 100 : 0;

  const _renderItem = ({item}) => {
    return <ComentsUser data={item} />;
  };

  const renderHeader = () => {
    return (
      <View>
        <View style={styles.reviewContainer}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={styles.title}>
            {t('rating:customer')}
          </CustomText>
          <View style={styles.totalWrap}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Star star={averageRating} />
            </View>
            <CustomText light={colors.rgb_153} dark={colors.rgb_153}>
              {averageRating} {t('rating:of5')}
            </CustomText>
          </View>

          <CustomText
            light={colors.black}
            dark={colors.white}
            style={styles.amountText}>
            {ratings.length > 300 ? '(+ 501)' : `(${ratings.length})`}{' '}
            {t('rating:customer')}
          </CustomText>

          <View style={{marginTop: 40}}>
            <View style={styles.spacer}>
              <PercentageBar starText="5" percentage={porcentaje4.toFixed(0)} />
            </View>
            <View style={styles.spacer}>
              <PercentageBar starText="4" percentage={porcentaje3.toFixed(0)} />
            </View>
            <View style={styles.spacer}>
              <PercentageBar starText="3" percentage={porcentaje2.toFixed(0)} />
            </View>
            <View style={styles.spacer}>
              <PercentageBar starText="2" percentage={porcentaje1.toFixed(0)} />
            </View>
            <View style={styles.spacer}>
              <PercentageBar starText="1" percentage={porcentaje.toFixed(0)} />
            </View>
          </View>

          <TouchableOpacity
            style={{marginTop: 20}}
            onPress={() => toggleModal()}>
            <CustomText
              light={colors.green}
              dark={colors.green}
              style={styles.howWeCalculate}>
              {t('rating:calcutale')}
            </CustomText>
          </TouchableOpacity>
        </View>

        <CustomText
          light={colors.black}
          dark={colors.white}
          style={styles.title}>
          {t('rating:Comentarios')}
        </CustomText>

        <Safe />
      </View>
    );
  };

  return (
    <>
      {loading ? (
        <View>
          <LoadingAnimated rating={true} />
        </View>
      ) : (
        <View style={styles.container}>
          <FlatList
            data={ratings}
            renderItem={item => _renderItem(item)}
            keyExtractor={item => item.id}
            ListHeaderComponent={renderHeader}
            ListFooterComponent={<View style={{marginBottom: 100}} />}
            showsVerticalScrollIndicator={false}
            horizontal={false}
            ListEmptyComponent={
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingHorizontal: 30,
                  marginTop: 30,
                }}>
                <LottieView
                  source={require('../../Assets/animated/star.json')}
                  autoPlay
                  loop
                  style={{width: 250, height: 200}}
                />
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[
                    stylesText.secondaryText,
                    {
                      textAlign: 'center',
                    },
                  ]}>
                  {t('Myrating:NoRating')}
                </CustomText>
              </View>
            }
          />

          <Modal isVisible={isModalVisible}>
            <View style={styles.modalContent}>
              <LottieView
                source={require('../../Assets/animated/star.json')}
                autoPlay
                loop
                style={{width: 250, height: 200}}
              />
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryText,
                  {
                    textAlign: 'center',
                    lineHeight: 20,
                    fontWeight: '400',
                  },
                ]}>
                {t('rating:MesageHow')}
              </CustomText>

              <View style={{marginTop: 40}}>
                <TouchableOpacity
                  onPress={toggleModal}
                  style={{
                    backgroundColor: colors.green,
                    padding: 10,
                    borderRadius: 100,
                    width: 150,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <CustomText
                    light={colors.white}
                    dark={colors.white}
                    style={[stylesText.TitleCard]}>
                    {t('rating:howBtn')}
                  </CustomText>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>
      )}
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modalContent: {
    width: dimensions.Width(90),
    height: dimensions.Height(60),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30,
  },

  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  reviewContainer: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 15,
    paddingHorizontal: 30,
    paddingVertical: 40,
    marginBottom: 30,
    marginTop: 10,
    marginHorizontal: 18,
    minWidth: '92%',
    shadowOffset: {width: 0, height: 5},
    shadowOpacity: 1.0,
    shadowRadius: 7,
    shadowColor: new DynamicValue('rgba(218, 218, 218, 0.5)', colors.black),
    elevation: 10,
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },

  spacer: {
    marginTop: 15,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  totalWrap: {
    marginTop: 20,
    marginBottom: 5,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  amountText: {
    fontSize: 16,
    textAlign: 'center',
    marginTop: 10,
  },

  howWeCalculate: {
    fontSize: 15,
    textAlign: 'center',
  },
});
