import React from 'react';
import {
  View,
  Image,
  TouchableWithoutFeedback,
  ImageBackground,
  Platform,
} from 'react-native';
import {CustomText} from '../CustomText';
import {dimensions, colors, stylesText, image} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {IAds} from '../../interfaces/index';
import {formaterPrice} from '../../Utils/formaterPrice';
import {useTranslation} from 'react-i18next';

export interface IData {
  data: IAds;
  navigation: any;
  refetch: any;
  favourite?: boolean;
  small: boolean;
}

export default function CardAdds(props: IData) {
  const {data, navigation, favourite, refetch, small} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  return (
    <TouchableWithoutFeedback
      onLongPress={() => {}}
      onPress={() => navigation.navigate('Details', {id: data._id})}>
      <View style={[styles.card]}>
        <ImageBackground
          style={[styles.mainImage]}
          imageStyle={{borderRadius: 15}}
          resizeMode="cover"
          source={image.PlaceHoldedCard}>
          <Image
            source={{uri: data.images[0].uri}}
            style={[styles.mainImage]}
          />
          <View style={[styles.header]}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  styles.iconContent_minimo,
                  {
                    width: 25,
                    marginHorizontal: 0,
                    marginRight: 8,
                  },
                ]}>
                <Icon
                  type="FontAwesome"
                  name={data.inFavourites ? 'bookmark' : 'bookmark-o'}
                  color={data.inFavourites ? colors.green : colors.black}
                  size={16}
                />
              </View>
              {data.minDays > 1 ? (
                <View style={styles.iconContent_minimo}>
                  <View>
                    <Icon
                      type="Feather"
                      name="calendar"
                      size={14}
                      color={colors.twitter_color}
                    />
                  </View>
                  <CustomText
                    numberOfLines={1}
                    style={[
                      stylesText.terciaryText,
                      {marginLeft: 5, fontWeight: '400'},
                    ]}
                    light={colors.black}
                    dark={colors.black}>
                    {data.minDays} {t('card:dayMin')}
                  </CustomText>
                </View>
              ) : (
                <View />
              )}
            </View>
            <View style={{flexDirection: 'row'}}>
              {data.airPortPickup && data.airPortPickup.available ? (
                <View style={[styles.type]}>
                  <Icon
                    type="MaterialCommunityIcons"
                    name="airplane"
                    size={18}
                    color={colors.twitter_color}
                  />
                </View>
              ) : null}
              {data.instantBooking ? (
                <View style={[styles.type]}>
                  <Icon
                    type="Entypo"
                    name="flash"
                    size={16}
                    color={colors.ERROR}
                  />
                </View>
              ) : null}
            </View>
          </View>
        </ImageBackground>
        <View style={[styles.contentInfo]}>
          <View style={{width: dimensions.Width(35)}}>
            <CustomText
              numberOfLines={1}
              style={[stylesText.secondaryText, {textAlign: 'left'}]}
              light={colors.back_dark}
              dark={colors.white}>
              {data.car.marker} {data.car.model} {data.car.year}
            </CustomText>
            <CustomText
              numberOfLines={1}
              style={[stylesText.TitleCard, {textAlign: 'left', marginTop: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {formaterPrice(
                data.prices.value,
                data.prices.localcode,
                data.prices.currency,
              )}
              /{t('card:day')}
            </CustomText>
          </View>
          <View style={styles.rating}>
            <Icon
              type="AntDesign"
              name="star"
              size={14}
              color={colors.orange1}
            />
            <CustomText
              numberOfLines={1}
              style={[
                stylesText.terciaryText,
                {marginLeft: 5, fontWeight: '600'},
              ]}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {data.rating}
            </CustomText>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  contentInfo: {
    padding: 10,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
  },

  header: {
    position: 'absolute',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 10,
    zIndex: 110,
    top: 0,
    width: dimensions.Width(59.8),
  },

  mainImage: {
    height: dimensions.Height(15),
    width: dimensions.Width(59.8),
    borderRadius: 15,
  },

  card: {
    borderRadius: 15,
    marginHorizontal: 10,
    width: dimensions.Width(60),
    marginVertical: 20,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },

  type: {
    backgroundColor: 'rgba(0,0,0,.5)',
    borderRadius: 100,
    marginHorizontal: 3,
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },

  rating: {
    backgroundColor: '#ffa6002d',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    width: 60,
    borderRadius: 5,
    paddingVertical: 3,
    marginBottom: 10,
  },
  iconContent_minimo: {
    width: 'auto',
    height: 25,
    paddingHorizontal: 7,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.white),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 100,
  },
});
