import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  Platform,
  Animated,
} from 'react-native';
import {CustomText} from '../CustomText';
import {dimensions, colors, stylesText, image} from '../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {IAds} from '../../interfaces/index';
import {formaterPrice} from '../../Utils/formaterPrice';
import {useTranslation} from 'react-i18next';

export interface IData {
  data: IAds;
  navigation: any;
  refetch: any;
  index: number;
  scrollY: any;
  feed: boolean;
  style?: any;
}

const ITEM_SIZE = dimensions.Width(38);

export default function CardAdds(props: IData) {
  const {data, navigation, refetch, index, scrollY, feed, style} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const opacityInputRange = [
    -1,
    0,
    ITEM_SIZE * index,
    ITEM_SIZE * (index + 0.3),
  ];

  const opacity = scrollY.interpolate({
    inputRange: opacityInputRange,
    outputRange: [1, 1, 1, 0],
  });

  return (
    <TouchableOpacity
      activeOpacity={100}
      onPress={() => navigation.navigate('Details', {id: data._id})}>
      <Animated.View
        style={[style, styles.card, {opacity: feed ? opacity : 100}]}>
        <ImageBackground
          style={[styles.mainImages]}
          imageStyle={{borderRadius: 15}}
          resizeMode="cover"
          source={image.PlaceHoldedCard}>
          <Image
            source={{uri: data.images[0].uri}}
            style={styles.mainImage}
            resizeMode="cover"
          />
          {data.popular ? (
            <View style={styles.destacado}>
              <Icon
                type="Feather"
                name="activity"
                size={18}
                color={colors.white}
                style={styles.mainIcon}
              />
            </View>
          ) : null}
        </ImageBackground>
        <View style={[styles.contentInfo]}>
          <View>
            <CustomText
              numberOfLines={1}
              style={[stylesText.TitleCard, {textAlign: 'left', marginTop: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {data.car.marker} {data.car.model} {data.car.year}
            </CustomText>
            <CustomText
              numberOfLines={1}
              style={[stylesText.TitleCard, {textAlign: 'left', marginTop: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {formaterPrice(
                data.prices.value,
                data.prices.localcode,
                data.prices.currency,
              )}
              /{t('card:day')}
            </CustomText>
            <View>
              {data.airPortPickup && data.airPortPickup.available ? (
                <View style={styles.iconContent}>
                  <View style={styles.icon}>
                    <Icon
                      type="MaterialCommunityIcons"
                      name="airplane"
                      size={14}
                      color={colors.twitter_color}
                    />
                  </View>
                  <CustomText
                    numberOfLines={1}
                    style={[
                      stylesText.secondaryText,
                      {marginLeft: 5, width: dimensions.Width(26)},
                    ]}
                    light={colors.rgb_153}
                    dark={colors.rgb_153}>
                    {t('card:airPort')}
                  </CustomText>
                </View>
              ) : (
                <View style={styles.iconContent}>
                  <View style={styles.icon}>
                    <Icon
                      type="Feather"
                      name="map-pin"
                      size={14}
                      color={colors.orange}
                    />
                  </View>
                  <CustomText
                    numberOfLines={1}
                    style={[
                      stylesText.secondaryText,
                      {marginLeft: 5, width: dimensions.Width(26)},
                    ]}
                    light={colors.rgb_153}
                    dark={colors.rgb_153}>
                    {data.city}
                  </CustomText>
                </View>
              )}

              {data.instantBooking ? (
                <View style={styles.iconContent}>
                  <View style={styles.icon}>
                    <Icon
                      type="Entypo"
                      name="flash"
                      size={16}
                      color={colors.ERROR}
                    />
                  </View>
                  <CustomText
                    numberOfLines={1}
                    style={[
                      stylesText.secondaryText,
                      {marginLeft: 5, width: dimensions.Width(26)},
                    ]}
                    light={colors.rgb_153}
                    dark={colors.rgb_153}>
                    {t('card:Instant')}
                  </CustomText>
                </View>
              ) : (
                <>
                  {data.minDays > 1 ? (
                    <View style={styles.iconContent}>
                      <View style={styles.icon}>
                        <Icon
                          type="Feather"
                          name="calendar"
                          size={14}
                          color={colors.main}
                        />
                      </View>
                      <CustomText
                        numberOfLines={1}
                        style={[stylesText.secondaryText, {marginLeft: 5}]}
                        light={colors.rgb_153}
                        dark={colors.rgb_153}>
                        {data.minDays} {t('card:minday')}
                      </CustomText>
                    </View>
                  ) : (
                    <View style={styles.iconContent}>
                      <View style={styles.icon}>
                        <Icon
                          type="AntDesign"
                          name="Safety"
                          size={16}
                          color={colors.green}
                        />
                      </View>
                      <CustomText
                        numberOfLines={1}
                        style={[
                          stylesText.secondaryText,
                          {marginLeft: 5, width: dimensions.Width(26)},
                        ]}
                        light={colors.rgb_153}
                        dark={colors.rgb_153}>
                        {t('details:warrantyTitle')}
                      </CustomText>
                    </View>
                  )}
                </>
              )}
            </View>
          </View>
        </View>
      </Animated.View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  contentInfo: {
    justifyContent: 'center',
    alignItems: 'center',
    width: dimensions.Width(34),
  },

  mainImages: {
    width: dimensions.Width(34),
    height: 100,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  mainImage: {
    width: dimensions.Width(33.1),
    height: 96,
    borderRadius: 15,
  },

  card: {
    width: dimensions.Width(38),
    minHeight: dimensions.Height(20),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 15,
    padding: 10,
    marginHorizontal: 5,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 8}),
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },
  tag: {
    zIndex: 230,
    padding: 3,
    width: dimensions.Width(33),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginTop: 10,
  },

  type: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 100,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  iconContent: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: 7,
  },

  icon: {
    width: 25,
    height: 25,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  destacado: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    position: 'absolute',
    right: 6,
    top: 6,
    backgroundColor: colors.green,
  },

  mainIcon: {
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
  },
});
