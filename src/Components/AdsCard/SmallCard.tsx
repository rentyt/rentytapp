import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  Platform,
  Animated,
  Dimensions,
} from 'react-native';
import {CustomText} from '../CustomText';
import {dimensions, colors, stylesText, image} from '../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {IAds} from '../../interfaces/index';
import {formaterPrice} from '../../Utils/formaterPrice';
import {useTranslation} from 'react-i18next';

const screens = Dimensions.get('window').height;

export interface IData {
  data: IAds;
  refetch: any;
  navigation: any;
  scrollY: any;
  index: number;
  favourite?: boolean;
  style?: any;
}

const ITEM_SIZE = dimensions.Width(96);

export default function CardAdds(props: IData) {
  const {data, navigation, refetch, scrollY, index, favourite, style} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const inputRange = [-1, 0, ITEM_SIZE * index, ITEM_SIZE * (index + 2)];
  const scale = scrollY.interpolate({
    inputRange,
    outputRange: [1, 1, 1, 0],
  });

  const opacityInputRange = [
    -1,
    0,
    ITEM_SIZE * index,
    ITEM_SIZE * (index + 0.7),
  ];

  const opacity = scrollY.interpolate({
    inputRange: opacityInputRange,
    outputRange: [1, 1, 1, 0],
  });

  const contentWidth =
    screens > 768
      ? screens < 900
        ? dimensions.Width(57)
        : dimensions.Width(60)
      : dimensions.Width(54);

  return (
    <TouchableOpacity
      activeOpacity={100}
      onPress={() => navigation.navigate('Details', {id: data._id})}>
      <Animated.View
        style={[
          style,
          styles.card,
          {
            transform: [{scale}],
            opacity,
            width: ITEM_SIZE,
            marginTop: favourite && index === 0 ? 15 : 0,
          },
        ]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            width: dimensions.Width(60),
          }}>
          <ImageBackground
            style={[styles.mainImages]}
            imageStyle={{borderRadius: 15}}
            resizeMode="cover"
            source={image.PlaceHoldedCard}>
            <Image
              source={{uri: data.images[0].uri}}
              style={styles.mainImage}
              resizeMode="cover"
            />
            {data.minDays > 1 ? (
              <View style={styles.iconContent_minimo}>
                <View style={styles.icons}>
                  <Icon
                    type="Feather"
                    name="calendar"
                    size={12}
                    color={colors.twitter_color}
                  />
                </View>
                <CustomText
                  numberOfLines={1}
                  style={[
                    stylesText.placeholderText,
                    {marginLeft: 3, fontWeight: '400'},
                  ]}
                  light={colors.black}
                  dark={colors.black}>
                  {data.minDays} {t('card:dayMin')}
                </CustomText>
              </View>
            ) : null}
          </ImageBackground>
          <View style={[styles.contentInfo]}>
            <View
              style={{
                width: contentWidth,
              }}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View>
                  <CustomText
                    numberOfLines={1}
                    style={[
                      stylesText.TitleCard,
                      {width: dimensions.Width(40)},
                    ]}
                    light={colors.back_dark}
                    dark={colors.white}>
                    {data.car.marker} {data.car.model} {data.car.year}
                  </CustomText>
                  <CustomText
                    numberOfLines={1}
                    light={colors.back_dark}
                    dark={colors.white}
                    style={[stylesText.secondaryText, {fontWeight: 'bold'}]}>
                    {formaterPrice(
                      data.prices.value,
                      data.prices.localcode,
                      data.prices.currency,
                    )}
                    /{t('card:day')}
                  </CustomText>
                </View>
              </View>

              <View style={{marginTop: 0}}>
                {data.airPortPickup && data.airPortPickup.available ? (
                  <View style={styles.iconContent}>
                    <View style={styles.icon}>
                      <Icon
                        type="MaterialCommunityIcons"
                        name="airplane"
                        size={14}
                        color={colors.twitter_color}
                      />
                    </View>
                    <CustomText
                      numberOfLines={1}
                      style={[
                        stylesText.secondaryText,
                        {marginLeft: 5, width: dimensions.Width(38)},
                      ]}
                      light={colors.rgb_153}
                      dark={colors.rgb_153}>
                      {t('card:airPort')}
                    </CustomText>
                  </View>
                ) : (
                  <View style={styles.iconContent}>
                    <View style={styles.icon}>
                      <Icon
                        type="Feather"
                        name="map-pin"
                        size={14}
                        color={colors.orange}
                      />
                    </View>
                    <CustomText
                      numberOfLines={1}
                      style={[
                        stylesText.secondaryText,
                        {marginLeft: 5, width: dimensions.Width(26)},
                      ]}
                      light={colors.rgb_153}
                      dark={colors.rgb_153}>
                      {data.city}
                    </CustomText>
                  </View>
                )}

                {data.instantBooking ? (
                  <View style={styles.iconContent}>
                    <View style={styles.icon}>
                      <Icon
                        type="Entypo"
                        name="flash"
                        size={16}
                        color={colors.ERROR}
                      />
                    </View>
                    <CustomText
                      numberOfLines={1}
                      style={[
                        stylesText.secondaryText,
                        {marginLeft: 5, width: dimensions.Width(30)},
                      ]}
                      light={colors.rgb_153}
                      dark={colors.rgb_153}>
                      {t('card:Instant')}
                    </CustomText>
                  </View>
                ) : (
                  <View style={styles.iconContent}>
                    <View style={styles.icon}>
                      <Icon
                        type="AntDesign"
                        name="Safety"
                        size={16}
                        color={colors.green}
                      />
                    </View>
                    <CustomText
                      numberOfLines={1}
                      style={[stylesText.secondaryText, {marginLeft: 5}]}
                      light={colors.rgb_153}
                      dark={colors.rgb_153}>
                      {t('details:warrantyTitle')}
                    </CustomText>
                  </View>
                )}
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-between',
            alignItems: 'flex-end',
          }}>
          <View style={[styles.icon, {width: 35, height: 35}]}>
            <Icon
              type="FontAwesome"
              name={data.inFavourites ? 'bookmark' : 'bookmark-o'}
              color={data.inFavourites ? colors.green : colors.rgb_153}
              size={20}
            />
          </View>
          <View style={styles.rating}>
            <Icon
              type="AntDesign"
              name="star"
              size={14}
              color={colors.orange1}
            />
            <CustomText
              numberOfLines={1}
              style={[
                stylesText.terciaryText,
                {marginLeft: 5, fontWeight: '600'},
              ]}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {data.rating}
            </CustomText>
          </View>
        </View>
      </Animated.View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15,
    padding: 10,
    marginHorizontal: 5,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 15,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 6,
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },

  contentInfo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginLeft: 12,
  },

  mainImages: {
    width: 120,
    height: 120,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  mainImage: {
    width: 116,
    height: 116,
    borderRadius: 15,
  },

  tag: {
    zIndex: 230,
    padding: 3,
    width: 116,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    bottom: 0,
    backgroundColor: colors.black,
  },

  header: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    width: dimensions.Width(40),
    marginTop: 15,
  },

  iconContent: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: 7,
    width: dimensions.Width(30),
  },

  icon: {
    width: 25,
    height: 25,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  rating: {
    backgroundColor: '#ffa6002d',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    width: 60,
    borderRadius: 5,
    marginLeft: 'auto',
    paddingVertical: 3,
  },

  icons: {},

  iconContent_minimo: {
    width: 'auto',
    height: 25,
    paddingHorizontal: 7,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.white),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 100,
    position: 'absolute',
    left: 5,
    top: 5,
  },
});
