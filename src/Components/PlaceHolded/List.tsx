import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dynamic';
import {colors, dimensions} from '../../Themes';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#e4e4e4', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#e9e9e9', '#303030');
  const back = useDynamicValue(colors.white, colors.black);
  return (
    <View
      style={{
        backgroundColor: back,
        height: dimensions.ScreenHeight,
      }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View>
          <View
            style={{
              marginHorizontal: 10,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: dimensions.Width(76),
                height: 120,
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: dimensions.Width(20),
                height: 120,
                borderRadius: 5,
                marginLeft: 1,
              }}
            />
          </View>
          <View
            style={{
              width: dimensions.Width(70),
              height: 12,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
          <View
            style={{
              width: dimensions.Width(50),
              height: 7,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
        </View>

        <View style={{marginTop: 20}}>
          <View
            style={{
              marginHorizontal: 10,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: dimensions.Width(76),
                height: 120,
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: dimensions.Width(20),
                height: 120,
                borderRadius: 5,
                marginLeft: 1,
              }}
            />
          </View>
          <View
            style={{
              width: dimensions.Width(70),
              height: 12,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
          <View
            style={{
              width: dimensions.Width(50),
              height: 7,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
        </View>

        <View style={{marginTop: 20}}>
          <View
            style={{
              marginHorizontal: 10,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: dimensions.Width(76),
                height: 120,
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: dimensions.Width(20),
                height: 120,
                borderRadius: 5,
                marginLeft: 1,
              }}
            />
          </View>
          <View
            style={{
              width: dimensions.Width(70),
              height: 12,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
          <View
            style={{
              width: dimensions.Width(50),
              height: 7,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
        </View>

        <View style={{marginTop: 20}}>
          <View
            style={{
              marginHorizontal: 10,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: dimensions.Width(76),
                height: 120,
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: dimensions.Width(20),
                height: 120,
                borderRadius: 5,
                marginLeft: 1,
              }}
            />
          </View>
          <View
            style={{
              width: dimensions.Width(70),
              height: 12,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
          <View
            style={{
              width: dimensions.Width(50),
              height: 7,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
        </View>

        <View style={{marginTop: 20}}>
          <View
            style={{
              marginHorizontal: 10,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: dimensions.Width(76),
                height: 120,
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: dimensions.Width(20),
                height: 120,
                borderRadius: 5,
                marginLeft: 1,
              }}
            />
          </View>
          <View
            style={{
              width: dimensions.Width(70),
              height: 12,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
          <View
            style={{
              width: dimensions.Width(50),
              height: 7,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
        </View>

        <View style={{marginTop: 20}}>
          <View
            style={{
              marginHorizontal: 10,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: dimensions.Width(76),
                height: 120,
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: dimensions.Width(20),
                height: 120,
                borderRadius: 5,
                marginLeft: 1,
              }}
            />
          </View>
          <View
            style={{
              width: dimensions.Width(70),
              height: 12,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
          <View
            style={{
              width: dimensions.Width(50),
              height: 7,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
        </View>

        <View style={{marginTop: 20}}>
          <View
            style={{
              marginHorizontal: 10,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: dimensions.Width(76),
                height: 120,
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: dimensions.Width(20),
                height: 120,
                borderRadius: 5,
                marginLeft: 1,
              }}
            />
          </View>
          <View
            style={{
              width: dimensions.Width(70),
              height: 12,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
          <View
            style={{
              width: dimensions.Width(50),
              height: 7,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 5,
            }}
          />
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}
