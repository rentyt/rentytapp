import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dynamic';
import {colors, dimensions} from '../../Themes';
import {View} from 'react-native';

const UserLoading = () => {
  const placeholderColor = useDynamicValue('#e4e4e4', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#e9e9e9', '#303030');
  const back = useDynamicValue(colors.white, colors.black);

  return (
    <View
      style={{
        backgroundColor: back,
        height: dimensions.ScreenHeight,
        width: dimensions.ScreenWidth,
      }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(100),
            height: 200,
            alignSelf: 'center',
            alignItems: 'center',
            marginHorizontal: 10,
            zIndex: 10,
          }}
        />

        <SkeletonPlaceholder.Item
          width={100}
          height={100}
          borderRadius={100}
          borderWidth={5}
          borderColor="red"
          alignSelf="center"
          position="relative"
          marginTop={-50}
          zIndex={1000}
        />
        <SkeletonPlaceholder.Item
          width={120}
          height={20}
          alignSelf="center"
          marginTop={12}
          borderRadius={100}
        />
        <SkeletonPlaceholder.Item
          width={240}
          height={20}
          alignSelf="center"
          marginTop={12}
          borderRadius={100}
        />
        <SkeletonPlaceholder.Item
          width={240}
          height={20}
          alignSelf="center"
          marginTop={12}
          borderRadius={100}
        />

        <SkeletonPlaceholder.Item
          width={dimensions.Width(90)}
          height={20}
          alignSelf="center"
          marginTop={12}
          borderRadius={100}
        />
      </SkeletonPlaceholder>
    </View>
  );
};

export default UserLoading;
