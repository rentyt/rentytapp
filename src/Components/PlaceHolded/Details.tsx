import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dynamic';
import {colors, dimensions} from '../../Themes';

export default function PlaceHolder(props: any) {
  const {rating} = props;
  const placeholderColor = useDynamicValue('#e4e4e4', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#e9e9e9', '#303030');
  const back = useDynamicValue(colors.white, colors.black);
  return (
    <View
      style={{
        alignSelf: 'center',
        paddingTop: rating ? 0 : dimensions.Height(10),
        backgroundColor: back,
        height: dimensions.ScreenHeight,
      }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 20,
          }}>
          <View
            style={{
              width: dimensions.Width(96),
              height: 200,
              borderRadius: 15,
              marginTop: dimensions.Height(1),
            }}
          />
          <View
            style={{
              width: dimensions.Width(96),
              height: 50,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(96),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(90),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(84),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(60),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(75),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />
        </View>

        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 20,
          }}>
          <View
            style={{
              width: dimensions.Width(96),
              height: 50,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(96),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(90),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(84),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(60),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />

          <View
            style={{
              width: dimensions.Width(75),
              height: 20,
              borderRadius: 100,
              marginTop: dimensions.Height(2),
            }}
          />
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}
