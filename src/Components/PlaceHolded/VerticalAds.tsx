import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dynamic';
import {colors, dimensions} from '../../Themes';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#e4e4e4', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#e9e9e9', '#303030');
  const back = useDynamicValue(colors.white, colors.black);
  return (
    <View
      style={{
        backgroundColor: back,
        height: dimensions.ScreenHeight,
      }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View style={{flexDirection: 'row', marginTop: 20}}>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 20}}>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 20}}>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 20}}>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 20}}>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 20}}>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
          <View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 170,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(43),
                  height: 30,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(35),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(25),
                  height: 10,
                  borderRadius: 100,
                  marginTop: 5,
                }}
              />
            </View>
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}
