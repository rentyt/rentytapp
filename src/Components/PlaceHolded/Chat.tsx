import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dynamic';
import {colors, dimensions} from '../../Themes';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#e4e4e4', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#e9e9e9', '#303030');
  const back = useDynamicValue(colors.white, colors.black);
  return (
    <View
      style={{
        backgroundColor: back,
        height: 'auto',
        width: dimensions.ScreenWidth,
        marginBottom: 20,
      }}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(100),
            alignSelf: 'center',
            alignItems: 'center',
            marginHorizontal: 10,
          }}>
          <View
            style={{
              width: dimensions.Width(96),
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: 90,
                height: 90,
                borderRadius: 100,
              }}
            />
            <View style={{alignSelf: 'center', marginLeft: 15}}>
              <View
                style={{
                  width: dimensions.Width(65),
                  height: 30,
                  borderRadius: 100,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(60),
                  height: 15,
                  borderRadius: 100,
                  marginTop: 10,
                }}
              />
            </View>
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}
