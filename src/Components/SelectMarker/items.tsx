import React from 'react';
import {View, Platform, TouchableOpacity} from 'react-native';
import {dimensions, colors, stylesText} from '../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Items({
  marke,
  deletedItem,
  SelectItems,
  setAll,
  all,
  item,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const ActAdd = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  const adds = marke.filter((x: any) => x === item.brand).length === 1;

  return (
    <TouchableOpacity
      onPress={() => {
        //@ts-ignore
        ReactNativeHapticFeedback.trigger(types, optiones);
        if (item.brand === 'all') {
          setAll(true);
        } else if (item.brand === 'menos') {
          setAll(false);
        } else {
          if (adds) {
            deletedItem(item.brand);
          } else {
            SelectItems(item.brand);
          }
        }
      }}>
      <View
        style={[
          styles.buttonView,
          {
            borderColor:
              adds || item.brand === 'all' || item.brand === 'menos'
                ? colors.main
                : mainColor,
          },
        ]}>
        {item.brand === 'all' || item.brand === 'menos' ? (
          <CustomText
            light={colors.green}
            dark={colors.green}
            style={[
              stylesText.miniTitle,
              styles.ActiveBTN,
              {fontWeight: '500'},
            ]}>
            {all ? t('homeList:SeeMenos') : t('homeList:SeeAll')}
          </CustomText>
        ) : (
          <>
            {adds ? (
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[
                  stylesText.miniTitle,
                  styles.ActiveBTN,
                  {fontWeight: '500'},
                ]}>
                {item.brand}
              </CustomText>
            ) : (
              <View style={[styles.buttonViewIN]}>
                <CustomText
                  light={colors.back_suave_dark}
                  dark={colors.white}
                  style={[stylesText.miniTitle, {fontWeight: '500'}]}>
                  {item.brand}
                </CustomText>
              </View>
            )}
          </>
        )}
      </View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  buttonView: {
    borderRadius: dimensions.Width(98),
    paddingHorizontal: 2,
    paddingVertical: 2,
    marginHorizontal: 7,
    marginVertical: 7,
    borderWidth: 1,
  },

  buttonViewIN: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: dimensions.Width(98),
    paddingHorizontal: 15,
    paddingVertical: 6,
  },

  ActiveBTN: {
    borderRadius: dimensions.Width(98),
    paddingHorizontal: 10,
    paddingVertical: 6,
  },
});
