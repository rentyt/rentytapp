import React, {useState, useContext, useEffect} from 'react';
import {View, Platform, Modal, ScrollView, Alert} from 'react-native';
import {dimensions, colors, stylesText} from '../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';
import data from '../../data/preview.json';
import dataAll from '../../data/model.json';
import {Button} from '../../Components/Buttom';
import Item from './items';
import {useMutation} from '@apollo/client';
import {mutations} from '../../GraphQL';
import Loader from '../../Components/ModalLoading';
import {setItem} from '../../helpers/AsyncStorage';
import {MainContext} from '../../store/MainProvider';
import RNRestart from 'react-native-restart';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function SelectMarker({
  visibleModal,
  setvisibleModal,
  navigation,
}) {
  const {getUser, users} = useContext(MainContext);
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const [seletecdItems, setseletecdItems] = useState([]);
  const [all, setAll] = useState(false);
  const [Loading, setLoading] = useState(false);

  const [updateUser] = useMutation(mutations.UPDATE_USUARIO);

  const datos = all ? dataAll : data;

  useEffect(() => {
    getUser();
  }, []);

  const SelectItems = (ids: string) => {
    getUser();
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setseletecdItems(seletecdItems.concat(ids));
  };

  const deletedItem = (ids: string) => {
    getUser();
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    var i = seletecdItems.findIndex((x: any) => x === ids);
    if (i !== -1) {
      seletecdItems.splice(i, 1);
      setseletecdItems(seletecdItems.concat());
    }
  };

  const _renderItem = (item, i) => {
    return (
      <Item
        marke={seletecdItems}
        deletedItem={deletedItem}
        SelectItems={SelectItems}
        setAll={setAll}
        all={all}
        item={item}
        key={i}
      />
    );
  };

  const input = {
    _id: users && users._id,
    myCategory: seletecdItems,
  };

  const saveMarker = () => {
    getUser();
    setLoading(true);
    if (users) {
      if (seletecdItems.length > 3) {
        updateUser({variables: {input: input}})
          .then(async res => {
            if (res.data.updateUser.success) {
              await setItem('user', JSON.stringify(res.data.updateUser.data));
              setLoading(false);
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(type, optiones);
              getUser();
              setTimeout(() => {
                RNRestart.Restart();
              }, 300);
            } else {
              getUser();
              setLoading(false);
              Alert.alert(t('login:bag'));
            }
          })
          .catch(e => {
            console.log(e);
            setLoading(false);
            Alert.alert(t('login:bag'));
          });
      } else {
        setLoading(false);
        Alert.alert(t('selectMark:alertTitle'), t('selectMark:message'));
        //@ts-ignore
        ReactNativeHapticFeedback.trigger(type, optiones);
      }
    } else {
      getUser();
      setLoading(false);
    }
  };

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      presentationStyle="formSheet"
      collapsable={true}
      statusBarTranslucent={true}>
      <Loader loading={Loading} />
      <ScrollView
        style={styles.contentModal}
        showsVerticalScrollIndicator={false}>
        <View style={styles.cont}>
          <CustomText
            style={stylesText.secondaryTextBold}
            light={colors.black}
            dark={colors.white}>
            {t('selectMark:title')}
          </CustomText>
          <View style={styles.container}>
            {datos.map((item, i) => {
              return _renderItem(item, i);
            })}
          </View>
        </View>
      </ScrollView>
      <View style={styles.buttonContent}>
        <Button
          light={colors.white}
          dark={colors.white}
          loading={false}
          onPress={saveMarker}
          title={t('selectMark:btn')}
        />
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    marginHorizontal: 15,
    marginTop: 80,
    marginBottom: dimensions.Height(20),
  },

  container: {
    width: dimensions.Width(96),
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 50,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  buttonContent: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(14),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 0,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 15}),
    zIndex: 100,
  },
});
