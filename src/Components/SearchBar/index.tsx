import React from 'react';
import {View, TouchableOpacity, TextInput, Platform} from 'react-native';
import {dimensions, colors} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import LinearGradient from 'react-native-linear-gradient';

export default function SearchBar({navigation, getNetworkAndLocation}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const ActAdd = {
    light: colors.main,
    dark: colors.main,
  };

  const PlaceHolded = {
    light: colors.black,
    dark: colors.white,
  };

  const placeHolded = PlaceHolded[mode];

  const mainColor = ActAdd[mode];

  return (
    <View style={[styles.searchCont]}>
      <View style={[styles.search]}>
        <View style={styles.input}>
          <TextInput
            style={styles.textInput}
            placeholder={t('search:placeholded')}
            placeholderTextColor={placeHolded}
            selectionColor={mainColor}
            returnKeyType="search"
            clearButtonMode="while-editing"
            onChangeText={value => {}}
            onFocus={() => {
              getNetworkAndLocation();
              navigation.navigate('Feed', {
                input: {autoFocus: true},
              });
            }}
          />
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('Feed', {
                input: {},
              })
            }>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              colors={[colors.secundary, colors.main]}
              style={styles.btns_search}>
              <Icon
                type="Feather"
                name="search"
                size={22}
                color={colors.white}
              />
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  search: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  searchCont: {
    width: dimensions.Width(96),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
  },

  btns: {
    width: 45,
    height: 45,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 10.27,
    elevation: 15,
  },

  input: {
    height: 60,
    width: dimensions.Width(92),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 8,
    elevation: Platform.select({ios: 15, android: 5}),
  },

  textInput: {
    height: 50,
    width: dimensions.IsIphoneX() ? dimensions.Width(79) : dimensions.Width(76),
    borderRadius: 15,
    fontSize: dimensions.FontSize(16),
    fontWeight: '400',
    paddingLeft: 15,
    fontFamily: 'Poppins',
    color: new DynamicValue(colors.black, colors.white),
  },

  btns_search: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
  },
});
