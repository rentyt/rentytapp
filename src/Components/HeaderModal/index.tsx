import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  Platform,
  Animated,
  ScrollView,
} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import {colors, dimensions, stylesText} from '../../Themes';
import {CustomText} from '../CustomText';
import AnimatedIcons from '../AnimatedIcons';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export interface IHeader {
  onPress: () => void;
  onPress1?: () => void;
  title: string;
  clean?: boolean;
  title2?: string;
  children: any;
  subtitle?: string;
  isOK?: boolean;
}

export default function HeaderModal(props: IHeader) {
  const {onPress, title, clean, title2, onPress1, children, subtitle, isOK} =
    props;
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [scrollY] = useState(new Animated.Value(0));

  const mode = useColorSchemeContext();

  const bgColor1 = {
    light: colors.white,
    dark: colors.black,
  };

  const bgColor = bgColor1[mode];

  const bgColor2 = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BGColor1 = bgColor2[mode];

  const borderBottom = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const borderBottomColor = borderBottom[mode];

  const bgicon = {
    light: colors.rgb_235,
    dark: colors.back_dark,
  };

  const bgIcon = bgicon[mode];

  const bgicon1 = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const bgIcon1 = bgicon1[mode];

  const bgColorchange = scrollY.interpolate({
    inputRange: [80, 700],
    outputRange: [bgColor, BGColor1],
    extrapolate: 'clamp',
  });

  const bgColorIconchange = scrollY.interpolate({
    inputRange: [80, 700],
    outputRange: [bgIcon, bgIcon1],
    extrapolate: 'clamp',
  });

  const bgColorTextchange = scrollY.interpolate({
    inputRange: [80, 700],
    outputRange: [colors.rgb_153, colors.white],
    extrapolate: 'clamp',
  });

  const borderBottomwidth = scrollY.interpolate({
    inputRange: [80, 700],
    outputRange: [0, 0.5],
    extrapolate: 'clamp',
  });

  return (
    <>
      <Animated.View
        style={[
          styles.content,
          {
            backgroundColor: bgColorchange,
            borderBottomWidth: borderBottomwidth,
            borderBottomColor: borderBottomColor,
          },
        ]}>
        <View>
          <CustomText
            numberOfLines={2}
            light={colors.back_suave_dark}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              {width: dimensions.Width(45)},
            ]}>
            {title}
          </CustomText>
          {subtitle ? (
            <CustomText
              numberOfLines={2}
              light={isOK ? colors.green : colors.ERROR}
              dark={isOK ? colors.green : colors.ERROR}
              style={[
                stylesText.secondaryText,
                {width: dimensions.Width(45), marginTop: 5},
              ]}>
              {subtitle}
            </CustomText>
          ) : null}
        </View>
        {clean ? (
          <Animated.View
            style={[
              styles.icon,
              {width: 80, backgroundColor: bgColorIconchange},
            ]}>
            <TouchableOpacity onPress={onPress1}>
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {fontWeight: '500', color: bgColorTextchange},
                ]}
                numberOfLines={1}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {title2}
              </CustomText>
            </TouchableOpacity>
          </Animated.View>
        ) : null}
        <Animated.View
          style={[styles.icon, {backgroundColor: bgColorIconchange}]}>
          <TouchableOpacity onPress={onPress}>
            <AnimatedIcons
              type="Feather"
              name="x"
              size={26}
              color={bgColorTextchange}
            />
          </TouchableOpacity>
        </Animated.View>
      </Animated.View>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event([
          {nativeEvent: {contentOffset: {y: scrollY}}},
        ])}
        scrollEventThrottle={16}
        stickyHeaderIndices={[1]}
        keyboardShouldPersistTaps="handled"
        style={styles.bg}>
        {children}
      </KeyboardAwareScrollView>
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: dimensions.Width(100),
    paddingTop: Platform.select({
      ios: 20,
      android: dimensions.Height(6),
    }),
  },

  bg: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  icon: {
    width: 40,
    height: 40,
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
