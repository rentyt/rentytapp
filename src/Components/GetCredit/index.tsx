import React from 'react';
import {View, TouchableOpacity, Image, Platform} from 'react-native';
import {image, dimensions} from '../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function GetCredit({navigation, languaje}) {
  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const imageByLanguaje = () => {
    switch (languaje) {
      case 'en':
        return image.FreeCredit_Inglish;
      case 'es':
        return image.FreeCredit;
      default:
        return image.FreeCredit;
    }
  };

  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: dimensions.Height(2),
        marginBottom: 30,
      }}>
      <TouchableOpacity
        style={{borderRadius: 15}}
        activeOpacity={100}
        onPress={() => {
          navigation.navigate('Share');
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(type, optiones);
        }}>
        <Image
          source={imageByLanguaje()}
          style={{
            width: dimensions.Width(95),
            height: 120,
            resizeMode: 'cover',
            borderRadius: 15,
          }}
        />
      </TouchableOpacity>
    </View>
  );
}
