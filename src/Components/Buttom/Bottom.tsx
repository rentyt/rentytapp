import React from 'react';
import {ActivityIndicator} from 'react-native';
import {colors, dimensions} from '../../Themes';
import {CustomText} from '../CustomText';
import LinearGradient from 'react-native-linear-gradient';
import {
  DynamicStyleSheet,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';

export const ButtonInactive = (props: any) => {
  const styles = useDynamicValue(dynamicStyles);

  const mode = useColorSchemeContext();

  const bgBTN = {
    light: colors.rgb_153,
    dark: colors.rgb_153,
  };

  const bgBTNColor = bgBTN[mode];

  const bgBTN2 = {
    light: colors.rgb_235,
    dark: colors.back_dark,
  };

  const bgBTNColor2 = bgBTN2[mode];

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 1}}
      colors={[bgBTNColor, bgBTNColor2]}
      style={[
        styles.buttonView,
        {
          minHeight: dimensions.IsIphoneX() ? 50 : 40,
          justifyContent: 'center',
        },
      ]}>
      {props.loading ? (
        <ActivityIndicator color={colors.white} size="small" />
      ) : (
        <CustomText style={styles.buttonTitle}>{props.title}</CustomText>
      )}
    </LinearGradient>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: 100,
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.IsIphoneX()
      ? dimensions.Width(4)
      : dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(16),
    textAlign: 'center',
    fontFamily: 'Poppins',
  },
});
