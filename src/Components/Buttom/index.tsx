import React from 'react';
import {TouchableOpacity, View, ActivityIndicator} from 'react-native';
import {colors, dimensions} from '../../Themes';
import {CustomText} from '../CustomText';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';

export const Button = (props: any) => {
  const styles = useDynamicValue(dynamicStyles);
  const modo = useColorSchemeContext();

  const inact = {
    light: colors.white,
    dark: colors.black,
  };

  const inactiveColor = inact[modo];

  return (
    <TouchableOpacity onPress={() => props.onPress()}>
      <View
        style={[
          styles.buttonView,
          {
            minHeight: dimensions.IsIphoneX() ? 50 : 40,
            justifyContent: 'center',
          },
        ]}>
        {props.loading ? (
          <ActivityIndicator color={inactiveColor} size="small" />
        ) : (
          <CustomText style={styles.buttonTitle}>{props.title}</CustomText>
        )}
      </View>
    </TouchableOpacity>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  buttonView: {
    backgroundColor: new DynamicValue(colors.black, colors.white),
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(100),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.IsIphoneX()
      ? dimensions.Width(4)
      : dimensions.Width(5),
    color: new DynamicValue(colors.white, colors.black),
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(16),
    textAlign: 'center',
    fontFamily: 'Poppins',
  },
});
