import React from 'react';
import {View} from 'react-native';
import {colors} from '../../Themes';
import {DynamicStyleSheet, useDynamicStyleSheet} from 'react-native-dynamic';
import {CustomText} from '../CustomText';

export interface Ibadget {
  number: number;
  bottom?: number;
  left?: number;
  right?: number;
}
export const Badget = (props: Ibadget) => {
  const {number, bottom, left, right} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <>
      {number > 0 ? (
        <View style={[styles.badge, {left, bottom, right}]}>
          <CustomText
            numberOfLines={1}
            style={{color: colors.white, fontSize: 12}}>
            {number > 9 ? '+9' : number}
          </CustomText>
        </View>
      ) : null}
    </>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  badge: {
    height: 20,
    width: 20,
    backgroundColor: colors.ERROR,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    position: 'absolute',
    zIndex: 100,
  },
});
