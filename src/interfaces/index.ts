export interface User {
  _id: string;
  name: string;
  lastName: string;
  phone: string;
  email: string;
  avatar: any;
  StripeID: string;
  OnesignalID: string;
  myCategory: string[];
  rating: string;
  description: string;
  verified: boolean;
  location: any;
  city: string;
}

export interface ILogin {
  firstName: string;
  lastName: string;
  email: string;
  token: string;
  provide: string;
}

export interface ICar {
  marker: string;
  model: string;
  year: string;
}

export interface ICoordenate {
  latitude: any;
  longitude: any;
}

export interface ILocation {
  address_components: any;
  cordenate: ICoordenate;
}

export interface IItems {
  title: string;
  data: string;
  iconName: string;
  iconType: string;
}

export interface IDetails {
  enrollment: IItems;
  combustible: IItems;
  transmission: IItems;
  plazas: IItems;
  year: IItems;
}

export interface IPrices {
  value: number;
  currency: string;
  localcode: string;
}

export interface IAvailability {
  startDate: Date;
  endDate: Date;
}

export interface IImages {
  uri: string;
}

export interface IEquipemnet {
  title: string;
}

export interface IAir {
  available: boolean;
  cost: number;
}

export interface Istatus {
  status: string;
  date: Date;
}

export interface IAds {
  _id: string;
  car: ICar;
  images: IImages[];
  description: string;
  owner: string;
  ownerData: User;
  instantBooking: boolean;
  inService: boolean;
  location: ILocation;
  details: IDetails;
  prices: IPrices;
  availability: IAvailability;
  equipemnet: IEquipemnet[];
  minDays: number;
  offerts: any[];
  anticipation: any[];
  inFavourites: boolean;
  popular: boolean;
  rating: string;
  airPortPickup: IAir;
  offertDay: boolean;
  offertMon: boolean;
  status: string;
  statusProcess: Istatus;
  visible: boolean;
  busyDays: any;
  city: string;
}

export interface MainContext {
  id: string;
  languaje: string;
  setLanguaje: React.Dispatch<React.SetStateAction<string>>;
  setId: React.Dispatch<React.SetStateAction<string>>;
  users: User;
  cards: any[];
  getCard: () => void;
  setUsers?: React.Dispatch<React.SetStateAction<User>>;
  getLanguaje: React.Dispatch<React.SetStateAction<void>>;
  whoosh: any;
  getUser: () => void;
}
