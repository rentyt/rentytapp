import {StackNavigationProp} from '@react-navigation/stack';
import {User} from './index';

export type Routes = 'Home' | 'Login';

export type NavPhone = {
  id: string;
  token: string;
  Register: boolean;
  user: User;
  password: string;
  avatar: string;
  StripeID: string;
};

export type RootStackParamList = {
  Home: undefined;
  Profile: undefined;
  Login: undefined;
  Orders: undefined;
  Forgot: undefined;
  Register: undefined;
  VerifyPhone: NavPhone;
  SendCode: undefined;
  Details: undefined;
  Feed: undefined;
  AddPaymentMethod: undefined;
  PaymentMethod: undefined;
  Result: undefined;
  ChatScreen: undefined;
  Web: undefined;
  MyAds: undefined;
  MyOrders: undefined;
  DetailsOrders: undefined;
  MyRequestDetails: undefined;
  MyRequest: undefined;
  UserPorfile: undefined;
  Rating: undefined;
  OwnerCar: undefined;
  MyRating: undefined;
  UpdateProfile: undefined;
  Share: undefined;
  PromoCode: undefined;
};

export type HomeScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Home'
>;
export type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Profile'
>;
export type LoginScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Login'
>;

export type OrderNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Orders'
>;

export type ForgotNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Forgot'
>;

export type RegisterNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Register'
>;

export type PhoneNavigationProp = StackNavigationProp<
  RootStackParamList,
  'VerifyPhone'
>;

export type CodeNavigationProp = StackNavigationProp<
  RootStackParamList,
  'SendCode'
>;

export type DetailsNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Details'
>;

export type FeddNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Feed'
>;

export type AddPaymentMethodNavigationProp = StackNavigationProp<
  RootStackParamList,
  'AddPaymentMethod'
>;

export type PaymentMethodNavigationProp = StackNavigationProp<
  RootStackParamList,
  'PaymentMethod'
>;

export type ResultNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Result'
>;

export type ChatScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'ChatScreen'
>;

export type WebcreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Web'
>;

export type MyAdscreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'MyAds'
>;

export type MyOrderscreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'MyOrders'
>;

export type DetailsOrderscreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'DetailsOrders'
>;

export type MyRequestcreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'MyRequest'
>;

export type MyRequestDetailsscreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'MyRequestDetails'
>;

export type UserPorfilescreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'UserPorfile'
>;

export type RatingscreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Rating'
>;

export type OwnerCarscreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'OwnerCar'
>;

export type MyRatingcreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'MyRating'
>;

export type UpdateProfilecreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'UpdateProfile'
>;

export type SharecreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Share'
>;

export type PromoCodecreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'PromoCode'
>;
