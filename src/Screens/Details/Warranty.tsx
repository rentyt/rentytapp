import React from 'react';
import {View} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';

export default function TitleAndPrice() {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: BorderColor, borderTopColor: BorderColor},
      ]}>
      <View>
        <Icon name="Safety" type="AntDesign" size={40} color={colors.green} />
      </View>
      <View style={{marginLeft: 10, width: dimensions.Width(80)}}>
        <CustomText
          style={[stylesText.secondaryTextBold]}
          numberOfLines={1}
          light={colors.back_suave_dark}
          dark={colors.white}>
          {t('details:warrantyTitle')}
        </CustomText>
        <CustomText
          style={[stylesText.secondaryText, {marginTop: 5}]}
          numberOfLines={5}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          {t('details:warrantyMessage')}
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    paddingHorizontal: 15,
    paddingVertical: 25,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
  },
});
