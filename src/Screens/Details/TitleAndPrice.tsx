import React from 'react';
import {View} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';
import {formaterPrice} from '../../Utils/formaterPrice';

export default function TitleAndPrice({data}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const ActAdd = {
    light: colors.colorBorder,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: mainColor, borderTopColor: mainColor},
      ]}>
      <View style={{width: dimensions.Width(50)}}>
        <CustomText
          style={stylesText.titleText}
          numberOfLines={2}
          light={colors.back_suave_dark}
          dark={colors.white}>
          {data.car.marker} {data.car.model} {data.car.year}
        </CustomText>
      </View>
      <View>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={[colors.main, colors.secundary]}
          style={[styles.tag]}>
          <CustomText
            numberOfLines={1}
            light={colors.white}
            dark={colors.white}
            style={[stylesText.secondaryText, {fontWeight: 'bold'}]}>
            {formaterPrice(
              data.prices.value,
              data.prices.localcode,
              data.prices.currency,
            )}{' '}
            / {t('card:day')}
          </CustomText>
        </LinearGradient>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
  },

  tag: {
    zIndex: 100,
    marginLeft: 10,
    padding: 5,
    width: 130,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  rating: {
    backgroundColor: '#ffa6002d',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    width: 60,
    borderRadius: 5,
    marginLeft: 'auto',
    marginTop: 5,
    paddingVertical: 3,
  },
});
