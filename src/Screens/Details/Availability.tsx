import React from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import {colors, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import moment from 'moment';
import 'moment/locale/es';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Availability({data, onPress, error, arrayDates}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const ActAdd = {
    light: colors.colorBorder,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  return (
    <TouchableOpacity
      activeOpacity={100}
      onPress={() => {
        onPress(true); //@ts-ignore
        ReactNativeHapticFeedback.trigger(type, optiones);
      }}
      style={[
        styles.content,
        {
          borderBottomColor: mainColor,
          borderTopColor: mainColor,
          borderTopWidth: data.instantBooking ? 0.5 : 0,
        },
      ]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <View style={styles.icon}>
          <Icon
            type="Feather"
            name="calendar"
            size={20}
            color={colors.twitter_color}
          />
        </View>
        <View style={{marginLeft: 10}}>
          <CustomText
            numberOfLines={1}
            style={[stylesText.mainText]}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {arrayDates.length > 0
              ? t('details:availabilitySelected')
              : t('details:availability')}
          </CustomText>
          <CustomText
            numberOfLines={1}
            style={[stylesText.secondaryText, {marginTop: 5}]}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {data.inService
              ? `${t('details:lastUpdate')} ${moment(data.updated_at).format(
                  'l',
                )}`
              : t('details:immediately')}
          </CustomText>
          {error ? (
            <CustomText
              numberOfLines={1}
              style={[stylesText.secondaryText, {marginTop: 5}]}
              light={colors.ERROR}
              dark={colors.ERROR}>
              {t('details:error')}
            </CustomText>
          ) : null}
        </View>
      </View>
      <View>
        <Icon
          type="Feather"
          name="chevron-right"
          size={16}
          color={colors.rgb_153}
        />
      </View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
  },

  icon: {
    width: 35,
    height: 35,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
