import React from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import {colors, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';

export default function Instant({data, onPress}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const ActAdd = {
    light: colors.colorBorder,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  return (
    <TouchableOpacity
      activeOpacity={100}
      onPress={onPress}
      style={[
        styles.content,
        {
          borderBottomColor: mainColor,
          borderTopColor: mainColor,
          borderBottomWidth: data.instantBooking ? 0 : 0.5,
          borderTopWidth: data.instantBooking ? 0 : 0.5,
        },
      ]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <View style={styles.icon}>
          <Icon type="Entypo" name="flash" size={22} color={colors.ERROR} />
        </View>
        <CustomText
          numberOfLines={1}
          style={[stylesText.mainText, {marginLeft: 8}]}
          light={colors.back_suave_dark}
          dark={colors.white}>
          {t('details:InstantDetail')}
        </CustomText>
      </View>
      <View>
        <Icon
          type="Feather"
          name="help-circle"
          size={16}
          color={colors.rgb_153}
        />
      </View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  icon: {
    width: 35,
    height: 35,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
