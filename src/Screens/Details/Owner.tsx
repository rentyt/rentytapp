import React from 'react';
import {
  View,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';
import Star from '../../Components/star';
import {Avatar} from '../../Components/Avatar';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useQuery} from '@apollo/client';
import {query} from '../../GraphQL';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

export default function Owner({ads, navigation, users}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const ActAdd = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  const {data, refetch, loading} = useQuery(query.GET_CONVERSATION_ONE, {
    variables: {userID: users._id, adsID: ads._id},
  });

  const chast =
    data && data.getConversationOne ? data.getConversationOne.data : {data: ''};

  const dataNAv = chast
    ? chast
    : {
        ads: {
          _id: ads._id,
          car: ads.car,
          imageCar: ads.images[0].uri,
          owner: ads.owner,
        },
        user: users,
        Owner: ads.OwnerData,
        userID: users._id,
        messages: [],
        adsID: ads._id,
        owner: ads.owner,
        userRoomID: `${Number.parseInt(getRandomArbitrary(10000, 100000000))}`,
        ownerRoomID: `${Number.parseInt(getRandomArbitrary(10000, 100000000))}`,
        created_at: new Date(),
      };

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={() =>
        navigation.navigate('UserPorfile', {id: ads.OwnerData._id, ads: ads})
      }
      style={[
        styles.content,
        {borderBottomColor: mainColor, borderTopColor: mainColor},
      ]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <Avatar uri={ads.OwnerData.avatar} width={60} height={60} />
        <View style={styles.names}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              width: dimensions.Width(45),
            }}>
            <CustomText
              style={[stylesText.secondaryTextBold, {fontSize: 20}]}
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}>
              {ads.OwnerData.name} {ads.OwnerData.lastName}
            </CustomText>
            {ads.OwnerData.verified ? (
              <Icon
                type="MaterialIcons"
                name="verified"
                size={16}
                color={colors.twitter_color}
                style={{marginLeft: 5, marginTop: 5}}
              />
            ) : null}
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-start',
              marginTop: 5,
            }}>
            <Star star={Number.parseFloat(ads.OwnerData.rating)} />
            <CustomText
              style={[
                stylesText.secondaryText,
                {color: colors.rgb_153, marginLeft: 10},
              ]}
              numberOfLines={1}
              light={colors.rgb_235}
              dark={colors.rgb_235}>
              {ads.OwnerData.rating} (+{ads.OwnerData.totalRating})
            </CustomText>
          </View>
          <CustomText
            style={[
              stylesText.secondaryText,
              {color: colors.rgb_153, marginTop: 5},
            ]}
            numberOfLines={1}
            light={colors.rgb_235}
            dark={colors.rgb_235}>
            <Icon
              type="MaterialCommunityIcons"
              name="fire"
              size={12}
              color={colors.orange1}
            />{' '}
            {t('details:response')}
          </CustomText>
        </View>
      </View>

      <View>
        {loading ? (
          <ActivityIndicator />
        ) : (
          <TouchableOpacity
            onPress={() => {
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(type, optiones);
              refetch();
              dataNAv.refetch = refetch;
              setTimeout(() => {
                navigation.navigate('ChatScreen', dataNAv);
              }, 100);
            }}
            style={[styles.buttonView]}>
            <CustomText
              light={colors.white}
              dark={colors.black}
              style={[stylesText.miniTitle, {fontWeight: '500'}]}>
              {t('details:chat')}
            </CustomText>
          </TouchableOpacity>
        )}
      </View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
    width: dimensions.Width(100),
  },

  names: {
    width: dimensions.IsIphoneX() ? dimensions.Width(58) : dimensions.Width(50),
    marginLeft: 10,
  },

  avatar: {
    width: 60,
    height: 60,
    borderRadius: 100,
  },

  buttonView: {
    borderRadius: dimensions.Width(100),
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: new DynamicValue(colors.black, colors.white),
  },
});
