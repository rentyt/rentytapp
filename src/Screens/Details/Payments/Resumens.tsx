import React, {useState, useRef, useEffect, useContext} from 'react';
import {View, Modal, Platform, Alert, Switch} from 'react-native';
import HeaderModal from '../../../Components/HeaderModal';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import Item from './items';
import BabySeat from '../BabySeat';
import ExtraEquipment from '../ExtraEquipment';
import Icon from 'react-native-dynamic-vector-icons';
import AddNote from './AddNote';
import Price from './Prices';
import ButtomBar from './ButtomBar';
import PaymentMethod from './PaymentMethod';
import {Modalize} from 'react-native-modalize';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import ModalSelecPayment from './ModalSelectPayment';
import {MAIN_URL} from '../../../Utils/Urls';
import {payApplePay, payGooglePay, payCreditCard} from './functionPay';
import {useApplePay, useGooglePay} from '@stripe/stripe-react-native';
import Loader from '../../../Components/ModalLoadingAnimated';
import ModalAddPaymentMethod from './ModalAddPaymentsMethod';
import moment from 'moment';
import Cupon from './Cupon';
import {useMutation} from '@apollo/client';
import {mutations} from '../../../GraphQL';
import {formaterPrice} from '../../../Utils/formaterPrice';
import PaypalModal from './PaypalModal';
import {MainContext} from '../../../store/MainProvider';
import {apiKey} from '../../../Utils/Urls';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

export default function Resumens({
  visibleModal,
  setvisibleModal,
  navigation,
  data,
  BabySeats,
  setBabySeats,
  extraEquipament,
  setextraEquipament,
  timePickup,
  deliveryTime,
  arrayDates,
  setdataBaby,
  dataBaby,
  _markedDatesToSave,
  descuento,
  LoadingCupon,
  clave,
  setclave,
  getDiscount,
}) {
  arrayDates.sort();

  const {users, getCard} = useContext(MainContext);
  const [crearModificarNewOrden] = useMutation(mutations.CREATED_ORDER);
  const modo = useColorSchemeContext();
  const typePayment = Platform.select({
    ios: 'Apple Pay',
    android: 'Google Pay',
  });
  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const {isApplePaySupported} = useApplePay();
  const [initialized, setInitialized] = useState(false);
  const [Loading, setLoading] = useState(false);
  const [visiblemodalPaypal, setvisiblemodalPaypal] = useState(false);
  const [paymentType, setpaymentType] = useState(typePayment);
  const [cardSeleted, setcardSeleted] = useState(null);
  const [note, setnote] = useState('');
  const {initGooglePay, presentGooglePay} = useGooglePay();
  const [airplane, setairplane] = useState(false);
  const [totalDolar, setTotalDolars] = useState(0);
  const modalizeRef = useRef<Modalize>(null);
  const modalizeRefAdd = useRef<Modalize>(null);

  const currecyPayment =
    data.prices.currency === 'DOP' ? 'USD' : data.prices.currency;

  const toggleSwitchAir = value => {
    setairplane(value);
  };

  const swichColor = {
    light: colors.white,
    dark: colors.white,
  };

  const swichColor1 = {
    light: '#767577',
    dark: colors.back_suave_dark,
  };

  const SwichColor = swichColor[modo];

  const SwichColor1 = swichColor1[modo];

  let totalBabySeats = 0;
  let totalextraEquipament = 0;
  let airCost =
    data.airPortPickup && data.airPortPickup && airplane
      ? data.airPortPickup.cost
      : 0;
  let gestionCost = data.gestionCost;
  let bookCost = data.prices.value;
  let bookCostTotal = bookCost * arrayDates.length;

  BabySeats.forEach(element => {
    totalBabySeats += element.priceExtra * element.quantity * arrayDates.length;
  });

  extraEquipament.forEach(element => {
    totalextraEquipament += element.priceExtra * arrayDates.length;
  });

  const getDiscountAnticipation = (): number => {
    let discountPorcentaje = 0;
    if (data.anticipation.length > 0) {
      data.anticipation.forEach(element => {
        const now = moment();
        const min = moment(now).add(element.date, element.type);
        const start = moment(arrayDates[0]);
        const isApply = start >= min;
        discountPorcentaje += isApply ? element.discount : 0;
      });
      return discountPorcentaje;
    } else {
      return 0;
    }
  };

  const getDiscountPeriodo = (): number => {
    let discountPorcentaje = 0;
    if (data.offerts.length > 0) {
      data.offerts.forEach(element => {
        const isapply = arrayDates.length >= element.moreDays;
        discountPorcentaje += isapply ? element.discount : 0;
      });
    }
    return discountPorcentaje;
  };

  const getDiscountClave = () => {
    const valorDescuentos = descuento.descuento;
    if (descuento.tipo === 'dinero') {
      return {
        displayDescuento: formaterPrice(
          valorDescuentos / 100,
          data.prices.localeCode,
          data.prices.currency,
        ),
        valorDescuento: bookCostTotal - valorDescuentos,
      };
    } else if (descuento.tipo === 'porcentaje') {
      return {
        displayDescuento: `${valorDescuentos}%`,
        valorDescuento: (bookCostTotal * valorDescuentos) / 100,
      };
    } else {
      return {
        displayDescuento: 0,
        valorDescuento: 0,
      };
    }
  };

  const porcentajeAnticipation =
    (bookCostTotal * getDiscountAnticipation()) / 100;

  const porcentajePeriodo = (bookCostTotal * getDiscountPeriodo()) / 100;

  let total =
    totalBabySeats +
    totalextraEquipament +
    bookCostTotal +
    airCost +
    gestionCost;

  let totalDiscount =
    porcentajeAnticipation +
    porcentajePeriodo +
    getDiscountClave().valorDescuento;

  let totalFinal = total - totalDiscount;
  const mainTotal = data.prices.currency === 'DOP' ? totalDolar : totalFinal;
  let totalToPay = Number(mainTotal.toFixed(0));
  let totalPaypal = totalToPay / 100;

  const converterCurrency = async (amount: number) => {
    if (data.prices.currency === 'DOP') {
      const converter = await fetch(
        `https://api.fastforex.io/convert?from=DOP&to=USD&amount=${amount}&api_key=${apiKey}`,
      );
      const red = await converter.json();
      setTotalDolars(red.result.USD);
    } else {
      setTotalDolars(0);
    }
  };

  useEffect(() => {
    converterCurrency(totalFinal);
  }, [totalFinal]);

  const initialize = async () => {
    const {error} = await initGooglePay({
      testEnv: false,
      merchantName: 'Rentyt',
      countryCode: 'US',
      billingAddressConfig: {
        format: 'FULL',
        isPhoneNumberRequired: false,
        isRequired: false,
      },
      existingPaymentMethodRequired: false,
      isEmailRequired: false,
    });

    if (error) {
      Alert.alert(error.code, error.message);
      return;
    }
    setInitialized(true);
  };

  useEffect(() => {
    setLoading(false);
    getCard();
    if (Platform.OS === 'android') {
      initialize();
    }
  }, [initialized]);

  const finishBooking = id => {
    setvisibleModal(false);
    setBabySeats([]);
    setextraEquipament([]);
    navigation.navigate('Result', {id: id});
  };

  const onOpen = () => {
    getCard();
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRef.current?.open();
  };

  const onCloset = () => {
    getCard();
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRef.current?.close();
  };

  const onOpenAdd = () => {
    getCard();
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefAdd.current?.open();
  };

  const onClosetAdd = () => {
    getCard();
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefAdd.current?.close();
  };

  const createdOrder = (isPay, paymentType) => {
    const statusPro = [
      {
        status: 'Nueva',
        date: moment().format(),
      },

      {
        status: 'Confirmada',
        date: moment().add(10, 'minutes').format(),
      },
    ];

    const statusProNoConfirm = [
      {
        status: 'Nueva',
        date: moment().format(),
      },
    ];

    const stat = data.instantBooking ? 'Confirmada' : 'Nueva';

    const input = {
      channelOrderDisplayId: Number.parseInt(
        getRandomArbitrary(10000, 100000000),
      ),
      pickupDate: {
        date: arrayDates[0],
        time: timePickup,
      },
      deliveryDate: {
        date: arrayDates[arrayDates.length - 1],
        time: deliveryTime,
      },
      customer: users._id,
      owner: data.owner,
      car: data._id,
      orderIsAlreadyPaid: isPay.succeeded,
      payment: {
        totalDay: bookCostTotal,
        cuponDiscount: descuento._id ? getDiscountClave().valorDescuento : 0,
        discountPerDay: porcentajePeriodo,
        discountPerMon: porcentajeAnticipation,
        serviceChange: gestionCost,
        babyChairs: totalBabySeats,
        extrasEquipment: totalextraEquipament,
        airCost: airCost,
        subtotal: total,
        totalDiscount: totalDiscount,
        total: totalFinal,
        totalDolar: totalDolar,
        currency: {
          currency: data.prices.currency,
          localcode: data.prices.localcode,
        },
      },
      note: note,
      IntegerValue: data.instantBooking ? 50 : 40,
      paymentMethod: paymentType,
      stripePaymentIntent:
        paymentType === 'Paypal' ? null : isPay.paymentIntent,
      pagoPaypal: paymentType === 'Paypal' ? isPay.paymentIntent : null,
      cupon: descuento._id,
      statusProcess: data.instantBooking ? statusPro : statusProNoConfirm,
      status: stat,
      holdedPartnerID: '',
      invoiceUrl: '',
      instantBooking: data.instantBooking,
      selectdDay: arrayDates,
      busyDays: _markedDatesToSave,
      airplanePickUp: airplane,
      babyChair: BabySeats,
      extraEquipament: extraEquipament,
    };

    crearModificarNewOrden({variables: {input: {data: input}}})
      .then(res => {
        if (res.data.crearModificarNewOrden.success) {
          setTimeout(() => {
            setLoading(false);
            finishBooking(res.data.crearModificarNewOrden.data._id);
          }, 3000);
        } else {
          setTimeout(() => {
            setLoading(false);
            Alert.alert(
              res.data.crearModificarNewOrden.message,
              res.data.crearModificarNewOrden.message,
            );
          }, 3000);
        }
      })
      .catch(e => {
        console.error(e);
        setLoading(false);
        if (e) {
          setLoading(false);
        }
      });
  };

  const payPaypal = () => {
    setvisiblemodalPaypal(true);
  };

  const paymentBooking = async () => {
    if (Platform.OS === 'android') {
      initialize();
    }
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    if (note) {
      if (paymentType === 'PayPal') {
        payPaypal();
      } else if (paymentType === 'Apple Pay') {
        const isPay = await payApplePay(
          isApplePaySupported,
          totalToPay,
          `Rentyt`,
          'US',
          currecyPayment.toLowerCase(),
          currecyPayment,
          setLoading,
          users.StripeID,
        );

        if (isPay.succeeded) {
          createdOrder(isPay, 'Apple Pay');
        } else {
          setLoading(false);
          Alert.alert(isPay.paymentIntent.code, isPay.paymentIntent.message);
        }
      } else if (paymentType === 'Google Pay') {
        const isPay = await payGooglePay(
          presentGooglePay,
          setInitialized,
          totalToPay,
          currecyPayment.toLowerCase(),
          setLoading,
          users.StripeID,
        );

        if (isPay.succeeded) {
          createdOrder(isPay, 'Google Pay');
        } else {
          setLoading(false);
          Alert.alert(isPay.paymentIntent.code, isPay.paymentIntent.message);
        }
      } else {
        const isPay = await payCreditCard(
          cardSeleted.id,
          cardSeleted.customer,
          totalToPay,
          currecyPayment.toLowerCase(),
          setLoading,
        );

        if (isPay.succeeded) {
          createdOrder(isPay, 'Tarjeta de credito');
        } else {
          setLoading(false);
          Alert.alert(isPay.paymentIntent.code, isPay.paymentIntent.message);
        }
      }
    } else {
      if (Platform.OS === 'android') {
        Alert.alert(
          t('addPaymentMethod:noNotetitle'),
          t('addPaymentMethod:noNoteMessage'),
        );
      } else {
        Alert.prompt(
          t('addPaymentMethod:noNotetitle'),
          t('addPaymentMethod:noNoteMessage'),
          [
            {
              text: t('addPaymentMethod:addNote:Acept'),
              onPress: value => setnote(value),
            },
          ],
        );
      }
    }
  };

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      presentationStyle="formSheet"
      collapsable={false}
      statusBarTranslucent={true}
      onRequestClose={() => setvisibleModal(false)}>
      <Loader loading={Loading} setVisible={setLoading} />
      <HeaderModal
        clean={false}
        onPress={() => setvisibleModal(false)}
        title={t('details:resumen:titleModal')}
        children={
          <View style={styles.content}>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.titleText,
                {marginLeft: 15, marginVertical: 15},
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:resumen:vehicle')}
            </CustomText>
            <Item data={data} />

            {data.babyChairs.length > 0 ? (
              <>
                <View
                  style={{
                    marginHorizontal: 15,
                    marginTop: 20,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <CustomText
                    style={stylesText.titleText}
                    light={colors.back_suave_dark}
                    dark={colors.white}>
                    {t('addPaymentMethod:resumen:extra')}
                  </CustomText>
                </View>
                <BabySeat
                  data={data}
                  babyChairs={dataBaby}
                  BabySeats={BabySeats}
                  setBabySeats={setBabySeats}
                  setdataBaby={setdataBaby}
                />
              </>
            ) : null}

            {data.ExtraEquipment.length > 0 ? (
              <>
                <View
                  style={{
                    marginHorizontal: 15,
                    marginTop: 20,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <CustomText
                    style={stylesText.titleText}
                    light={colors.back_suave_dark}
                    dark={colors.white}>
                    {t('details:ExtraEquipmentTitle')}
                  </CustomText>
                </View>

                <ExtraEquipment
                  data={data}
                  extraEquipament={extraEquipament}
                  setextraEquipament={setextraEquipament}
                />
              </>
            ) : null}

            {data.airPortPickup && data.airPortPickup.available ? (
              <>
                <View
                  style={{
                    marginHorizontal: 15,
                    marginTop: 20,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <CustomText
                    style={stylesText.titleText}
                    light={colors.back_suave_dark}
                    dark={colors.white}>
                    {t('card:airPort')}
                  </CustomText>
                </View>
                <View style={styles.container}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View style={styles.icon}>
                      <Icon
                        type="Entypo"
                        name="flash"
                        size={24}
                        color={colors.twitter_color}
                      />
                    </View>
                    <View style={{marginLeft: 10}}>
                      <CustomText
                        style={stylesText.TitleCard}
                        light={colors.black}
                        dark={colors.white}>
                        {t('addCar:airTitle')}
                      </CustomText>
                      <CustomText
                        style={[
                          stylesText.secondaryText,
                          {width: dimensions.Width(55)},
                        ]}
                        light={colors.rgb_153}
                        dark={colors.rgb_153}>
                        {t('details:airTitleDescription')}
                      </CustomText>
                      {data.airPortPickup.cost > 0 ? (
                        <CustomText
                          numberOfLines={1}
                          style={[
                            stylesText.secondaryText,
                            {fontWeight: 'bold'},
                          ]}
                          light={colors.green}
                          dark={colors.green}>
                          +{' '}
                          {formaterPrice(
                            data.airPortPickup.cost,
                            data.prices.localcode,
                            data.prices.currency,
                          )}
                        </CustomText>
                      ) : (
                        <CustomText
                          numberOfLines={1}
                          style={[
                            stylesText.secondaryText,
                            {fontWeight: 'bold'},
                          ]}
                          light={colors.green}
                          dark={colors.green}>
                          {t('details:airCostfree')}
                        </CustomText>
                      )}
                    </View>
                  </View>
                  <View style={{marginRight: 10}}>
                    <Switch
                      trackColor={{
                        false: SwichColor1,
                        true: colors.twitter_color,
                      }}
                      thumbColor={airplane ? SwichColor : SwichColor}
                      ios_backgroundColor={colors.rgb_235}
                      onValueChange={toggleSwitchAir}
                      value={airplane}
                    />
                  </View>
                </View>
              </>
            ) : null}

            <CustomText
              numberOfLines={2}
              style={[
                stylesText.titleText,
                {marginLeft: 15, marginVertical: 15},
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:resumen:propietario')}
            </CustomText>
            <AddNote setnote={setnote} data={data} note={note} />
            <CustomText
              light={colors.ERROR}
              dark={colors.ERROR}
              style={[
                stylesText.secondaryText,
                {marginLeft: 5, marginVertical: 5},
              ]}>
              {t('addPaymentMethod:obligatorio')}
            </CustomText>

            <CustomText
              style={[
                stylesText.secondaryText,
                {
                  color: colors.rgb_153,
                  marginHorizontal: 15,
                  marginVertical: 25,
                },
              ]}
              numberOfLines={5}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('details:locationInfo')}
            </CustomText>

            <CustomText
              numberOfLines={2}
              style={[
                stylesText.titleText,
                {marginLeft: 15, marginVertical: 15},
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:resumen:price')}
            </CustomText>
            <Price
              data={data}
              totalBabySeats={totalBabySeats}
              totalextraEquipament={totalextraEquipament}
              gestionCost={gestionCost}
              bookCost={bookCostTotal}
              discountPeriodo={porcentajePeriodo}
              discountAnticipation={porcentajeAnticipation}
              timePickup={timePickup}
              deliveryTime={deliveryTime}
              arrayDates={arrayDates}
              airCost={airCost}
              discount={getDiscountClave}
              airplane={airplane}
              totalDiscount={totalDiscount}
              total={total}
              totalFinal={totalFinal}
              currecyPayment={currecyPayment}
              totalDolar={totalDolar}
            />

            {data.prices.currency === 'DOP' ? (
              <View
                style={[
                  styles.secure,
                  {
                    marginBottom: 0,
                    marginTop: 15,
                    backgroundColor: 'rgba(255, 165, 0, 0.23)',
                  },
                ]}>
                <View>
                  <Icon
                    name="warning"
                    type="AntDesign"
                    size={24}
                    color={colors.orange1}
                  />
                </View>

                <View style={{marginLeft: 15}}>
                  <CustomText
                    style={[
                      stylesText.secondaryText,
                      {color: colors.orange1, fontWeight: '500'},
                    ]}>
                    {t('addPaymentMethod:resumen:DOP')}
                  </CustomText>
                </View>
              </View>
            ) : null}

            <CustomText
              numberOfLines={2}
              style={[stylesText.titleText, {marginLeft: 15, marginTop: 20}]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:cupon')}
            </CustomText>

            <Cupon
              descuento={descuento}
              handleSubmit={getDiscount}
              setclave={setclave}
              Loading={LoadingCupon}
              clave={clave}
            />

            <CustomText
              numberOfLines={2}
              style={[
                stylesText.titleText,
                {marginLeft: 15, marginVertical: 15},
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:headerTitle')}
            </CustomText>

            <PaymentMethod
              data={data}
              paymentType={paymentType}
              setpaymentType={setpaymentType}
              onOpen={onOpen}
              cardSeleted={cardSeleted}
            />

            <View style={styles.secure}>
              <View>
                <Icon
                  name="lock"
                  type="Feather"
                  size={24}
                  color={colors.green}
                />
              </View>

              <View style={{marginLeft: 15}}>
                <CustomText
                  style={[
                    stylesText.secondaryText,
                    {color: colors.green, fontWeight: '500'},
                  ]}>
                  {t('addPaymentMethod:resumen:secure')}
                </CustomText>
              </View>
            </View>
          </View>
        }
      />
      <ButtomBar
        onPress={paymentBooking}
        data={data}
        total={totalToPay}
        currency={currecyPayment}
      />
      <ModalSelecPayment
        navigation={navigation}
        data={data}
        modalizeRef={modalizeRef}
        onCloset={onCloset}
        cardSeleted={cardSeleted}
        setcardSeleted={setcardSeleted}
        paymentType={paymentType}
        setpaymentType={setpaymentType}
        onOpenAdd={onOpenAdd}
      />
      <ModalAddPaymentMethod
        modalizeRef={modalizeRefAdd}
        onCloset={onClosetAdd}
        setcardSeleted={setcardSeleted}
        setpaymentType={setpaymentType}
      />
      <PaypalModal
        createdOrder={createdOrder}
        setLoading={setLoading}
        url={`${MAIN_URL}/paypal?price=${totalPaypal.toFixed(
          2,
        )}&currency=${currecyPayment}`}
        visibleModal={visiblemodalPaypal}
        setvisibleModal={setvisiblemodalPaypal}
      />
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    flex: 1,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    paddingBottom: dimensions.Height(10),
  },

  secure: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 20,
    backgroundColor: 'rgba(41, 216, 131, 0.205)',
    marginHorizontal: 20,
    marginTop: 40,
    borderRadius: 10,
    marginBottom: dimensions.Height(5),
  },

  icon: {
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginLeft: 10,
  },

  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    marginTop: 40,
    marginHorizontal: 5,
  },
});
