import React from 'react';
import {View, Image, TouchableOpacity, Alert, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';

export default function ItemsSecundary({
  imagen,
  title,
  description,
  onPress,
  paymentType,
  item,
  cardSeleted,
  dataCard,
}) {
  const styles = useDynamicValue(dynamicStyles);

  return (
    <TouchableOpacity style={[styles.container]} onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}>
        <View>
          <Image
            source={imagen}
            style={{width: 60, height: 40, resizeMode: 'contain'}}
          />
        </View>
        <View style={{marginLeft: 10}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              {width: dimensions.Width(50)},
            ]}>
            {title}
          </CustomText>
          <View style={{marginTop: 5}}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText]}>
              {description}
            </CustomText>
          </View>
        </View>
      </View>
      {paymentType === item && cardSeleted === null ? (
        <View>
          <Icon name="check" type="Feather" size={24} color={colors.green} />
        </View>
      ) : (
        <>
          {cardSeleted && cardSeleted.id === item ? (
            <View>
              <Icon
                name="check"
                type="Feather"
                size={24}
                color={colors.green}
              />
            </View>
          ) : (
            <Icon name="check" type="Feather" size={24} style={styles.icon} />
          )}
        </>
      )}
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    shadowColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: Platform.select({ios: 13, android: 5}),
    elevation: Platform.select({ios: 23, android: 8}),
    padding: 10,
    marginBottom: 20,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  trash: {
    backgroundColor: new DynamicValue(
      colors.slight_grey,
      colors.back_suave_dark,
    ),
    padding: 10,
    borderRadius: 100,
  },

  icon: {
    color: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },
});
