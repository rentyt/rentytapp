import {
  presentApplePay,
  confirmApplePayPayment,
  handleNextAction,
} from '@stripe/stripe-react-native';
import {MAIN_URL} from '../../../Utils/Urls';

const fetchPaymentIntentClientSecret = async (amount, currency, customer) => {
  const response = await fetch(`${MAIN_URL}/create-payment-intent`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      amount: amount,
      currency: currency,
      customer: customer,
      request_three_d_secure: 'automatic',
    }),
  });
  const {paymentIntent} = await response.json();
  return paymentIntent;
};

const fetchPaymentIntentClient = async (
  payment_method,
  customers,
  amount,
  currency,
) => {
  const response = await fetch(`${MAIN_URL}/payment-card`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      payment_method: payment_method,
      customers: customers,
      amount: amount,
      currency: currency,
      order: '',
    }),
  });
  const data = await response.json();

  return data;
};

export const payApplePay = async (
  isApplePaySupported,
  amount,
  label,
  country,
  countryStripe,
  currency,
  setLoading,
  customer,
) => {
  console.log(
    isApplePaySupported,
    amount,
    label,
    country,
    countryStripe,
    currency,
    setLoading,
    customer,
  );
  if (!isApplePaySupported) return;

  setLoading(true);
  const monto = amount / 100;
  const {error} = await presentApplePay({
    cartItems: [{label: label, amount: `${monto}`}],
    country: country,
    currency: currency,
  });
  if (error) {
    return {
      succeeded: false,
      paymentIntent: error,
    };
  } else {
    const paymentIntent = await fetchPaymentIntentClientSecret(
      amount,
      countryStripe,
      customer,
    );

    const {error: confirmError} = await confirmApplePayPayment(
      paymentIntent.client_secret,
    );

    if (confirmError.code === 'Failed') {
      setLoading(false);
      return {
        succeeded: false,
        paymentIntent: confirmError,
      };
    } else if (confirmError.code === 'Canceled') {
      setLoading(false);

      return {
        succeeded: false,
        paymentIntent: confirmError,
      };
    } else if (confirmError.code === 'Unknown') {
      setLoading(false);
      return {
        succeeded: false,
        paymentIntent: confirmError,
      };
    } else {
      return {
        succeeded: true,
        paymentIntent: paymentIntent,
      };
    }
  }
};

export const payGooglePay = async (
  presentGooglePay,
  setInitialized,
  amount,
  currency,
  setLoading,
  customer,
) => {
  setLoading(true);
  // 2. Fetch payment intent client secret
  const paymentIntent = await fetchPaymentIntentClientSecret(
    amount,
    currency,
    customer,
  );

  const clientSecret = paymentIntent.client_secret;
  // 3. Open Google Pay sheet and proceed a payment
  const {error} = await presentGooglePay({
    clientSecret: clientSecret,
    forSetupIntent: false,
  });

  if (error.code === 'Failed') {
    setLoading(true);
    return {
      succeeded: false,
      paymentIntent: error,
    };
  } else if (error.code === 'Canceled') {
    setLoading(true);
    return {
      succeeded: false,
      paymentIntent: error,
    };
  } else if (error.code === 'Unknown') {
    setLoading(true);
    return {
      succeeded: false,
      paymentIntent: error,
    };
  } else {
    setInitialized(false);
    return {
      succeeded: true,
      paymentIntent: paymentIntent,
    };
  }
};

export const payCreditCard = async (
  payment_method,
  customers,
  amount,
  currency,
  setLoading,
) => {
  setLoading(true);
  const paymentIntents = await fetchPaymentIntentClient(
    payment_method,
    customers,
    amount,
    currency,
  );
  const {status, client_secret} = paymentIntents;

  if (status === 'succeeded') {
    return {
      succeeded: true,
      paymentIntent: paymentIntents,
    };
  } else if (status === 'requires_action') {
    const {error, paymentIntent} = await handleNextAction(client_secret);
    if (error) {
      setLoading(false);
      return {
        succeeded: false,
        paymentIntent: error,
      };
    } else if (paymentIntent) {
      if (paymentIntent.status === 'Succeeded') {
        return {
          succeeded: true,
          paymentIntent: paymentIntent,
        };
      } else if (status === 'Canceled') {
        setLoading(false);
        return {
          succeeded: false,
          paymentIntent: {
            code: 'Fail',
            message: 'You have canceled the payment',
          },
        };
      } else {
        setLoading(false);
        return {
          succeeded: false,
          paymentIntent: error,
        };
      }
    } else {
      setLoading(false);
      return {
        succeeded: false,
        paymentIntent: {
          code: 'Something went wrong',
          message: 'Something went wrong try again',
        },
      };
    }
  } else if (status === 'canceled') {
    setLoading(false);
    return {
      succeeded: false,
      paymentIntent: {
        code: 'Something went wrong',
        message: 'Something went wrong try again',
      },
    };
  } else if (status === 'requires_payment_method') {
    setLoading(false);
    return {
      succeeded: false,
      paymentIntent: {
        code: 'Something went wrong',
        message: 'Something went wrong try again',
      },
    };
  } else {
    setTimeout(() => {
      return {
        succeeded: false,
        paymentIntent: {
          code: 'Something went wrong',
          message: 'Something went wrong try again',
        },
      };
    }, 12000);
  }
};
