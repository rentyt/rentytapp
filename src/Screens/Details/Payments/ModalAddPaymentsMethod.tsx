import React, {useState} from 'react';
import {View, Image, Platform} from 'react-native';
import {colors, dimensions, stylesText, image} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {Modalize} from 'react-native-modalize';
import {CustomText} from '../../../Components/CustomText';
import AddPayment from '../../PaymentMethod/AddPaymentScreen/addPayment';

export default function AddPaymentMethod({
  modalizeRef,
  onCloset,
  setcardSeleted,
  setpaymentType,
}) {
  const [handle, setHandle] = useState(false);
  const [Loading, setLoading] = useState(false);
  const [toggle] = useState(true);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const HEADER_HEIGHT = 300;

  const {t} = useTranslation();

  const handlePosition = position => {
    setHandle(position === 'top');
  };

  return (
    <Modalize
      ref={modalizeRef}
      adjustToContentHeight={toggle}
      snapPoint={HEADER_HEIGHT}
      scrollViewProps={{showsVerticalScrollIndicator: false}}
      modalStyle={styles.modals}
      withHandle={handle}
      modalTopOffset={100}
      handlePosition="inside"
      handleStyle={{
        top: 25,
        width: 40,
        height: handle ? 6 : 6,
        backgroundColor: colors.main,
      }}
      onPositionChange={handlePosition}>
      <View style={styles.contentModal}>
        <View
          style={{
            width: dimensions.Width(100),
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 0,
          }}>
          <View>
            <CustomText
              style={[
                stylesText.secondaryTextBold,
                {textAlign: 'center', marginTop: 15, marginBottom: 20},
              ]}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {t(`addPaymentMethod:title`)}
            </CustomText>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Image
                source={image.LogoCard}
                style={{width: 300, height: 80, resizeMode: 'contain'}}
              />
            </View>

            <View style={[styles.item]}>
              <AddPayment
                navigation={null}
                setLoading={setLoading}
                onCloset={onCloset}
                isPayment={true}
                setcardSeleted={setcardSeleted}
                setpaymentType={setpaymentType}
              />
            </View>
          </View>
        </View>
      </View>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modals: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contentModal: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginTop: 30,
    paddingBottom: Platform.select({
      ios: dimensions.Height(5),
      android: dimensions.Height(40),
    }),
  },

  item: {
    width: dimensions.Width(100),
    marginTop: 15,
  },
});
