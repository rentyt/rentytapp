import React from 'react';
import {View, Modal, Alert} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Loader from '../../../Components/ModalLoading';
import {WebView} from 'react-native-webview';
import {colors} from '../../../Themes';

export default function PaypalModal({
  createdOrder,
  url,
  visibleModal,
  setvisibleModal,
  setLoading,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const onNavigationStateChange = async data => {
    if (data.title === 'success') {
      const isPay = {
        succeeded: true,
        paymentIntent: {
          pago: 'Paypal',
          status: data.title,
        },
      };
      createdOrder(isPay, 'Paypal');
      setvisibleModal(false);
      setLoading(true);
    } else if (data.title === 'cancel') {
      setLoading(false);
      setvisibleModal(false);
      Alert.alert('Canceled', 'Paypal payment has been canceled');
    }
  };

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      presentationStyle="formSheet"
      collapsable={true}
      statusBarTranslucent={true}
      onRequestClose={() => setvisibleModal(false)}>
      <View style={styles.container}>
        <WebView
          source={{
            uri: url,
          }}
          startInLoadingState={true}
          useWebKit={true}
          renderLoading={() => <Loader color={colors.main} />}
          onNavigationStateChange={onNavigationStateChange}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          injectedJavaScript={`document.f1.submit()`}
        />
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },
});
