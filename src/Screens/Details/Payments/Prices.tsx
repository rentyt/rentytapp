import React from 'react';
import {View} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import {formaterPrice} from '../../../Utils/formaterPrice';
import moment from 'moment';

export default function Prices({
  data,
  totalBabySeats,
  totalextraEquipament,
  gestionCost,
  total,
  bookCost,
  discountPeriodo,
  discountAnticipation,
  timePickup,
  deliveryTime,
  arrayDates,
  airCost,
  discount,
  airplane,
  totalDiscount,
  totalFinal,
  currecyPayment,
  totalDolar,
}) {
  arrayDates.sort();

  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];
  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: BorderColor, borderTopColor: BorderColor},
      ]}>
      <View style={styles.prices}>
        <View>
          <CustomText
            numberOfLines={2}
            style={[stylesText.secondaryText, {marginBottom: 5}]}
            light={colors.back_dark}
            dark={colors.white}>
            {t('addPaymentMethod:price:price')} {arrayDates.length}{' '}
            {arrayDates.length === 1
              ? t('details:DaySelected:día')
              : t('details:DaySelected:días')}
          </CustomText>
          <CustomText
            numberOfLines={2}
            style={[stylesText.terciaryText, {width: dimensions.Width(70)}]}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {moment(arrayDates[0]).format('ll')}, {timePickup}{' '}
            {t('addPaymentMethod:price:a')}{' '}
            {moment(arrayDates[arrayDates.length - 1]).format('ll')}{' '}
            {deliveryTime}
          </CustomText>
        </View>

        <View>
          <CustomText
            numberOfLines={2}
            style={[
              stylesText.secondaryText,
              {textAlign: 'right', fontWeight: 'bold'},
            ]}
            light={colors.back_dark}
            dark={colors.white}>
            {formaterPrice(
              bookCost,
              data.prices.localcode,
              data.prices.currency,
            )}
          </CustomText>
        </View>
      </View>

      {discount().valorDescuento > 0 ? (
        <View style={styles.prices}>
          <View>
            <CustomText
              numberOfLines={2}
              style={[stylesText.secondaryText, {marginBottom: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:price:discountCupon')}
            </CustomText>
          </View>

          <View>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.secondaryText,
                {textAlign: 'right', fontWeight: 'bold'},
              ]}
              light={colors.green}
              dark={colors.green}>
              {discount().displayDescuento}
            </CustomText>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.terciaryText,
                {textAlign: 'right', fontWeight: 'bold'},
              ]}
              light={colors.black}
              dark={colors.white}>
              -{' '}
              {formaterPrice(
                discount().valorDescuento,
                data.prices.localcode,
                data.prices.currency,
              )}
            </CustomText>
          </View>
        </View>
      ) : null}

      {discountPeriodo > 0 ? (
        <View style={styles.prices}>
          <View>
            <CustomText
              numberOfLines={2}
              style={[stylesText.secondaryText, {marginBottom: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:price:discount')}
            </CustomText>
            <CustomText
              numberOfLines={2}
              style={[stylesText.terciaryText]}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('addPaymentMethod:price:discount')}
            </CustomText>
          </View>
          <View>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.secondaryText,
                {textAlign: 'right', fontWeight: 'bold'},
              ]}
              light={colors.green}
              dark={colors.green}>
              <CustomText
                numberOfLines={2}
                style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
                light={colors.green}
                dark={colors.green}>
                -
              </CustomText>{' '}
              {formaterPrice(
                discountPeriodo,
                data.prices.localcode,
                data.prices.currency,
              )}
            </CustomText>
          </View>
        </View>
      ) : null}

      {discountAnticipation > 0 ? (
        <View style={styles.prices}>
          <View>
            <CustomText
              numberOfLines={2}
              style={[stylesText.secondaryText, {marginBottom: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:price:discountAnticipation')}
            </CustomText>
            <CustomText
              numberOfLines={2}
              style={[stylesText.terciaryText]}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('addPaymentMethod:price:discountDesciption')}
            </CustomText>
          </View>

          <View>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.secondaryText,
                {textAlign: 'right', fontWeight: 'bold'},
              ]}
              light={colors.green}
              dark={colors.green}>
              <CustomText
                numberOfLines={2}
                style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
                light={colors.green}
                dark={colors.green}>
                -
              </CustomText>{' '}
              {formaterPrice(
                discountAnticipation,
                data.prices.localcode,
                data.prices.currency,
              )}
            </CustomText>
          </View>
        </View>
      ) : null}

      <View style={styles.prices}>
        <View>
          <CustomText
            numberOfLines={2}
            style={[stylesText.secondaryText, {marginBottom: 5}]}
            light={colors.back_dark}
            dark={colors.white}>
            {t('addPaymentMethod:price:seguro')}
          </CustomText>
          <CustomText
            numberOfLines={2}
            style={[stylesText.terciaryText]}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {t('addPaymentMethod:price:segurodescription')}
          </CustomText>
        </View>

        <View>
          <CustomText
            numberOfLines={2}
            style={[
              stylesText.secondaryText,
              {textAlign: 'right', fontWeight: 'bold'},
            ]}
            light={colors.back_dark}
            dark={colors.white}>
            {formaterPrice(
              gestionCost,
              data.prices.localcode,
              data.prices.currency,
            )}
          </CustomText>
        </View>
      </View>

      {data.airPortPickup && data.airPortPickup.available && airplane ? (
        <View style={styles.prices}>
          <View>
            <CustomText
              numberOfLines={2}
              style={[stylesText.secondaryText, {marginBottom: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:price:air')}
            </CustomText>
          </View>

          <View>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.secondaryText,
                {textAlign: 'right', fontWeight: 'bold'},
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              {formaterPrice(
                airCost,
                data.prices.localcode,
                data.prices.currency,
              )}
            </CustomText>
          </View>
        </View>
      ) : null}

      {totalextraEquipament > 0 ? (
        <View style={styles.prices}>
          <View>
            <CustomText
              numberOfLines={2}
              style={[stylesText.secondaryText, {marginBottom: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:price:extra')}
            </CustomText>
          </View>

          <View>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.secondaryText,
                {textAlign: 'right', fontWeight: 'bold'},
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              {formaterPrice(
                totalextraEquipament,
                data.prices.localcode,
                data.prices.currency,
              )}
            </CustomText>
          </View>
        </View>
      ) : null}

      {totalBabySeats > 0 ? (
        <View style={styles.prices}>
          <View>
            <CustomText
              numberOfLines={2}
              style={[stylesText.secondaryText, {marginBottom: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:price:baby')}
            </CustomText>
          </View>

          <View>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.secondaryText,
                {textAlign: 'right', fontWeight: 'bold'},
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              {formaterPrice(
                totalBabySeats,
                data.prices.localcode,
                data.prices.currency,
              )}
            </CustomText>
          </View>
        </View>
      ) : null}

      <View style={styles.footer} />

      <View style={styles.prices}>
        <View>
          <CustomText
            numberOfLines={2}
            style={[
              stylesText.secondaryText,
              {marginBottom: 5, fontWeight: 'bold'},
            ]}
            light={colors.back_dark}
            dark={colors.white}>
            {t('addPaymentMethod:price:subtotal')}
          </CustomText>
        </View>

        <View>
          <CustomText
            numberOfLines={2}
            style={[
              stylesText.secondaryText,
              {textAlign: 'right', fontWeight: 'bold'},
            ]}
            light={colors.back_dark}
            dark={colors.white}>
            {formaterPrice(total, data.prices.localcode, data.prices.currency)}
          </CustomText>
        </View>
      </View>
      {totalDiscount > 0 ? (
        <View style={styles.prices}>
          <View>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.secondaryText,
                {marginBottom: 5, fontWeight: 'bold'},
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('addPaymentMethod:price:Descuentototal')}
            </CustomText>
          </View>

          <View>
            <CustomText
              numberOfLines={2}
              style={[
                stylesText.secondaryText,
                {textAlign: 'right', fontWeight: 'bold'},
              ]}
              light={colors.green}
              dark={colors.green}>
              -{' '}
              {formaterPrice(
                totalDiscount,
                data.prices.localcode,
                data.prices.currency,
              )}
            </CustomText>
          </View>
        </View>
      ) : null}

      <View style={styles.prices}>
        <View>
          <CustomText
            numberOfLines={2}
            style={[stylesText.titleText, {marginBottom: 5}]}
            light={colors.back_dark}
            dark={colors.white}>
            {t('addPaymentMethod:price:total')}
          </CustomText>
        </View>

        <View>
          <CustomText
            numberOfLines={2}
            style={[stylesText.titleText, {textAlign: 'right'}]}
            light={colors.back_dark}
            dark={colors.white}>
            {formaterPrice(
              totalFinal,
              data.prices.localcode,
              data.prices.currency,
            )}
          </CustomText>
        </View>
      </View>

      {data.prices.currency === 'DOP' ? (
        <View style={styles.prices}>
          <View>
            <CustomText
              numberOfLines={2}
              style={[stylesText.secondaryTextBold, {marginBottom: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {t('myOrder:details:resumenPrice:totalDolar')}
            </CustomText>
          </View>

          <View>
            <CustomText
              numberOfLines={2}
              style={[stylesText.secondaryTextBold, {textAlign: 'right'}]}
              light={colors.back_dark}
              dark={colors.white}>
              {formaterPrice(totalDolar, data.prices.localcode, currecyPayment)}
            </CustomText>
          </View>
        </View>
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    padding: 15,
  },

  prices: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: dimensions.Width(93),
    marginBottom: 15,
  },

  footer: {
    borderTopWidth: 0.8,
    marginBottom: 10,
    borderTopColor: new DynamicValue(
      colors.colorBorder,
      colors.back_suave_dark,
    ),
  },
});
