import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {dimensions, colors, image, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import {useTranslation} from 'react-i18next';

export default function PaymenMethod({
  data,
  paymentType,
  setpaymentType,
  cardSeleted,
  onOpen,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  const maincolor = {
    light: colors.green,
    dark: colors.green,
  };

  const MainColor = maincolor[mode];

  const renderLogoBrand = () => {
    switch (cardSeleted.card.brand) {
      case 'visa':
        return image.VisaLogo;

      case 'mastercard':
        return image.MasterCardLogo;

      case 'amex':
        return image.AmericamLogo;

      case 'dicover':
        return image.DiscoverLogo;

      default:
        return image.VisaLogo;
    }
  };

  const paymentTypeS = () => {
    switch (paymentType) {
      case 'Apple Pay':
        return {
          logoPayment: image.ApplePayLogo,
          titlepayment: 'Apple Pay',
          subtitle: '',
        };

      case 'Google Pay':
        return {
          logoPayment: image.GooglePayLogo,
          titlepayment: 'Google Pay',
          subtitle: '',
        };

      case 'PayPal':
        return {
          logoPayment: image.PaypalLogo,
          titlepayment: 'PayPal',
          subtitle: '',
        };

      case 'Card':
        return {
          logoPayment: renderLogoBrand(),
          titlepayment: `**** ${cardSeleted.card.last4}`,
          subtitle: `${t('addPaymentMethod:expire')}: ${
            cardSeleted.card.exp_month
          } / ${cardSeleted.card.exp_year}`,
        };

      default:
        return {
          logoPayment: image.ApplePayLogo,
          titlepayment: 'Apple pay',
          subtitle: '',
        };
    }
  };

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: BorderColor, borderTopColor: BorderColor},
      ]}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}>
        <Image
          source={paymentTypeS().logoPayment}
          style={{width: 60, height: 30, resizeMode: 'contain'}}
        />
        <View>
          <CustomText
            numberOfLines={1}
            style={[
              stylesText.secondaryTextBold,
              {width: dimensions.Width(50)},
            ]}
            light={colors.back_dark}
            dark={colors.white}>
            {paymentTypeS().titlepayment}
          </CustomText>
          {paymentTypeS().subtitle ? (
            <View>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[stylesText.secondaryText]}>
                {paymentTypeS().subtitle}
              </CustomText>
            </View>
          ) : null}
        </View>
      </View>
      <TouchableOpacity onPress={() => onOpen()}>
        <CustomText
          style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
          light={MainColor}
          dark={MainColor}>
          {t('addPaymentMethod:btnChange')}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    paddingVertical: 15,
    paddingHorizontal: 15,
  },
});
