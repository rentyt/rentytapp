import React from 'react';
import {View, TextInput} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {dimensions, colors} from '../../../Themes';
import {useTranslation} from 'react-i18next';
import Textarea from 'react-native-textarea';

export default function AddNote({setnote, data, note}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: BorderColor, borderTopColor: BorderColor},
      ]}>
      <Textarea
        containerStyle={styles.textareaContainer}
        //@ts-ignore
        style={styles.textarea}
        onChangeText={setnote}
        defaultValue={note}
        maxLength={200}
        minLength={100}
        selectionColor={colors.green}
        placeholder={`${t('addPaymentMethod:addNote:placeholder')} ${
          data.OwnerData.name
        } ${data.OwnerData.lastName}`}
        placeholderTextColor={'#c7c7c7'}
        underlineColorAndroid={'transparent'}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    flexDirection: 'row',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    paddingVertical: 15,
  },

  textareaContainer: {
    height: 'auto',
    minHeight: 120,
    padding: 10,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    borderRadius: 10,
    marginHorizontal: 15,
    width: '92%',
    borderWidth: 1,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },
  textarea: {
    textAlignVertical: 'top', // hack android
    height: 'auto',
    minHeight: 120,
    fontSize: 18,
    fontWeight: '300',
    color: new DynamicValue(colors.black, colors.white),
    fontFamily: 'Poppins',
  },
});
