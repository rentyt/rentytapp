import React from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {CustomText} from '../../../Components/CustomText';
import {colors, dimensions, stylesText} from '../../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';

export default function Cupon(props) {
  const {descuento, Loading, setclave, handleSubmit, clave} = props;

  const styles = useDynamicValue(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const Main = {
    light: colors.green,
    dark: colors.green,
  };

  const mainsColor = Main[mode];

  const bgTramp = {
    light: 'rgba(41, 216, 131, 0.205)',
    dark: 'rgba(41, 216, 131, 0.205)',
  };

  const bgsColor = bgTramp[mode];

  return (
    <View>
      {descuento.descuento ? (
        <View
          style={{
            padding: 10,
            width: dimensions.Width(96),
            height: 70,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            backgroundColor: bgsColor,
            marginTop: 20,
            borderRadius: 10,
          }}>
          <CustomText
            light={mainsColor}
            dark={mainsColor}
            style={stylesText.secondaryTextBold}>
            <Icon
              name="checkcircle"
              type="AntDesign"
              size={20}
              color={mainsColor}
              style={{marginTop: 10}}
            />{' '}
            {t('addPaymentMethod:cuponComp:success')}
          </CustomText>
        </View>
      ) : (
        <>
          <View
            style={{
              padding: 20,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {
                    marginLeft: 0,
                    width: '70%',
                    lineHeight: 18,
                  },
                ]}>
                {t('addPaymentMethod:cuponComp:message')}
              </CustomText>
            </View>
          </View>
          <View style={styles.formView}>
            <Icon
              name="ticket-percent-outline"
              type="MaterialCommunityIcons"
              size={24}
              color={mainsColor}
            />
            <TextInput
              placeholder={t('addPaymentMethod:cuponComp:placeholder')}
              autoCompleteType="name"
              autoCapitalize="none"
              autoCorrect={false}
              clearButtonMode="always"
              selectionColor={colors.green}
              placeholderTextColor={colors.rgb_153}
              style={styles.input}
              onChangeText={(clave: any) =>
                setclave(clave.toUpperCase().replace(/\s/g, ''))
              }
            />
            <View style={styles.btn}>
              <TouchableOpacity onPress={clave ? handleSubmit : null}>
                {Loading ? (
                  <ActivityIndicator color={mainsColor} size="small" />
                ) : (
                  <CustomText
                    light={clave ? mainsColor : colors.rgb_153}
                    dark={clave ? mainsColor : colors.rgb_153}
                    style={{fontFamily: 'Poppins', fontWeight: 'bold'}}>
                    {t('addPaymentMethod:cuponComp:btn')}
                  </CustomText>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </>
      )}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  formView: {
    marginTop: dimensions.Height(1),
    marginHorizontal: 15,
    paddingHorizontal: 10,
    borderRadius: 5,
    height: 40,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: new DynamicValue(colors.rgb_153, colors.rgb_153),
    borderWidth: 0.5,
    marginBottom: dimensions.Height(2),
  },

  input: {
    width: dimensions.Width(60),
    marginLeft: 10,
    color: colors.rgb_153,
    flexDirection: 'row',
    fontSize: 16,
    fontFamily: 'Poppins',
    fontWeight: '300',
  },

  btn: {
    width: dimensions.Width(18),
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 0.5,
    borderLeftColor: new DynamicValue(colors.rgb_153, colors.rgb_153),
  },
});
