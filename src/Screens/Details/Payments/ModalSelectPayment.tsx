import React, {useState, useContext} from 'react';
import {
  View,
  Platform,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import {colors, dimensions, stylesText, image} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {Modalize} from 'react-native-modalize';
import {Button} from '../../../Components/Buttom';
import {CustomText} from '../../../Components/CustomText';
import ItemsSecundary from './ItemsSecudanry';
import Icon from 'react-native-dynamic-vector-icons';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import LottieView from 'lottie-react-native';
import {MainContext} from '../../../store/MainProvider';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ModalInstant({
  data,
  modalizeRef,
  onCloset,
  navigation,
  cardSeleted,
  setcardSeleted,
  setpaymentType,
  paymentType,
  onOpenAdd,
}) {
  const [handle, setHandle] = useState(false);
  const [toggle] = useState(true);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {cards} = useContext(MainContext);

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const HEADER_HEIGHT = 300;

  const {t} = useTranslation();

  const handlePosition = position => {
    setHandle(position === 'top');
  };

  const mode = useColorSchemeContext();

  const main = {
    light: colors.main,
    dark: colors.main,
  };

  const mainColor = main[mode];

  const selectPaymets = (tipo: string, data: any) => {
    onCloset();
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (data) {
      setcardSeleted(data);
      setpaymentType(tipo);
    } else {
      setpaymentType(tipo);
      setcardSeleted(null);
    }
  };

  const renderItems = (item, i) => {
    const renderLogoBrand = () => {
      switch (item.card.brand) {
        case 'visa':
          return image.VisaLogo;

        case 'mastercard':
          return image.MasterCardLogo;

        case 'amex':
          return image.AmericamLogo;

        case 'dicover':
          return image.DiscoverLogo;

        default:
          return image.VisaLogo;
      }
    };

    return (
      <ItemsSecundary
        key={i}
        onPress={() => selectPaymets('Card', item)}
        imagen={renderLogoBrand()}
        title={`**** ${item.card.last4}`}
        description={`${t('addPaymentMethod:expire')}: ${
          item.card.exp_month
        } / ${item.card.exp_year}`}
        paymentType={paymentType}
        item={item.id}
        cardSeleted={cardSeleted}
        dataCard={item}
      />
    );
  };

  return (
    <Modalize
      ref={modalizeRef}
      adjustToContentHeight={toggle}
      snapPoint={HEADER_HEIGHT}
      scrollViewProps={{showsVerticalScrollIndicator: false}}
      modalStyle={styles.modals}
      withHandle={handle}
      modalTopOffset={100}
      handlePosition="inside"
      handleStyle={{
        top: 25,
        width: 40,
        height: handle ? 6 : 6,
        backgroundColor: colors.main,
      }}
      onPositionChange={handlePosition}>
      <ScrollView style={styles.contentModal}>
        <View
          style={{
            width: dimensions.Width(100),
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 0,
          }}>
          <View>
            <CustomText
              style={[
                stylesText.secondaryTextBold,
                {marginHorizontal: 15, marginBottom: 15, marginTop: 10},
              ]}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {t(`addPaymentMethod:selectPaymentTitle`)}
            </CustomText>

            <TouchableOpacity
              style={[styles.addCont, {borderColor: mainColor}]}
              onPress={() => {
                onCloset();
                onOpenAdd();
                //@ts-ignore
                ReactNativeHapticFeedback.trigger(type, optiones);
              }}>
              <Icon
                name="plus-circle"
                type="Feather"
                size={24}
                color={mainColor}
              />
              <CustomText
                light={mainColor}
                dark={mainColor}
                style={[
                  stylesText.secondaryText,
                  {paddingBottom: 5, marginTop: 5},
                ]}>
                {t('addPaymentMethod:title')}
              </CustomText>
            </TouchableOpacity>

            <View style={[styles.item]}>
              {Platform.OS === 'ios' ? (
                <ItemsSecundary
                  onPress={() => selectPaymets('Apple Pay', null)}
                  imagen={image.ApplePayLogo}
                  title={t('addPaymentMethod:item_Apple_title')}
                  description={t('addPaymentMethod:item_Apple_description')}
                  paymentType={paymentType}
                  item="Apple Pay"
                  cardSeleted={cardSeleted}
                  dataCard={null}
                />
              ) : (
                <ItemsSecundary
                  onPress={() => selectPaymets('Google Pay', null)}
                  imagen={image.GooglePayLogo}
                  title={t('addPaymentMethod:item_Google_title')}
                  description={t('addPaymentMethod:item_Google_description')}
                  paymentType={paymentType}
                  item="Google Pay"
                  cardSeleted={cardSeleted}
                  dataCard={null}
                />
              )}
              <ItemsSecundary
                onPress={() => selectPaymets('PayPal', null)}
                imagen={image.PaypalLogo}
                title={t('addPaymentMethod:item_Paypal_title')}
                description={t('addPaymentMethod:item_Paypal_description')}
                paymentType={paymentType}
                item="PayPal"
                cardSeleted={cardSeleted}
                dataCard={null}
              />
              {cards.length > 0 ? (
                <>
                  {cards.map((item, i) => {
                    return renderItems(item, i);
                  })}
                </>
              ) : (
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <LottieView
                    source={require('../../../Assets/animated/credit-card-blue.json')}
                    autoPlay
                    loop
                    style={{width: 250}}
                  />
                  <CustomText
                    light={colors.back_suave_dark}
                    dark={colors.white}
                    style={[stylesText.secondaryText, {paddingBottom: 5}]}>
                    {t('addPaymentMethod:emptyText')}
                  </CustomText>
                </View>
              )}
            </View>
          </View>
        </View>
      </ScrollView>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modals: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contentModal: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginTop: 30,
    paddingBottom: dimensions.Height(10),
    flex: 1,
  },

  item: {
    width: dimensions.Width(92),
    marginTop: 15,
    marginHorizontal: 15,
  },

  addCont: {
    padding: 10,
    margin: 15,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderWidth: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
});
