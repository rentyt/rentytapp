import React from 'react';
import {View, Image, ImageBackground, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {colors, image, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import {formaterPrice} from '../../../Utils/formaterPrice';

export default function Item({data}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: BorderColor, borderTopColor: BorderColor},
      ]}>
      <ImageBackground
        style={[styles.mainImages]}
        imageStyle={{borderRadius: 15}}
        resizeMode="cover"
        source={image.PlaceHoldedCard}>
        <Image
          source={{uri: data.images[0].uri}}
          style={styles.mainImage}
          resizeMode="cover"
        />
      </ImageBackground>
      <View style={{marginLeft: 15}}>
        <CustomText
          numberOfLines={2}
          style={stylesText.secondaryTextBold}
          light={colors.back_dark}
          dark={colors.white}>
          {data.car.marker} {data.car.model} {data.car.year}
        </CustomText>
        <CustomText
          numberOfLines={2}
          style={[stylesText.secondaryTextBold]}
          light={colors.back_dark}
          dark={colors.white}>
          {formaterPrice(
            data.prices.value,
            data.prices.localcode,
            data.prices.currency,
          )}
          /{t('card:day')}
        </CustomText>
        <CustomText
          numberOfLines={2}
          style={[stylesText.secondaryText]}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          {data.city}
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    padding: 15,
    flexDirection: 'row',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
  },

  mainImages: {
    width: 75,
    height: 75,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  mainImage: {
    width: 71,
    height: 71,
    borderRadius: 15,
  },
});
