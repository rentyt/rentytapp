import React from 'react';
import {View, Platform} from 'react-native';
import {colors, stylesText, dimensions} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';

export default function Offert({data}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const ActAdd = {
    light: colors.colorBorder,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  const renderItem = (item, i) => {
    return (
      <View
        key={i}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: dimensions.Width(83),
          marginLeft: 7,
          marginVertical: 10,
        }}>
        <View style={{width: dimensions.Width(60)}}>
          <CustomText
            numberOfLines={3}
            style={[stylesText.mainText]}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('details:ofdertName')} {item.moreDays} {t('details:offertdays')}
          </CustomText>
        </View>
        <View>
          <CustomText
            style={[stylesText.secondaryTextBold]}
            light={colors.orange1}
            dark={colors.orange1}>
            {item.discount}%
          </CustomText>
        </View>
      </View>
    );
  };

  return (
    <View
      style={[
        styles.content,
        {
          borderBottomColor: mainColor,
          borderTopColor: mainColor,
          borderBottomWidth: 0.5,
          borderTopWidth: data.instantBooking ? 0 : 0.5,
        },
      ]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          width: 50,
        }}>
        <View style={styles.icon}>
          <Icon
            type="MaterialCommunityIcons"
            name="ticket-percent-outline"
            size={20}
            color={colors.orange1}
          />
        </View>
        <View>
          {data.offerts &&
            data.offerts.map((datos, i) => {
              return renderItem(datos, i);
            })}
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  icon: {
    width: 35,
    height: 35,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
