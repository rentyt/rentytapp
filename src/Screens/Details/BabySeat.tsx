import React, {useState, useEffect} from 'react';
import {FlatList, View} from 'react-native';
import {colors} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Item from './Items/ItemBaby';

export default function BabySeats({
  data,
  babyChairs,
  BabySeats,
  setBabySeats,
  setdataBaby,
}) {
  const [arraysDataBaby, setarraysDataBaby] = useState(
    JSON.parse(JSON.stringify(babyChairs)),
  );

  useEffect(() => {
    setdataBaby(arraysDataBaby);
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);

  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  const renderItem = ({item, index}) => {
    return (
      <Item
        data={data}
        item={item}
        title={item.title}
        index={index}
        iconType={item.iconType}
        iconName={item.iconName}
        BabySeats={BabySeats}
        setarraysDataBaby={setarraysDataBaby}
        setBabySeats={setBabySeats}
        arrays={arraysDataBaby}
      />
    );
  };

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: BorderColor, borderTopColor: BorderColor},
      ]}>
      <FlatList
        data={arraysDataBaby}
        renderItem={(item: any) => renderItem(item)}
        keyExtractor={(item: any) => item.title}
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
  },
});
