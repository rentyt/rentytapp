import React from 'react';
import {View} from 'react-native';
import {CalendarList} from 'react-native-calendars';
import {colors} from '../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import moment from 'moment';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

const _format = 'YYYY-MM-DD';
const _today = moment().add(1, 'day').format(_format);

export default function AvailabilityContent({
  _markedDates,
  set_markedDates,
  _markedDatesToSave,
  set_markedDatesToSave,
  setarrayDates,
  arrayDates,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const bg = {
    light: colors.white,
    dark: colors.black,
  };

  const mode = useColorSchemeContext();
  const bgColor = bg[mode];

  const main = {
    light: colors.main,
    dark: colors.main,
  };

  const mainColor = main[mode];

  const text = {
    light: colors.black,
    dark: colors.white,
  };

  const textColor = text[mode];

  const textDisabled = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const TextDisabled = textDisabled[mode];

  const onDaySelect = day => {
    ReactNativeHapticFeedback.trigger(type, optiones);
    const _selectedDay = moment(day.dateString).format(_format);

    let selected = true;
    if (_markedDates[_selectedDay]) {
      selected = !_markedDates[_selectedDay].selected;
    }

    setarrayDates(arrayDates.concat(_selectedDay));

    const updatedMarkedDates = {
      ..._markedDates,
      ...{
        [_selectedDay]: {
          selected,
          color: mainColor,
          textColor: 'white',
          disabled: true,
          disableTouchEvent: true,
        },
      },
    };
    const updatedMarkedDatesToSave = {
      ..._markedDatesToSave,
      ...{
        [_selectedDay]: {
          selected: true,
          disabled: true,
          disableTouchEvent: true,
          marked: true,
          dotColor: colors.main,
          textColor: 'rgb(153,153,153)',
        },
      },
    };
    set_markedDatesToSave(updatedMarkedDatesToSave);
    set_markedDates(updatedMarkedDates);
  };

  return (
    <View style={styles.container}>
      <CalendarList
        futureScrollRange={12}
        pastScrollRange={0}
        markedDates={_markedDates}
        markingType={'period'}
        onDayPress={onDaySelect}
        minDate={_today}
        theme={{
          calendarBackground: bgColor,
          selectedDayBackgroundColor: mainColor,
          selectedDayTextColor: '#ffffff',
          dayTextColor: textColor,
          monthTextColor: textColor,
          textDisabledColor: TextDisabled,
          todayTextColor: mainColor,
        }}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.black),
  },

  content: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
