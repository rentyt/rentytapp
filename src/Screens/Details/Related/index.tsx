import React, {useRef} from 'react';
import {View, ScrollView, Animated} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {colors, stylesText, dimensions} from '../../../Themes';
import {useQuery} from '@apollo/client';
import {query} from '../../../GraphQL';
import CardStore from '../../../Components/AdsCard/Vertical';
import {CustomText} from '../../../Components/CustomText';
import LoadingComponent from '../../../Components/PlaceHolded/ListHome';

export default function Related({datas, navigation}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const scrollY = useRef(new Animated.Value(0)).current;

  const {t} = useTranslation();

  const {data, loading, refetch} = useQuery(query.GET_ALL_ADS, {
    variables: {
      input: {_id: {$ne: datas._id}, owner: datas.owner, status: 'Succees'},
      page: 1,
      limit: 10,
    },
  });

  const ads = data && data.getAds ? data.getAds.data : [];

  const renderItem = (item: any, i: number) => {
    return (
      <CardStore
        data={item}
        navigation={navigation}
        key={i}
        refetch={refetch}
        scrollY={scrollY}
        index={i}
        feed={false}
        style={{marginTop: 15, marginHorizontal: 20}}
      />
    );
  };

  if (loading) {
    return (
      <>
        <View style={{marginHorizontal: 15, marginTop: 20}}>
          <CustomText
            style={stylesText.titleText}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('details:Related')}
          </CustomText>
        </View>
        <View style={styles.content}>
          <LoadingComponent />
        </View>
      </>
    );
  }

  return (
    <>
      {ads.length > 0 ? (
        <>
          <View style={{marginHorizontal: 15, marginTop: 20}}>
            <CustomText
              style={stylesText.titleText}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {t('details:Related')}
            </CustomText>
          </View>
          <View style={styles.content}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              style={{width: '100%'}}>
              {ads &&
                ads.map((item: any, i: number) => {
                  return renderItem(item, i);
                })}
            </ScrollView>
          </View>
        </>
      ) : null}
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    paddingVertical: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: 20,
    width: dimensions.Width(100),
  },
});
