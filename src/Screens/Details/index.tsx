import React, {useState, useContext, useRef, useEffect} from 'react';
import {
  View,
  ScrollView,
  Animated,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {colors, stylesText, dimensions} from '../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import ImageView from './ImagenView';
import Header from './Header';
import TitleAndPrice from './TitleAndPrice';
import Description from './Description';
import Propietary from './Owner';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';
import {MainContext} from '../../store/MainProvider';
import Location from './Location';
import Detail from './Details';
import Policy from './Policy';
import Warranty from './Warranty';
import {Modalize} from 'react-native-modalize';
import ModalsDetails from './Modals/ModalDetails';
import ModalPolicy from './Modals/Modalpolicy';
import ModalInstan from './Modals/ModalInstant';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import ButtomBar from './ButtomBar';
import Instant from './Instantan';
import ExtraEquipment from './ExtraEquipment';
import Icon from 'react-native-dynamic-vector-icons';
import ModalExtra from './Modals/ModalExtra';
import BabySeat from './BabySeat';
import Availability from './Availability';
import Preferences from './Preferences';
import SelecDate from './SelectDate';
import DaySelected from './DaySelected';
import LottieView from 'lottie-react-native';
import source from '../../Assets/animated/heart-burst.json';
import ModalExit from './Modals/ModalExist';
import Resumen from './Payments/Resumens';
import ModalShare from './Modals/ModalShare';
import Offert from './Offerts';
import Anticipation from './Anticipation';
import {useQuery, useMutation} from '@apollo/client';
import {query, mutations} from '../../GraphQL';
import {
  AddStoreToFavorite,
  DeleteStoreToFavorite,
} from '../../Utils/AddFavourite';
import PlaceHoldedLoading from '../../Components/PlaceHolded/PlaceHolderDetails';
import AirPort from './Airport';
import ModalAir from './Modals/ModalAir';
import Toast from 'react-native-simple-toast';
import {MAIN_URL} from '../../Utils/Urls';
import AddComponent from '../../Components/AddCar';
import Related from './Related';
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Details({route, navigation}) {
  const {id} = route.params;

  const {data, loading, refetch, error} = useQuery(query.GET_ADS_ID, {
    variables: {id: id},
  });

  if (error) {
    return navigation.goBack();
  }

  const ads = data && data.getAdsForID ? data.getAdsForID.data : null;

  const [deletedFavourite] = useMutation(mutations.DELETE_FAVOURITE);
  const [createFavoritoAds] = useMutation(mutations.CREATED_FAVOURITE);

  const {users} = useContext(MainContext);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [extraEquipament, setextraEquipament] = useState([]);
  const [BabySeats, setBabySeats] = useState([]);
  const [visibleModal, setvisibleModal] = useState(false);
  const [timePickup, settimePickup] = useState(null);
  const [deliveryTime, setdeliveryTime] = useState(null);
  const [errors, setError] = useState(false);
  const [dataBaby, setdataBaby] = useState([]);
  const [modalEdit, setmodalEdit] = useState(false);

  const modalizeRef = useRef<Modalize>(null);
  const modalizeRefPolicy = useRef<Modalize>(null);
  const modalizeRefInstant = useRef<Modalize>(null);
  const modalizeRefEstra = useRef<Modalize>(null);
  const modalizeRefShare = useRef<Modalize>(null);
  const modalizeAir = useRef<Modalize>(null);

  const [scrollY] = useState(new Animated.Value(0));

  const ActAdd = {
    light: colors.white,
    dark: colors.black,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  const [_markedDatesToSave, set_markedDatesToSave] = useState({});
  const [arrayDates, setarrayDates] = useState([]);
  const [showAnimation, setshowAnimation] = useState(false);
  const [exitmodalVisible, setexitmodalVisible] = useState(false);
  const [modalResumen, setmodalResumen] = useState(false);
  const [descuento, setDescuento] = useState({
    _id: null,
    clave: '',
    descuento: 0,
    tipo: '',
  });
  const [LoadingCupon, setLoadingCupon] = useState(false);
  const [clave, setclave] = useState('');

  arrayDates.sort();

  useEffect(() => {
    if (Platform.OS === 'ios') {
      requestMultiple([PERMISSIONS.IOS.LOCATION_ALWAYS]).then(statuses => {});
      requestMultiple([PERMISSIONS.IOS.LOCATION_WHEN_IN_USE]).then(
        statuses => {},
      );
    } else {
      requestMultiple([PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION]).then(
        statuses => {},
      );
      requestMultiple([PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION]).then(
        statuses => {},
      );
    }
  }, []);

  const getDiscount = async () => {
    setLoadingCupon(true);
    const resp = await fetch(
      `${MAIN_URL}/get-discount?clave=${clave}&user=${users._id}`,
    );
    const desc = await resp.json();

    if (desc.success) {
      setDescuento(desc.data);
      setLoadingCupon(false);
    } else {
      Toast.show(desc.message, Toast.LONG, ['UIAlertController']);
      setLoadingCupon(false);
    }
  };

  const bgcolor = scrollY.interpolate({
    inputRange: [0, 125],
    outputRange: ['transparent', mainColor],
    extrapolate: 'clamp',
  });

  const opacity = scrollY.interpolate({
    inputRange: [100, 1000],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  const {t} = useTranslation();

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const onOpen = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRef.current?.open();
  };

  const onCloset = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRef.current?.close();
  };

  const onOpenPoliciy = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefPolicy.current?.open();
  };

  const onClosetPolicy = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefPolicy.current?.close();
  };

  const onOpenInstant = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefInstant.current?.open();
  };

  const onClosetInstant = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefInstant.current?.close();
  };

  const onOpenAir = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeAir.current?.open();
  };

  const onClosetAir = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeAir.current?.close();
  };

  const onOpenExtra = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefEstra.current?.open();
  };

  const onClosetEstra = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefEstra.current?.close();
  };

  const onOpenShare = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefShare.current?.open();
  };

  const onClosetShare = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefShare.current?.close();
  };

  const StartReservation = () => {
    if (arrayDates.length === 0) {
      setvisibleModal(true);
      setError(true);
      ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    } else {
      //@ts-ignore
      ReactNativeHapticFeedback.trigger(type, optiones);
      setmodalResumen(true);
    }
  };

  const addTofavourite = () => {
    if (users) {
      setshowAnimation(true);
      if (ads.inFavourites) {
        DeleteStoreToFavorite(ads._id, refetch, deletedFavourite, t);
      } else {
        AddStoreToFavorite(
          users._id,
          ads._id,
          refetch,
          createFavoritoAds,
          t,
          navigation,
        );
      }
    } else {
      navigation.navigate('Login');
    }
  };

  if (showAnimation) {
    setTimeout(() => {
      setshowAnimation(false);
    }, 3000);
  }

  const backToStore = async () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (arrayDates.length > 0) {
      setexitmodalVisible(true);
    } else {
      navigation.goBack();
    }
  };

  if (loading) {
    return <PlaceHoldedLoading />;
  }

  return (
    <View style={styles.container}>
      <Header
        bgcolor={bgcolor}
        opacity={opacity}
        title={`${ads.car.marker} ${ads.car.model}`}
        navigation={navigation}
        data={ads}
        addTofavourite={addTofavourite}
        addTofavorite={ads.inFavourites}
        backToStore={backToStore}
        share={onOpenShare}
        users={users}
        setvisibleModal={setmodalEdit}
      />
      {showAnimation ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: dimensions.ScreenHeight,
            width: dimensions.ScreenWidth,
            position: 'absolute',
            zIndex: 1000,
          }}>
          <LottieView
            source={source}
            autoPlay
            loop
            style={{
              width: 400,
              height: 300,
            }}
          />
        </View>
      ) : null}
      <ScrollView
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event([
          {nativeEvent: {contentOffset: {y: scrollY}}},
        ])}
        scrollEventThrottle={16}
        stickyHeaderIndices={[1]}
        keyboardShouldPersistTaps="handled">
        <ImageView data={ads} />
        <TitleAndPrice data={ads} />
        {ads.offerts.length > 0 ? <Offert data={ads} /> : null}
        {ads.anticipation.length > 0 ? <Anticipation data={ads} /> : null}

        {ads.instantBooking ? (
          <Instant data={ads} onPress={onOpenInstant} />
        ) : null}

        {ads.airPortPickup && ads.airPortPickup.available ? (
          <AirPort data={ads} onPress={onOpenAir} />
        ) : null}
        <Availability
          data={ads}
          onPress={setvisibleModal}
          error={errors}
          arrayDates={arrayDates}
        />
        {arrayDates.length > 0 ? (
          <DaySelected
            data={ads}
            user={users}
            arrayDates={arrayDates}
            timePickup={timePickup}
            deliveryTime={deliveryTime}
          />
        ) : null}

        <View style={{marginHorizontal: 15, marginTop: 20}}>
          <CustomText
            style={stylesText.titleText}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('details:description')}
          </CustomText>
        </View>

        <Description data={ads} />

        {users && ads.owner !== users._id ? (
          <>
            <View style={{marginHorizontal: 15, marginTop: 20}}>
              <CustomText
                style={stylesText.titleText}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {t('details:owner')}
              </CustomText>
            </View>
            <Propietary ads={ads} navigation={navigation} users={users} />
          </>
        ) : null}

        <View style={{marginHorizontal: 15, marginTop: 20}}>
          <CustomText
            style={stylesText.titleText}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('details:location')}
          </CustomText>
        </View>
        <Location data={ads} user={users} />
        <View style={{marginHorizontal: 15, marginTop: 20}}>
          <CustomText
            style={stylesText.titleText}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('details:detailsTitle')}
          </CustomText>
        </View>
        <Detail data={ads} user={users} onOpen={onOpen} />
        {ads.ExtraEquipment.length > 0 ? (
          <>
            <TouchableOpacity
              activeOpacity={100}
              onPress={() => onOpenExtra()}
              style={{
                marginHorizontal: 15,
                marginTop: 20,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <CustomText
                style={stylesText.titleText}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {t('details:ExtraEquipmentTitle')}
              </CustomText>

              <Icon
                type="Feather"
                name="help-circle"
                size={20}
                color={colors.rgb_153}
              />
            </TouchableOpacity>

            <ExtraEquipment
              data={ads}
              extraEquipament={extraEquipament}
              setextraEquipament={setextraEquipament}
            />
          </>
        ) : null}

        {/* {ads.babyChairs.length > 0 ? (
          <>
            <TouchableOpacity
              activeOpacity={100}
              onPress={() => onOpenExtra()}
              style={{
                marginHorizontal: 15,
                marginTop: 20,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <CustomText
                style={stylesText.titleText}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {t('details:BabyseatTitle')}
              </CustomText>

              <Icon
                type="Feather"
                name="help-circle"
                size={20}
                color={colors.rgb_153}
              />
            </TouchableOpacity>

            <BabySeat
              data={ads}
              babyChairs={ads.babyChairs}
              BabySeats={BabySeats}
              setBabySeats={setBabySeats}
            />
          </>
        ) : null} */}
        <Preferences data={ads} user={users} />
        <Related datas={ads} navigation={navigation} />
        <Warranty />
        <Policy Toast={Toast} onOpen={onOpenPoliciy} />
      </ScrollView>
      <ModalsDetails data={ads} modalizeRef={modalizeRef} onCloset={onCloset} />
      <ModalPolicy
        data={data}
        modalizeRef={modalizeRefPolicy}
        onCloset={onClosetPolicy}
      />
      <ModalInstan
        data={ads}
        modalizeRef={modalizeRefInstant}
        onCloset={onClosetInstant}
      />

      <ModalAir data={ads} modalizeRef={modalizeAir} onCloset={onClosetAir} />
      <ModalExtra
        data={ads}
        modalizeRef={modalizeRefEstra}
        onCloset={onClosetEstra}
      />
      <ModalShare
        data={ads}
        modalizeRef={modalizeRefShare}
        onCloset={onClosetShare}
      />

      {users && ads.owner !== users._id ? (
        <ButtomBar onPress={StartReservation} data={ads} />
      ) : null}

      <SelecDate
        visibleModal={visibleModal}
        setvisibleModal={setvisibleModal}
        _markedDatesToSave={_markedDatesToSave}
        set_markedDatesToSave={set_markedDatesToSave}
        timePickup={timePickup}
        settimePickup={settimePickup}
        deliveryTime={deliveryTime}
        setdeliveryTime={setdeliveryTime}
        arrayDates={arrayDates}
        setarrayDates={setarrayDates}
        data={ads}
        setError={setError}
      />
      <ModalExit
        modalVisible={exitmodalVisible}
        setModalVisible={setexitmodalVisible}
        setarrayDates={setarrayDates}
        navigation={navigation}
      />
      <Resumen
        visibleModal={modalResumen}
        setvisibleModal={setmodalResumen}
        navigation={navigation}
        data={ads}
        extraEquipament={extraEquipament}
        setextraEquipament={setextraEquipament}
        BabySeats={BabySeats}
        setBabySeats={setBabySeats}
        timePickup={timePickup}
        deliveryTime={deliveryTime}
        arrayDates={arrayDates}
        dataBaby={ads.babyChairs}
        setdataBaby={setdataBaby}
        _markedDatesToSave={_markedDatesToSave}
        descuento={descuento}
        LoadingCupon={LoadingCupon}
        clave={clave}
        setclave={setclave}
        getDiscount={getDiscount}
      />

      <AddComponent
        visibleModal={modalEdit}
        setvisibleModal={setmodalEdit}
        edit={true}
        refetch={refetch}
        data={ads}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.black),
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
