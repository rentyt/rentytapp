import React from 'react';
import {FlatList, View} from 'react-native';
import {colors, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Item from './Items/ItemPreferences';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';

export default function Preferences({data, user}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  const {t} = useTranslation();

  const renderItem = ({item}) => {
    return (
      <>
        {item.active ? (
          <Item
            data={data}
            item={item}
            iconType={item.iconType}
            iconName={item.iconName}
          />
        ) : null}
      </>
    );
  };

  return (
    <>
      <View style={{marginHorizontal: 15, marginTop: 20}}>
        <CustomText
          style={stylesText.titleText}
          light={colors.back_suave_dark}
          dark={colors.white}>
          {t('details:preferencesTitle')}
        </CustomText>
      </View>
      <View
        style={[
          styles.content,
          {borderBottomColor: BorderColor, borderTopColor: BorderColor},
        ]}>
        <FlatList
          data={data.preferences}
          renderItem={(item: any) => renderItem(item)}
          keyExtractor={(item: any) => item.title}
          showsVerticalScrollIndicator={false}
          scrollEnabled={false}
        />
      </View>
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
  },
});
