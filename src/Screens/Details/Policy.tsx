import React from 'react';
import {
  TouchableOpacity,
  View,
  Platform,
  Alert,
  ActionSheetIOS,
} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {OpenURLButton} from '../../Utils/onpenUrl';
import Mailer from 'react-native-mail';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Policy({Toast, onOpen}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();
  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  const MainColor = {
    light: colors.main,
    dark: colors.main,
  };

  const mainColor = MainColor[mode];

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const handleEmail = motive => {
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: `Hola quiero reportar este anuncion por el motivo: ${motive}`,
          recipients: ['report@rentytapp.com'],
          body: `<b>Hola quiero reportar este anuncion por el motivo: ${motive} ID del anuncio: ID</b>`,
          isHTML: true,
        },
        (error, event) => {
          if (event) {
            Toast.show({
              text1: t('details:Emailcorrecto'),
              text2: t('details:Emailcorrecto'),
              position: 'top',
              type: 'success',
              topOffset: 50,
            });
          } else {
            Toast.show({
              text1: t('details:Emailincorrecto'),
              text2: t('details:Emailincorrecto'),
              position: 'top',
              type: 'error',
              topOffset: 50,
            });
          }
        },
      );
    } else {
      OpenURLButton('mailto:info@rentytapp.com');
    }
  };

  const reportAd = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    Alert.alert(
      t('details:reportMessageTitle'),
      t('details:reportMessage'),
      [
        {
          text: t('profile:cancel'),
          onPress: () => console.log('OK Pressed'),
          style: 'cancel',
        },
        {
          text: t('details:okReport'),
          style: 'destructive',
          onPress: async () => {
            onPress();
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
          },
        },
        ,
      ],
      {cancelable: false},
    );
  };

  const onPress = () => {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: t('details:reporRazon'),
          message: t('details:reporRazonMessage'),
          options: [
            t('details:calcel'),
            t('details:Duplicado'),
            t('details:Ofensivo'),
            t('details:CategoriaIncorrecta'),
            t('details:Fraude'),
            t('details:Tipodecuentaequivocada'),
            t('details:Otras'),
          ],
          cancelButtonIndex: 0,
        },
        buttonIndex => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            handleEmail(t('details:Duplicado'));
          } else if (buttonIndex === 2) {
            handleEmail(t('details:Ofensivo'));
          } else if (buttonIndex === 3) {
            handleEmail(t('details:CategoriaIncorrecta'));
          } else if (buttonIndex === 4) {
            handleEmail(t('details:Fraude'));
          } else if (buttonIndex === 5) {
            handleEmail(t('details:Tipodecuentaequivocada'));
          } else if (buttonIndex === 6) {
            handleEmail(t('details:Otras'));
          }
        },
      );
    } else {
      OpenURLButton(
        `mailto:report@rentytapp.com?subject=Reportar anuncio&body=Hola quiero reportar este anuncio con ID `,
      );
    }
  };

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: BorderColor, borderTopColor: BorderColor},
      ]}>
      <TouchableOpacity
        style={[styles.items, {borderBottomColor: BorderColor}]}
        onPress={() => {
          onOpen();
        }}>
        <CustomText
          style={[stylesText.secondaryTextBold]}
          numberOfLines={1}
          light={colors.green}
          dark={colors.green}>
          {t(`details:devolution`)}
        </CustomText>
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          styles.items,
          {borderBottomColor: BorderColor, borderBottomWidth: 0},
        ]}
        onPress={() => reportAd()}>
        <CustomText
          style={[stylesText.secondaryTextBold]}
          numberOfLines={1}
          light={colors.ERROR}
          dark={colors.ERROR}>
          {t(`details:report`)}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: 20,
    marginBottom: dimensions.Height(15),
  },

  items: {
    alignItems: 'center',
    justifyContent: 'center',
    width: dimensions.Width(92),
    borderBottomWidth: 0.5,
    padding: 15,
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
