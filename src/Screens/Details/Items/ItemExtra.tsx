import React from 'react';
import {View, Platform} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../../Components/CustomText';
import {formaterPrice} from '../../../Utils/formaterPrice';
import CheckBox from '@react-native-community/checkbox';
import {deleteItemExtra, AddItemExtra} from '../../../Utils/addItemExtra';
export default function ItemExtra({
  data,
  item,
  title,
  iconType,
  iconName,
  extraEquipament,
  setextraEquipament,
  setarraysDataBaby,
  arraysDataBaby,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const main = {
    light: colors.green,
    dark: colors.green,
  };

  const mainColor = main[mode];

  const ifExist = extraEquipament.filter(x => x.title === title).length > 0;

  return (
    <View style={[styles.items]}>
      <View style={styles.icon}>
        <Icon type={iconType} name={iconName} size={28} color={colors.green} />
      </View>
      <View
        style={{
          marginLeft: 10,
          justifyContent: 'space-between',
          flexDirection: 'row',
          width: dimensions.IsIphoneX()
            ? dimensions.Width(78)
            : dimensions.Width(77),
        }}>
        <View>
          <CustomText
            style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
            numberOfLines={1}
            light={colors.black}
            dark={colors.white}>
            {t(`details:ExtraEquipment:${title}`)}
          </CustomText>
          <CustomText
            style={[stylesText.secondaryText, {marginTop: 5}]}
            numberOfLines={1}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {t(`details:ExtraEquipmentExtra`)}
          </CustomText>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View>
            <CustomText
              style={[stylesText.terciaryText, {fontWeight: 'bold'}]}
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}>
              {formaterPrice(
                item.priceExtra,
                data.prices.localcode,
                data.prices.currency,
              )}
            </CustomText>
            <CustomText
              style={[stylesText.terciaryText, {marginTop: 5}]}
              numberOfLines={3}
              light={colors.black}
              dark={colors.white}>
              / {t('card:day')}
            </CustomText>
          </View>
          <CheckBox
            value={ifExist}
            disabled={false}
            onValueChange={value => {
              if (value) {
                AddItemExtra(
                  extraEquipament,
                  title,
                  item,
                  setextraEquipament,
                  setarraysDataBaby,
                  arraysDataBaby,
                );
              } else {
                deleteItemExtra(
                  extraEquipament,
                  title,
                  setextraEquipament,
                  setarraysDataBaby,
                  arraysDataBaby,
                );
              }
            }}
            onTintColor={mainColor}
            tintColors={{true: mainColor, false: colors.rgb_153}}
            onCheckColor={mainColor}
            onAnimationType="fill"
            offAnimationType="fill"
            style={{marginLeft: 10}}
          />
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingBottom: 10,
    width: dimensions.Width(100),
    marginTop: 15,
  },

  icon: {
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginLeft: 5,
  },
});
