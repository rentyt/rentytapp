import React from 'react';
import {View, Platform} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../../Components/CustomText';

export default function Details({item, dataname, iconType, iconName}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const border = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const borderColor = border[mode];

  return (
    <View style={[styles.items, {borderBottomColor: borderColor}]}>
      <View style={styles.icon}>
        <Icon type={iconType} name={iconName} size={28} color={colors.green} />
      </View>
      <View style={{marginLeft: 15}}>
        <CustomText
          style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}>
          {t(`details:${item}`)}
        </CustomText>
        <CustomText
          style={[stylesText.secondaryText, {color: colors.rgb_153}]}
          numberOfLines={1}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          {dataname}
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    paddingBottom: 10,
    width: dimensions.Width(92),
    marginTop: 15,
  },

  icon: {
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
