import React from 'react';
import {View, Platform} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../../Components/CustomText';

export default function ItemPreference({data, item, iconType, iconName}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  return (
    <View style={[styles.items]}>
      <View style={styles.icon}>
        <Icon type={iconType} name={iconName} size={22} color={colors.ERROR} />
      </View>
      <View
        style={{
          marginLeft: 10,
          justifyContent: 'flex-start',
          flexDirection: 'row',
          width: dimensions.IsIphoneX()
            ? dimensions.Width(81)
            : dimensions.Width(80),
        }}>
        <View>
          <CustomText
            style={[stylesText.secondaryText, {fontWeight: '300'}]}
            numberOfLines={1}
            light={colors.black}
            dark={colors.white}>
            {t(`details:preferences:${item.title}`)}
          </CustomText>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingBottom: 10,
    width: dimensions.Width(96),
    marginTop: 5,
  },

  icon: {
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginLeft: 5,
  },
});
