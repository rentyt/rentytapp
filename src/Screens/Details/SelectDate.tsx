import React, {useRef, useState, useEffect} from 'react';
import {View, Modal, Platform, TouchableOpacity, Alert} from 'react-native';
import HeaderModal from '../../Components/HeaderModal';
import CalendarsList from './AvailabilityContent';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {colors, dimensions, stylesText} from '../../Themes';
import {useTranslation} from 'react-i18next';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {Button} from '../../Components/Buttom';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../Components/CustomText';
import ModalTimePickUp from './Modals/ModalTimePickUp';
import ModalTimeDelivery from './Modals/ModalTimeDelivery';
import {Modalize} from 'react-native-modalize';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function SelectDate({
  visibleModal,
  setvisibleModal,
  _markedDatesToSave,
  set_markedDatesToSave,
  timePickup,
  settimePickup,
  deliveryTime,
  setdeliveryTime,
  data,
  arrayDates,
  setarrayDates,
  setError,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const initialState = {};

  const isBusi = {...initialState, ...data.busyDays};
  const isBusiToSave = {...data.busyDays};
  const [_markedDates, set_markedDates] = useState(isBusi);

  const resetState = () => {
    set_markedDatesToSave(isBusiToSave);
    set_markedDates(isBusi);
    setarrayDates([]);
    settimePickup(null);
    setdeliveryTime(null);
  };

  useEffect(() => {
    set_markedDatesToSave(isBusiToSave);
  }, []);

  const {t} = useTranslation();

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const modalizeRefTimePickUp = useRef<Modalize>(null);
  const modalizeRefTimeDelivery = useRef<Modalize>(null);

  const mode = useColorSchemeContext();

  const main = {
    light: colors.main,
    dark: colors.main,
  };

  const mainColor = main[mode];

  const onOpenTimePickUp = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefTimePickUp.current?.open();
  };

  const onClosetTimePickUp = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefTimePickUp.current?.close();
  };

  const onOpenTimeDelivery = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefTimeDelivery.current?.open();
  };

  const onClosetTimeDelivery = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    modalizeRefTimeDelivery.current?.close();
  };

  const OnPress = () => {
    if (arrayDates.length > 0) {
      Alert.alert(
        t('details:exitModalTitle'),
        t('details:exitModalMEssages'),
        [
          {
            text: t('details:cancel'),
            onPress: () => {},

            style: 'cancel',
          },
          {
            text: t('details:okexit'),
            style: 'destructive',
            onPress: async () => {
              resetState();
              setvisibleModal(false);
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(type, optiones);
            },
          },
          ,
        ],
        {cancelable: false},
      );
    } else {
      resetState();
      setvisibleModal(false);
      //@ts-ignore
      ReactNativeHapticFeedback.trigger(type, optiones);
    }
  };

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      presentationStyle="formSheet"
      collapsable={true}
      statusBarTranslucent={true}
      onRequestClose={OnPress}>
      <HeaderModal
        clean={true}
        onPress1={() => {
          resetState();
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(type, optiones);
        }}
        onPress={OnPress}
        title={t('details:availability')}
        title2={t('details:modal:clear')}
        subtitle={
          data.minDays > 1 ? `${data.minDays} ${t('card:minday')}` : null
        }
        isOK={arrayDates.length >= data.minDays}
        children={
          <View style={styles.contentModal}>
            <View style={{marginBottom: 170}}>
              <CalendarsList
                _markedDates={_markedDates}
                set_markedDates={set_markedDates}
                _markedDatesToSave={_markedDatesToSave}
                set_markedDatesToSave={set_markedDatesToSave}
                arrayDates={arrayDates}
                setarrayDates={setarrayDates}
              />
            </View>
          </View>
        }
      />
      <View style={styles.butonModal}>
        <View
          style={{
            width: dimensions.Width(100),
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 25,
          }}>
          <TouchableOpacity
            onPress={() => {
              if (arrayDates.length >= data.minDays) {
                onOpenTimePickUp();
              } else {
                //@ts-ignore
                ReactNativeHapticFeedback.trigger(type, optiones);
                Alert.alert(t('details:pickuptimeSelect'));
              }
            }}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <Icon
              type="Feather"
              name="clock"
              size={28}
              color={
                arrayDates.length >= data.minDays ? mainColor : colors.rgb_153
              }
            />
            <View style={{marginLeft: 10}}>
              <CustomText
                style={[stylesText.secondaryText]}
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}>
                {t('details:modal:selectHourPickUp')}
              </CustomText>
              <CustomText
                style={[stylesText.secondaryText]}
                numberOfLines={1}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {timePickup ? timePickup : t('details:modal:AddHour')}
              </CustomText>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              if (arrayDates.length >= data.minDays && timePickup) {
                onOpenTimeDelivery();
              } else {
                //@ts-ignore
                ReactNativeHapticFeedback.trigger(type, optiones);
                Alert.alert(t('details:deliverytimeSelect'));
              }
            }}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <Icon
              type="Feather"
              name="clock"
              size={28}
              color={
                arrayDates.length > 0 && timePickup ? mainColor : colors.rgb_153
              }
            />
            <View style={{marginLeft: 10}}>
              <CustomText
                style={[stylesText.secondaryText]}
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}>
                {t('details:modal:selectHourDelivery')}
              </CustomText>
              <CustomText
                style={[stylesText.secondaryText]}
                numberOfLines={1}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {deliveryTime ? deliveryTime : t('details:modal:AddHour')}
              </CustomText>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{marginBottom: 30}}>
          <Button
            light={colors.white}
            dark={colors.white}
            loading={false}
            onPress={() => {
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(type, optiones);
              if (
                timePickup &&
                deliveryTime &&
                arrayDates.length >= data.minDays
              ) {
                setError(false);
                setvisibleModal(false);
              } else {
                //@ts-ignore
                ReactNativeHapticFeedback.trigger(type, optiones);
                if (arrayDates.length < data.minDays) {
                  Alert.alert(
                    t('details:NodaySelected'),
                    t('details:NodaySelectedMessage'),
                  );
                } else if (!timePickup) {
                  Alert.alert(t('details:pickuptimeSelect'));
                } else if (!deliveryTime) {
                  Alert.alert(t('details:deliverytimeSelect'));
                }
              }
            }}
            title={t('details:modal:titleBTN')}
          />
        </View>
      </View>
      <ModalTimePickUp
        modalizeRef={modalizeRefTimePickUp}
        onCloset={onClosetTimePickUp}
        timePickup={timePickup}
        settimePickup={settimePickup}
      />
      <ModalTimeDelivery
        modalizeRef={modalizeRefTimeDelivery}
        onCloset={onClosetTimeDelivery}
        timePickup={deliveryTime}
        settimePickup={setdeliveryTime}
      />
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  butonModal: {
    width: dimensions.Width(100),
    height: 180,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    bottom: -5,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
  },
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.black),
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.IsIphoneX()
      ? dimensions.Width(4)
      : dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(16),
    textAlign: 'center',
  },
});
