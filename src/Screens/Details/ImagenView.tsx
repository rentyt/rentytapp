import React, {useState} from 'react';
import {colors, dimensions} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
//@ts-ignore
import {SliderBox} from 'react-native-image-slider-box';
import ImageView from 'react-native-image-viewing';
import {Platform} from 'react-native';
import {View} from 'react-native';

export default function Details({data}) {
  const [visible, setIsVisible] = useState(false);
  const [index, setindex] = useState(0);

  const styles = useDynamicStyleSheet(dynamicStyles);

  const openViewImagen = i => {
    setindex(i);
    setIsVisible(true);
  };

  const ActAdd = {
    light: colors.main,
    dark: colors.main,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  return (
    <View>
      <SliderBox
        images={data.images}
        sliderBoxHeight={300}
        activeOpacity={100}
        resizeMethod={'resize'}
        resizeMode={'cover'}
        onCurrentImagePressed={index => openViewImagen(index)}
        dotColor={mainColor}
        inactiveDotColor={colors.rgb_235}
        imageLoadingColor={colors.rgb_235}
        ImageComponentStyle={{
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          marginTop: Platform.select({
            ios: dimensions.IsIphoneX() ? 38 : 24,
            android: 36,
          }),
        }}
      />
      <ImageView
        images={data.images}
        imageIndex={index}
        visible={visible}
        presentationStyle="overFullScreen"
        onRequestClose={() => setIsVisible(false)}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({});
