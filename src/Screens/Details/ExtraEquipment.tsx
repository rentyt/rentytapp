import React, {useState} from 'react';
import {FlatList, View} from 'react-native';
import {colors} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Item from './Items/ItemExtra';

export default function Details({data, extraEquipament, setextraEquipament}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const [arraysDataBaby, setarraysDataBaby] = useState(
    JSON.parse(JSON.stringify(data.ExtraEquipment)),
  );

  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  const renderItem = ({item}) => {
    return (
      <Item
        data={data}
        item={item}
        title={item.title}
        iconType={item.iconType}
        iconName={item.iconName}
        extraEquipament={extraEquipament}
        setextraEquipament={setextraEquipament}
        setarraysDataBaby={setarraysDataBaby}
        arraysDataBaby={arraysDataBaby}
      />
    );
  };

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: BorderColor, borderTopColor: BorderColor},
      ]}>
      <FlatList
        data={arraysDataBaby}
        renderItem={(item: any) => renderItem(item)}
        keyExtractor={(item: any) => item.title}
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
  },
});
