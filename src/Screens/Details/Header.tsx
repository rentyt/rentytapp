import React from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  Animated,
} from 'react-native';
import {colors, dimensions} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {MAIN_URL} from '../../Utils/Urls';

export default function Header({
  bgcolor,
  title,
  opacity,
  navigation,
  data,
  addTofavourite,
  addTofavorite,
  backToStore,
  share,
  users,
  setvisibleModal,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const mode = useColorSchemeContext();

  const text = {
    light: colors.black,
    dark: colors.white,
  };

  const textColor = text[mode];

  const icon = {
    light: colors.black,
    dark: colors.white,
  };

  const iconColor = icon[mode];

  const createdView = async () => {
    if (users) {
      const datos = {
        adsID: data._id,
        userID: users && users._id,
      };
      await fetch(`${MAIN_URL}/created-view`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(datos),
      });
    }
  };

  return (
    <Animated.View style={[styles.container, {backgroundColor: bgcolor}]}>
      <SafeAreaView style={styles.containerIcon}>
        <Animated.View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingTop: Platform.select({
              ios: dimensions.Height(0),
              android: dimensions.Height(4),
            }),
          }}>
          <TouchableOpacity
            onPress={() => {
              backToStore();
              if (users && users._id !== data.owner) {
                createdView();
              }
            }}
            style={styles.icon}>
            <Icon
              name="arrow-left"
              type="Feather"
              size={24}
              color={iconColor}
            />
          </TouchableOpacity>
          <Animated.Text
            numberOfLines={1}
            style={{
              opacity: opacity,
              fontWeight: 'bold',
              textAlign: 'center',
              marginLeft: 40,
              width: dimensions.Width(30),
              color: textColor,
              fontSize: 18,
              fontFamily: 'Poppins',
            }}>
            {title}
          </Animated.Text>
          <View
            style={{
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
              width: 90,
              marginRight: 7,
            }}>
            {users && data.owner === users._id ? (
              <TouchableOpacity
                style={styles.icon}
                onPress={() => setvisibleModal(true)}>
                <Icon type="Feather" name="edit" size={24} color={iconColor} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={styles.icon} onPress={addTofavourite}>
                {addTofavorite ? (
                  <Icon
                    type="FontAwesome"
                    name="bookmark"
                    size={24}
                    color={colors.main}
                  />
                ) : (
                  <Icon
                    type="FontAwesome"
                    name="bookmark-o"
                    size={24}
                    color={iconColor}
                  />
                )}
              </TouchableOpacity>
            )}

            <TouchableOpacity style={styles.icon} onPress={share}>
              <Icon type="Feather" name="share" size={24} color={iconColor} />
            </TouchableOpacity>
          </View>
        </Animated.View>
      </SafeAreaView>
    </Animated.View>
  );
}

const color1 = 'rgba(226, 226, 226, 0.5)';
const color2 = 'rgba(43, 43, 43, 0.445)';

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.Width(100),
    position: 'absolute',
    paddingTop: Platform.select({
      ios: dimensions.IsIphoneX() ? dimensions.Height(0) : dimensions.Height(4),
      android: dimensions.Height(2),
    }),
    zIndex: 100,
    paddingBottom: 15,
  },

  containerIcon: {
    width: dimensions.Width(96),
    marginHorizontal: 10,
  },

  icons: {
    color: new DynamicValue(colors.black, colors.white),
  },

  icon: {
    width: 40,
    height: 40,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 14,
    elevation: 23,
  },
});
