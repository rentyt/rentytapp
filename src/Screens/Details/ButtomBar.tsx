import React from 'react';
import {TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {colors, dimensions, stylesText} from '../../Themes';
import {CustomText} from '../../Components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';

export default function ButtomBar({onPress, data}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 1}}
      colors={[colors.main, colors.secundary]}
      style={[styles.buttonView]}>
      <TouchableOpacity onPress={onPress} activeOpacity={0.8}>
        <CustomText
          light={colors.white}
          dark={colors.white}
          style={[
            stylesText.titleText,
            {
              fontWeight: '500',
              marginBottom: dimensions.IsIphoneX() ? 15 : 5,
            },
          ]}>
          {data.instantBooking ? (
            <Icon type="Entypo" name="flash" size={26} color={colors.white} />
          ) : null}
          {t('details:reservation')}
        </CustomText>
      </TouchableOpacity>
    </LinearGradient>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  buttonView: {
    position: 'absolute',
    bottom: 0,
    width: dimensions.ScreenWidth,
    height: dimensions.IsIphoneX() ? 90 : 60,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 20,
    elevation: 67,
  },
});
