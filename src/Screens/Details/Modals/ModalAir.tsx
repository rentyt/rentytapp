import React, {useState} from 'react';
import {View, Platform} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {Modalize} from 'react-native-modalize';
import {Button} from '../../../Components/Buttom';
import {CustomText} from '../../../Components/CustomText';

export default function ModalAir({data, modalizeRef, onCloset}) {
  const [handle, setHandle] = useState(false);
  const [toggle] = useState(true);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const HEADER_HEIGHT = 300;

  const {t} = useTranslation();

  const handlePosition = position => {
    setHandle(position === 'top');
  };

  return (
    <Modalize
      ref={modalizeRef}
      adjustToContentHeight={toggle}
      snapPoint={HEADER_HEIGHT}
      scrollViewProps={{showsVerticalScrollIndicator: false}}
      modalStyle={styles.modals}
      withHandle={handle}
      modalTopOffset={100}
      handlePosition="inside"
      handleStyle={{
        top: 25,
        width: 40,
        height: handle ? 6 : 6,
        backgroundColor: colors.main,
      }}
      onPositionChange={handlePosition}>
      <View style={styles.contentModal}>
        <View
          style={{
            width: dimensions.Width(92),
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 0,
          }}>
          <View
            style={{
              marginBottom: 30,
            }}>
            <CustomText
              style={[stylesText.secondaryTextBold]}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {t(`card:airPort`)}
            </CustomText>

            <View style={[styles.item]}>
              <CustomText
                style={[stylesText.titleText200]}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {t(`details:airPortMessage`)}
              </CustomText>
            </View>
          </View>
          <View
            style={{
              marginBottom: Platform.select({
                ios: 0,
                android: 50,
              }),
            }}>
            <Button
              light={colors.white}
              dark={colors.white}
              loading={false}
              onPress={() => onCloset()}
              title={t('details:btn')}
            />
          </View>
        </View>
      </View>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modals: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contentModal: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginTop: 30,
    paddingBottom: dimensions.Height(5),
    padding: 15,
  },

  item: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    width: dimensions.Width(92),
    marginTop: 15,
  },
});
