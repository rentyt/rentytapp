import React, {useState} from 'react';
import {
  View,
  Platform,
  TouchableOpacity,
  Linking,
  Alert,
  Image,
  ImageBackground,
} from 'react-native';
import {colors, dimensions, stylesText, image} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
  useColorSchemeContext,
  useDynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../../Components/CustomText';
import {Modalize} from 'react-native-modalize';
import {Button} from '../../../Components/Buttom';
import Icon from 'react-native-dynamic-vector-icons';
import Share from 'react-native-share';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import SendSMS from 'react-native-sms';
import Mailer from 'react-native-mail';
import {OpenURLButton} from '../../../Utils/onpenUrl';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ModalShare({data, modalizeRef, onCloset}) {
  const [handle, setHandle] = useState(false);
  const [toggle] = useState(true);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const HEADER_HEIGHT = 300;

  const {t} = useTranslation();

  const handlePosition = position => {
    setHandle(position === 'top');
  };

  const logoUri = new DynamicValue(
    image.IconPlaceHoldedLigth,
    image.IconPlaceHoldedDark,
  );

  const source = useDynamicValue(logoUri);

  const mode = useColorSchemeContext();

  const border = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const borderColor = border[mode];

  const main = {
    light: colors.back_dark,
    dark: colors.white,
  };

  const mainColor = main[mode];

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const url = `https://rentytapp.com/ads/${data._id}`;
  const message = `${data.car.marker} ${data.car.model} ${data.car.year} | ${t(
    'details:shareMessage',
  )} `;

  let urlWhatsApp = `whatsapp://send?text=${message} ${url}'`;

  const whatsappShare = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    Linking.openURL(urlWhatsApp)
      .then(data => {
        console.log('WhatsApp Opened');
      })
      .catch(() => {
        Alert.alert('Upss', t('details:wthMessage'));
      });
  };

  const optionsShare = Platform.select({
    ios: {
      activityItemSources: [
        {
          placeholderItem: {
            type: 'text',
            content: `${message} ${url}`,
          },
          item: {
            default: {
              type: 'text',
              content: `${message} ${url}`,
            },
          },
          linkMetadata: {
            title: message,
          },
        },
      ],
    },
    default: {
      message: `${message} ${url}`,
    },
  });

  const onShare = async () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    //@ts-ignore
    Share.open(optionsShare)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const sendSms = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    SendSMS.send(
      {
        body: `${message} ${url}`,
        recipients: [],
        //@ts-ignore
        successTypes: ['sent', 'queued'],
        allowAndroidSendWithoutReadPermission: true,
      },
      (completed, cancelled, error) => {
        console.log(
          'SMS Callback: completed: ' +
            completed +
            ' cancelled: ' +
            cancelled +
            'error: ' +
            error,
        );
      },
    );
  };

  const handleEmail = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: message,
          recipients: [],
          body: `<b>${message} ${url}</b>`,
          isHTML: true,
        },
        (error, event) => {},
      );
    } else {
      OpenURLButton(`mailto:""?subject=${message}&body=${message} ${url}`);
    }
  };

  const publishOnFB = () => {
    const urls = `https://www.facebook.com/sharer/sharer.php?u=${url}`;
    Linking.openURL(urls);
  };

  return (
    <Modalize
      ref={modalizeRef}
      adjustToContentHeight={toggle}
      snapPoint={HEADER_HEIGHT}
      scrollViewProps={{showsVerticalScrollIndicator: false}}
      modalStyle={styles.modals}
      withHandle={handle}
      modalTopOffset={100}
      handlePosition="inside"
      handleStyle={{
        top: 25,
        width: 40,
        height: handle ? 6 : 6,
        backgroundColor: colors.main,
      }}
      onPositionChange={handlePosition}>
      <View style={styles.contentModal}>
        <View style={{marginHorizontal: 20}}>
          <CustomText
            style={[stylesText.secondaryTextBold]}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t(`details:share:sharetitle`)}
          </CustomText>

          <View style={{marginVertical: 20, flexDirection: 'row'}}>
            <View style={styles.mainImages}>
              <ImageBackground
                source={source}
                imageStyle={{borderRadius: 15}}
                style={styles.mainImage}
                resizeMode="cover">
                <Image
                  source={{uri: data.images[0].uri}}
                  style={styles.mainImage}
                  resizeMode="cover"
                />
              </ImageBackground>
            </View>
            <View style={{marginLeft: 10}}>
              <CustomText
                style={[stylesText.secondaryTextBold]}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {data.car.marker}
              </CustomText>
              <CustomText
                style={[stylesText.secondaryText]}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {data.car.model} {data.car.year}
              </CustomText>
              <CustomText
                style={[stylesText.secondaryText]}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {data.city}
              </CustomText>
            </View>
          </View>

          <TouchableOpacity
            onPress={whatsappShare}
            style={[styles.item, {borderColor: borderColor}]}>
            <View>
              <CustomText
                style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
                numberOfLines={2}
                light={colors.black}
                dark={colors.white}>
                {t(`details:share:whatsapp`)}
              </CustomText>
            </View>
            <View>
              <Image source={image.WahtsApp} style={styles.icons} />
            </View>
          </TouchableOpacity>

          {Platform.OS === 'ios' ? (
            <TouchableOpacity
              onPress={sendSms}
              style={[styles.item, {borderColor: borderColor}]}>
              <View>
                <CustomText
                  style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
                  numberOfLines={2}
                  light={colors.black}
                  dark={colors.white}>
                  {t(`details:share:sms`)}
                </CustomText>
              </View>
              <View>
                <Image source={image.SMS} style={styles.icons} />
              </View>
            </TouchableOpacity>
          ) : null}

          <TouchableOpacity
            onPress={handleEmail}
            style={[styles.item, {borderColor: borderColor}]}>
            <View>
              <CustomText
                style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
                numberOfLines={2}
                light={colors.black}
                dark={colors.white}>
                {t(`details:share:mail`)}
              </CustomText>
            </View>
            <View>
              <Image source={image.EmailIcon} style={styles.icons} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={publishOnFB}
            style={[styles.item, {borderColor: borderColor}]}>
            <View>
              <CustomText
                style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
                numberOfLines={2}
                light={colors.black}
                dark={colors.white}>
                {t(`details:share:facebook`)}
              </CustomText>
            </View>
            <View>
              <Image source={image.FaceBook} style={styles.icons} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={onShare}
            style={[styles.item, {borderColor: borderColor}]}>
            <View>
              <CustomText
                style={[stylesText.secondaryText, {fontWeight: 'bold'}]}
                numberOfLines={2}
                light={colors.black}
                dark={colors.white}>
                {t(`details:share:other`)}
              </CustomText>
            </View>
            <View>
              <Icon
                type="Feather"
                name="more-horizontal"
                size={40}
                color={mainColor}
              />
            </View>
          </TouchableOpacity>

          <View
            style={{
              width: dimensions.Width(90),
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 50,
            }}>
            <View
              style={{
                marginBottom: Platform.select({
                  ios: 0,
                  android: 50,
                }),
              }}>
              <Button
                light={colors.white}
                dark={colors.white}
                loading={false}
                onPress={() => onCloset()}
                title={t('details:share:btn')}
              />
            </View>
          </View>
        </View>
      </View>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
  },

  modals: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
  },

  contentModal: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
    marginTop: 50,
    paddingBottom: dimensions.Height(5),
  },

  item: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderWidth: 1,
    width: dimensions.Width(90),
    borderRadius: 10,
    marginTop: 10,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    paddingHorizontal: 10,
    paddingVertical: 10,
  },

  mainImages: {
    width: 80,
    height: 80,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  mainImage: {
    width: 76,
    height: 76,
    borderRadius: 15,
  },

  icons: {
    width: 40,
    height: 40,
    borderRadius: 10,
  },
});
