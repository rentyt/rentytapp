import React from 'react';
import {View, Modal, TouchableOpacity, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import LottieView from 'lottie-react-native';
import {Button} from '../../../Components/Buttom';
import {useTranslation} from 'react-i18next';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ModalExist(props) {
  const {modalVisible, setModalVisible, setarrayDates, navigation} = props;
  const styles = useDynamicValue(dynamicStyles);

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const exitStore = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setarrayDates([]);
    navigation.goBack();
  };

  const {t} = useTranslation();

  return (
    <Modal
      animationType="fade"
      visible={modalVisible}
      statusBarTranslucent={true}
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.centeredView}>
        <View style={styles.content}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.titleText, {textAlign: 'center'}]}>
              {t('details:quetionExit')}
            </CustomText>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <LottieView
              source={require('../../../Assets/animated/LoadingModal.json')}
              autoPlay
              loop
              style={{width: 400}}
            />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryText, {textAlign: 'center'}]}>
              {t('details:quetionExitMessage')}
            </CustomText>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={styles.buttonView}
                onPress={() => exitStore()}
                title={t('details:quetionBtn')}
                titleStyle={styles.buttonTitle}
              />
            </View>
            <TouchableOpacity
              style={{marginTop: 30}}
              onPress={() => {
                setModalVisible(false);
                //@ts-ignore
                ReactNativeHapticFeedback.trigger(type, optiones);
              }}>
              <CustomText
                light={colors.green}
                dark={colors.green}
                style={[
                  stylesText.secondaryTextBold,
                  {textAlign: 'center', color: colors.green},
                ]}>
                {t('details:quetionBtnText')}
              </CustomText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  centeredView: {
    backgroundColor: new DynamicValue(colors.white, 'black'),
    flex: 1,
  },

  content: {
    justifyContent: 'space-between',
    flexDirection: 'column',
    height: dimensions.Height(70),
    marginTop: dimensions.Height(15),
    marginBottom: dimensions.Height(10),
    marginHorizontal: dimensions.Width(5),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});
