import React, {useState} from 'react';
import {View, Platform} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {Modalize} from 'react-native-modalize';
import {Button} from '../../../Components/Buttom';
import {CustomText} from '../../../Components/CustomText';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import {ButtonInactive} from '../../../Components/Buttom/Bottom';

export default function ModalTimePickUp({
  modalizeRef,
  onCloset,
  settimePickup,
  timePickup,
}) {
  const [date, setdate] = useState(new Date());
  const [handle, setHandle] = useState(false);
  const [toggle] = useState(true);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const HEADER_HEIGHT = 300;

  const {t} = useTranslation();

  const handlePosition = position => {
    setHandle(position === 'top');
  };

  const colorsText = {
    light: colors.black,
    dark: colors.white,
  };

  const mode = useColorSchemeContext();
  const backgroundColor = colorsText[mode];

  const setDateTime = time => {
    setdate(time);
    const tm = moment(time).format('HH:mm');
    settimePickup(tm);
  };

  return (
    <Modalize
      ref={modalizeRef}
      adjustToContentHeight={toggle}
      snapPoint={HEADER_HEIGHT}
      scrollViewProps={{showsVerticalScrollIndicator: false}}
      modalStyle={styles.modals}
      withHandle={handle}
      modalTopOffset={100}
      handlePosition="inside"
      handleStyle={{
        top: 25,
        width: 40,
        height: handle ? 6 : 6,
        backgroundColor: colors.main,
      }}
      onPositionChange={handlePosition}>
      <View style={styles.contentModal}>
        <View
          style={{
            width: dimensions.Width(92),
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 0,
          }}>
          <View style={{marginBottom: 30}}>
            <CustomText
              style={[stylesText.secondaryTextBold]}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {t(`details:modal:selectHourPickUp`)}
            </CustomText>

            <View style={[styles.item]}>
              <DatePicker
                style={{marginLeft: 30}}
                mode="time"
                locale="es"
                date={date}
                androidVariant={
                  Platform.OS === 'android' ? 'nativeAndroid' : 'iosClone'
                }
                onDateChange={setDateTime}
                onCancel={() => {
                  onCloset();
                }}
                //@ts-ignore
                pickerContainerStyleIOS={{
                  justifyContent: 'center',
                  paddingHorizontal: 40,
                }}
                textColor={backgroundColor}
              />
            </View>
          </View>
          {timePickup ? (
            <Button
              light={colors.white}
              dark={colors.white}
              loading={false}
              onPress={() => onCloset()}
              title={t('details:modal:titleBTN')}
            />
          ) : (
            <ButtonInactive
              light={colors.white}
              dark={colors.white}
              loading={false}
              title={t('details:modal:titleBTN')}
            />
          )}
        </View>
      </View>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  modals: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contentModal: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginTop: 30,
    paddingBottom: dimensions.Height(5),
    padding: 15,
  },

  item: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    width: dimensions.Width(92),
    marginTop: 15,
  },
});
