import React, {useState} from 'react';
import {View, FlatList, Platform} from 'react-native';
import {colors, dimensions} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import Equipemnet from '../Equipemnet';
import {Modalize} from 'react-native-modalize';
import {Button} from '../../../Components/Buttom';

export default function Details({data, modalizeRef, onCloset}) {
  const [handle, setHandle] = useState(false);
  const [toggle] = useState(true);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const HEADER_HEIGHT = 300;

  const {t} = useTranslation();

  const renderItem = ({item}) => {
    return <>{item.active ? <Equipemnet item={item} /> : null}</>;
  };

  const handlePosition = position => {
    setHandle(position === 'top');
  };

  return (
    <Modalize
      ref={modalizeRef}
      adjustToContentHeight={toggle}
      snapPoint={HEADER_HEIGHT}
      scrollViewProps={{showsVerticalScrollIndicator: false}}
      modalStyle={styles.modals}
      withHandle={handle}
      modalTopOffset={100}
      handlePosition="inside"
      handleStyle={{
        top: 25,
        width: 40,
        height: handle ? 6 : 6,
        backgroundColor: colors.main,
      }}
      onPositionChange={handlePosition}>
      <View style={styles.contentModal}>
        <FlatList
          data={data.equipemnet}
          renderItem={(item: any) => renderItem(item)}
          keyExtractor={(item: any) => item.title}
          showsVerticalScrollIndicator={false}
        />
        <View
          style={{
            width: dimensions.Width(100),
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 30,
          }}>
          <View
            style={{
              marginBottom: Platform.select({
                ios: 0,
                android: 50,
              }),
            }}>
            <Button
              light={colors.white}
              dark={colors.white}
              loading={false}
              onPress={() => onCloset()}
              title={t('details:btn')}
            />
          </View>
        </View>
      </View>
    </Modalize>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
  },

  modals: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contentModal: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginTop: 50,
    paddingBottom: dimensions.Height(5),
  },

  items: {
    alignItems: 'center',
    justifyContent: 'center',
    width: dimensions.Width(92),
    marginTop: 15,
  },
});
