import React from 'react';
import {View} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';

export default function Equipemnet({item}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const border = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const borderColor = border[mode];

  const main = {
    light: colors.green,
    dark: colors.green,
  };

  const mainColor = main[mode];

  return (
    <View style={[styles.items, {borderBottomColor: borderColor}]}>
      <View style={{marginLeft: 15}}>
        <CustomText
          style={[stylesText.titleText200, {fontWeight: 'bold'}]}
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}>
          <Icon
            type="Feather"
            name="chevrons-right"
            size={14}
            color={mainColor}
          />{' '}
          {t(`details:equipemnet:${item.title}`)}
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    paddingBottom: 10,
    width: dimensions.Width(100),
    marginTop: 15,
  },
});
