import React, {useState, useContext} from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';
import LinearGradient from 'react-native-linear-gradient';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Icon from 'react-native-dynamic-vector-icons';
import {MAIN_URL} from '../../Utils/Urls';
import {MainContext} from '../../store/MainProvider';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Description({data}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {languaje} = useContext(MainContext);

  const [line, setline] = useState(4);
  const [more, setmore] = useState(true);
  const [translated, settranslated] = useState(false);
  const [description, setdescription] = useState(data.description);

  const setMore = () => {
    setline(25);
    setmore(false);
  };

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const Main = {
    light: colors.green,
    dark: colors.green,
  };

  const mainsColor = Main[mode];

  const ActAdd = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const mainColor = ActAdd[mode];

  const shasow = {
    light: '#fafafa34',
    dark: '#16161618',
  };

  const shasowColor = shasow[mode];

  const shasowTuo = {
    light: colors.white,
    dark: colors.back_dark,
  };

  const shasowTuoColor = shasowTuo[mode];

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const trsnlateText = async (text: string, translateds: boolean) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);

    settranslated(translateds);
    const datas = {
      text: text,
      target: languaje,
    };

    const resp = await fetch(`${MAIN_URL}/translate-text`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(datas),
    });

    const result = await resp.json();

    if (translated) {
      setdescription(data.description);
    } else {
      setdescription(result.text);
    }
    setline(25);
    setmore(false);
  };

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: mainColor, borderTopColor: mainColor},
      ]}>
      <View
        style={{
          width: dimensions.Width(92),
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View />
        <TouchableOpacity
          onPress={() => trsnlateText(data.description, !translated)}>
          <CustomText
            style={[stylesText.secondaryText]}
            numberOfLines={1}
            light={mainsColor}
            dark={mainsColor}>
            <Icon
              type="MaterialCommunityIcons"
              name="google-translate"
              size={20}
              color={mainsColor}
            />{' '}
            {translated ? t('details:untranslate') : t('details:translate')}
          </CustomText>
        </TouchableOpacity>
      </View>
      <CustomText
        style={[stylesText.titleText200, {marginTop: 15, fontWeight: '300'}]}
        numberOfLines={line}
        light={colors.rgb_153}
        dark={colors.rgb_153}>
        {description}
      </CustomText>
      {more ? (
        <TouchableOpacity
          activeOpacity={100}
          onPress={() => {
            setMore();
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
          }}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 0, y: 1}}
            colors={[shasowColor, shasowTuoColor]}
            style={[styles.more]}>
            <CustomText
              style={[stylesText.secondaryTextBold, {marginTop: 40}]}
              numberOfLines={4}
              light={mainsColor}
              dark={mainsColor}>
              {t('details:readMore')}
            </CustomText>
          </LinearGradient>
        </TouchableOpacity>
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
  },

  more: {
    width: dimensions.Width(100),
    height: dimensions.Height(9),
    marginLeft: -15,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -60,
  },
});
