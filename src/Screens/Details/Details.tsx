import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {colors, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import Item from './Items/ItemDetails';
import {CustomText} from '../../Components/CustomText';
import GooglePay from '../PaymentMethod/googlePay';

export default function Details({data, user, onOpen}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  const MainColor = {
    light: colors.green,
    dark: colors.green,
  };

  const mainColor = MainColor[mode];

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: BorderColor, borderTopColor: BorderColor},
      ]}>
      <Item
        item={data.details.enrollment.title}
        dataname={data.details.enrollment.data}
        iconType={data.details.enrollment.iconType}
        iconName={data.details.enrollment.iconName}
      />

      <Item
        item={data.details.combustible.title}
        dataname={data.details.combustible.data}
        iconType={data.details.combustible.iconType}
        iconName={data.details.combustible.iconName}
      />

      <Item
        item={data.details.transmission.title}
        dataname={data.details.transmission.data}
        iconType={data.details.transmission.iconType}
        iconName={data.details.transmission.iconName}
      />

      <Item
        item={data.details.plazas.title}
        dataname={data.details.plazas.data}
        iconType={data.details.plazas.iconType}
        iconName={data.details.plazas.iconName}
      />

      <Item
        item={data.details.year.title}
        dataname={data.details.year.data}
        iconType={data.details.year.iconType}
        iconName={data.details.year.iconName}
      />

      <TouchableOpacity
        style={styles.items}
        onPress={() => {
          onOpen();
        }}>
        <CustomText
          style={[stylesText.secondaryTextBold]}
          numberOfLines={1}
          light={mainColor}
          dark={mainColor}>
          {t(`details:allEquipemnet`)}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
  },

  items: {
    alignItems: 'center',
    justifyContent: 'center',
    width: dimensions.Width(92),
    marginTop: 15,
  },
});
