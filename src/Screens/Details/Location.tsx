import React from 'react';
import {View, Image, Platform} from 'react-native';
import {colors, image, dimensions, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {customMaspStyles} from '../../Components/MapStyle';
import {CustomText} from '../../Components/CustomText';

export default function Location({data, user}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const stylos = useDynamicValue(customMaspStyles);

  const {t} = useTranslation();

  const ActAdd = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  const lat =
    data.location &&
    data.location.geometry &&
    data.location.geometry.location.lat
      ? data.location.geometry.location.lat
      : 18.4860575;
  const lgn =
    data.location &&
    data.location.geometry &&
    data.location.geometry.location.lng
      ? data.location.geometry.location.lng
      : -69.9312117;

  return (
    <View
      style={[
        styles.content,
        {borderBottomColor: mainColor, borderTopColor: mainColor},
      ]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 15,
        }}>
        <View style={styles.icon}>
          <Icon type="Feather" name="map-pin" size={20} color={colors.green} />
        </View>
        <CustomText
          style={[stylesText.titleText200, {marginLeft: 5, fontWeight: 'bold'}]}
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}>
          {data.city}
        </CustomText>
      </View>

      <View style={styles.mapContent}>
        <MapView
          showsUserLocation={true}
          provider={PROVIDER_GOOGLE}
          customMapStyle={stylos}
          style={{
            height: dimensions.Height(15),
            width: dimensions.Width(92),
            borderRadius: 20,
          }}
          region={{
            latitude: lat,
            longitude: lgn,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
          <Marker
            coordinate={{
              latitude: lat,
              longitude: lgn,
            }}>
            <Image source={image.MapPin} style={{width: 50, height: 50}} />
          </Marker>
        </MapView>
      </View>
      <CustomText
        style={[
          stylesText.secondaryText,
          {color: colors.rgb_153, marginTop: 20},
        ]}
        numberOfLines={5}
        light={colors.rgb_153}
        dark={colors.rgb_153}>
        {t('details:locationInfo')}
      </CustomText>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    marginTop: 20,
  },

  mapContent: {
    overflow: 'hidden',
    height: dimensions.Height(15),
    width: dimensions.Width(92),
    borderRadius: 20,
  },

  icon: {
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
