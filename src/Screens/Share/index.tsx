import React, {useState, useEffect} from 'react';
import {View, ScrollView, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';
import Header from '../../Components/Header';
import {dimensions, colors, stylesText} from '../../Themes';
import {Button} from '../../Components/Buttom';
import Confetti from 'react-native-confetti';
import LottieView from 'lottie-react-native';
import source from '../../Assets/animated/gitf.json';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Share from 'react-native-share';
import {useTranslation} from 'react-i18next';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const Comparte = ({navigation}) => {
  const [_confettiView, set_confettiView] = useState(null);
  const styles = useDynamicValue(dynamicStyles);

  const {t} = useTranslation();

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  useEffect(() => {
    if (_confettiView) {
      _confettiView.startConfetti();
    }
  }, [_confettiView]);

  const message = t('share:mgs');

  const url = 'http://onelink.to/z3awpp';

  const optionsShare = Platform.select({
    ios: {
      activityItemSources: [
        {
          placeholderItem: {
            type: 'text',
            content: `${message} ${url}`,
          },
          item: {
            default: {
              type: 'text',
              content: `${message}`,
            },
          },
          linkMetadata: {
            title: message,
          },
        },
      ],
    },
    default: {
      message: `${message}`,
    },
  });

  const onShare = async () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    //@ts-ignore
    Share.open(optionsShare)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <View style={styles.container}>
      <Confetti ref={node => set_confettiView(node)} />
      <Header title={t('share:headerTitle')} navigation={navigation} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            alignSelf: 'center',
            marginTop: dimensions.Height(10),
            alignItems: 'center',
          }}>
          <LottieView
            source={source}
            autoPlay
            loop
            style={{width: 250, height: 250}}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={{
              textAlign: 'center',
              fontSize: dimensions.FontSize(24),
              fontWeight: '900',
              marginTop: 20,
              paddingHorizontal: 30,
              marginBottom: 15,
            }}>
            {t('share:title')}
          </CustomText>

          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              {paddingHorizontal: 20, textAlign: 'center'},
            ]}>
            {t('share:message')}
          </CustomText>
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              onPress={() => onShare()}
              title={t('share:btn')}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
  buttonView: {
    backgroundColor: colors.main,
    width: dimensions.Width(90),
    borderRadius: dimensions.Width(2),
  },
  buttonTitle: {
    alignSelf: 'center',
    marginHorizontal: dimensions.Width(4),
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
    textAlign: 'center',
  },
});

export default Comparte;
