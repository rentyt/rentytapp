import React, {useState} from 'react';
import {View, Modal, Platform} from 'react-native';
import HeaderModal from '../../Components/HeaderModal';
import CalendarsList from './AvailabilityContent';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import {colors, dimensions} from '../../Themes';
import {useTranslation} from 'react-i18next';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {Button} from '../../Components/Buttom';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function SelectDate({
  visibleModal,
  setvisibleModal,
  _markedDatesToSave,
  set_markedDatesToSave,
  isBusiToSave,
  data,
  updatedads,
  visible,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const initialState = {};
  const isBusi = {...initialState, ...data.busyDays};
  const [_markedDates, set_markedDates] = useState(isBusi);

  const resetState = () => {
    set_markedDatesToSave(isBusiToSave);
    set_markedDates(isBusi);
  };

  const {t} = useTranslation();

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      presentationStyle="formSheet"
      collapsable={true}
      statusBarTranslucent={true}
      onRequestClose={() => setvisibleModal(false)}>
      <HeaderModal
        clean={true}
        onPress1={() => {
          resetState();
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(type, optiones);
        }}
        onPress={() => {
          setvisibleModal(false);
        }}
        title={`${data.car.marker} ${data.car.model} ${data.car.year}`}
        isOK={true}
        title2={t('details:modal:clear')}
        children={
          <View style={styles.contentModal}>
            <View style={{marginBottom: 170}}>
              <CalendarsList
                _markedDates={_markedDates}
                set_markedDates={set_markedDates}
                _markedDatesToSave={_markedDatesToSave}
                set_markedDatesToSave={set_markedDatesToSave}
              />
            </View>
          </View>
        }
      />
      <View style={styles.butonModal}>
        <View style={{alignSelf: 'center', marginBottom: 30}}>
          <Button
            title={t('details:modal:titleBTN')}
            light={colors.white}
            dark={colors.white}
            loading={false}
            onPress={() => updatedads(visible)}
          />
        </View>
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  butonModal: {
    width: dimensions.Width(100),
    height: 100,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    bottom: -5,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
  },
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.black),
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
