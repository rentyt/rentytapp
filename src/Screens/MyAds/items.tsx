import React, {useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  Platform,
  ActionSheetIOS,
  Alert,
} from 'react-native';
import {CustomText} from '../../Components/CustomText';
import {dimensions, colors, stylesText, image} from '../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {IAds} from '../../interfaces/index';
import {formaterPrice} from '../../Utils/formaterPrice';
import {useTranslation} from 'react-i18next';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useMutation} from '@apollo/client';
import {mutations} from '../../GraphQL';
import Toast from 'react-native-simple-toast';
import SelecDate from './SelectDate';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export interface IData {
  data: IAds;
  navigation: any;
  refetch: any;
}

export default function CardAdds(props: IData) {
  const {data, navigation, refetch} = props;

  const isBusiToSave = {...data.busyDays};

  const styles = useDynamicStyleSheet(dynamicStyles);
  const [visibleModalDate, setvisibleModalDate] = useState(false);
  const [_markedDatesToSave, set_markedDatesToSave] = useState(isBusiToSave);
  const [Loading, setLoading] = useState(false);

  const [deletedAds] = useMutation(mutations.DELETE_ADS);
  const [updateAds] = useMutation(mutations.UPDATE_ADS);

  const {t} = useTranslation();

  const deletedads = () => {
    deletedAds({variables: {id: data._id}})
      .then(res => {
        if (res.data.deletedAds.success) {
          refetch();
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);

          setTimeout(() => {
            Toast.show(res.data.deletedAds.messages, Toast.LONG, [
              'UIAlertController',
            ]);
          }, 1000);
        } else {
          Toast.show(res.data.deletedAds.messages, Toast.LONG, [
            'UIAlertController',
          ]);
        }
      })
      .catch(() => {
        //@ts-ignore
        ReactNativeHapticFeedback.trigger(types, optiones);

        setTimeout(() => {
          Toast.show('Algo Salio mal intentalo de nuevo', Toast.LONG, [
            'UIAlertController',
          ]);
        }, 1000);
      });
  };

  const updatedads = visible => {
    setLoading(true);
    const input = {
      _id: data._id,
      visible: visible,
      busyDays: _markedDatesToSave,
    };
    updateAds({variables: {input: {data: input}}})
      .then(res => {
        if (res.data.updateAds.success) {
          refetch();
          setvisibleModalDate(false);
          setLoading(false);
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          setTimeout(() => {
            Toast.show(res.data.updateAds.messages, Toast.LONG, [
              'UIAlertController',
            ]);
          }, 1000);
        } else {
          setLoading(false);
          Toast.show(res.data.updateAds.messages, Toast.LONG, [
            'UIAlertController',
          ]);
        }
      })
      .catch(() => {
        //@ts-ignore
        ReactNativeHapticFeedback.trigger(types, optiones);
        setLoading(false);
        setTimeout(() => {
          Toast.show('Algo Salio mal intentalo de nuevo', Toast.LONG, [
            'UIAlertController',
          ]);
        }, 1000);
      });
  };

  const onPress = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: `${t('myCar:items:titleDeleted')}`,
          message: t('myCar:items:Descripcion'),
          options: [t('chat:btn:cancel'), t('chat:btn:delete')],
          cancelButtonIndex: 0,
          destructiveButtonIndex: 1,
        },
        buttonIndex => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            deletedads();
          }
        },
      );
    } else {
      Alert.alert(
        `${t('myCar:items:titleDeleted')}`,
        t('myCar:items:Descripcion'),
        [
          {
            text: t('chat:btn:delete'),
            onPress: () => deletedads(),
          },

          {
            text: t('chat:btn:cancel'),
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    }
  };

  const onPressOcultar = (visible: boolean) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: `${t('myCar:items:titleHidel')}`,
          message: t('myCar:items:DescripcionHiden'),
          options: [
            t('chat:btn:cancel'),
            data.visible
              ? t('myCar:items:hiden')
              : t('myCar:items:Visibilizar'),
          ],
          cancelButtonIndex: 0,
          destructiveButtonIndex: 1,
        },
        buttonIndex => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            updatedads(visible);
          }
        },
      );
    } else {
      Alert.alert(
        `${t('myCar:items:titleDeleted')}`,
        t('myCar:items:Descripcion'),
        [
          {
            text: t('myCar:items:hiden'),
            onPress: () => updatedads(visible),
          },

          {
            text: t('chat:btn:cancel'),
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    }
  };

  return (
    <>
      <TouchableOpacity
        activeOpacity={100}
        onPress={() => navigation.navigate('Details', {id: data._id})}
        style={styles.card}>
        <ImageBackground
          style={[styles.mainImages]}
          imageStyle={{borderRadius: 15}}
          resizeMode="cover"
          source={image.PlaceHoldedCard}>
          <Image
            source={{uri: data.images[0].uri}}
            style={styles.mainImage}
            resizeMode="cover"
          />
          {data.popular ? (
            <View style={styles.destacado}>
              <Icon
                type="Feather"
                name="activity"
                size={18}
                color={colors.white}
                style={styles.mainIcon}
              />
            </View>
          ) : null}
        </ImageBackground>
        <View style={[styles.contentInfo]}>
          <View>
            <CustomText
              numberOfLines={1}
              style={[stylesText.TitleCard, {textAlign: 'left', marginTop: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {data.car.marker} {data.car.model} {data.car.year}
            </CustomText>
            <CustomText
              numberOfLines={1}
              style={[stylesText.TitleCard, {textAlign: 'left', marginTop: 5}]}
              light={colors.back_dark}
              dark={colors.white}>
              {formaterPrice(
                data.prices.value,
                data.prices.localcode,
                data.prices.currency,
              )}{' '}
              / {t('card:day')}
            </CustomText>
            <View style={styles.iconContent}>
              <View style={styles.icon}>
                <Icon
                  type="Feather"
                  name="map-pin"
                  size={14}
                  color={colors.twitter_color}
                />
              </View>
              <CustomText
                numberOfLines={1}
                style={[
                  stylesText.secondaryText,
                  {marginLeft: 5, width: dimensions.Width(32)},
                ]}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {data.city}
              </CustomText>
            </View>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              style={[styles.btn, {borderColor: colors.orange1}]}
              onPress={() => onPressOcultar(data.visible ? false : true)}>
              <CustomText
                numberOfLines={1}
                style={[stylesText.placeholderText, {fontWeight: 'bold'}]}
                light={colors.orange1}
                dark={colors.orange1}>
                <Icon
                  type="Feather"
                  name={data.visible ? 'eye' : 'eye-off'}
                  size={12}
                  color={colors.orange1}
                />{' '}
                {data.visible
                  ? t('myCar:items:hiden')
                  : t('myCar:items:Visibilizar')}
              </CustomText>
            </TouchableOpacity>

            <TouchableOpacity style={styles.btn} onPress={onPress}>
              <CustomText
                numberOfLines={1}
                style={[stylesText.placeholderText, {fontWeight: 'bold'}]}
                light={colors.ERROR}
                dark={colors.ERROR}>
                <Icon
                  type="Feather"
                  name="trash-2"
                  size={12}
                  color={colors.ERROR}
                />{' '}
                {t('myCar:items:delete')}
              </CustomText>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={[styles.btns]}
            onPress={() => setvisibleModalDate(true)}>
            <CustomText
              numberOfLines={1}
              style={[stylesText.placeholderText, {fontWeight: 'bold'}]}
              light={colors.black}
              dark={colors.white}>
              <Icon
                type="Feather"
                name="calendar"
                size={12}
                style={styles.icons}
              />{' '}
              {t('myCar:items:calendar')}
            </CustomText>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
      <SelecDate
        visibleModal={visibleModalDate}
        setvisibleModal={setvisibleModalDate}
        _markedDatesToSave={_markedDatesToSave}
        set_markedDatesToSave={set_markedDatesToSave}
        isBusiToSave={isBusiToSave}
        data={data}
        visible={data.visible}
        updatedads={updatedads}
      />
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  contentInfo: {
    justifyContent: 'center',
    alignItems: 'center',
    width: dimensions.Width(43),
  },

  mainImages: {
    width: dimensions.Width(41),
    height: 100,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  mainImage: {
    width: dimensions.Width(40),
    height: 96,
    borderRadius: 15,
  },

  card: {
    width: dimensions.Width(45),
    minHeight: dimensions.Height(20),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 15,
    padding: 10,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },
  tag: {
    zIndex: 230,
    padding: 3,
    width: dimensions.Width(43),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginTop: 10,
  },

  header: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: dimensions.Width(43),
    marginTop: 5,
  },

  type: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 100,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },

  icons: {
    color: new DynamicValue(colors.black, colors.white),
  },

  iconContent: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: 7,
  },

  icon: {
    width: 25,
    height: 25,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  destacado: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    position: 'absolute',
    right: 6,
    top: 6,
    backgroundColor: colors.green,
  },

  mainIcon: {
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
  },

  btn: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderWidth: 1,
    borderColor: colors.ERROR,
    marginTop: 12,
    width: dimensions.Width(38) / 2,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginHorizontal: 5,
  },

  btns: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderWidth: 1,
    borderColor: new DynamicValue(colors.black, colors.white),
    marginTop: 12,
    width: '94%',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginHorizontal: 5,
  },
});
