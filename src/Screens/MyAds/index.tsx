import React, {useContext, useState} from 'react';
import Header from '../../Components/Header';
import {View, Platform, FlatList, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../Themes';
import {useTranslation} from 'react-i18next';
import {MainContext} from '../../store/MainProvider';
import AddComponent from '../../Components/AddCar';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import SegmentedControl from '@react-native-community/segmented-control';
import {useQuery} from '@apollo/client';
import {query} from '../../GraphQL';
import Item from './items';
import LottieView from 'lottie-react-native';
import {CustomText} from '../../Components/CustomText';
import LoadingComponent from '../../Components/PlaceHolded/VerticalAds';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function MyAds({navigation}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const {users} = useContext(MainContext);
  const [visibleModal, setvisibleModal] = useState(false);
  const [active, setActive] = useState(0);

  const getstatus = (): string => {
    switch (active) {
      case 0:
        return 'Succees';

      case 1:
        return 'In review';

      case 2:
        return 'Rejected';
    }
  };

  const {data, loading, refetch} = useQuery(query.GET_ALL_ADS, {
    variables: {
      input: {owner: users._id, status: getstatus()},
    },
  });

  const ads = data && data.getAds ? data.getAds.data : [];

  setTimeout(() => {
    refetch();
  }, 2000);

  const renderItem = ({item}) => {
    return <Item data={item} navigation={navigation} refetch={refetch} />;
  };

  const publish = () => {
    setvisibleModal(true);
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
  };

  return (
    <View style={styles.container}>
      <Header
        navigation={navigation}
        title={t('myCar:titleHeader')}
        leftIcon={true}
        name="plus"
        type="Feather"
        onPress={publish}
      />
      <View
        style={{
          width: '96%',
          alignSelf: 'center',
          marginTop: 20,
          marginBottom: 10,
          marginHorizontal: 15,
        }}>
        <SegmentedControl
          values={[
            t('myCar:publish'),
            t('myCar:inreview'),
            t('myCar:Rejected'),
          ]}
          selectedIndex={active}
          onChange={event => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
            setActive(event.nativeEvent.selectedSegmentIndex);
          }}
        />
      </View>

      <View
        style={{
          width: '100%',
          alignSelf: 'center',
          paddingTop: 10,
          marginBottom: 20,
        }}>
        {loading ? (
          <LoadingComponent />
        ) : (
          <FlatList
            data={ads}
            renderItem={(item: any) => renderItem(item)}
            keyExtractor={(item: any) => item._id}
            showsVerticalScrollIndicator={false}
            numColumns={2}
            ListEmptyComponent={
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <LottieView
                  source={require('../../Assets/animated/Nodata.json')}
                  autoPlay
                  loop
                  style={{width: 200}}
                />
                <CustomText
                  light={colors.black}
                  dark={colors.white}
                  style={[
                    stylesText.secondaryText,
                    {textAlign: 'center', marginHorizontal: 30},
                  ]}>
                  {t('myCar:noData')}
                </CustomText>
                <TouchableOpacity
                  onPress={publish}
                  style={{
                    marginTop: 30,
                    backgroundColor: colors.green,
                    paddingVertical: 15,
                    paddingHorizontal: 50,
                    borderRadius: 100,
                  }}>
                  <CustomText
                    light={colors.white}
                    dark={colors.white}
                    style={[
                      stylesText.secondaryTextBold,
                      {textAlign: 'center'},
                    ]}>
                    {t('myCar:btn')}
                  </CustomText>
                </TouchableOpacity>
              </View>
            }
            ListFooterComponent={
              <View style={{marginBottom: dimensions.Height(30)}} />
            }
          />
        )}
      </View>

      <AddComponent
        visibleModal={visibleModal}
        setvisibleModal={setvisibleModal}
        edit={false}
        refetch={null}
        data={null}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  addCont: {
    padding: 10,
    margin: 15,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderWidth: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
});
