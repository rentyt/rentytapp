import React from 'react';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import {useQuery} from '@apollo/client';
import {userQuery} from '../../GraphQL';
//@ts-ignore
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import {CustomText} from '../../Components/CustomText';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText, image} from '../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import Star from '../../Components/star';
import UserLoading from '../../Components/PlaceHolded/User';
import {Avatar} from '../../Components/Avatar';
import Items from './items';
import Items2 from './Items2';
import Items3 from './Items3';
import Items4 from './Items4';
import Items5 from './Items5';

const window = Dimensions.get('window');

export default function UserPorfile({route, navigation}) {
  const {id, ads} = route.params;
  const styles = useDynamicValue(dynamicStyles);

  const {data, loading, refetch} = useQuery(userQuery.GET_USER, {
    variables: {id: id},
  });

  const user = data && data.getUser ? data.getUser.data : null;

  if (loading) {
    return <UserLoading />;
  }
  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor="transparent"
        stickyHeaderHeight={100}
        parallaxHeaderHeight={200}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background" style={{backgroundColor: colors.white}}>
            <ImageBackground
              style={styles.imagenbg}
              source={image.HeaderUser}
            />
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <TouchableOpacity
              style={styles.left}
              onPress={() => navigation.goBack()}>
              <Icon
                name="arrow-left"
                type="Feather"
                size={24}
                style={styles.close}
              />
            </TouchableOpacity>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                {marginLeft: 65, width: dimensions.Width(75)},
              ]}>
              {user.name} {user.lastName}{' '}
              {user.verified ? (
                <Icon
                  type="MaterialIcons"
                  name="verified"
                  size={16}
                  color={colors.twitter_color}
                  style={{marginLeft: 5}}
                />
              ) : null}
            </CustomText>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View
            style={{
              position: 'absolute',
              top: -50,
              width: window.width,
              justifyContent: 'center',
              alignItems: 'center',
              zIndex: 1001,
            }}>
            <Avatar uri={user.avatar} width={100} height={100} />
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 5,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <CustomText
                  numberOfLines={1}
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.titleText]}>
                  {user.name} {user.lastName}
                </CustomText>
                {user.verified ? (
                  <Icon
                    type="MaterialIcons"
                    name="verified"
                    size={16}
                    color={colors.twitter_color}
                    style={{marginLeft: 5}}
                  />
                ) : null}
              </View>
              <View style={{flexDirection: 'row', marginTop: 3}}>
                <Star star={Number.parseFloat(user.rating)} />
                <CustomText
                  numberOfLines={1}
                  light={colors.orange}
                  dark={colors.orange}
                  style={[stylesText.secondaryTextBold, {marginLeft: 10}]}>
                  {user.rating}
                </CustomText>
              </View>
            </View>
          </View>

          <View style={{marginTop: 150}}>
            <Items user={user} navigation={navigation} />
            {ads ? (
              <Items2 user={user} ads={ads} navigation={navigation} />
            ) : null}
            <Items3 user={user} />
            <Items4 user={user} />
            <Items5 user={user} />
          </View>
        </View>
      </ParallaxScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contenedor: {
    flex: 1,
    width: dimensions.ScreenWidth,
    height: 'auto',
    minHeight: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  imagenbg: {
    width: dimensions.ScreenWidth,
    height: 200,
  },

  stickySection: {
    height: 100,
    width: '100%',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: 40,
  },

  fixedSection: {
    position: 'absolute',
    left: 15,
    flexDirection: 'row',
    top: 50,
  },

  left: {
    marginRight: dimensions.Width(4),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
  },

  close: {
    color: new DynamicValue(colors.black, colors.white),
  },
});
