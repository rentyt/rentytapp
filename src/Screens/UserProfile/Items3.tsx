import React from 'react';
import {View} from 'react-native';
import {CustomText} from '../../Components/CustomText';
import {colors, stylesText, dimensions} from '../../Themes';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';

export default function Items({user}) {
  const {t} = useTranslation();
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={{marginHorizontal: 15, marginTop: 30}}>
      <CustomText
        numberOfLines={2}
        light={colors.black}
        dark={colors.white}
        style={[stylesText.secondaryTextBold]}>
        {t('userProfile:item3:title')}
      </CustomText>

      <View style={styles.container}>
        <CustomText
          numberOfLines={8}
          style={stylesText.secondaryText}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          <Icon
            type="Fontisto"
            name="quote-a-right"
            size={10}
            style={styles.icon}
          />{' '}
          {`  ${
            user.description
              ? user.description
              : `${t('userProfile:item3:nodata')} ${user.name}`
          }  `}{' '}
          <Icon
            type="Fontisto"
            name="quote-a-left"
            size={10}
            style={styles.icon}
          />
        </CustomText>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: dimensions.Width(92),
    marginTop: 15,
  },
  icon: {
    color: new DynamicValue('black', 'white'),
  },
});
