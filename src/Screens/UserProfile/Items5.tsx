import React from 'react';
import {View, Linking, Platform, TouchableOpacity} from 'react-native';
import {CustomText} from '../../Components/CustomText';
import {colors, stylesText, dimensions} from '../../Themes';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import moment from 'moment';
import Mailer from 'react-native-mail';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Items4({user}) {
  const {t} = useTranslation();
  const styles = useDynamicStyleSheet(dynamicStyles);

  const message = `Report from user ${user.name} ${user.lastName} ${user.email}`;

  const handleEmail = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: message,
          recipients: ['report@rentytapp.com'],
          body: `<b>${message}</b>`,
          isHTML: true,
        },
        (error, event) => {},
      );
    } else {
      Linking.openURL(
        `mailto:"report@rentytapp.com"?subject=${message}&body=${message}`,
      );
    }
  };

  return (
    <View style={{marginHorizontal: 15, marginTop: 40}}>
      <CustomText
        numberOfLines={2}
        light={colors.black}
        dark={colors.white}
        style={[stylesText.secondaryTextBold]}>
        {t('userProfile:item5:title')}
      </CustomText>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginTop: 20,
        }}>
        <View>
          <CustomText
            numberOfLines={2}
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryText]}>
            <Icon
              type="Feather"
              name="map-pin"
              size={16}
              color={colors.green}
            />{' '}
            {t('userProfile:item5:location')}
          </CustomText>
        </View>
        <CustomText
          numberOfLines={2}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryText]}>
          {user.city ? user.city : t('userProfile:item5:locationname')}
        </CustomText>
      </View>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginTop: 20,
        }}>
        <View>
          <CustomText
            numberOfLines={2}
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryText]}>
            <Icon
              name="calendar"
              type="Feather"
              size={16}
              color={colors.green}
            />{' '}
            {t('userProfile:item5:from')}
          </CustomText>
        </View>
        <CustomText
          numberOfLines={2}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryText]}>
          {moment(user.created_at).format('ll')}
        </CustomText>
      </View>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginTop: 20,
        }}>
        <View>
          <CustomText
            numberOfLines={2}
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryText]}>
            <Icon name="wifi" type="Feather" size={16} color={colors.green} />{' '}
            {t('userProfile:item5:last')}
          </CustomText>
        </View>
        <CustomText
          numberOfLines={2}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryText]}>
          {moment(user.lastSeen ? user.lastSeen : new Date()).format('lll')}
        </CustomText>
      </View>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginTop: 20,
        }}>
        <View>
          <CustomText
            numberOfLines={2}
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryText]}>
            <Icon name="mail" type="Feather" size={16} color={colors.green} />{' '}
            {t('userProfile:item5:mail')}
          </CustomText>
        </View>
        <CustomText
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryText]}>
          {user.email}
        </CustomText>
      </View>

      <TouchableOpacity
        style={{alignSelf: 'center', marginTop: 60}}
        onPress={handleEmail}>
        <CustomText
          numberOfLines={2}
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[stylesText.secondaryText]}>
          <Icon type="Feather" name="flag" size={16} color={colors.rgb_153} />{' '}
          {t('userProfile:item5:report')}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: dimensions.Width(92),
    marginTop: 15,
  },
  icon: {
    color: new DynamicValue('black', 'white'),
  },
});
