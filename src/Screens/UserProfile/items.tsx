import React from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import {CustomText} from '../../Components/CustomText';
import {colors, stylesText} from '../../Themes';
import {useTranslation} from 'react-i18next';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Items({user, navigation}) {
  const {t} = useTranslation();

  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 25,
      }}>
      <TouchableOpacity
        style={{alignItems: 'center', justifyContent: 'center'}}
        onPress={() => {
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(type, optiones);
          navigation.navigate('Rating', {id: user._id});
        }}>
        <CustomText
          numberOfLines={1}
          light={colors.orange}
          dark={colors.orange}
          style={stylesText.secondaryTextBold}>
          {user.totalRating > 300 ? '500+' : user.totalRating}
        </CustomText>
        <CustomText
          numberOfLines={1}
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={stylesText.secondaryText}>
          {t('userProfile:item1:rating')}
        </CustomText>
      </TouchableOpacity>
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <CustomText
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}
          style={stylesText.secondaryTextBold}>
          {user.totalOrder > 300 ? '100+' : user.totalOrder}
        </CustomText>
        <CustomText
          numberOfLines={1}
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={stylesText.secondaryText}>
          {t('userProfile:item1:book')}
        </CustomText>
      </View>
      <TouchableOpacity
        style={{alignItems: 'center', justifyContent: 'center'}}
        onPress={() => {
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(type, optiones);
          navigation.navigate('OwnerCar', {id: user._id});
        }}>
        <CustomText
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}
          style={stylesText.secondaryTextBold}>
          {user.totalCar > 50 ? '50+' : user.totalCar}
        </CustomText>
        <CustomText
          numberOfLines={1}
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={stylesText.secondaryText}>
          {t('userProfile:item1:ads')}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}
