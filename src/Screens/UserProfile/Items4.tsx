import React from 'react';
import {View} from 'react-native';
import {CustomText} from '../../Components/CustomText';
import {colors, stylesText, dimensions} from '../../Themes';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';

export default function Items4({user}) {
  const {t} = useTranslation();
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <View style={{marginHorizontal: 15, marginTop: 40}}>
      {user.verifyPhone || user.verified ? (
        <CustomText
          numberOfLines={2}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold]}>
          {t('userProfile:item4:title')}
        </CustomText>
      ) : null}

      {user.verifyPhone ? (
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 20,
          }}>
          <View>
            <CustomText
              numberOfLines={2}
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryText]}>
              <Icon
                type="Feather"
                name="phone"
                size={16}
                color={colors.green}
              />{' '}
              {t('userProfile:item4:phone')}
            </CustomText>
          </View>
          <Icon
            type="Feather"
            name="check-circle"
            size={16}
            color={colors.green}
          />
        </View>
      ) : null}

      {user.verified ? (
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 20,
          }}>
          <View>
            <CustomText
              numberOfLines={2}
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryText]}>
              <Icon
                name="Safety"
                type="AntDesign"
                size={16}
                color={colors.green}
              />{' '}
              {t('userProfile:item4:rating')}
            </CustomText>
          </View>
          <Icon
            type="Feather"
            name="check-circle"
            size={16}
            color={colors.green}
          />
        </View>
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: dimensions.Width(92),
    marginTop: 15,
  },
  icon: {
    color: new DynamicValue('black', 'white'),
  },
});
