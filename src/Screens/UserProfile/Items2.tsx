import React from 'react';
import {View, TouchableOpacity, Image, Platform} from 'react-native';
import {CustomText} from '../../Components/CustomText';
import {colors, stylesText, image, dimensions} from '../../Themes';
import {useTranslation} from 'react-i18next';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useQuery} from '@apollo/client';
import {query} from '../../GraphQL';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

export default function Items({user, ads, navigation}) {
  const {t} = useTranslation();

  const {data, refetch, loading} = useQuery(query.GET_CONVERSATION_ONE, {
    variables: {userID: user._id, adsID: ads._id},
  });

  const chast =
    data && data.getConversationOne ? data.getConversationOne.data : {data: ''};

  const dataNAv = chast
    ? chast
    : {
        ads: {
          _id: ads._id,
          car: ads.car,
          imageCar: ads.images[0].uri,
          owner: ads.owner,
        },
        user: user,
        Owner: ads.OwnerData,
        messages: [],
        userID: user._id,
        adsID: ads._id,
        owner: ads.owner,
        userRoomID: `${Number.parseInt(getRandomArbitrary(10000, 100000000))}`,
        ownerRoomID: `${Number.parseInt(getRandomArbitrary(10000, 100000000))}`,
        created_at: new Date(),
      };

  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 15,
        marginTop: 30,
        borderWidth: 1,
        borderColor: colors.green,
        borderRadius: 15,
        padding: 15,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          width: dimensions.Width(45),
        }}>
        <Image
          source={image.Chat}
          style={{width: 60, height: 60}}
          resizeMode="cover"
        />
        <CustomText
          numberOfLines={2}
          light={colors.green}
          dark={colors.green}
          style={[stylesText.secondaryText, {marginLeft: 7}]}>
          {t('userProfile:item2:title')} {user.name} {user.lastName}
        </CustomText>
      </View>
      <TouchableOpacity
        onPress={() => {
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(type, optiones);
          refetch();
          dataNAv.refetch = refetch;
          setTimeout(() => {
            navigation.navigate('ChatScreen', dataNAv);
          }, 100);
        }}
        style={{
          backgroundColor: colors.green,
          padding: 10,
          width: 80,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 100,
        }}>
        <CustomText
          numberOfLines={1}
          light={colors.white}
          dark={colors.white}
          style={stylesText.TitleCard}>
          {t('userProfile:item2:btn')}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}
