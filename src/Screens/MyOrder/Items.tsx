import React from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import {CustomText} from '../../Components/CustomText';
import {dimensions, colors, stylesText} from '../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import moment from 'moment';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export interface IData {
  data: any;
  navigation: any;
  refetch: any;
  request: boolean;
}

export default function CardAdds(props: IData) {
  const {data, navigation, refetch, request} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const navigateDetails = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (request) {
      navigation.navigate('MyRequestDetails', {id: data._id});
    } else {
      navigation.navigate('DetailsOrders', {id: data._id});
    }
  };

  const name = request
    ? `${data.Customer.name} ${data.Customer.lastName}`
    : `${data.Owner.name} ${data.Owner.lastName}`;

  const colorsStatus = () => {
    if (data.status === 'Rechazado') {
      return colors.ERROR;
    } else if (data.status === 'Devuelto') {
      return colors.ERROR;
    } else if (data.status === 'Pendiente de pago') {
      return colors.orange1;
    } else if (data.status === 'Nueva') {
      return colors.orange1;
    } else {
      return colors.green;
    }
  };

  return (
    <TouchableOpacity
      activeOpacity={100}
      onPress={navigateDetails}
      style={styles.card}>
      <View style={{padding: 15}}>
        <View style={[styles.status, {backgroundColor: colorsStatus()}]}>
          <CustomText
            numberOfLines={1}
            style={[stylesText.TitleCard]}
            light={colors.white}
            dark={colors.white}>
            {t(`status:${data.status}`).toUpperCase()}
          </CustomText>
        </View>
        <View>
          <CustomText
            numberOfLines={1}
            style={[stylesText.TitleCard, {fontWeight: '500'}]}
            light={colors.black}
            dark={colors.white}>
            {`${data.Car.car.marker} ${data.Car.car.model} ${data.Car.car.year}`.toUpperCase()}
          </CustomText>
          <CustomText
            numberOfLines={1}
            style={[stylesText.TitleCard, {fontWeight: '500', marginTop: 5}]}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {name}
          </CustomText>
          <CustomText
            numberOfLines={1}
            style={[stylesText.terciaryText, {marginTop: 5}]}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {moment(data.created_at).format('LL')}
          </CustomText>
        </View>
      </View>
      <View style={styles.date}>
        <CustomText
          numberOfLines={1}
          style={[stylesText.terciaryText]}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          {moment(data.pickupDate.date).format('MMMM').toUpperCase()}
        </CustomText>
        <CustomText
          numberOfLines={1}
          style={[stylesText.titleText, {fontWeight: '300', fontSize: 52}]}
          light={colors.back_dark}
          dark={colors.white}>
          {moment(data.pickupDate.date).format('DD')}
        </CustomText>
        <CustomText
          numberOfLines={1}
          style={[stylesText.terciaryText]}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          {data.pickupDate.time}
        </CustomText>
      </View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  card: {
    width: dimensions.Width(96),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    margin: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    shadowColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 12,
    elevation: 23,
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.colorBorder, colors.back_suave_dark),
  },

  date: {
    width: 100,
    height: 'auto',
    borderLeftWidth: 0.5,
    borderLeftColor: new DynamicValue(
      colors.colorBorder,
      colors.back_suave_dark,
    ),
    justifyContent: 'center',
    alignItems: 'center',
  },

  status: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 100,
    marginBottom: 8,
    maxWidth: 150,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
