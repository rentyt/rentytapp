import React from 'react';
import {
  View,
  ImageBackground,
  Image,
  Platform,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {CustomText} from '../../../Components/CustomText';
import {dimensions, colors, stylesText, image} from '../../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';

export default function Car({data, navigation, request}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {t} = useTranslation();

  return (
    <View
      style={{
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}>
      <ImageBackground
        style={[styles.mainImages]}
        imageStyle={{borderRadius: 15}}
        resizeMode="cover"
        source={image.PlaceHoldedCard}>
        <Image
          source={{uri: data.Car.images[0].uri}}
          style={styles.mainImage}
          resizeMode="cover"
        />
      </ImageBackground>
      <View style={{marginLeft: 10}}>
        <CustomText
          style={[stylesText.secondaryTextBold]}
          light={colors.back_suave_dark}
          dark={colors.white}>
          {data.Car.car.marker} {data.Car.car.model}
        </CustomText>
        <CustomText
          style={[stylesText.secondaryTextBold]}
          light={colors.back_suave_dark}
          dark={colors.white}>
          {data.Car.car.year}
        </CustomText>
        <CustomText
          numberOfLines={1}
          style={[stylesText.secondaryText, {width: dimensions.Width(50)}]}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          {data.Car.city}
        </CustomText>
        <TouchableOpacity
          onPress={() => {
            if (data.Car.status === 'Succees' && !request) {
              navigation.navigate('Details', {id: data.Car._id});
            } else if (request) {
              navigation.navigate('Details', {id: data.Car._id});
            } else {
              Alert.alert(t('myOrder:details:car:alertTitle'));
            }
          }}
          style={{
            backgroundColor: colors.green,
            width: 120,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 8,
            padding: 5,
            borderRadius: 100,
          }}>
          <CustomText
            numberOfLines={1}
            style={[stylesText.TitleCard]}
            light={colors.white}
            dark={colors.white}>
            {t('myOrder:details:car:btndetalles')}
          </CustomText>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  mainImages: {
    width: 100,
    height: 100,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  mainImage: {
    width: 96,
    height: 96,
    borderRadius: 15,
  },

  iconContent: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: 7,
  },

  icon: {
    width: 25,
    height: 25,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
