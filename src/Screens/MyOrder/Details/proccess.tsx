import React from 'react';
import {View} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {colors} from '../../../Themes';
import Timeline from 'react-native-timeline-flatlist';
import moment from 'moment';
import {useTranslation} from 'react-i18next';

const Process = (props: any) => {
  const styles = useDynamicValue(dynamicStyles);

  const {t} = useTranslation();

  const {data} = props;

  const d = data ? data : [];

  const datas = d.map(x => {
    console.log(x);
    return {
      time: moment(x.date).format('DD-MM - HH:mm'),
      title: t(`status:${x.status}`),
      circleColor: colors.green,
      lineColor: colors.green,
    };
  });

  return (
    <View style={styles.container}>
      <Timeline
        titleStyle={styles.text}
        style={styles.list}
        data={datas}
        separator={true}
        circleSize={20}
        circleColor="rgba(41, 216, 131, 0.205)"
        lineColor="rgba(41, 216, 131, 0.205)"
        timeContainerStyle={{minWidth: 110, marginTop: -5}}
        separatorStyle={styles.separator}
        listViewContainerStyle={{width: 300}}
        circleStyle={{marginLeft: 0}}
        timeStyle={{
          textAlign: 'center',
          backgroundColor: 'rgba(41, 216, 131, 0.205)',
          color: colors.green,
          padding: 5,
          borderRadius: 13,
          overflow: 'hidden',
        }}
        descriptionStyle={{color: 'gray'}}
        //@ts-ignore
        options={{
          style: {paddingTop: 5},
        }}
      />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  list: {
    flex: 1,
    marginTop: 10,
  },

  text: {
    color: new DynamicValue(colors.black, colors.white),
    fontFamily: 'Poppins',
    margin: 0,
    padding: 0,
  },

  separator: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },
});

export default Process;
