import React from 'react';
import {View, Platform} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import CheckBox from '@react-native-community/checkbox';

export default function Items({data}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {t} = useTranslation();
  return (
    <View>
      {data.instantBooking ? (
        <View style={styles.container}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={styles.icon}>
              <Icon type="Entypo" name="flash" size={24} color={colors.ERROR} />
            </View>
            <View style={{marginLeft: 10}}>
              <CustomText
                style={stylesText.TitleCard}
                light={colors.black}
                dark={colors.white}>
                {t('details:InstantDetail')}
              </CustomText>
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {width: dimensions.Width(70)},
                ]}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {t('myOrder:details:instantaneaDescription')}
              </CustomText>
            </View>
          </View>
          <View style={{marginRight: 10}}>
            <CheckBox
              value={true}
              disabled={true}
              onTintColor={colors.green}
              tintColors={{true: colors.green, false: colors.rgb_153}}
              onCheckColor={colors.green}
              onAnimationType="fill"
              offAnimationType="fill"
              style={{marginLeft: 10}}
            />
          </View>
        </View>
      ) : null}

      {data.airplanePickUp ? (
        <View style={styles.container}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={styles.icon}>
              <Icon
                type="MaterialCommunityIcons"
                name="airplane"
                size={24}
                color={colors.twitter_color}
              />
            </View>
            <View style={{marginLeft: 10}}>
              <CustomText
                style={stylesText.TitleCard}
                light={colors.black}
                dark={colors.white}>
                {t('addCar:airTitle')}
              </CustomText>
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {width: dimensions.Width(70)},
                ]}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {t('myOrder:details:airPlaneDescription')}
              </CustomText>
            </View>
          </View>
          <View style={{marginRight: 10}}>
            <CheckBox
              value={data.airplanePickUp}
              disabled={true}
              onTintColor={colors.green}
              tintColors={{true: colors.green, false: colors.rgb_153}}
              onCheckColor={colors.green}
              onAnimationType="fill"
              offAnimationType="fill"
              style={{marginLeft: 10}}
            />
          </View>
        </View>
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  icon: {
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    marginTop: 20,
    marginHorizontal: 15,
  },
});
