import React, {useState, useContext} from 'react';
import {View, Image, Modal, Platform, Alert} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import Star from '../../../Components/star';
import {useMutation} from '@apollo/client';
import {mutations} from '../../../GraphQL';
import StarRating from 'react-native-star-rating';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {Button} from '../../../Components/Buttom';
import HeaderModal from '../../../Components/HeaderModal';
import Textarea from 'react-native-textarea';
import {useTranslation} from 'react-i18next';
import Success from './Succeess';
import ModalLoading from '../../../Components/ModalLoading';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Valoration({
  visibleModal,
  setvisibleModal,
  data,
  refetch,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const [ratingCar, setRatingCar] = useState(0);
  const [userRating, setuserRating] = useState(0);
  const [coment, setComent] = useState(null);
  const [success, serSuccess] = useState(false);
  const [Loading, setLoading] = useState(false);
  const {t} = useTranslation();

  const [crearRating] = useMutation(mutations.CREATE_RATING);

  const onStarRatingPressCar = (rating: number) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, options);
    setRatingCar(rating);
  };

  const onStarRatingPressUser = (rating: number) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, options);
    setuserRating(rating);
  };

  const isOk = (): boolean => {
    if (userRating > 0 && ratingCar > 0) {
      return true;
    } else {
      return false;
    }
  };

  const crearRatingFunc = () => {
    setLoading(true);
    const input = {
      owner: data.owner,
      customer: data.customer,
      car: data.car,
      order: data._id,
      rating: {
        userRating: userRating,
        carRating: ratingCar,
      },
      comment: coment,
      reply: null,
    };
    if (isOk()) {
      crearRating({variables: {input: {data: input}}})
        .then(res => {
          if (res.data.crearRating.success) {
            refetch();
            setLoading(false);
            serSuccess(true);
          } else {
            refetch();
            serSuccess(false);
            Alert.alert(res.data.crearRating.messages);
          }
        })
        .catch(() => {
          setLoading(false);
          serSuccess(false);
          Alert.alert('Algo salio mal intentalo de nuevo');
        });
    } else {
      setLoading(false);
      Alert.alert(t('myOrder:rating:alert'));
    }
  };

  const renderRating = () => {
    return (
      <View>
        <View style={{marginHorizontal: 20, marginTop: 30}}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.titleText, {fontSize: 26}]}>
            {t('myOrder:rating:title')}
          </CustomText>
        </View>
        <View
          style={[
            styles.cards,
            {
              flexDirection: 'row',
              alignItems: 'center',
              padding: 15,
            },
          ]}>
          <View>
            <Image source={data.Car.images[0]} style={styles.logos} />
          </View>
          <View>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {data.Car.car.marker} {data.Car.car.model} {data.Car.car.year}
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText]}>
              {data.Car.city}
            </CustomText>
            <View style={{flexDirection: 'row'}}>
              <Star
                star={Number.parseFloat(data.Car.rating)}
                styles={{marginTop: 5}}
              />
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {marginTop: 5, marginLeft: 5},
                ]}>
                {data.Car.rating}
              </CustomText>
            </View>
          </View>
        </View>

        <View
          style={{
            justifyContent: 'flex-start',
            marginTop: dimensions.Height(4),
            marginBottom: 50,
            width: dimensions.Width(50),
            marginLeft: 15,
          }}>
          <View>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.mainText, {marginBottom: 25}]}>
              {t('myOrder:rating:owner')}
            </CustomText>
            <StarRating
              disabled={false}
              maxStars={5}
              rating={userRating}
              selectedStar={rating => onStarRatingPressUser(rating)}
              fullStarColor={colors.orange}
            />
          </View>

          <View style={{marginTop: 30}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.mainText, {marginBottom: 25}]}>
              {t('myOrder:rating:car')}
            </CustomText>
            <StarRating
              disabled={false}
              maxStars={5}
              rating={ratingCar}
              selectedStar={rating => onStarRatingPressCar(rating)}
              fullStarColor={colors.orange}
            />
          </View>
        </View>

        <View style={{marginHorizontal: 15}}>
          <CustomText
            style={[stylesText.mainText]}
            light={colors.black}
            dark={colors.white}>
            {t('myOrder:rating:coment')}
          </CustomText>

          <Textarea
            containerStyle={styles.textareaContainer}
            //@ts-ignore
            style={styles.textarea}
            onChangeText={value => setComent(value)}
            defaultValue={coment}
            maxLength={200}
            minLength={100}
            selectionColor={colors.green}
            placeholder={t('myOrder:rating:coment')}
            placeholderTextColor={'#c7c7c7'}
            underlineColorAndroid={'transparent'}
          />
        </View>
      </View>
    );
  };

  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={visibleModal}
      statusBarTranslucent={true}
      presentationStyle="formSheet">
      <ModalLoading loading={Loading} />
      {success ? (
        <Success setvisibleModal={setvisibleModal} />
      ) : (
        <HeaderModal
          onPress={() => setvisibleModal(false)}
          title={t('myOrder:rating:header')}
          children={<View style={styles.centeredView}>{renderRating()}</View>}
        />
      )}
      {success ? null : (
        <View style={styles.signupButtonContainer}>
          <Button
            light={colors.white}
            dark={colors.white}
            onPress={() => crearRatingFunc()}
            title={t('myOrder:rating:btn')}
          />
        </View>
      )}
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  centeredView: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginBottom: dimensions.Height(40),
  },

  cards: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    marginTop: 30,
    padding: 15,
    borderRadius: 15,
    marginHorizontal: 10,
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.colorBorder, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  logos: {
    width: 60,
    height: 60,
    marginRight: 15,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
  },

  signupButtonContainer: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 15,
  },

  textareaContainer: {
    height: 140,
    padding: 15,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderRadius: 10,
    marginTop: 15,
  },
  textarea: {
    textAlignVertical: 'top', // hack android
    height: 170,
    fontSize: 16,
    color: new DynamicValue(colors.black, colors.white),
    fontFamily: 'Poppins',
  },
});
