import React from 'react';
import {View} from 'react-native';
import {colors, dimensions, stylesText} from '../../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../../../Components/CustomText';
import moment from 'moment';

export default function DaySeleted({item}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const border = {
    light: colors.green,
    dark: colors.green,
  };

  const borderColor = border[mode];

  return (
    <View style={[styles.items]}>
      <View>
        <Icon type="Feather" name="minus" size={14} color={borderColor} />
      </View>
      <View
        style={{
          marginLeft: 5,
          justifyContent: 'flex-start',
          flexDirection: 'row',
          width: dimensions.IsIphoneX()
            ? dimensions.Width(81)
            : dimensions.Width(80),
        }}>
        <View>
          <CustomText
            style={[stylesText.titleText200]}
            numberOfLines={1}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {moment(item).format('ll')}
          </CustomText>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  items: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',

    width: dimensions.Width(92),
    marginTop: 15,
  },
});
