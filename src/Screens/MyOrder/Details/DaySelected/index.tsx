import React, {useState} from 'react';
import {FlatList, View, TouchableOpacity, Platform} from 'react-native';
import {colors, dimensions, stylesText} from '../../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import Item from './Items';
import {CustomText} from '../../../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function DaySelected({arrayDates, deliveryTime, timePickup}) {
  const [show, setshow] = useState(false);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const mode = useColorSchemeContext();

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const borderColor = {
    light: colors.colorBorder,
    dark: colors.back_dark,
  };

  const BorderColor = borderColor[mode];

  const {t} = useTranslation();

  const renderItem = ({item}) => {
    return <Item item={item} />;
  };

  return (
    <View style={[styles.content]}>
      <TouchableOpacity
        style={styles.datesSelected}
        onPress={() => {
          setshow(!show);
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(type, optiones);
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.icon}>
            <Icon
              type="Feather"
              name="calendar"
              size={22}
              color={colors.green}
            />
          </View>
          <CustomText
            style={[stylesText.secondaryTextBold, {marginLeft: 10}]}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('details:DaySelected:curentSelected')} {arrayDates.length}{' '}
            {arrayDates.length === 1
              ? t('details:DaySelected:día')
              : t('details:DaySelected:días')}
          </CustomText>
        </View>
        {show ? (
          <Icon
            type="Feather"
            name="chevron-up"
            size={22}
            color={colors.rgb_153}
          />
        ) : (
          <Icon
            type="Feather"
            name="chevron-down"
            size={22}
            color={colors.rgb_153}
          />
        )}
      </TouchableOpacity>
      {show ? (
        <FlatList
          data={arrayDates}
          renderItem={(item: any) => renderItem(item)}
          keyExtractor={(item: any) => item}
          showsVerticalScrollIndicator={false}
          scrollEnabled={false}
        />
      ) : null}
      <View style={[styles.datesSelected, {marginTop: 20}]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon type="Feather" name="clock" size={20} color={colors.green} />
          <View>
            <CustomText
              style={[stylesText.secondaryText, {marginLeft: 10}]}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {t('details:modal:selectHourPickUp')}
            </CustomText>
            <CustomText
              style={[stylesText.secondaryText, {marginLeft: 10, marginTop: 3}]}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {timePickup}
            </CustomText>
          </View>
        </View>
      </View>
      <View style={[styles.datesSelected, {marginTop: 20}]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon type="Feather" name="clock" size={20} color={colors.green} />
          <View>
            <CustomText
              style={[stylesText.secondaryText, {marginLeft: 10}]}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {t('details:modal:selectHourDelivery')}
            </CustomText>
            <CustomText
              style={[stylesText.secondaryText, {marginLeft: 10, marginTop: 3}]}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {deliveryTime}
            </CustomText>
          </View>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    padding: 15,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginTop: 20,
  },

  datesSelected: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    width: dimensions.Width(92),
  },

  icon: {
    width: 35,
    height: 35,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
