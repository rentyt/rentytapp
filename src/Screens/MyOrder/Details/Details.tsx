import React, {useContext, useState} from 'react';
import {View, Modal, TouchableOpacity, Platform} from 'react-native';
import {CustomText} from '../../../Components/CustomText';
import {dimensions, colors, stylesText} from '../../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import HeaderScrollView from 'react-native-header-scroll-view';
import {useQuery} from '@apollo/client';
import {queryOrder} from '../../../GraphQL';
import {MainContext} from '../../../store/MainProvider';
import Car from './Car';
import Owner from './Owner';
import Items from './Items';
import DaySeleted from './DaySelected';
import Proccess from './proccess';
import HeaderModal from '../../../Components/HeaderModal';
import Note from './Note';
import Resumen from './Resumen';
import ItemBaby from './Itemsbaby';
import ItemExtra from './ItemsExtra';
import LoadingComponent from '../../../Components/PlaceHolded/Details';
import Rating from './Rating';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Details({route, navigation}) {
  const {id} = route.params;
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {t} = useTranslation();
  const modo = useColorSchemeContext();
  const {users} = useContext(MainContext);
  const [visible, setVisible] = useState(false);
  const [VisibleRating, setVisiRating] = useState(false);

  const {data, loading, refetch} = useQuery(queryOrder.GET_ORDER_ID, {
    variables: {id: id},
  });

  const order = data && data.getOrder ? data.getOrder.data : null;

  setTimeout(() => {
    refetch();
  }, 1500);

  const titleColor = {
    light: colors.black,
    dark: colors.white,
  };

  const textColor = titleColor[modo];

  const bgColor = {
    light: colors.colorInput,
    dark: colors.back_dark,
  };

  const bg = bgColor[modo];

  const borderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const border = borderColor[modo];

  const bgBTN = {
    light: colors.black,
    dark: colors.white,
  };

  const bgBtn = bgBTN[modo];

  if (loading) {
    return (
      <View style={{height: dimensions.ScreenHeight}}>
        <LoadingComponent />
      </View>
    );
  }

  const onPressBTN = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (order.status === 'Recogido') {
      setVisiRating(true);
    } else {
      navigation.goBack();
    }
  };

  const colorsStatus = () => {
    if (order.status === 'Rechazado') {
      return colors.ERROR;
    } else if (order.status === 'Devuelto') {
      return colors.ERROR;
    } else if (order.status === 'Pendiente de pago') {
      return colors.orange1;
    } else if (order.status === 'Nueva') {
      return colors.orange1;
    } else {
      return colors.green;
    }
  };

  const displayStatus = t(`status:${order.status}`);

  return (
    <View style={styles.container}>
      <HeaderScrollView
        title={`ID #${order.channelOrderDisplayId}`}
        titleStyle={{color: textColor}}
        containerStyle={{
          backgroundColor: 'transparent',
        }}
        headerContainerStyle={{
          backgroundColor: bg,
          borderBottomColor: border,
        }}
        headlineStyle={{
          color: textColor,
          marginTop: 30,
          fontSize: 20,
          fontWeight: 'bold',
        }}
        scrollViewProps={{
          showsVerticalScrollIndicator: false,
          keyboardShouldPersistTaps: 'handled',
        }}>
        <View style={{marginTop: 20, marginBottom: dimensions.Height(20)}}>
          <TouchableOpacity
            style={[styles.status, {backgroundColor: colorsStatus()}]}
            onPress={() => setVisible(true)}>
            <CustomText
              numberOfLines={1}
              style={[stylesText.TitleCard]}
              light={colors.white}
              dark={colors.white}>
              {displayStatus.toUpperCase()}
            </CustomText>
          </TouchableOpacity>
          <View style={{margin: 20}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {t('myOrder:details:ownerTitle')}
            </CustomText>
            <Owner data={order} users={users} navigation={navigation} />
          </View>

          <View style={{marginHorizontal: 20, marginTop: 20}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {t('myOrder:details:carTitle')}
            </CustomText>
          </View>
          <Car data={order} navigation={navigation} request={false} />
          <Items data={order} />
          {order.babyChair.length > 0 ? <ItemBaby data={order} /> : null}
          {order.extraEquipament.length > 0 ? <ItemExtra data={order} /> : null}

          <DaySeleted
            arrayDates={order.selectdDay}
            deliveryTime={order.deliveryDate.time}
            timePickup={order.pickupDate.time}
          />
          <View style={{marginHorizontal: 20, marginTop: 20}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {t('myOrder:details:noteTitle')}
            </CustomText>
          </View>
          <Note data={order} />

          <View style={{marginHorizontal: 20, marginTop: 30}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {t('myOrder:details:resumen')}
            </CustomText>
          </View>
          <Resumen data={order} request={false} />
        </View>
      </HeaderScrollView>
      <TouchableOpacity
        onPress={onPressBTN}
        activeOpacity={100}
        style={[
          styles.btnBack,
          {
            backgroundColor:
              order.status === 'Recogido' ? colors.orange1 : bgBtn,
          },
        ]}>
        <CustomText
          light={colors.white}
          dark={colors.black}
          style={[stylesText.secondaryTextBold]}>
          {order.status === 'Recogido'
            ? t('myOrder:details:rating')
            : t('myOrder:details:back')}
        </CustomText>
      </TouchableOpacity>
      <Modal
        animationType="slide"
        visible={visible}
        presentationStyle="formSheet"
        statusBarTranslucent={true}
        onRequestClose={() => setVisible(false)}>
        <HeaderModal
          onPress={() => setVisible(false)}
          title={t('myOrder:details:status')}
          children={
            <View style={styles.centeredView}>
              <Proccess data={order.statusProcess} />
            </View>
          }
        />
      </Modal>
      <Rating
        data={order}
        visibleModal={VisibleRating}
        setvisibleModal={setVisiRating}
        refetch={refetch}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  status: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 100,
    maxWidth: 150,
    marginLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  btnBack: {
    position: 'absolute',
    bottom: 20,
    padding: 10,
    width: '90%',
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 100,
  },
});
