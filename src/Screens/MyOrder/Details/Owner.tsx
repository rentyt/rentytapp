import React, {useEffect} from 'react';
import {
  View,
  Image,
  Platform,
  TouchableOpacity,
  Linking,
  ActionSheetIOS,
} from 'react-native';
import {CustomText} from '../../../Components/CustomText';
import {dimensions, colors, stylesText, image} from '../../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useDynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {customMaspStyles} from '../../../Components/MapStyle';
import {useQuery} from '@apollo/client';
import {query} from '../../../GraphQL';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useTranslation} from 'react-i18next';
import {Avatar} from '../../../Components/Avatar';
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

export default function Car({data, users, navigation}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const stylos = useDynamicValue(customMaspStyles);
  const {t} = useTranslation();

  useEffect(() => {
    if (Platform.OS === 'ios') {
      requestMultiple([PERMISSIONS.IOS.LOCATION_ALWAYS]);
      requestMultiple([PERMISSIONS.IOS.LOCATION_WHEN_IN_USE]);
    } else {
      requestMultiple([PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION]);
      requestMultiple([PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION]);
    }
  }, []);

  const lat =
    data.Car && data.Car.location && data.Car.location.geometry
      ? data.Car.location.geometry.location.lat
      : 18.4860575;
  const lng =
    data.Car && data.Car.location && data.Car.location.geometry
      ? data.Car.location.geometry.location.lng
      : -69.9312117;

  const location = data.Car.location;

  const reponse = useQuery(query.GET_CONVERSATION_ONE, {
    variables: {userID: users._id, adsID: data.car},
  });

  const chast =
    reponse && reponse.data && reponse.data.getConversationOne
      ? reponse.data.getConversationOne.data
      : {data: ''};

  const dataNAv = chast
    ? chast
    : {
        ads: {
          _id: data.Car._id,
          car: data.Car.car,
          imageCar: data.Car.images[0].uri,
          owner: data.Car.owner,
        },
        user: users,
        Owner: data.Owner,
        messages: [],
        userID: users._id,
        adsID: data.car,
        owner: data.wner,
        userRoomID: `${Number.parseInt(getRandomArbitrary(10000, 100000000))}`,
        ownerRoomID: `${Number.parseInt(getRandomArbitrary(10000, 100000000))}`,
        created_at: new Date(),
      };

  const onPress = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: t('myOrder:details:ActionSheetIOS:title'),
          message: t('myOrder:details:ActionSheetIOS:message'),
          options: [
            t('myOrder:details:ActionSheetIOS:btn1'),
            t('myOrder:details:ActionSheetIOS:btn2'),
            t('myOrder:details:ActionSheetIOS:btn3'),
          ],
          cancelButtonIndex: 2,
        },
        buttonIndex => {
          if (buttonIndex === 0) {
            Linking.openURL(`http://maps.apple.com/?saddr=${lat}&daddr=${lng}`);
          } else if (buttonIndex === 1) {
            Linking.openURL(
              `https://www.google.com/maps/search/?api=1&query=${lat},${lng}`,
            );
          } else if (buttonIndex === 2) {
          }
        },
      );
    } else {
      Linking.openURL(
        `https://www.google.com/maps/search/?api=1&query=${lat},${lng}`,
      );
    }
  };

  const initChat = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    reponse.refetch();
    dataNAv.refetch = reponse.refetch;
    setTimeout(() => {
      navigation.navigate('ChatScreen', dataNAv);
    }, 100);
  };

  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
      }}>
      <View style={styles.mapContent}>
        {lat && lng ? (
          <MapView
            showsUserLocation={true}
            provider={PROVIDER_GOOGLE}
            customMapStyle={stylos}
            style={{
              height: dimensions.Height(20),
              width: dimensions.Width(94),
              borderRadius: 10,
            }}
            region={{
              latitude: lat,
              longitude: lng,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}>
            <Marker
              coordinate={{
                latitude: lat,
                longitude: lng,
              }}>
              <Image source={image.MapPin} style={{width: 50, height: 50}} />
            </Marker>
          </MapView>
        ) : null}
      </View>

      <View style={[styles.items]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            width: dimensions.Width(50),
          }}>
          <View style={{width: 65}}>
            <Avatar uri={data.Owner.avatar} width={60} height={60} />
          </View>
          <View style={{marginLeft: 5}}>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {data.Owner.name} {data.Owner.lastName}{' '}
            </CustomText>
            <CustomText
              numberOfLines={2}
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText]}>
              {location.formatted_address}
            </CustomText>
          </View>
        </View>
        <View style={styles.line}>
          <TouchableOpacity onPress={onPress}>
            <Icon
              name="location-arrow"
              type="FontAwesome"
              size={30}
              color={colors.green}
            />
          </TouchableOpacity>
        </View>
      </View>

      <TouchableOpacity style={styles.btn} onPress={initChat}>
        <Icon
          name="message-square"
          type="Feather"
          size={20}
          color={colors.green}
          style={{marginTop: 3, marginRight: 7}}
        />
        <CustomText
          numberOfLines={1}
          light={colors.green}
          dark={colors.green}
          style={stylesText.secondaryTextBold}>
          {t('myOrder:details:chatBtn')} {data.Owner.name.toUpperCase()}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  iconContent: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: 7,
  },

  icon: {
    width: 25,
    height: 25,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  mapContent: {
    overflow: 'hidden',
    height: dimensions.Height(20),
    width: dimensions.Width(94),
    borderRadius: 10,
  },

  items: {
    height: 'auto',
    width: dimensions.Width(90),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    padding: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: -95,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
  },

  btn: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    borderColor: colors.green,
    borderWidth: 1,
    width: dimensions.Width(94),
    marginTop: 30,
    padding: 16,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  line: {
    borderLeftWidth: 0.5,
    borderLeftColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
