import React from 'react';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import {View} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';

export default function Note({data}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <View style={styles.container}>
      <CustomText
        numberOfLines={8}
        style={stylesText.secondaryText}
        light={colors.rgb_153}
        dark={colors.rgb_153}>
        <Icon
          type="Fontisto"
          name="quote-a-right"
          size={14}
          style={styles.icon}
        />{' '}
        {`  ${data.note}  `}{' '}
        <Icon
          type="Fontisto"
          name="quote-a-left"
          size={14}
          style={styles.icon}
        />
      </CustomText>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    width: dimensions.Width(92),
    marginTop: 15,
    alignSelf: 'center',
  },
  icon: {
    color: new DynamicValue('black', 'white'),
  },
});
