import React, {useEffect, useState} from 'react';
import {View, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';

import {CustomText} from '../../../Components/CustomText';
import {Button} from '../../../Components/Buttom';
import {stylesText, dimensions, colors} from '../../../Themes';
import Confetti from 'react-native-confetti';
import LottieView from 'lottie-react-native';
import {useTranslation} from 'react-i18next';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

function Result({setvisibleModal}) {
  const [_confettiView, set_confettiView] = useState(null);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  useEffect(() => {
    if (_confettiView) {
      _confettiView.startConfetti();
    }
  }, [_confettiView]);

  return (
    <View style={styles.container}>
      <Confetti ref={node => set_confettiView(node)} />
      <View
        style={{
          alignSelf: 'center',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: dimensions.Height(15),
        }}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <LottieView
            source={require('../../../Assets/animated/star.json')}
            autoPlay
            loop
            style={{width: 300}}
          />
        </View>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryTextBold,
            {textAlign: 'center', paddingHorizontal: 30, marginBottom: 20},
          ]}>
          {t('myOrder:rating:success:title')}
        </CustomText>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryText,
            {textAlign: 'center', paddingHorizontal: 30},
          ]}>
          {t('myOrder:rating:success:description')}
        </CustomText>
        <View style={{marginTop: 50}}>
          <Button
            dark={colors.white}
            light={colors.white}
            onPress={() => {
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(type, optiones);
              setvisibleModal(false);
            }}
            title={t('addCar:resultbtn')}
          />
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  signupButtonContainer: {
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
});

export default Result;
