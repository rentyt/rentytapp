import React from 'react';
import {View} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import {CustomText} from '../../../Components/CustomText';
import {colors, stylesText} from '../../../Themes';
import {formaterPrice} from '../../../Utils/formaterPrice';
import Icon from 'react-native-dynamic-vector-icons';
import moment from 'moment';
import {useTranslation} from 'react-i18next';

export default function Resumen({data, request}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  const pay =
    data.stripePaymentIntent &&
    data.stripePaymentIntent.charges &&
    data.stripePaymentIntent.charges.data
      ? data.stripePaymentIntent.charges.data[0]
      : null;

  const iconPay = () => {
    switch (data.paymentMethod) {
      case 'Paypal':
        return {
          iconName: 'FontAwesome',
          iconType: 'cc-paypal',
          title: t('myOrder:details:resumenPrice:paypalPay'),
        };
      case 'Tarjeta de credito':
        return {
          iconName: 'Tarjeta',
          iconType: 'Tarjeta',
          title: t('myOrder:details:resumenPrice:card'),
          car: pay
            ? pay.payment_method_details.card
            : {last4: 'No card', brand: 'visa'},
        };
      case 'Efectivo':
        return {
          iconName: 'FontAwesome',
          iconType: 'money',
          title: t('myOrder:details:resumenPrice:cash'),
        };
      case 'Apple Pay':
        return {
          iconName: 'Fontisto',
          iconType: 'apple-pay',
          title: t('myOrder:details:resumenPrice:apple'),
        };
      case 'Google Pay':
        return {
          iconName: 'AntDesign',
          iconType: 'google',
          title: t('myOrder:details:resumenPrice:google'),
        };
    }
  };
  return (
    <View style={{marginTop: 30}}>
      <View style={styles.colunm}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.TitleCard]}>
          {t('myOrder:details:resumenPrice:days')}
        </CustomText>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.TitleCard]}>
          {formaterPrice(
            data.payment.totalDay,
            data.payment.currency.localcode,
            data.payment.currency.currency,
          )}
        </CustomText>
      </View>

      {data.payment.cuponDiscount > 0 ? (
        <View style={styles.colunm}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.TitleCard]}>
            {t('myOrder:details:resumenPrice:cupon')}
          </CustomText>
          <CustomText
            light={colors.green}
            dark={colors.green}
            style={[stylesText.TitleCard]}>
            -{' '}
            {formaterPrice(
              data.payment.cuponDiscount,
              data.payment.currency.localcode,
              data.payment.currency.currency,
            )}
          </CustomText>
        </View>
      ) : null}

      {data.payment.discountPerDay ? (
        <View style={styles.colunm}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.TitleCard]}>
            {t('myOrder:details:resumenPrice:discountperday')}
          </CustomText>
          <CustomText
            light={colors.green}
            dark={colors.green}
            style={[stylesText.TitleCard]}>
            -{' '}
            {formaterPrice(
              data.payment.discountPerDay,
              data.payment.currency.localcode,
              data.payment.currency.currency,
            )}
          </CustomText>
        </View>
      ) : null}

      {data.payment.discountPerMon > 0 ? (
        <View style={styles.colunm}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.TitleCard]}>
            {t('myOrder:details:resumenPrice:antelacion')}
          </CustomText>
          <CustomText
            light={colors.green}
            dark={colors.green}
            style={[stylesText.TitleCard]}>
            -{' '}
            {formaterPrice(
              data.payment.discountPerMon,
              data.payment.currency.localcode,
              data.payment.currency.currency,
            )}
          </CustomText>
        </View>
      ) : null}

      <View style={styles.colunm}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.TitleCard]}>
          {t('myOrder:details:resumenPrice:services')}
        </CustomText>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.TitleCard]}>
          {' '}
          {formaterPrice(
            data.payment.serviceChange,
            data.payment.currency.localcode,
            data.payment.currency.currency,
          )}
        </CustomText>
      </View>

      {data.payment.babyChairs > 0 ? (
        <View style={styles.colunm}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.TitleCard]}>
            {t('myOrder:details:resumenPrice:babyseat')}
          </CustomText>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.TitleCard]}>
            {formaterPrice(
              data.payment.babyChairs,
              data.payment.currency.localcode,
              data.payment.currency.currency,
            )}
          </CustomText>
        </View>
      ) : null}

      {data.payment.extrasEquipment > 0 ? (
        <View style={styles.colunm}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.TitleCard]}>
            {t('myOrder:details:resumenPrice:extra')}
          </CustomText>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.TitleCard]}>
            {formaterPrice(
              data.payment.extrasEquipment,
              data.payment.currency.localcode,
              data.payment.currency.currency,
            )}
          </CustomText>
        </View>
      ) : null}

      {data.payment.airCost > 0 ? (
        <View style={styles.colunm}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.TitleCard]}>
            {t('myOrder:details:resumenPrice:airport')}
          </CustomText>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.TitleCard]}>
            {formaterPrice(
              data.payment.airCost,
              data.payment.currency.localcode,
              data.payment.currency.currency,
            )}
          </CustomText>
        </View>
      ) : null}

      <View style={styles.colunm}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.TitleCard]}>
          {t('myOrder:details:resumenPrice:subtotal')}
        </CustomText>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.TitleCard]}>
          {formaterPrice(
            data.payment.subtotal,
            data.payment.currency.localcode,
            data.payment.currency.currency,
          )}
        </CustomText>
      </View>

      <View style={styles.colunm}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.TitleCard]}>
          {t('myOrder:details:resumenPrice:totaldiscount')}
        </CustomText>
        <CustomText
          light={colors.green}
          dark={colors.green}
          style={[stylesText.TitleCard]}>
          -{' '}
          {formaterPrice(
            data.payment.totalDiscount,
            data.payment.currency.localcode,
            data.payment.currency.currency,
          )}
        </CustomText>
      </View>

      <View style={styles.colunm}>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold]}>
          {t('myOrder:details:resumenPrice:total')}
        </CustomText>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold]}>
          {' '}
          {formaterPrice(
            data.payment.total,
            data.payment.currency.localcode,
            data.payment.currency.currency,
          )}
        </CustomText>
      </View>

      {data.payment.totalDolar > 0 ? (
        <View style={styles.colunm}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryTextBold]}>
            {t('myOrder:details:resumenPrice:totalDolar')}
          </CustomText>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.secondaryTextBold]}>
            {' '}
            {formaterPrice(
              data.payment.totalDolar,
              data.payment.currency.localcode,
              'USD',
            )}
          </CustomText>
        </View>
      ) : null}

      {request ? null : (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            marginLeft: 20,
            marginTop: 20,
          }}>
          {iconPay().iconName === 'Tarjeta' ? (
            <>
              {iconPay().car.brand === 'mastercard' ? (
                <Icon
                  type="Fontisto"
                  name="mastercard"
                  size={14}
                  color={colors.rgb_153}
                />
              ) : null}
              {iconPay().car.brand === 'visa' ? (
                <Icon
                  type="Fontisto"
                  name="visa"
                  size={14}
                  color={colors.rgb_153}
                />
              ) : null}
              {iconPay().car.brand === 'amex' ? (
                <Icon
                  type="Fontisto"
                  name="american-express"
                  size={14}
                  color={colors.rgb_153}
                />
              ) : null}
            </>
          ) : (
            <Icon
              //@ts-ignore
              type={`${iconPay().iconName}`}
              name={iconPay().iconType}
              size={14}
              color={colors.rgb_153}
            />
          )}

          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.terciaryText, {marginLeft: 7}]}>
            {iconPay().title}{' '}
            {iconPay().iconName === 'Tarjeta' ? iconPay().car.last4 : ''}
          </CustomText>
        </View>
      )}

      <CustomText
        light={colors.black}
        dark={colors.white}
        style={[
          stylesText.terciaryText,
          {marginLeft: 20, marginTop: 20, marginRight: 15},
        ]}>
        {`${t('myOrder:details:resumenPrice:hour')}  ${moment(
          data.created_at,
        ).format('LLL')}`}
      </CustomText>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  colunm: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    paddingVertical: 15,
  },
});
