import React from 'react';
import {View, Switch, Platform, FlatList} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useDynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import CheckBox from '@react-native-community/checkbox';

export default function Items({data}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {t} = useTranslation();

  const renderItems = ({item}) => {
    return (
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.icon}>
            <Icon
              type={item.iconType}
              name={item.iconName}
              size={28}
              color={colors.green}
            />
          </View>
          <View style={{marginLeft: 10, width: dimensions.Width(52)}}>
            <CustomText
              style={stylesText.TitleCard}
              light={colors.black}
              dark={colors.white}>
              <CustomText
                style={stylesText.TitleCard}
                light={colors.green}
                dark={colors.green}>
                {item.quantity} ×
              </CustomText>{' '}
              {t(`details:babyChairs:${item.title}`)}
            </CustomText>
            <CustomText
              style={[stylesText.secondaryText, {width: dimensions.Width(52)}]}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t(`details:babyChairs:${item.description}`)}
            </CustomText>
          </View>
        </View>
        <View style={{marginRight: 10}}>
          <CheckBox
            value={true}
            disabled={true}
            onTintColor={colors.green}
            tintColors={{true: colors.green, false: colors.rgb_153}}
            onCheckColor={colors.green}
            onAnimationType="fill"
            offAnimationType="fill"
            style={{marginLeft: 10}}
          />
        </View>
      </View>
    );
  };
  return (
    <View>
      <FlatList
        data={data.babyChair}
        renderItem={(item: any) => renderItems(item)}
        keyExtractor={(item: any) => item._id}
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  icon: {
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    marginTop: 20,
    marginHorizontal: 15,
  },
});
