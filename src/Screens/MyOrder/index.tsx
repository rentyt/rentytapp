import React, {useContext, useState} from 'react';
import Header from '../../Components/Header';
import {View, Platform, FlatList, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../Themes';
import {useTranslation} from 'react-i18next';
import {MainContext} from '../../store/MainProvider';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import SegmentedControl from '@react-native-community/segmented-control';
import {useQuery} from '@apollo/client';
import {queryOrder} from '../../GraphQL';
import LottieView from 'lottie-react-native';
import {CustomText} from '../../Components/CustomText';
import Items from './Items';
import LoadingComponent from '../../Components/PlaceHolded/List';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function MyOrder({navigation}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const {users} = useContext(MainContext);

  const [active, setActive] = useState(0);

  const status = ['Nueva', 'Confirmada', 'Recogido', 'Entregado'];

  const statusLst = ['Rechazado', 'Finalizada', 'Devuelto'];

  const {data, refetch, loading} = useQuery(queryOrder.GET_ALL_ORDER, {
    variables: {user: users._id, status: active === 0 ? status : statusLst},
  });

  const order = data && data.getAllOrder ? data.getAllOrder.data : [];

  setTimeout(() => {
    refetch();
  }, 2000);

  const renderItem = ({item}) => {
    return (
      <Items
        data={item}
        navigation={navigation}
        refetch={refetch}
        request={false}
      />
    );
  };

  return (
    <View style={styles.container}>
      <Header
        navigation={navigation}
        title={t('myOrder:titleHeader')}
        leftIcon={false}
      />
      <View
        style={{
          width: '96%',
          alignSelf: 'center',
          marginTop: 20,
          marginBottom: 30,
          marginHorizontal: 15,
        }}>
        <SegmentedControl
          values={[t('myOrder:incourse'), t('myOrder:last')]}
          selectedIndex={active}
          onChange={event => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
            setActive(event.nativeEvent.selectedSegmentIndex);
          }}
        />
      </View>

      {loading ? (
        <View style={{height: dimensions.ScreenHeight}}>
          <LoadingComponent />
        </View>
      ) : (
        <FlatList
          data={order}
          renderItem={(item: any) => renderItem(item)}
          keyExtractor={(item: any) => item._id}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <LottieView
                source={require('../../Assets/animated/Nodata.json')}
                autoPlay
                loop
                style={{width: 200}}
              />
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryText,
                  {textAlign: 'center', marginHorizontal: 30},
                ]}>
                {t('myOrder:noData')}
              </CustomText>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('Feed', {
                    input: {},
                  })
                }
                style={{
                  marginTop: 30,
                  backgroundColor: colors.green,
                  paddingVertical: 15,
                  paddingHorizontal: 50,
                  borderRadius: 100,
                }}>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={[stylesText.secondaryTextBold, {textAlign: 'center'}]}>
                  {t('myOrder:btn')}
                </CustomText>
              </TouchableOpacity>
            </View>
          }
          ListFooterComponent={
            <View style={{marginBottom: dimensions.Height(30)}} />
          }
        />
      )}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
