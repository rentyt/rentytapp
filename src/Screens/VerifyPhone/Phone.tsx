import React, {useRef, useState, useContext} from 'react';
import {View, Animated, Keyboard, TouchableOpacity, Alert} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {colors, dimensions, image, stylesText} from '../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../Components/CustomText';
import PhoneInput from 'react-native-phone-number-input';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {Button} from '../../Components/Buttom';
import {MAIN_URL} from '../../Utils/Urls';
import Toast from 'react-native-toast-message';
import ModalLoading from '../../Components/ModalLoading';
import {useTranslation} from 'react-i18next';
import {MainContext} from '../../store/MainProvider';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Phone({route, navigation}) {
  const styles = useDynamicValue(dynamicStyles);
  const phoneInput = useRef<PhoneInput>(null);
  const [value, setValue] = useState('');
  const [Loading, setLoading] = useState(false);
  const [CountryCode, setCountryCode] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [Calling, setCallingCode] = useState('809');
  const [widthImage] = useState(new Animated.Value(dimensions.Height(30)));
  const [top] = useState(new Animated.Value(dimensions.Height(5)));

  const {getUser, getCard} = useContext(MainContext);

  const {t} = useTranslation();

  const {id, token, Register, user, password} = route.params;

  const phone = CountryCode === 'DO' ? phoneNumber : Calling + value;

  const checkValid = phoneInput.current?.isValidNumber(phone);

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];

  const modalColor = {
    light: false,
    dark: true,
  };

  const modalColors = modalColor[mode];

  const animatedIcon = () => {
    Animated.sequence([
      //@ts-ignore
      Animated.timing(widthImage, {
        toValue: dimensions.Height(15),
        duration: 500,
      }),
      //@ts-ignore
      Animated.timing(top, {
        toValue: dimensions.Height(0),
        duration: 500,
      }),
    ]).start();
  };

  const animatedImagen = () => {
    Keyboard.dismiss();
    Animated.sequence([
      //@ts-ignore
      Animated.timing(widthImage, {
        toValue: dimensions.Height(30),
        duration: 200,
      }),
      //@ts-ignore
      Animated.timing(top, {
        toValue: dimensions.Height(5),
        duration: 200,
      }),
    ]).start();
  };

  const goToBack = () => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    navigation.goBack();
  };

  const VerifyPhone = async () => {
    animatedImagen();
    ReactNativeHapticFeedback.trigger('selection', optiones);
    setLoading(true);
    const datas = {
      phone,
      id,
      token,
      Register,
      user,
      password,
    };
    fetch(`${MAIN_URL}/verify-phone`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({phone}),
    })
      .then(async res => {
        const verify = await res.json();
        if (verify.success) {
          setLoading(false);
          getUser();
          getCard();
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          navigation.navigate('SendCode', {data: datas});
        } else {
          Alert.alert(`Error con el número ${phone}`);
          setLoading(false);
        }
      })
      .catch(err => {
        setLoading(false);
        ReactNativeHapticFeedback.trigger('notificationError', optiones);
        Toast.show({
          text1: t('login:bag'),
          text2: t('forgot:error'),
          position: 'top',
          type: 'error',
          topOffset: 50,
        });
      });
  };

  return (
    <View style={styles.container}>
      <ModalLoading loading={Loading} />
      <Toast ref={ref => Toast.setRef(ref)} />
      <TouchableOpacity style={styles.back} onPress={goToBack}>
        <Icon
          type="Feather"
          name="arrow-left"
          size={26}
          color={colors.rgb_153}
        />
      </TouchableOpacity>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Animated.View
            style={{
              alignItems: 'center',
              zIndex: 0,
            }}>
            <Icon
              type="Feather"
              name="smartphone"
              size={56}
              color={colors.green}
            />
          </Animated.View>
          <View style={styles.content}>
            <CustomText
              style={[stylesText.titleText, {textAlign: 'center'}]}
              light={colors.black}
              dark={colors.white}>
              {t('phone:title')}
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {
                  marginTop: dimensions.Height(3),
                  textAlign: 'center',
                  marginHorizontal: 15,
                  marginBottom: 15,
                },
              ]}>
              {t('phone:subtitle')}
            </CustomText>
            <PhoneInput
              ref={phoneInput}
              defaultValue={value}
              code={Calling}
              defaultCode="DO"
              layout="first"
              placeholder={t('phone:placeholder')}
              textInputProps={{
                selectionColor: colors.green,
                placeholderTextColor: colors.rgb_153,
                onFocus: animatedIcon,
              }}
              onChangeText={text => {
                setValue(text);
              }}
              onChangeCode={text => setCallingCode(text)}
              withDarkTheme={modalColors}
              autoFocus
              containerStyle={[styles.containerInput, {marginLeft: 15}]}
              textContainerStyle={styles.containerInput}
              textInputStyle={[
                styles.textInput,
                {backgroundColor: backgroundColor},
              ]}
              flagButtonStyle={styles.flagButtonStyle}
              codeTextStyle={styles.codeTextStyle}
              onChangeFormattedText={text => {
                setCountryCode(phoneInput.current?.getCountryCode() || '');
                setCallingCode(phoneInput.current?.getCallingCode() || '');
                setPhoneNumber(text);
              }}
            />
            {checkValid ? (
              <View style={{marginTop: 30}}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  loading={false}
                  onPress={() => VerifyPhone()}
                  title={t('phone:btn')}
                />
              </View>
            ) : null}
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  containerInput: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    width: dimensions.Width(100),
    borderRadius: 10,
  },

  flagButtonStyle: {
    height: 57,
    marginTop: 20,
    borderRadius: 10,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },

  codeTextStyle: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    padding: 20,
    borderRadius: 10,
    width: 80,
    overflow: 'hidden',
    color: colors.rgb_153,
  },

  textInput: {
    padding: 20,
    borderRadius: 10,
    color: colors.rgb_153,
  },

  content: {
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },

  back: {
    width: 50,
    height: 50,
    marginTop: 60,
    marginBottom: 10,
    marginLeft: 15,
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 0,
  },
});
