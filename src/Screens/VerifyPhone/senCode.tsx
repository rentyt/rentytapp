import React, {useState, useContext} from 'react';
import {View, Animated, Keyboard, TouchableOpacity, Alert} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {colors, dimensions, stylesText} from '../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../Components/CustomText';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {Button} from '../../Components/Buttom';
import CountDown from 'react-native-countdown-component';
import {MAIN_URL} from '../../Utils/Urls';
import {MainContext} from '../../store/MainProvider';
import {setItem} from '../../helpers/AsyncStorage';
import {useMutation} from '@apollo/client';
import {mutations} from '../../GraphQL';
import {useTranslation} from 'react-i18next';
import ModalLoading from '../../Components/ModalLoading';
import SelectMarker from '../../Components/SelectMarker';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function SendCode({route, navigation}) {
  const styles = useDynamicValue(dynamicStyles);
  const [value, setValue] = useState('');
  const [Loading, setLoading] = useState(false);
  const [widthImage] = useState(new Animated.Value(dimensions.Height(30)));
  const [top] = useState(new Animated.Value(dimensions.Height(5)));
  const {setId, getUser, getCard} = useContext(MainContext);
  const [visibleMak, setvisibleMark] = useState(false);

  const CELL_COUNT = 4;

  const {t} = useTranslation();

  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const {id, token, Register, phone, password, user} = route.params.data;

  console.log(user);

  const [LoginUser] = useMutation(mutations.AUTENTICAR_USUARIO);

  const animatedIcon = () => {
    Animated.sequence([
      //@ts-ignore
      Animated.timing(widthImage, {
        toValue: dimensions.Height(15),
        duration: 500,
      }),
      //@ts-ignore
      Animated.timing(top, {
        toValue: dimensions.Height(0),
        duration: 500,
      }),
    ]).start();
  };

  const animatedImagen = () => {
    Keyboard.dismiss();
    Animated.sequence([
      //@ts-ignore
      Animated.timing(widthImage, {
        toValue: dimensions.Height(30),
        duration: 200,
      }),
      //@ts-ignore
      Animated.timing(top, {
        toValue: dimensions.Height(5),
        duration: 200,
      }),
    ]).start();
  };

  const goToBack = () => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    navigation.goBack();
  };

  const sendCode = async () => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    animatedImagen();
    setLoading(true);
    const input = {
      phone: phone,
      code: value,
      id: id,
    };
    fetch(`${MAIN_URL}/verify-code`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(input),
    })
      .then(async res => {
        const confirm = await res.json();
        if (confirm.success) {
          setLoading(false);
          if (Register) {
            LoginUser({
              variables: {
                email: user.email,
                password: password,
              },
            })
              .then(async res => {
                if (res.data.LoginUser.success) {
                  ReactNativeHapticFeedback.trigger(
                    'notificationSuccess',
                    optiones,
                  );
                  const user = res.data.LoginUser.data;
                  await setItem('token', user.token);
                  await setItem('id', user.id);
                  await setItem('user', JSON.stringify(user.user));
                  setId(user.id);

                  getUser();
                  getCard();

                  if (user.user.myCategory.length > 0) {
                    navigation.navigate('Home');
                  } else {
                    setvisibleMark(true);
                  }
                } else {
                  navigation.navigate('Login');
                  getUser();
                  getCard();
                }
              })
              .catch(() => {
                navigation.navigate('Login');
              });
          } else {
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
            await setItem('token', token);
            await setItem('id', id);
            await setItem('user', JSON.stringify(user));
            setId(id);
            getUser();
            getCard();
            setTimeout(() => {
              if (user.myCategory.length > 0) {
                navigation.navigate('Home');
              } else {
                setvisibleMark(true);
              }
            }, 1000);
          }
        } else {
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          Alert.alert(
            t('forgot:error'),
            t('forgot:error'),
            [
              {
                text: t('sendCode:resend'),
                onPress: () => VerifyPhone(phone),
              },
            ],

            {cancelable: false},
          );
        }
      })
      .catch(err => console.log(err));
  };

  const finisehd = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    Alert.alert(
      t('sendCode:expireMessage'),
      t('sendCode:expirecontent'),
      [
        {
          text: t('sendCode:resend'),
          onPress: () => VerifyPhone(phone),
        },
        {
          text: t('sendCode:cancel'),
          onPress: () => {},
        },
      ],

      {cancelable: false},
    );
  };

  const VerifyPhone = async (phone: any) => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    setLoading(true);
    fetch(`${MAIN_URL}/verify-phone`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({phone}),
    })
      .then(async res => {
        const confirm = await res.json();
        if (confirm.success) {
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          setLoading(false);
        } else {
          Alert.alert(
            t('forgot:error'),
            t('forgot:error'),
            [
              {
                text: t('sendCode:cancel'),
                onPress: () => console.log('ok'),
              },
            ],

            {cancelable: false},
          );
          setLoading(false);
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <View style={styles.container}>
      <ModalLoading loading={Loading} />
      <TouchableOpacity style={styles.back} onPress={goToBack}>
        <Icon
          type="Feather"
          name="arrow-left"
          size={26}
          color={colors.rgb_153}
        />
      </TouchableOpacity>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Animated.View
            style={{
              alignItems: 'center',
              zIndex: 0,
            }}>
            <Icon
              type="Feather"
              name="check-circle"
              size={56}
              color={value.length === 4 ? colors.green : colors.rgb_153}
            />
          </Animated.View>
          <View style={styles.content}>
            <CustomText
              style={[stylesText.titleText, {textAlign: 'center'}]}
              light={colors.black}
              dark={colors.white}>
              {t('sendCode:title')}
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {
                  marginTop: dimensions.Height(3),
                  textAlign: 'center',
                  marginHorizontal: 15,
                  marginBottom: 15,
                },
              ]}>
              {t('sendCode:subtitle')} {''} {phone}
            </CustomText>

            <CodeField
              ref={ref}
              {...props}
              value={value}
              onChangeText={setValue}
              onFocus={animatedIcon}
              cellCount={CELL_COUNT}
              autoFocus
              rootStyle={styles.codeFiledRoot}
              keyboardType="number-pad"
              textContentType="oneTimeCode"
              renderCell={({index, symbol, isFocused}) => (
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  key={index}
                  style={[styles.cell, isFocused && styles.focusCell]}
                  onLayout={getCellOnLayoutHandler(index)}>
                  {symbol || (isFocused ? <Cursor /> : null)}
                </CustomText>
              )}
            />
            <View style={{flexDirection: 'row'}}>
              <CustomText
                style={{
                  color: colors.rgb_153,
                  fontWeight: '200',
                  fontSize: 14,
                  paddingTop: 30,
                }}>
                {t('sendCode:counDown')}{' '}
              </CustomText>

              <CountDown
                until={60 * 5 + 0}
                size={8}
                onFinish={() => finisehd()}
                digitStyle={{
                  backgroundColor: 'transparent',
                  marginTop: 29,
                }}
                digitTxtStyle={{
                  color: colors.rgb_153,
                  fontWeight: '200',
                  fontSize: 14,
                }}
                timeToShow={['M', 'S']}
                timeLabels={false}
              />
            </View>
            {value.length === 4 ? (
              <View style={{marginTop: 30}}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  loading={false}
                  onPress={() => sendCode()}
                  title={t('sendCode:btn')}
                />
              </View>
            ) : null}
          </View>
        </View>
      </KeyboardAwareScrollView>
      {visibleMak ? (
        <SelectMarker
          visibleModal={visibleMak}
          setvisibleModal={setvisibleMark}
          navigation={navigation}
        />
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  containerInput: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    width: dimensions.Width(100),
    borderRadius: 10,
  },

  flagButtonStyle: {
    height: 57,
    marginTop: 20,
    borderRadius: 10,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },

  codeTextStyle: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    padding: 20,
    borderRadius: 10,
    overflow: 'hidden',
    color: colors.rgb_153,
  },

  textInput: {
    padding: 20,
    borderRadius: 10,
    color: colors.rgb_153,
  },

  content: {
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },

  back: {
    width: 50,
    height: 50,
    marginTop: 60,
    marginBottom: 10,
    marginLeft: 15,
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 0,
  },

  cell: {
    width: 70,
    height: 70,
    lineHeight: 48,
    fontSize: 24,
    borderRadius: 35,
    borderWidth: 1,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    textAlign: 'center',
    paddingTop: 10,
    marginHorizontal: 10,
  },
  focusCell: {
    borderColor: colors.main,
  },

  codeFiledRoot: {marginTop: 20},
});
