import React from 'react';
import {View} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Loader from '../../Components/ModalLoading';
import {WebView} from 'react-native-webview';
import {colors} from '../../Themes';

const WebViews = ({route, navigation}) => {
  const {url, createdOrder} = route.params;
  const styles = useDynamicValue(dynamicStyles);

  const onNavigationStateChange = async data => {
    console.log(url, createdOrder);
    if (data.title === 'success') {
      createdOrder();
    }
  };
  return (
    <View style={styles.container}>
      <WebView
        source={{
          uri: url,
        }}
        startInLoadingState={true}
        useWebKit={true}
        renderLoading={() => <Loader color={colors.main} />}
        onNavigationStateChange={onNavigationStateChange}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        injectedJavaScript={`document.f1.submit()`}
      />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },
});

export default WebViews;
