import React, {useState, useContext} from 'react';
import {View, Platform, FlatList, Modal, Alert} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {dimensions, colors, stylesText} from '../../Themes';
import Header from '../../Components/Header';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {MainContext} from '../../store/MainProvider';
import {useQuery} from '@apollo/client';
import {queryRating} from '../../GraphQL';
import {RatingCalculatorUser} from '../../Utils/RatingCalculartorUser';
import {RatingCalculator} from '../../Utils/RatingCalculator';
import {CustomText} from '../../Components/CustomText';
import LottieView from 'lottie-react-native';
import Star from '../../Components/star';
import {ComentsUser} from './items';
import {Button} from '../../Components/Buttom';
import HeaderModal from '../../Components/HeaderModal';
import Textarea from 'react-native-textarea';
import Success from './Succeess';
import {useMutation} from '@apollo/client';
import {mutations} from '../../GraphQL';
import ModalLoading from '../../Components/ModalLoading';
import LoadingComponent from '../../Components/PlaceHolded/Details';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function MyRating({navigation}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const {users} = useContext(MainContext);
  const [dataVal, setDataVal] = useState(null);
  const [visibleModal, setvisibleModal] = useState(false);
  const [sucess, setSucess] = useState(false);
  const [coments, setComent] = useState('');
  const [Loading, setLoading] = useState(false);
  const [updateRating] = useMutation(mutations.UPDATE_RATING);

  const {data, loading, refetch} = useQuery(queryRating.GET_RATING, {
    variables: {owner: users._id},
  });

  const ratings = data && data.getRating ? data.getRating.data : [];

  setTimeout(() => {
    refetch();
  }, 1500);

  const averageRatingUser = RatingCalculatorUser(ratings);
  const averageRating = RatingCalculator(ratings);

  const replyComent = val => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setDataVal(val);
    setvisibleModal(true);
  };

  const closetModal = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setDataVal(null);
    setvisibleModal(false);
  };

  const updateRatingFunc = (id: string) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setLoading(true);
    const input = {
      _id: id,
      reply: {
        coment: coments,
        date: new Date(),
      },
    };
    if (coments) {
      updateRating({variables: {input: {data: input}}})
        .then(res => {
          if (res.data.updateRating.success) {
            refetch();
            setLoading(false);
            setSucess(true);
            setComent('');
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
          } else {
            refetch();
            setSucess(false);
            Alert.alert(res.data.updateRating.messages);
          }
        })
        .catch(() => {
          setLoading(false);
          setSucess(false);
          Alert.alert('Algo salio mal intentalo de nuevo');
        });
    } else {
      setLoading(false);
      Alert.alert(t('Myrating:alert'));
    }
  };

  const _renderItem = ({item}) => {
    return <ComentsUser data={item} onPress={() => replyComent(item)} />;
  };

  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <View style={styles.subItemd}>
          <View style={{width: dimensions.Width(50)}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryText]}>
              {t('Myrating:carRating')}
            </CustomText>
          </View>
          <View style={{justifyContent: 'flex-end', alignItems: 'flex-end'}}>
            <Star star={averageRating} />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold, {marginTop: 5}]}>
              {averageRating}
            </CustomText>
          </View>
        </View>

        <View style={[styles.subItemd, {borderBottomWidth: 0, marginTop: 10}]}>
          <View style={{width: dimensions.Width(50)}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryText]}>
              {t('Myrating:userRating')}
            </CustomText>
          </View>
          <View style={{justifyContent: 'flex-end', alignItems: 'flex-end'}}>
            <Star star={averageRatingUser} />
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold, {marginTop: 5}]}>
              {averageRatingUser}
            </CustomText>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header
        navigation={navigation}
        title={t('Myrating:titleHeader')}
        leftIcon={false}
      />

      {loading ? (
        <View>
          <LoadingComponent rating={true} />
        </View>
      ) : (
        <FlatList
          data={ratings}
          renderItem={item => _renderItem(item)}
          keyExtractor={item => item.id}
          ListHeaderComponent={renderHeader}
          ListFooterComponent={<View style={{marginBottom: 100}} />}
          showsVerticalScrollIndicator={false}
          horizontal={false}
          ListEmptyComponent={
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: 30,
                marginTop: 30,
              }}>
              <LottieView
                source={require('../../Assets/animated/star.json')}
                autoPlay
                loop
                style={{width: 250, height: 200}}
              />
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryText,
                  {
                    textAlign: 'center',
                  },
                ]}>
                {t('Myrating:NoRating')}
              </CustomText>
            </View>
          }
        />
      )}

      {dataVal ? (
        <Modal
          animationType="slide"
          visible={visibleModal}
          presentationStyle="formSheet"
          collapsable={true}
          statusBarTranslucent={true}
          onRequestClose={() => setvisibleModal(false)}>
          <ModalLoading loading={Loading} />
          {sucess ? (
            <Success
              setvisibleModal={setvisibleModal}
              setDataVal={setDataVal}
              setSucess={setSucess}
            />
          ) : (
            <>
              <HeaderModal
                clean={false}
                onPress={closetModal}
                title={t('Myrating:modalTitle')}
                children={
                  <View style={styles.contentModal}>
                    <View style={{marginTop: 20}}>
                      <Textarea
                        containerStyle={styles.textareaContainer}
                        //@ts-ignore
                        style={styles.textarea}
                        onChangeText={value => setComent(value)}
                        defaultValue={
                          dataVal.reply ? dataVal.reply.coment : coments
                        }
                        maxLength={250}
                        minLength={100}
                        selectionColor={colors.green}
                        placeholder={t('Myrating:placeHolded')}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                      />
                    </View>
                  </View>
                }
              />
              <View style={styles.butonModal}>
                <View style={{alignSelf: 'center', marginBottom: 30}}>
                  <Button
                    title={t('Myrating:btn')}
                    light={colors.white}
                    dark={colors.white}
                    loading={false}
                    onPress={() => updateRatingFunc(dataVal._id)}
                  />
                </View>
              </View>
            </>
          )}
        </Modal>
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  header: {
    width: dimensions.Width(96),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    marginHorizontal: 10,
    marginVertical: 20,
    alignItems: 'center',
    shadowColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 12,
    elevation: 23,
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.colorBorder, colors.back_suave_dark),
    padding: 15,
  },

  subItemd: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    borderBottomWidth: 0.5,
    borderBottomColor: new DynamicValue(
      colors.colorBorder,
      colors.back_suave_dark,
    ),
    paddingBottom: 5,
  },

  butonModal: {
    width: dimensions.Width(100),
    height: 100,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    bottom: -5,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  textareaContainer: {
    height: 180,
    padding: 10,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderRadius: 10,
    marginTop: 20,
    marginHorizontal: 15,
    width: '93%',
  },
  textarea: {
    textAlignVertical: 'top', // hack android
    height: 170,
    fontSize: 16,
    color: new DynamicValue(colors.black, colors.white),
    fontFamily: 'Poppins',
  },
});
