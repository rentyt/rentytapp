import React, {useEffect, useRef} from 'react';
import {View, FlatList, Animated} from 'react-native';
import CardStore from '../../Components/AdsCard/SmallCard';
import {dimensions, stylesText, colors} from '../../Themes';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import {useQuery} from '@apollo/client';
import {query} from '../../GraphQL';
import Loading from '../../Components/PlaceHolded/SmallCard';
import LottieView from 'lottie-react-native';

export default function Favourites({navigation, setNumberConut, user}) {
  const scrollY = useRef(new Animated.Value(0)).current;

  const {data, refetch, loading} = useQuery(query.GET_ADS_FAVOURITES, {
    variables: {id: user._id},
  });

  const ads = data && data.getAdsFavourites ? data.getAdsFavourites.data : [];

  setNumberConut(ads.length);

  const {t} = useTranslation();

  setTimeout(() => {
    refetch();
  }, 2000);

  const renderItem = ({item, index}) => {
    return (
      <CardStore
        data={item.Ads}
        navigation={navigation}
        refetch={refetch}
        scrollY={scrollY}
        index={index}
        favourite={true}
      />
    );
  };

  const ListEmptyComponent = () => {
    return (
      <View style={{marginTop: dimensions.Height(5)}}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <LottieView
            source={require('../../Assets/animated/Favoritos.json')}
            autoPlay
            loop
            style={{width: 200}}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              {textAlign: 'center', marginHorizontal: 20, marginTop: 40},
            ]}>
            {t('favourites:title')}
          </CustomText>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              {textAlign: 'center', marginTop: 15, marginHorizontal: 20},
            ]}>
            {t('favourites:messages')}
          </CustomText>
        </View>
      </View>
    );
  };

  if (loading) {
    return (
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 50,
        }}>
        <Loading />
        <Loading />
        <Loading />
        <Loading />
        <Loading />
        <Loading />
      </View>
    );
  }

  return (
    <FlatList
      data={ads}
      renderItem={(item: any) => renderItem(item)}
      keyExtractor={(item: any) => item.marker}
      style={{
        paddingBottom: dimensions.Height(15),
        marginTop: 30,
        marginLeft: 3,
      }}
      showsVerticalScrollIndicator={false}
      ListEmptyComponent={ListEmptyComponent}
    />
  );
}
