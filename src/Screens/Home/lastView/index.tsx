import React, {useRef} from 'react';
import {View, Animated, ScrollView, Platform} from 'react-native';
import CardStore from '../../../Components/AdsCard/Vertical';
import {dimensions, colors} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {User} from '../../../interfaces/index';
import {CustomText} from '../../../Components/CustomText';
import {stylesText} from '../../../Themes/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import {useQuery} from '@apollo/client';
import {query} from '../../../GraphQL';
import LoadingComponent from '../../../Components/PlaceHolded/ListHome';
import * as Animatable from 'react-native-animatable';

export interface IData {
  navigation: any;
  user: User;
  getNetworkAndLocation: () => void;
}

export default function LastView(props: IData) {
  const {navigation, user, getNetworkAndLocation} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const scrollY = useRef(new Animated.Value(0)).current;

  const {data, loading, refetch} = useQuery(query.GET_ADS_LAST_VIEW, {
    variables: {id: user && user._id},
  });

  setTimeout(() => {
    refetch();
  }, 1000);

  const ads = data && data.getAdsLastView ? data.getAdsLastView.data : [];

  const FilterAds =
    ads &&
    ads.filter(ad => {
      return ad.Ads !== null;
    });

  const {t} = useTranslation();

  const renderItem = (item: any, i: number) => {
    return (
      <CardStore
        data={item.Ads}
        navigation={navigation}
        key={i}
        refetch={refetch}
        scrollY={scrollY}
        index={i}
        feed={false}
      />
    );
  };

  return (
    <Animatable.View animation="fadeInUp" duration={500} iterationCount={1}>
      <View style={styles.container}>
        {ads.length > 0 ? (
          <View
            style={{
              width: dimensions.Width(96),
              marginTop: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={styles.icon}>
                <Icon
                  type="Feather"
                  name="eye"
                  size={20}
                  color={colors.twitter_color}
                />
              </View>
              <CustomText
                style={[stylesText.titleText, {marginTop: 3}]}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {' '}
                {t('homeList:lastView')}
              </CustomText>
            </View>
          </View>
        ) : null}
        {loading ? (
          <View style={styles.containers}>
            <View
              style={{
                width: dimensions.Width(96),
                marginTop: 10,
                marginLeft: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={styles.icon}>
                  <Icon
                    type="Feather"
                    name="eye"
                    size={20}
                    color={colors.twitter_color}
                  />
                </View>
                <CustomText
                  style={[stylesText.titleText, {marginTop: 3}]}
                  light={colors.back_suave_dark}
                  dark={colors.white}>
                  {' '}
                  {t('homeList:lastView')}
                </CustomText>
              </View>
            </View>
            <LoadingComponent />
          </View>
        ) : (
          <>
            {ads.length > 0 ? (
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <View style={styles.containers}>
                  {FilterAds &&
                    FilterAds.map((item: any, i: number) => {
                      return renderItem(item, i);
                    })}
                </View>
              </ScrollView>
            ) : null}
          </>
        )}
      </View>
    </Animatable.View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: dimensions.ScreenWidth,
  },

  containers: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
    minWidth: dimensions.ScreenWidth,
  },

  icon: {
    width: 35,
    height: 35,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
