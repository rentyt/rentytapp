import React, {useRef} from 'react';
import {
  View,
  Animated,
  TouchableOpacity,
  ScrollView,
  Platform,
} from 'react-native';
import CardStore from '../../../Components/AdsCard/SmallCard';
import {dimensions, colors} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {IAds} from '../../../interfaces/index';
import {CustomText} from '../../../Components/CustomText';
import {stylesText} from '../../../Themes/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import {useQuery} from '@apollo/client';
import {query} from '../../../GraphQL';
import LoadingComponent from '../../../Components/PlaceHolded/SmallCard';
import * as Animatable from 'react-native-animatable';

export interface IData {
  navigation: any;
  getNetworkAndLocation: () => void;
}

export default function PopularCard(props: IData) {
  const {navigation, getNetworkAndLocation} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const scrollY = useRef(new Animated.Value(0)).current;

  const {data, loading, refetch, error} = useQuery(query.GET_ALL_ADS, {
    variables: {input: {popular: true, visible: true}, page: 1, limit: 10},
  });

  const ads = data && data.getAds ? data.getAds.data : [];

  const {t} = useTranslation();

  const renderItem = (item, i) => {
    return (
      <CardStore
        data={item}
        navigation={navigation}
        key={i}
        refetch={refetch}
        scrollY={scrollY}
        index={i}
        style={{marginBottom: 10}}
      />
    );
  };

  const ActAdd = {
    light: colors.green,
    dark: colors.green,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  const input = {popular: true};

  return (
    <Animatable.View animation="fadeInUp" duration={500} iterationCount={1}>
      <View style={styles.container}>
        <View
          style={{
            width: dimensions.Width(96),
            marginTop: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={styles.icon}>
              <Icon
                type="MaterialCommunityIcons"
                name="fire"
                size={20}
                color={colors.orange1}
              />
            </View>
            <CustomText
              style={stylesText.titleText}
              light={colors.back_suave_dark}
              dark={colors.white}>
              {' '}
              {t('homeList:popular')}
            </CustomText>
          </View>

          <TouchableOpacity
            onPress={() => {
              getNetworkAndLocation();
              navigation.navigate('Feed', {input: input});
            }}>
            <CustomText
              style={stylesText.secondaryText}
              light={mainColor}
              dark={mainColor}>
              {t('homeList:SeeAll')}
            </CustomText>
          </TouchableOpacity>
        </View>
        {loading ? (
          <View style={styles.containers}>
            <LoadingComponent />
          </View>
        ) : (
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.containers}>
              {ads &&
                ads.map((item: IAds, i: number) => {
                  return renderItem(item, i);
                })}
            </View>
          </ScrollView>
        )}
      </View>
    </Animatable.View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: dimensions.ScreenWidth,
  },

  containers: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 10,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  icon: {
    width: 35,
    height: 35,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
