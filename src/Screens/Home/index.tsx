import React, {useContext, useState, useEffect} from 'react';
import {View, Platform, DeviceEventEmitter} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {colors, dimensions} from '../../Themes';
import {MainContext} from '../../store/MainProvider';
import ButtonBar from '../../Components/ButtomBar';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import HeaderScrollView from 'react-native-header-scroll-view';
import {useTranslation} from 'react-i18next';
import QuickActions from 'react-native-quick-actions';
import OneSignal from 'react-native-onesignal';
import RNLocation from 'react-native-location';
import {checkNotifications} from 'react-native-permissions';
import AsyncStorage from '@react-native-community/async-storage';
import {getTrackingStatus} from 'react-native-tracking-transparency';
import Notification from '../../Components/ModalPermision/Notification';
import Location from '../../Components/ModalPermision/Location';
import NoInternet from '../../Components/ModalPermision/NoInternet';
import NetInfo from '@react-native-community/netinfo';
import TrackingModal from '../../Components/ModalPermision/TrackingModal';

//pages
import Profile from '../Profile';
import Inicio from './Inicio';
import Favourites from '../Favourites';
import Chats from '../Chats';
import Publish from '../Publish';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function HomeScreen({route, navigation}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const [ruteName, setruteName] = useState('Home');
  const {users, languaje, getLanguaje, getUser} = useContext(MainContext);
  const [favouriteConut, setfavouriteConut] = useState(0);
  const [activate, setactivate] = useState('activate');
  const [isNotification, setIsNotification] = useState(true);
  const [ModalNotification, setModalNotification] = useState(false);
  const [ModalLocation, setModalLocation] = useState(false);
  const [ModalInternet, setModalInternet] = useState(false);
  const [isLocation, setisLocation] = useState(true);
  const [conectedInterner, setconectedInterner] = useState(true);
  const [ModalTrackin, setModalTrackin] = useState(false);

  const getNotificationPermissions = async () => {
    const noti = await AsyncStorage.getItem('notification');
    setactivate(noti ? noti : 'activate');
  };

  const setNotification = async () => {
    const deviceState = await OneSignal.getDeviceState();
    checkNotifications().then(({status}) => {
      setIsNotification(status === 'granted' ? true : false);
      if (status === 'denied') {
        if (activate === 'activate') {
          setModalNotification(true);
        }
      } else if (status === 'blocked') {
        if (activate === 'activate') {
          setModalNotification(true);
        }
      } else if (status === 'unavailable') {
        if (activate === 'activate') {
          setModalNotification(true);
        }
      } else {
        if (deviceState.isSubscribed === false) {
          OneSignal.addTrigger('prompt_ios', 'true');
        }
        if (activate === 'activate') {
          setModalNotification(false);
        }
      }
    });
  };

  const getTransparency = async () => {
    const trackingStatus = await getTrackingStatus();
    if (
      trackingStatus === 'unavailable' ||
      trackingStatus === 'not-determined'
    ) {
      if (Platform.OS === 'ios') {
        setModalTrackin(true);
      }
    } else {
      setModalTrackin(false);
    }
  };

  const getLocation = () => {
    RNLocation.checkPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
      },
    }).then(per => {
      if (per) {
        setisLocation(per);
        setModalLocation(false);
      } else {
        setisLocation(per);
        setModalLocation(true);
      }
    });
  };

  const getNetwork = () => {
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
        setModalInternet(true);
        setconectedInterner(state.isConnected);
      } else {
        setconectedInterner(state.isConnected);
        setModalInternet(false);
      }
    });
  };

  const getNetworkAndLocation = () => {
    getLocation();
    getNetwork();
    getUser();
  };

  useEffect(() => {
    getUser();
    getNotificationPermissions();
    getLanguaje();
    getNetwork();
  }, []);

  OneSignal.setNotificationOpenedHandler(notification => {
    const extraData =
      notification &&
      notification.notification &&
      notification.notification.additionalData
        ? notification.notification.additionalData
        : {};
    //@ts-ignore
    if (extraData) {
      //@ts-ignore
      if (extraData.navigate) {
        //@ts-ignore
        navigation.navigate(extraData.router, extraData.data);
      }
    }
  });

  const changeTap = (rut: string) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (users) {
      setruteName(rut);
      if (rut === 'Chat') {
        setNotification();
      } else if (rut === 'Favourites') {
        getTransparency();
      } else {
        getNetworkAndLocation();
      }
    } else {
      if (rut !== 'Home') {
        navigation.navigate('Login');
      }
    }
  };

  const titleColor = {
    light: colors.black,
    dark: colors.white,
  };

  const modo = useColorSchemeContext();
  const textColor = titleColor[modo];

  const bgColor = {
    light: colors.colorInput,
    dark: colors.back_dark,
  };

  const bg = bgColor[modo];

  const borderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const border = borderColor[modo];

  const bgProfileColor = {
    light: colors.colorInput,
    dark: 'transparent',
  };

  const bgProfile = bgProfileColor[modo];

  const title =
    ruteName === 'Home'
      ? `${t(`home:${ruteName}`)}${
          users && users.name ? `, ${users.name}` : ''
        }`
      : t(`home:${ruteName}`);

  if (Platform.OS === 'ios') {
    //@ts-ignore
    QuickActions.setShortcutItems([
      //@ts-ignore
      {
        type: 'Favourites', // Required
        title: t('QuickActions:Favoritos'), // Optional, if empty, `type` will be used instead
        icon: 'Love', // Icons instructions below
        userInfo: {
          url: 'rentytapp.com://app/home/Favourites',
        },
      },
      //@ts-ignore
      {
        type: 'Chat', // Required
        title: t('QuickActions:Chats'), // Optional, if empty, `type` will be used instead
        icon: 'Message', // Icons instructions below
        userInfo: {
          url: 'rentytapp.com://app/home/Chat',
        },
      },
      //@ts-ignore
      {
        type: 'Add', // Required
        title: t('QuickActions:Añadir'), // Optional, if empty, `type` will be used instead
        icon: 'Add', // Icons instructions below
        userInfo: {
          url: 'rentytapp.com://app/add',
        },
      },
      //@ts-ignore
      {
        type: 'Account', // Required
        title: t('QuickActions:MiCuenta'), // Optional, if empty, `type` will be used instead
        icon: 'Contact', // Icons instructions below
        userInfo: {
          url: 'rentytapp.com://app/add',
        },
      },
      //@ts-ignore
      {
        type: 'Search', // Required
        title: t('QuickActions:Search'), // Optional, if empty, `type` will be used instead
        icon: 'Search', // Icons instructions below
        userInfo: {
          url: 'rentytapp.com://app/Search',
        },
      },
    ]);
  }

  DeviceEventEmitter.addListener('quickActionShortcut', data => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (data.type === 'Search') {
      navigation.navigate('Feed', {
        input: {autoFocus: true},
      });
    } else {
      setruteName(data.type);
    }
  });

  const gatFavouriteCont = (count: number) => {
    setfavouriteConut(count);
  };

  return (
    <View style={[styles.container]}>
      <HeaderScrollView
        title={title}
        titleStyle={{color: textColor}}
        containerStyle={{
          backgroundColor: ruteName === 'Account' ? bgProfile : 'transparent',
        }}
        headerContainerStyle={{
          backgroundColor: bg,
          borderBottomColor: border,
        }}
        headlineStyle={{
          color: textColor,
          marginTop: 30,
          fontSize: 20,
          fontWeight: 'bold',
        }}
        scrollViewProps={{
          showsVerticalScrollIndicator: false,
          keyboardShouldPersistTaps: 'handled',
        }}>
        <View>
          {ruteName === 'Account' ? (
            <View style={styles.content}>
              <Profile navigation={navigation} setruteName={setruteName} />
            </View>
          ) : null}
          {ruteName === 'Home' ? (
            <Inicio
              navigation={navigation}
              languaje={languaje}
              user={users}
              getNetworkAndLocation={getNetworkAndLocation}
            />
          ) : null}
          {ruteName === 'Favourites' ? (
            <View>
              <Favourites
                navigation={navigation}
                setNumberConut={gatFavouriteCont}
                user={users}
              />
            </View>
          ) : null}
          {ruteName === 'Chat' ? (
            <View>
              <Chats navigation={navigation} isNotification={isNotification} />
            </View>
          ) : null}
          {ruteName === 'Add' ? (
            <View>
              <Publish navigation={navigation} user={users} />
            </View>
          ) : null}
        </View>
      </HeaderScrollView>
      <ButtonBar
        onPressHome={() => changeTap('Home')}
        onPressOrder={() => changeTap('Chat')}
        onPressFavourites={() => changeTap('Favourites')}
        onPressAccount={() => changeTap('Account')}
        onPressAdd={() => changeTap('Add')}
        ruteName={ruteName}
        favouriteContu={() => favouriteConut}
      />
      <Notification
        visibleModal={ModalNotification}
        setModalVisible={setModalNotification}
      />
      <Location
        visibleModal={ModalLocation}
        setModalVisible={setModalLocation}
        setisLocation={setisLocation}
      />
      <NoInternet
        visibleModal={ModalInternet}
        setModalVisible={setModalInternet}
        isINternet={conectedInterner}
        setconectedInterner={setconectedInterner}
      />
      <TrackingModal
        visibleModal={ModalTrackin}
        setModalVisible={setModalTrackin}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    height: dimensions.Height(150),
  },

  content: {
    marginHorizontal: 10,
  },
});
