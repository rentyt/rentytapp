import React from 'react';
import {View, FlatList, TouchableOpacity, Platform} from 'react-native';
import CardStore from '../../../Components/AdsCard';
import {dimensions, colors} from '../../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import {User} from '../../../interfaces/index';
import {CustomText} from '../../../Components/CustomText';
import {stylesText} from '../../../Themes/TextStyle';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {useQuery} from '@apollo/client';
import {query} from '../../../GraphQL';
import LoadingAnimated from '../../../Components/PlaceHolded/ListHomeLargue';
import * as Animatable from 'react-native-animatable';

export interface IData {
  navigation: any;
  user: User;
  getNetworkAndLocation: () => void;
}

export default function ForyourCard(props: IData) {
  const {navigation, user, getNetworkAndLocation} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {data, loading, refetch} = useQuery(query.GET_ALL_ADS, {
    variables: {
      input: {
        'car.marker': {
          $in: user ? user.myCategory : [],
        },
        visible: true,
      },
      page: 1,
      limit: 10,
    },
  });

  const ads = data && data.getAds ? data.getAds.data : [];

  const {t} = useTranslation();

  const renderItem = ({item}) => {
    return (
      <CardStore
        data={item}
        navigation={navigation}
        refetch={refetch}
        small={true}
      />
    );
  };

  const ActAdd = {
    light: colors.green,
    dark: colors.green,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  return (
    <Animatable.View animation="fadeInUp" duration={500} iterationCount={1}>
      <View style={styles.container}>
        {ads.length > 0 ? (
          <View
            style={{
              width: dimensions.Width(96),
              marginTop: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={styles.icon}>
                <Icon
                  type="AntDesign"
                  name="like2"
                  size={20}
                  color={colors.green}
                />
              </View>
              <CustomText
                style={stylesText.titleText}
                light={colors.back_suave_dark}
                dark={colors.white}>
                {' '}
                {t('homeList:foryour')}
              </CustomText>
            </View>
            <TouchableOpacity
              onPress={() => {
                getNetworkAndLocation();
                navigation.navigate('Feed', {
                  input: {userCategory: user.myCategory},
                });
              }}>
              <CustomText
                style={stylesText.secondaryText}
                light={mainColor}
                dark={mainColor}>
                {t('homeList:SeeAll')}
              </CustomText>
            </TouchableOpacity>
          </View>
        ) : null}
        {loading ? (
          <>
            <View
              style={{
                width: dimensions.Width(96),
                marginTop: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={styles.icon}>
                  <Icon
                    type="AntDesign"
                    name="like2"
                    size={20}
                    color={colors.green}
                  />
                </View>
                <CustomText
                  style={stylesText.titleText}
                  light={colors.back_suave_dark}
                  dark={colors.white}>
                  {' '}
                  {t('homeList:foryour')}
                </CustomText>
              </View>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Feed', {
                    input: {userCategory: user.myCategory},
                  });
                }}>
                <CustomText
                  style={stylesText.secondaryText}
                  light={mainColor}
                  dark={mainColor}>
                  {t('homeList:SeeAll')}
                </CustomText>
              </TouchableOpacity>
            </View>
            <LoadingAnimated />
          </>
        ) : (
          <FlatList
            data={ads}
            renderItem={(item: any) => renderItem(item)}
            keyExtractor={(item: any) => item.marker}
            style={{paddingLeft: 5}}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
          />
        )}
      </View>
    </Animatable.View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: dimensions.ScreenWidth,
  },
  icon: {
    width: 35,
    height: 35,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
