import React from 'react';
import {View, ScrollView, Platform} from 'react-native';
import Popular from '../Popular';
import InstantantCard from '../Instantant';
import ForyourCard from '../Foryou';
import {dimensions, colors, stylesText} from '../../../Themes';
import GetCredit from '../../../Components/GetCredit';
import {CustomText} from '../../../Components/CustomText';
import SearchBar from '../../../Components/SearchBar';
import Category from '../Category';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import LastView from '../lastView';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';

export default function Inicio({
  navigation,
  languaje,
  user,
  getNetworkAndLocation,
}) {
  const {t} = useTranslation();
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View
      style={{
        marginBottom: Platform.select({
          ios: dimensions.Height(10),
          android: dimensions.Height(15),
        }),
      }}>
      <View
        style={{
          width: dimensions.Width(100),
          justifyContent: 'center',
          alignItems: 'center',
          marginHorizontal: 10,
        }}>
        <SearchBar
          navigation={navigation}
          getNetworkAndLocation={getNetworkAndLocation}
        />
      </View>

      <View style={{paddingHorizontal: 10, marginTop: 30}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            width: '100%',
          }}>
          <View style={styles.icon}>
            <Icon
              type="Feather"
              name="trending-up"
              size={20}
              color={colors.orange}
            />
          </View>
          <CustomText
            style={stylesText.titleText}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {' '}
            {t('homeList:principalMarker')}
          </CustomText>
        </View>

        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <Category
            navigation={navigation}
            getNetworkAndLocation={getNetworkAndLocation}
          />
        </ScrollView>
      </View>

      <LastView
        user={user}
        navigation={navigation}
        getNetworkAndLocation={getNetworkAndLocation}
      />

      <ForyourCard
        user={user}
        navigation={navigation}
        getNetworkAndLocation={getNetworkAndLocation}
      />

      <Popular
        navigation={navigation}
        getNetworkAndLocation={getNetworkAndLocation}
      />
      <InstantantCard
        navigation={navigation}
        getNetworkAndLocation={getNetworkAndLocation}
      />
      <GetCredit navigation={navigation} languaje={languaje} />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  icon: {
    width: 35,
    height: 35,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
