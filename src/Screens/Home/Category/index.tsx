import React from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import data from '../../../data/principalMarker.json';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import * as Animatable from 'react-native-animatable';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Category({navigation, getNetworkAndLocation}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const {t} = useTranslation();

  return (
    <View style={{marginBottom: 10}}>
      <View style={styles.container}>
        {data &&
          data.map((x, i) => {
            return (
              <Animatable.View
                animation="fadeInUp"
                duration={500}
                iterationCount={1}>
                <TouchableOpacity
                  onPress={() => {
                    //@ts-ignore
                    ReactNativeHapticFeedback.trigger(type, optiones);
                    getNetworkAndLocation();
                    if (x.brand === 'all') {
                      navigation.navigate('Feed', {
                        input: {},
                      });
                    } else {
                      navigation.navigate('Feed', {
                        input: {data: x},
                      });
                    }
                  }}
                  key={i}>
                  <View style={[styles.buttonView]}>
                    {x.brand === 'all' ? (
                      <View style={[styles.buttonViewIN]}>
                        <CustomText
                          light={colors.back_suave_dark}
                          dark={colors.white}
                          style={[
                            stylesText.miniTitle,
                            styles.ActiveBTN,
                            {fontWeight: '500'},
                          ]}>
                          {t('homeList:SeeAll')}
                        </CustomText>
                      </View>
                    ) : (
                      <View style={[styles.buttonViewIN]}>
                        <CustomText
                          light={colors.back_suave_dark}
                          dark={colors.white}
                          style={[stylesText.miniTitle, {fontWeight: '500'}]}>
                          {x.brand}
                        </CustomText>
                      </View>
                    )}
                  </View>
                </TouchableOpacity>
              </Animatable.View>
            );
          })}
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.Width(100),
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  buttonView: {
    borderRadius: dimensions.Width(100),
    paddingHorizontal: 2,
    paddingVertical: 2,
    marginHorizontal: 7,
    marginVertical: 7,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 8,
    elevation: Platform.select({ios: 15, android: 5}),
  },

  buttonViewIN: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: dimensions.Width(100),
    paddingHorizontal: 15,
    paddingVertical: 6,
  },

  ActiveBTN: {
    borderRadius: dimensions.Width(100),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
});
