import React from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';
import {stylesText, dimensions, colors} from '../../Themes';
import moment from 'moment';
import Icon from 'react-native-dynamic-vector-icons';
import {formaterPrice} from '../../Utils/formaterPrice';
import Clipboard from '@react-native-clipboard/clipboard';
import Toast from 'react-native-simple-toast';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useTranslation} from 'react-i18next';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function PromoCode(props: any) {
  const {data} = props;
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const borderColor = backgroundColors[mode];

  const copyToClipboard = () => {
    Clipboard.setString(data.clave);
    Toast.show(t('cupon:item:message'), Toast.LONG, ['UIAlertController']);
    ReactNativeHapticFeedback.trigger('selection', options);
  };

  return (
    <TouchableOpacity
      style={[styles.container, {borderColor: borderColor}]}
      onPress={() => copyToClipboard()}>
      <View style={styles.icon}>
        <Icon
          name="ticket-percent-outline"
          type="MaterialCommunityIcons"
          size={24}
          color={colors.green}
        />
      </View>
      <View style={{marginLeft: 10, width: dimensions.Width(45)}}>
        <CustomText
          style={stylesText.secondaryTextBold}
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}>
          {data.clave}
        </CustomText>
        {data.description ? (
          <CustomText
            numberOfLines={2}
            style={stylesText.secondaryText}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {data.description}
          </CustomText>
        ) : null}

        {data.exprirable ? (
          <CustomText
            style={stylesText.secondaryText}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            Expira:{' '}
            {data.vencido
              ? t('cupon:item:Expirado')
              : moment(data.expire).format('LL')}
          </CustomText>
        ) : null}
        <CustomText
          style={stylesText.secondaryText}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          {t('cupon:item:use')} {data.usage}{' '}
          {data.usage > 1 ? t('cupon:item:veces') : t('cupon:item:vez')}
        </CustomText>
        {data.uniqueCar ? (
          <CustomText
            style={stylesText.secondaryText}
            light={colors.main}
            dark={colors.main}>
            {t('cupon:item:car')}: {data.Car.car.marker} {data.Car.car.model}
          </CustomText>
        ) : null}
        {data.canjeado ? (
          <CustomText
            style={[stylesText.secondaryText, {marginTop: 5}]}
            light={colors.ERROR}
            dark={colors.ERROR}>
            {t('cupon:item:Utilizado')}
          </CustomText>
        ) : null}
      </View>
      <View
        style={{
          marginLeft: 'auto',
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}>
        {data.tipo === 'porcentaje' ? (
          <CustomText
            style={stylesText.secondaryTextBold}
            light={colors.black}
            dark={colors.white}>
            {data.descuento}%
          </CustomText>
        ) : (
          <CustomText
            style={stylesText.secondaryTextBold}
            light={colors.black}
            dark={colors.white}>
            {formaterPrice(data.descuento, 'en-US', 'US')}
          </CustomText>
        )}
        <CustomText
          style={stylesText.secondaryText}
          light={colors.rgb_153}
          dark={colors.rgb_153}>
          {t('cupon:item:book')}
        </CustomText>
      </View>
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    borderWidth: 0.5,
    padding: 10,
    marginVertical: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  icon: {
    width: 35,
    height: 35,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
