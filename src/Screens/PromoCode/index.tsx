import React, {useContext, useState} from 'react';
import {View, ScrollView, FlatList, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';
import Header from '../../Components/Header';
import {dimensions, colors, stylesText} from '../../Themes';
import LottieView from 'lottie-react-native';
import source from '../../Assets/animated/cupon.json';
import {useQuery} from '@apollo/client';
import {query} from '../../GraphQL';
import CuponCard from './Item';
import {MainContext} from '../../store/MainProvider';
import {useTranslation} from 'react-i18next';
import SegmentedControl from '@react-native-community/segmented-control';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Loader from '../../Components/PlaceHolded/List';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

const Cupones = ({navigation}) => {
  const styles = useDynamicValue(dynamicStyles);
  const {users} = useContext(MainContext);
  const {t} = useTranslation();
  const [active, setActive] = useState(0);

  const input = active === 0 ? {_id: users._id} : {};

  const {data, loading, refetch} = useQuery(query.GET_PROMO_CODE, {
    variables: {input: input},
  });

  const respuesta = data && data.getPromocode ? data.getPromocode.data : [];

  const _itemsRender = ({item}, refetch) => {
    return <CuponCard data={item} refetch={refetch} />;
  };

  return (
    <View style={styles.container}>
      <Header title={t('cupon:headerTitle')} navigation={navigation} />
      <View
        style={{
          width: '96%',
          alignSelf: 'center',
          marginTop: 20,
          marginBottom: 30,
          marginHorizontal: 15,
        }}>
        <SegmentedControl
          values={[t('cupon:MyCupon'), t('cupon:allCupon')]}
          selectedIndex={active}
          onChange={event => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
            setActive(event.nativeEvent.selectedSegmentIndex);
          }}
        />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            paddingHorizontal: 20,
            paddingBottom: dimensions.Height(10),
          }}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[stylesText.h1, {marginVertical: 10}]}>
            {active === 0 ? t('cupon:MyCupon') : t('cupon:allCupon')}
          </CustomText>
          {loading ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Loader />
            </View>
          ) : (
            <FlatList
              data={respuesta}
              renderItem={(item: any) => _itemsRender(item, refetch)}
              keyExtractor={(item: any) => item._id}
              showsVerticalScrollIndicator={false}
              ListEmptyComponent={
                <View
                  style={{
                    alignSelf: 'center',
                    marginVertical: 30,
                    alignItems: 'center',
                  }}>
                  <LottieView
                    source={source}
                    autoPlay
                    loop
                    resizeMode="cover"
                    style={{width: 150}}
                  />
                  <CustomText
                    light={colors.black}
                    dark={colors.white}
                    style={{
                      textAlign: 'center',
                      fontSize: dimensions.FontSize(13),
                      fontWeight: '200',
                      marginTop: 20,
                      paddingHorizontal: 30,
                    }}>
                    {t('cupon:message')}
                  </CustomText>
                </View>
              }
            />
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.colorInput, 'black'),
    flex: 1,
  },
});

export default Cupones;
