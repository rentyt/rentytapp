import React from 'react';
import {
  Alert,
  TouchableOpacity,
  SafeAreaView,
  View,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import CameraRoll from '@react-native-community/cameraroll';
import RNFetchBlob from 'rn-fetch-blob';
import {dimensions, colors} from '../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {useDynamicStyleSheet, DynamicStyleSheet} from 'react-native-dynamic';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Toast from 'react-native-simple-toast';
import {useTranslation} from 'react-i18next';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export const ImagenView = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {url, close} = props;

  const {t} = useTranslation();

  const getPermissionAndroid = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Image Download Permission',
          message: 'Your permission is required to save images to your device',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        return true;
      }
      Alert.alert(
        'Save remote Image',
        'Grant Me Permission to save Image',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
    } catch (err) {
      Alert.alert(
        'Save remote Image',
        'Failed to save Image: ' + err.message,
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
    }
  };

  const saveImage = async () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'android') {
      const granted = await getPermissionAndroid();
      if (!granted) {
        return;
      }
    }

    RNFetchBlob.config({
      fileCache: true,
      appendExt: 'png',
    })
      .fetch('GET', url)
      .then(res => {
        console.log(res);
        CameraRoll.save(res.data, {type: 'photo'})
          .then(() => {
            Toast.show(t(`chat:imageSaved`), Toast.LONG, ['UIAlertController']);
          })
          .catch(err => {
            Toast.show(t(`chat:imageSavedError`), Toast.LONG, [
              'UIAlertController',
            ]);
          });
      })
      .catch(error => {
        Toast.show(t(`chat:imageSavedError`), Toast.LONG, [
          'UIAlertController',
        ]);
      });
  };
  return (
    <SafeAreaView style={styles.headerView}>
      <View
        style={{
          justifyContent: 'space-between',
          width: '100%',
          flexDirection: 'row',
          paddingHorizontal: 15,
          paddingTop: Platform.select({
            ios: dimensions.IsIphoneX() ? 0 : 10,
            android: dimensions.Height(3.5),
          }),
        }}>
        <TouchableOpacity
          onPress={() => {
            close();
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
          }}>
          <Icon name="x" type="Feather" size={34} color={colors.white} />
        </TouchableOpacity>

        <TouchableOpacity onPress={saveImage}>
          <Icon
            name="arrow-down-circle"
            type="Feather"
            size={34}
            color={colors.white}
          />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  headerView: {
    backgroundColor: colors.back_dark,
    height: dimensions.Height(12),
    width: dimensions.ScreenWidth,
  },
});
