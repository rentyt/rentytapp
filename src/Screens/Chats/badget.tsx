import React from 'react';
import {Badget} from '../../Components/Badget';
import {useQuery} from '@apollo/client';
import {query} from '../../GraphQL';

export default function Badgets({id, userID}) {
  const {data, loading, refetch, error} = useQuery(
    query.GET_CONVERSATION_COUNT,
    {
      variables: {id: id, userID: userID},
    },
  );

  setTimeout(() => {
    refetch();
  }, 2000);

  const badget =
    data && data.getConversationNoRead
      ? data.getConversationNoRead.conversation
      : [];

  const badGets = badget && badget.filter(x => x._id === id);

  if (loading) {
    return null;
  }
  return (
    <Badget
      number={badGets && badGets.length > 0 ? badGets[0].count : 0}
      left={55}
      bottom={-30}
    />
  );
}
