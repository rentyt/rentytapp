import React, {useEffect} from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Image,
  ImageBackground,
  Alert,
  Platform,
  ActionSheetIOS,
} from 'react-native';
import {dimensions, stylesText, colors, image} from '../../Themes';
import {
  DynamicValue,
  useDynamicStyleSheet,
  DynamicStyleSheet,
  useColorSchemeContext,
  useDynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import moment from 'moment';
import Badget from './badget';
import {useMutation} from '@apollo/client';
import {mutations} from '../../GraphQL';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Mailer from 'react-native-mail';
import {OpenURLButton} from '../../Utils/onpenUrl';
import Toast from 'react-native-simple-toast';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ItemList({data, navigation, users, refetch, loading}) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const [readMessage] = useMutation(mutations.READ_MESSAGES);
  const [deleteConversation] = useMutation(mutations.DELETE_MESSAGES);

  const {t} = useTranslation();

  const types = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const logoUri = new DynamicValue(
    image.IconPlaceHoldedLigth,
    image.IconPlaceHoldedDark,
  );

  const source = useDynamicValue(logoUri);

  const mode = useColorSchemeContext();

  const bubbleright = {
    light: colors.colorBorder,
    dark: colors.back_suave_dark,
  };

  const borderColor = bubbleright[mode];

  const isOwner = data.owner === users._id;

  const name = isOwner ? data.user.name : data.Owner.name;
  const lastName = isOwner ? data.user.lastName : data.Owner.lastName;
  const isVerified = isOwner ? data.user.verified : data.Owner.verified;

  const userID = isOwner ? data.user._id : data.Owner._id;

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const readMessages = datos => {
    refetch();
    readMessage({variables: {id: datos._id, userID: userID}});
    setTimeout(() => {
      navigation.navigate('ChatScreen', datos);
    }, 500);
  };

  const deleteMessages = () => {
    deleteConversation({variables: {id: data._id}})
      .then(result => {
        const res = result.data.deleteConversation;
        if (res.success) {
          refetch();
          Toast.show(t(`chat:${res.messages}`), Toast.LONG, [
            'UIAlertController',
          ]);
        } else {
          refetch();
          Toast.show(t(`chat:${res.messages}`), Toast.LONG, [
            'UIAlertController',
          ]);
        }
      })
      .catch(() => {
        Toast.show(t(`chat:deteted_conversation_error`), Toast.LONG, [
          'UIAlertController',
        ]);
      });
  };

  const lastMessage =
    data.messages && data.messages[0] ? data.messages[0] : null;

  const message = t('chat:reportMessage');

  const handleEmail = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: message,
          recipients: ['report@rentytapp.com'],
          body: `<b>${message}</b>`,
          isHTML: true,
        },
        (error, event) => {},
      );
    } else {
      OpenURLButton(
        `mailto:report@rentytapp.com?subject=${message}&body=${message}`,
      );
    }
  };

  const onPress = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: `${t('chat:deteleTitle')} ${name} ${lastName}`,
          message: t('chat:deletemessage'),
          options: [
            t('chat:btn:cancel'),
            t('chat:btn:delete'),
            t('chat:btn:report'),
          ],
          cancelButtonIndex: 0,
          destructiveButtonIndex: 1,
        },
        buttonIndex => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            deleteMessages();
          } else if (buttonIndex === 2) {
            handleEmail();
          }
        },
      );
    } else {
      Alert.alert(
        `${t('chat:deteleTitle')} ${name} ${lastName}`,
        t('chat:deletemessage'),
        [
          {
            text: t('chat:btn:delete'),
            onPress: () => deleteMessages(),
          },

          {
            text: t('chat:btn:report'),
            onPress: () => handleEmail(),
          },

          {
            text: t('chat:btn:cancel'),
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    }
  };

  return (
    <TouchableWithoutFeedback
      style={styles.ItemContent}
      onPress={() => readMessages(data)}
      onLongPress={onPress}>
      <View style={[styles.content, {borderBottomColor: borderColor}]}>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.item}>
            <ImageBackground
              source={source}
              imageStyle={{borderRadius: 100}}
              resizeMode="cover"
              style={[styles.avatarcar, {marginTop: 10}]}>
              <Image
                source={{
                  uri: data.ads.imageCar,
                }}
                style={[styles.avatarcarImg]}
                resizeMode="cover"
              />
            </ImageBackground>
            <Image
              source={{
                uri: isOwner ? data.user.avatar : data.Owner.avatar,
              }}
              style={[styles.avatar, {marginLeft: -10}]}
              resizeMode="cover"
            />
          </View>

          <View
            style={{
              width: dimensions.Width(45),
              marginLeft: 15,
              marginTop: 15,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
                width: dimensions.Width(45),
              }}>
              <CustomText
                style={[stylesText.secondaryTextBold]}
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}>
                {name} {lastName}
              </CustomText>
              {isVerified ? (
                <Icon
                  type="MaterialIcons"
                  name="verified"
                  size={16}
                  color={colors.twitter_color}
                  style={{marginLeft: 5, marginTop: 1}}
                />
              ) : null}
            </View>
            <CustomText
              style={[stylesText.secondaryText, {fontWeight: '500'}]}
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}>
              {data.ads.car.marker} {data.ads.car.model} {data.ads.car.year}
            </CustomText>
            <CustomText
              style={[
                stylesText.terciaryText,
                {fontWeight: '300', marginTop: 3},
              ]}
              numberOfLines={1}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {lastMessage.received ? (
                <Icon
                  type="Ionicons"
                  name="checkmark-done-outline"
                  size={16}
                  color={
                    lastMessage.read ? colors.twitter_color : colors.rgb_153
                  }
                  style={{paddingTop: 5}}
                />
              ) : (
                <Icon
                  type="Ionicons"
                  name="ios-checkmark"
                  size={16}
                  color={colors.rgb_153}
                  style={{paddingTop: 5}}
                />
              )}{' '}
              {!lastMessage.text ? (
                <Icon
                  type="Feather"
                  name="camera"
                  size={14}
                  color={colors.rgb_153}
                  style={{paddingTop: 5}}
                />
              ) : null}
              {''}
              {lastMessage.text
                ? lastMessage.text
                : `  ${t('chat:photo')}`}{' '}
            </CustomText>
          </View>
        </View>

        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            width: 85,
          }}>
          <CustomText
            style={[stylesText.secondaryText, {fontWeight: '400'}]}
            numberOfLines={1}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            {moment(data.messages[0].createdAt).format('dd DD HH:mm')}
          </CustomText>
          <Badget id={data._id} userID={userID} />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  ItemContent: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    width: '100%',
  },
  content: {
    width: '100%',
    paddingHorizontal: 10,
    borderBottomWidth: 0.5,
    height: 100,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  item: {
    height: 65,
    width: 65,
    position: 'relative',
  },
  avatarcar: {
    width: 70,
    height: 70,
    borderRadius: 100,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    position: 'relative',
    marginTop: 7,
    borderWidth: 4,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  avatarcarImg: {
    width: 62,
    height: 62,
    borderRadius: 100,
  },

  avatar: {
    width: 37,
    height: 37,
    borderRadius: 100,
    position: 'absolute',
    top: 0,
    left: 45,
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.black),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },
});
