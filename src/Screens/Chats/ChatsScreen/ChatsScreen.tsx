import React, {
  useState,
  useCallback,
  useEffect,
  useRef,
  useContext,
} from 'react';
import {View, Platform, ImageBackground} from 'react-native';
import {GiftedChat, Send, InputToolbar, Bubble} from 'react-native-gifted-chat';
import {dimensions, stylesText, colors, image} from '../../../Themes';
import {
  DynamicValue,
  useDynamicStyleSheet,
  DynamicStyleSheet,
  useColorSchemeContext,
  useDynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import HeaderChat from './../HeaderChat';
import {MainContext} from '../../../store/MainProvider';
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';
import {ImagenView} from './../ImagenView';
import {PingMessage} from './../ping';
import SilentListener from 'react-native-silent-listener';
import * as Animatable from 'react-native-animatable';
import {MAIN_URL} from '../../../Utils/Urls';
import io from 'socket.io-client';
import RenderAction from './PickPhoto';
import uuid from 'react-native-uuid';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

let timeout = undefined;

const wallpaper = new DynamicValue(
  image.WallPaper_White,
  image.WallPaper_Black,
);

export function ChatScreen({route, navigation}) {
  const {ads, user, Owner, _id, userRoomID, ownerRoomID, messages, created_at} =
    route.params;
  const source = useDynamicValue(wallpaper);
  const {users, whoosh} = useContext(MainContext);
  const {t} = useTranslation();
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [message, setMessages] = useState(messages);
  const [text, settext] = useState('');
  const [isMuted, setmuted] = useState(false);
  const [typing, settyping] = useState(false);
  const [connected, setconnected] = useState(false);

  if (Platform.OS === 'ios') {
    requestMultiple([PERMISSIONS.IOS.CAMERA]);
  } else {
    requestMultiple([PERMISSIONS.ANDROID.CAMERA]);
  }

  const socket = io(MAIN_URL, {
    forceNew: true,
  });

  const mode = useColorSchemeContext();

  const ActAdd = {
    light: colors.main,
    dark: colors.main,
  };

  const mainColor = ActAdd[mode];

  const inactivesend = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const inactiveSend = inactivesend[mode];

  const activesend = {
    light: colors.green,
    dark: colors.green,
  };

  const activeSend = activesend[mode];

  const textInputcolor = {
    light: colors.black,
    dark: colors.white,
  };

  const textInputColor = textInputcolor[mode];

  const bubbleright = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const bubbleRight = bubbleright[mode];

  const chat = useRef();

  SilentListener.setCheckInterval(1);

  const onMuteChange = (nextVal: boolean) => {
    setmuted(nextVal);
  };

  const owner = ads.owner;
  const client = user._id;
  const isOwner = owner === users._id;

  const type = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  whoosh.setVolume(0.05);

  const connetSocket = () => {
    socket.on('connect', () => {
      socket.emit('online_user', {
        id: isOwner ? ownerRoomID : userRoomID,
        emiter: users._id,
        receptor: isOwner ? userRoomID : ownerRoomID,
        owner: owner,
        adsID: ads._id,
        conversationID: _id,
      });
      SilentListener.addEventListener(onMuteChange);
    });

    socket.on('online', datas => {
      setconnected(datas);
    });
  };

  function timeoutFunction() {
    socket.emit('typing', {
      emiter: users._id,
      receptor: isOwner ? userRoomID : ownerRoomID,
      owner: owner,
      typing: false,
    });
  }

  useEffect(() => {
    connetSocket();
    setMessages(
      messages.concat([
        {
          _id: uuid.v4(),
          createdAt: created_at,
          system: true,
        },
      ]),
    );
  }, []);

  useEffect(() => {
    socket.on('messages', datas => {
      setMessages(previousMessages =>
        GiftedChat.append(previousMessages, datas),
      );
    });

    socket.on('typing', datas => {
      settyping(datas.typing);
    });

    socket.on('online', datas => {
      setconnected(datas);
    });

    socket.on('exits', datas => {
      setconnected(datas);
    });

    connetSocket();
  }, [message]);

  const onSend = useCallback((messages = []) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    messages[0].sent = true;
    messages[0].received = connected;
    messages[0].pending = false;
    messages[0].system = false;
    messages[0].image = '';
    messages[0].read = connected;
    setMessages(previousMessages =>
      GiftedChat.append(previousMessages, messages),
    );

    if (isMuted === false) {
      PingMessage(whoosh);
    }

    clearTimeout(timeout);
    timeout = setTimeout(timeoutFunction, 5000);

    socket.emit('private_message', {
      messages,
      emiter: users._id,
      receptor: isOwner ? userRoomID : ownerRoomID,
      receptorNotification: isOwner ? client : owner,
      owner: owner,
      ads: ads,
      user: user,
      Owner: Owner,
      id: _id,
      ownerRoomID: ownerRoomID,
      userRoomID: userRoomID,
    });
  }, []);

  const renderSend = props => {
    return (
      <Send {...props}>
        <View
          style={{
            marginBottom: 39,
            marginRight: 15,
            marginLeft: 10,
          }}>
          <Icon
            type="Feather"
            name="send"
            size={30}
            color={text ? activeSend : inactiveSend}
          />
        </View>
      </Send>
    );
  };

  const renderInputToolbar = props => {
    return <InputToolbar {...props} containerStyle={styles.containerStyle} />;
  };

  const renderBubble = props => {
    return (
      <Bubble
        {...props}
        textStyle={{
          right: {
            color: colors.black,
            fontWeight: '400',
            fontFamily: 'Poppins',
          },
          left: {
            color: textInputColor,
            fontWeight: '400',
            fontFamily: 'Poppins',
          },
        }}
        wrapperStyle={{
          left: {
            backgroundColor: bubbleRight,
          },
          right: {
            backgroundColor: mainColor,
          },
        }}
      />
    );
  };

  const scrollToBottomComponent = () => {
    return (
      <View
        style={{
          backgroundColor: bubbleRight,
          borderRadius: 100,
          height: 45,
          width: 45,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Icon
          type="Feather"
          name="chevron-down"
          size={36}
          style={{marginTop: 5}}
        />
      </View>
    );
  };

  const textInputStyle = {
    color: textInputColor,
    backgroundColor: bubbleRight,
    borderRadius: 100,
    paddingTop: Platform.select({ios: 5, android: 8}),
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 5,
    fontFamily: 'Poppins',
    fontSize: 14,
  };

  const renderSystemMessage = props => {
    return (
      <View style={styles.secure}>
        <View>
          <Icon name="lock" type="Feather" size={24} color={colors.green} />
        </View>

        <View style={{marginLeft: 15}}>
          <CustomText
            style={[
              stylesText.secondaryText,
              {color: colors.green, fontWeight: '500'},
            ]}>
            {t('chat:systemMessage')}
          </CustomText>
        </View>
      </View>
    );
  };

  const renderHeader = (close, data) => {
    return <ImagenView close={close} url={data.props.source.uri} />;
  };

  const renderFooter = () => {
    return (
      <>
        {typing ? (
          <View style={{marginLeft: 15, marginBottom: 10}}>
            <Animatable.Text
              animation="pulse"
              easing="ease-in"
              iterationCount="infinite"
              style={styles.typing}>
              {t('chat:typing')}
            </Animatable.Text>
          </View>
        ) : null}
      </>
    );
  };

  const handleKeyPress = ({nativeEvent: {key: keyValue}}) => {
    if (keyValue === 'Enter') {
      clearTimeout(timeout);
      timeout = setTimeout(timeoutFunction, 5000);
    } else {
      socket.emit('typing', {
        emiter: users._id,
        receptor: isOwner ? userRoomID : ownerRoomID,
        owner: owner,
        typing: true,
        lastSeen: new Date(),
      });
      timeout = setTimeout(timeoutFunction, 5000);
    }
  };

  const renderActions = () => {
    return (
      <RenderAction
        setMessages={setMessages}
        type={type}
        connected={connected}
        users={users}
        GiftedChat={GiftedChat}
        PingMessage={PingMessage}
        isMuted={isMuted}
        whoosh={whoosh}
        socket={socket}
        isOwner={isOwner}
        owner={owner}
        client={client}
        ads={ads}
        user={user}
        Owner={Owner}
        connetSocket={connetSocket}
        t={t}
        id={_id}
        ownerRoomID={ownerRoomID}
        userRoomID={userRoomID}
      />
    );
  };

  return (
    <ImageBackground source={source} style={styles.bg}>
      <HeaderChat
        navigation={navigation}
        data={ads}
        connected={connected}
        users={users}
        user={user}
        Owner={Owner}
      />
      <GiftedChat
        ref={chat}
        messages={message}
        onSend={messages => onSend(messages)}
        showUserAvatar={true}
        scrollToBottom={true}
        onInputTextChanged={text => {
          connetSocket();
          settext(text);
        }}
        placeholder={t('chat:placeholde')}
        isKeyboardInternallyHandled={true}
        renderSend={renderSend}
        renderActions={renderActions}
        renderBubble={renderBubble}
        renderInputToolbar={renderInputToolbar}
        scrollToBottomComponent={scrollToBottomComponent}
        renderSystemMessage={renderSystemMessage}
        renderFooter={renderFooter}
        listViewProps={{showsVerticalScrollIndicator: false}}
        //@ts-ignore
        scrollToBottomOffset={150}
        textInputStyle={textInputStyle}
        textInputProps={{
          selectionColor: mainColor,
          onKeyPress: handleKeyPress,
        }}
        multiline={false}
        bottomOffset={dimensions.IsIphoneX() ? 22 : -12}
        alwaysShowSend={true}
        imageStyle={{width: 280, height: 280}}
        lightboxProps={{renderHeader}}
        user={{
          _id: users._id,
          name: `${users.name} ${users.lastName}`,
          avatar: users.avatar,
        }}
      />
    </ImageBackground>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  bg: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  Send: {
    marginLeft: 'auto',
  },

  containerStyle: {
    borderTopWidth: 0.2,
    borderTopColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    height: 85,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginBottom: -35,
  },

  action: {
    borderRadius: 100,
    height: 33,
    width: 33,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
    marginLeft: 10,
  },

  icon: {
    color: new DynamicValue(colors.black, colors.white),
  },

  secure: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 20,
    backgroundColor: 'rgba(41, 216, 131, 0.205)',
    marginHorizontal: 20,
    marginTop: 40,
    borderRadius: 10,
    marginBottom: dimensions.Height(5),
  },

  typing: {
    fontWeight: '500',
    fontFamily: 'Poppins',
    color: colors.green,
  },
});
