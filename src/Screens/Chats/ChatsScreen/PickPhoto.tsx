import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  Modal,
  Image,
  ImageBackground,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicValue,
  useDynamicStyleSheet,
  DynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {colors} from '../../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import uuid from 'react-native-uuid';
import {useMutation} from '@apollo/client';
import {mutations} from '../../../GraphQL';
import HeaderModal from '../../../Components/HeaderModal';
import {image, dimensions, stylesText} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import ImagePicker from 'react-native-image-crop-picker';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function PickPhoto({
  setMessages,
  type,
  connected,
  users,
  GiftedChat,
  PingMessage,
  isMuted,
  whoosh,
  socket,
  isOwner,
  owner,
  client,
  ads,
  user,
  Owner,
  connetSocket,
  t,
  id,
  ownerRoomID,
  userRoomID,
}) {
  const [visibleModal, setvisibleModal] = useState(false);
  const [text, setText] = useState('');
  const [imagen, setImagen] = useState(null);
  const [Loading, setLoading] = useState(false);
  const [LoadingMessage, setLoadingMessage] = useState(false);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const mode = useColorSchemeContext();

  const [singleUploadToAws] = useMutation(mutations.UPLOAD_FILE);

  const bubbleright = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const bubbleRight = bubbleright[mode];

  const canselModal = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setvisibleModal(false);
  };

  const OpenlModal = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setvisibleModal(true);
  };

  const sendMessage = () => {
    setLoadingMessage(true);
    const messagechat = [
      {
        _id: uuid.v4(),
        text: text,
        createdAt: new Date(),
        image: imagen,
        sent: true,
        received: connected.connected ? true : false,
        pending: false,
        system: false,
        read: false,
        user: {
          _id: users._id,
          name: `${users.name} ${users.lastName}`,
          avatar: users.avatar,
        },
      },
    ];

    setMessages(previousMessages =>
      GiftedChat.append(previousMessages, messagechat),
    );

    socket.emit('private_message', {
      messages: messagechat,
      emiter: users._id,
      receptor: isOwner ? userRoomID : ownerRoomID,
      receptorNotification: isOwner ? client : owner,
      owner: owner,
      ads: ads,
      user: user,
      Owner: Owner,
      id: id,
      ownerRoomID: ownerRoomID,
      userRoomID: userRoomID,
    });

    setTimeout(() => {
      setLoadingMessage(false);
      setvisibleModal(false);
      setImagen(null);
      setText('');
    }, 1000);
  };

  const OpenCameraData = async () => {
    setLoading(true);
    connetSocket();
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);

    if (isMuted === false) {
      PingMessage(whoosh);
    }

    ImagePicker.openCamera({
      mediaType: 'photo',
      width: 512,
      height: 512,
      cropping: true,
      includeBase64: true,
      useFrontCamera: false,
      cropperChooseText: t('updateProfile:Seleccionar'),
      cropperCancelText: t('updateProfile:Cancelar'),
      cropperToolbarTitle: t('chat:titlePhoto'),
    })
      .then(image => {
        const file = 'data:image/jpeg;base64,' + image.data;
        singleUploadToAws({variables: {file: file}})
          .then(res => {
            const result = res.data.singleUploadToAws.data;
            const img = result.Location;
            if (result) {
              setLoading(false);
              setImagen(img);
            } else {
              setLoading(false);
            }
          })
          .catch(() => {
            setLoading(false);
          });
      })
      .catch(e => {
        setLoading(false);
        console.log(e);
      });
  };

  const getImageData = async () => {
    setLoading(true);
    connetSocket();
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);

    if (isMuted === false) {
      PingMessage(whoosh);
    }

    ImagePicker.openPicker({
      mediaType: 'photo',
      width: 512,
      height: 512,
      cropping: true,
      includeBase64: true,
      useFrontCamera: false,
      cropperChooseText: t('updateProfile:Seleccionar'),
      cropperCancelText: t('updateProfile:Cancelar'),
      cropperToolbarTitle: t('chat:titlePhoto'),
    })
      .then(image => {
        const file = 'data:image/jpeg;base64,' + image.data;
        singleUploadToAws({variables: {file: file}})
          .then(res => {
            const result = res.data.singleUploadToAws.data;
            const img = result.Location;
            if (result) {
              setLoading(false);
              setImagen(img);
            } else {
              setLoading(false);
            }
          })
          .catch(() => {
            setLoading(false);
          });
      })
      .catch(e => {
        setLoading(false);
        console.log(e);
      });
  };

  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          style={[styles.action, {backgroundColor: bubbleRight}]}
          onPress={OpenlModal}>
          <Icon type="Feather" name="plus" size={26} style={styles.icon} />
        </TouchableOpacity>
      </View>
      <Modal
        animationType="slide"
        visible={visibleModal}
        presentationStyle="formSheet"
        collapsable={true}
        statusBarTranslucent={true}
        onRequestClose={canselModal}>
        <HeaderModal
          onPress={canselModal}
          title={t('chat:ModalTitle')}
          children={
            <View>
              <View style={styles.input}>
                <ImageBackground
                  source={image.PlaceHoldedCard}
                  style={styles.avatar}
                  imageStyle={{borderRadius: 100}}
                  resizeMode="cover">
                  <Image
                    source={{uri: users.avatar}}
                    style={styles.avatar}
                    resizeMode="cover"
                  />
                </ImageBackground>
                <TextInput
                  placeholder={t('chat:placeholde')}
                  style={styles.TextInput}
                  selectionColor={colors.green}
                  placeholderTextColor={colors.rgb_153}
                  autoCorrect={false}
                  multiline={true}
                  underlineColorAndroid="transparent"
                  onChangeText={setText}
                />
              </View>

              <View>
                {Loading ? (
                  <>
                    <View style={styles.loading}>
                      <ActivityIndicator size="large" color={colors.rgb_153} />
                    </View>
                  </>
                ) : (
                  <>
                    {!imagen ? (
                      <View style={styles.btn_Cont}>
                        <TouchableOpacity
                          style={styles.btn}
                          onPress={getImageData}>
                          <View
                            style={{
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Icon
                              name="image"
                              type="Feather"
                              size={24}
                              color={colors.green}
                            />
                            <CustomText
                              numberOfLines={1}
                              light={colors.green}
                              dark={colors.green}
                              style={[
                                stylesText.secondaryText,
                                {
                                  paddingBottom: 5,
                                  marginTop: 5,
                                  textAlign: 'center',
                                },
                              ]}>
                              {t('chat:addPhoto')}
                            </CustomText>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.btn}
                          onPress={OpenCameraData}>
                          <View
                            style={{
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Icon
                              name="camera"
                              type="Feather"
                              size={24}
                              color={colors.green}
                            />
                            <CustomText
                              numberOfLines={1}
                              light={colors.green}
                              dark={colors.green}
                              style={[
                                stylesText.secondaryText,
                                {
                                  paddingBottom: 5,
                                  marginTop: 5,
                                  textAlign: 'center',
                                },
                              ]}>
                              {t('chat:openCamera')}
                            </CustomText>
                          </View>
                        </TouchableOpacity>
                      </View>
                    ) : (
                      <View>
                        <TouchableOpacity
                          style={styles.delete}
                          onPress={() => setImagen(null)}>
                          <Icon
                            name="x"
                            type="Feather"
                            size={24}
                            color={colors.ERROR}
                          />
                        </TouchableOpacity>
                        <Image
                          source={{uri: imagen}}
                          style={styles.imagen}
                          resizeMode="cover"
                        />
                      </View>
                    )}
                  </>
                )}
              </View>

              {imagen ? (
                <TouchableOpacity
                  style={[styles.send, {backgroundColor: colors.green}]}
                  onPress={sendMessage}>
                  {LoadingMessage ? (
                    <ActivityIndicator size="small" color={colors.white} />
                  ) : (
                    <CustomText
                      light={colors.white}
                      dark={colors.white}
                      style={[stylesText.secondaryTextBold]}>
                      Enviar mensaje
                    </CustomText>
                  )}
                </TouchableOpacity>
              ) : null}
            </View>
          }
        />
      </Modal>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  action: {
    borderRadius: 100,
    height: 33,
    width: 33,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
    marginLeft: 10,
  },

  icon: {
    color: new DynamicValue(colors.black, colors.white),
  },

  avatar: {
    width: 35,
    height: 35,
    borderRadius: 100,
  },

  TextInput: {
    marginLeft: 10,
    fontFamily: 'Poppins',
    fontSize: 18,
    fontWeight: '400',
    color: new DynamicValue(colors.black, colors.white),
    width: dimensions.Width(82),
  },

  input: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: dimensions.Width(94),
    alignSelf: 'center',
  },

  btn_Cont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
  },

  btn: {
    padding: 10,
    margin: 7,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderWidth: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderColor: colors.green,
    width: dimensions.Width(45),
  },

  imagen: {
    width: dimensions.Width(94),
    height: dimensions.Height(30),
    alignSelf: 'center',
    marginTop: 50,
    borderRadius: 10,
  },

  delete: {
    position: 'absolute',
    zIndex: 100,
    top: 30,
    right: 30,
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    padding: 8,
    borderRadius: 100,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
  },

  loading: {
    width: dimensions.Width(94),
    height: dimensions.Height(30),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
    borderRadius: 10,
    borderStyle: 'dashed',
    borderColor: colors.green,
    borderWidth: 1,
  },

  send: {
    width: dimensions.Width(94),
    height: 55,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    borderRadius: 100,
  },
});
