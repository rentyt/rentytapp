import React, {useContext, useEffect, useState, useCallback} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Linking,
  Animated,
  ActivityIndicator,
} from 'react-native';
import ItemList from './itemList';
import {dimensions, stylesText, colors} from '../../Themes';
import {useDynamicValue, DynamicStyleSheet} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import {MainContext} from '../../store/MainProvider';
import Loading from '../../Components/PlaceHolded/Chat';
import LottieView from 'lottie-react-native';
import {useQuery} from '@apollo/client';
import {query} from '../../GraphQL';
import {useFocusEffect} from '@react-navigation/native';

export default function Chas({navigation, isNotification}) {
  const styles = useDynamicValue(dynamicStyles);
  const [height] = useState(new Animated.Value(0));

  const {t} = useTranslation();

  const {users} = useContext(MainContext);

  const {data, loading, refetch} = useQuery(query.GET_CONVERSATION_LIST, {
    variables: {userID: users._id},
  });

  const chats =
    data && data.getConversationList ? data.getConversationList.data : [];

  const animatedHeight = () => {
    refetch();
    Animated.sequence([
      //@ts-ignore
      Animated.timing(height, {
        toValue: 35,
        duration: 800,
      }),
    ]).start();
  };

  const animatedHiden = () => {
    refetch();
    Animated.sequence([
      //@ts-ignore
      Animated.timing(height, {
        toValue: 0,
        duration: 800,
      }),
    ]).start();
  };

  useFocusEffect(
    useCallback(() => {
      const unsubscribe = animatedHeight;
      return () => unsubscribe();
    }, [chats]),
  );

  useEffect(() => {
    animatedHeight();
  }, []);

  setTimeout(() => {
    animatedHiden();
  }, 2000);

  const renderItem = ({item}) => {
    return (
      <ItemList
        data={item}
        navigation={navigation}
        users={users}
        refetch={refetch}
        loading={loading}
      />
    );
  };

  const ListEmptyComponent = () => {
    return (
      <View style={{marginTop: dimensions.Height(5)}}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <LottieView
            source={require('../../Assets/animated/Chats.json')}
            autoPlay
            loop
            style={{width: 250}}
          />
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              {textAlign: 'center', marginHorizontal: 20},
            ]}>
            {t('chats:title')}
          </CustomText>
          <CustomText
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[
              stylesText.secondaryText,
              {textAlign: 'center', marginTop: 15, marginHorizontal: 20},
            ]}>
            {t('chats:messages')}
          </CustomText>
        </View>
      </View>
    );
  };

  return (
    <>
      {isNotification ? null : (
        <View style={styles.verify}>
          <Icon
            type="Feather"
            name="bell"
            size={30}
            color={colors.twitter_color}
          />
          <View style={{width: dimensions.Height(35), marginLeft: 10}}>
            <CustomText
              numberOfLines={1}
              style={[stylesText.secondaryTextBold]}
              light={colors.twitter_color}
              dark={colors.twitter_color}>
              {t('notification:title')}
            </CustomText>
            <CustomText
              numberOfLines={3}
              style={[stylesText.secondaryText]}
              light={colors.twitter_color}
              dark={colors.twitter_color}>
              {t('notification:message')}
            </CustomText>
            <TouchableOpacity
              onPress={() => Linking.openSettings()}
              style={{
                marginTop: 10,
                borderWidth: 1,
                borderColor: colors.twitter_color,
                width: 200,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
                borderRadius: 100,
              }}>
              <CustomText
                numberOfLines={3}
                style={[stylesText.TitleCard]}
                light={colors.twitter_color}
                dark={colors.twitter_color}>
                {t('notification:Empezar')}
              </CustomText>
            </TouchableOpacity>
          </View>
        </View>
      )}
      {loading ? (
        <View style={{marginTop: 50}}>
          <Loading />
          <Loading />
          <Loading />
          <Loading />
          <Loading />
          <Loading />
        </View>
      ) : (
        <View>
          <Animated.View style={[styles.loading, {height: height}]}>
            <ActivityIndicator size="small" color={colors.white} />
            <CustomText
              light={colors.white}
              dark={colors.white}
              style={[
                stylesText.terciaryText,
                {marginLeft: 10, fontWeight: 'bold'},
              ]}>
              Actualizando
            </CustomText>
          </Animated.View>
          <FlatList
            data={chats}
            renderItem={(item: any) => renderItem(item)}
            keyExtractor={(item: any) => item.marker}
            style={{paddingBottom: dimensions.Height(15), marginTop: 0}}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={ListEmptyComponent}
          />
        </View>
      )}
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  verify: {
    backgroundColor: '#1b94e029',
    marginTop: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 15,
    borderRadius: 15,
    marginHorizontal: 15,
  },

  loading: {
    backgroundColor: colors.twitter_color,
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
