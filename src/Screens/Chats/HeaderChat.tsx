import React from 'react';
import {
  View,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  Image,
  ActionSheetIOS,
  ImageBackground,
  Alert,
} from 'react-native';
import {dimensions, image, stylesText, colors} from '../../Themes';
import {
  DynamicValue,
  DynamicStyleSheet,
  useColorSchemeContext,
  useDynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import moment from 'moment';
import Mailer from 'react-native-mail';
import {OpenURLButton} from '../../Utils/onpenUrl';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function HeaderChat({
  navigation,
  data,
  connected,
  users,
  user,
  Owner,
}) {
  const styles = useDynamicValue(dynamicStyles);

  const logoUri = new DynamicValue(
    image.IconPlaceHoldedLigth,
    image.IconPlaceHoldedDark,
  );

  const source = useDynamicValue(logoUri);

  const ActAdd = {
    light: colors.rgb_235,
    dark: colors.back_dark,
  };

  const mode = useColorSchemeContext();
  const borderColor = ActAdd[mode];

  const types = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const {t} = useTranslation();

  const message = t('chat:reportMessage');

  const handleEmail = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: message,
          recipients: ['report@rentytapp.com'],
          body: `<b>${message}</b>`,
          isHTML: true,
        },
        (error, event) => {},
      );
    } else {
      OpenURLButton(
        `mailto:report@rentytapp.com?subject=${message}&body=${message}`,
      );
    }
  };

  const onPress = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: `${t('chat:ReportTitle')} ${name} ${lastName}`,
          message: t('chat:Reportmessage'),
          options: [t('chat:btn:cancel'), t('chat:btn:report')],
          cancelButtonIndex: 0,
          destructiveButtonIndex: 1,
        },
        buttonIndex => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            handleEmail();
          }
        },
      );
    } else {
      Alert.alert(
        `${t('chat:ReportTitle')} ${name} ${lastName}`,
        t('chat:Reportmessage'),
        [
          {
            text: t('chat:btn:report'),
            onPress: () => handleEmail(),
          },

          {
            text: t('chat:btn:cancel'),
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    }
  };

  const isOwner = data.owner === users._id;

  const name = isOwner ? user.name : Owner.name;
  const lastName = isOwner ? user.lastName : Owner.lastName;
  const isVerified = isOwner ? user.verified : Owner.verified;
  const date = isOwner ? user.lastSeen : Owner.lastSeen;

  return (
    <SafeAreaView
      style={[
        styles.container,
        {
          borderBottomColor: borderColor,
        },
      ]}>
      <View style={styles.contentContainer}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity
            onPress={() => {
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(types, optiones);
              navigation.goBack();
            }}
            style={styles.icon}>
            <Icon
              name="arrow-left"
              type="Feather"
              size={34}
              style={styles.icon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('Details', {id: data._id})}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <ImageBackground
              source={source}
              imageStyle={{borderRadius: 100, width: 66, height: 66}}
              resizeMode="cover"
              style={styles.avatarcar}>
              <Image
                source={{
                  uri: data.imageCar,
                }}
                style={styles.avatarcarImg}
                resizeMode="cover"
              />
              <Image
                source={{
                  uri: isOwner ? user.avatar : Owner.avatar,
                }}
                style={[styles.avatar, {marginLeft: -20}]}
                resizeMode="cover"
              />
            </ImageBackground>
            <View
              style={{
                width: dimensions.Width(45),
                marginLeft: 15,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  width: dimensions.Width(45),
                }}>
                <CustomText
                  style={[stylesText.secondaryTextBold]}
                  numberOfLines={1}
                  light={colors.black}
                  dark={colors.white}>
                  {name} {lastName}
                </CustomText>
                {isVerified ? (
                  <Icon
                    type="MaterialIcons"
                    name="verified"
                    size={16}
                    color={colors.twitter_color}
                    style={{marginLeft: 5, marginTop: 1}}
                  />
                ) : null}
              </View>
              <CustomText
                style={[stylesText.secondaryText, {fontWeight: '400'}]}
                numberOfLines={1}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {data.car.marker} {data.car.model} {data.car.year}
              </CustomText>
              {connected ? (
                <CustomText
                  style={[stylesText.secondaryText, {marginTop: 3}]}
                  numberOfLines={1}
                  light={colors.green}
                  dark={colors.green}>
                  {t('chat:online')}
                </CustomText>
              ) : (
                <CustomText
                  style={[stylesText.secondaryText, {marginTop: 3}]}
                  numberOfLines={1}
                  light={colors.rgb_153}
                  dark={colors.rgb_153}>
                  {t('chat:lastseen')} {moment(date).fromNow()}
                </CustomText>
              )}
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity onPress={onPress}>
          <Icon
            name="more-vertical"
            type="Feather"
            size={30}
            style={styles.icon}
          />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    borderBottomWidth: 0.5,
    paddingTop: Platform.select({
      ios: dimensions.IsIphoneX() ? 0 : 10,
      android: dimensions.Height(5),
    }),
  },

  icon: {
    width: 40,
    height: 40,
    color: new DynamicValue(colors.black, colors.white),
  },

  contentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 15,
  },

  avatarcar: {
    width: 70,
    height: 70,
    borderRadius: 100,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  avatarcarImg: {
    width: 66,
    height: 66,
    borderRadius: 100,
  },

  avatar: {
    width: 40,
    height: 40,
    borderRadius: 100,
    position: 'absolute',
    top: -15,
    right: -10,
    borderWidth: 4,
    borderColor: new DynamicValue(colors.white, colors.black),
  },
});
