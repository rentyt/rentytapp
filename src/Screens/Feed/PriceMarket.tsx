import React from 'react';
import {View, Image} from 'react-native';
import {formaterPrice} from '../../Utils/formaterPrice';
import {colors, stylesText} from '../../Themes';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../../Components/CustomText';

export default function PriceMarket({amount, currency, localcode, data}) {
  const {t} = useTranslation();
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <View style={styles.bubble}>
        <Image source={data.images[0]} style={styles.imagen} />
        <View style={{marginLeft: 7, maxWidth: 150}}>
          <CustomText
            style={stylesText.secondaryText}
            numberOfLines={1}
            light={colors.black}
            dark={colors.white}>
            {data.car.marker} · {data.car.year}
          </CustomText>
          <CustomText
            style={stylesText.terciaryText}
            numberOfLines={1}
            light={colors.black}
            dark={colors.white}>
            {formaterPrice(amount, localcode, currency)} / {t('card:day')} {}
          </CustomText>
        </View>
      </View>
      <View style={styles.arrowBorder} />
      <View style={styles.arrow} />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
    maxWidth: 200,
  },
  bubble: {
    flex: 0,
    maxWidth: 200,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    padding: 10,
    borderRadius: 10,
    borderColor: colors.main,
    borderWidth: 0.5,
  },
  amount: {
    color: '#FFFFFF',
    fontSize: 13,
  },
  arrow: {
    backgroundColor: 'transparent',
    borderWidth: 4,
    borderColor: 'transparent',
    borderTopColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
    marginTop: -9,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderWidth: 4,
    borderColor: 'transparent',
    borderTopColor: colors.main,
    alignSelf: 'center',
    marginTop: -0.5,
  },

  imagen: {
    width: 35,
    height: 35,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: colors.rgb_235,
  },
});
