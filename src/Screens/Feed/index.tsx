import React, {useState, useEffect, useRef, useContext} from 'react';
import {View, Platform, Animated} from 'react-native';
import {
  DynamicValue,
  useDynamicStyleSheet,
  DynamicStyleSheet,
} from 'react-native-dynamic';
import {colors} from '../../Themes';
import MapViews from './MapView';
import Header from './HeaderMap';
import ListViews from './ListView';
import Filter from './Filters';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useQuery} from '@apollo/client';
import {queryCar} from '../../GraphQL';
import {MainContext} from '../../store/MainProvider';
import Geolocation from '@react-native-community/geolocation';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function FeedScreen({route, navigation}) {
  const {input} = route.params;
  const {languaje} = useContext(MainContext);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [active, setActive] = useState(0);
  const [page, setpage] = useState(1);
  const [limit] = useState(10);
  const [datos, setdatos] = useState([]);
  const [stopFetchMore, setStopFetchMore] = useState(true);
  const [visibleModal, setVisibleModal] = useState(false);
  const [marke, setMarke] = useState(input.data ? input.data : null);
  const [seletedModel, setSeletedModel] = useState([]);
  const [seletecdItemsYear, setseletecdItemsYear] = useState([]);
  const [value, setValue] = useState(1000);
  const [minimoDay, setMinimoDay] = useState(1);
  const [isEnabledInstant, setisEnabledInstant] = useState(
    input.instantBooking ? true : false,
  );
  const [popular, setPopular] = useState(input.popular ? true : false);
  const [isEnabledOffert, setisEnabledOffert] = useState(false);
  const [airPortPickup, setairPortPickup] = useState(false);
  const [userCategory, setuserCategory] = useState(
    input.userCategory ? input.userCategory : [],
  );
  const [load, setLoad] = useState(false);
  const [search, setsearch] = useState(null);
  const [position, setPosition] = useState({
    lat: 18.4860575,
    lgn: -69.9312117,
  });

  const scrollY = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Geolocation.getCurrentPosition(info => {
      if (info.coords.latitude && info.coords.longitude) {
        setPosition({
          lat: info.coords.latitude,
          lgn: info.coords.longitude,
        });
      }
    });
  }, []);

  const isFilter = () => {
    if (marke) {
      return true;
    } else if (seletedModel.length > 0) {
      return true;
    } else if (seletecdItemsYear.length > 0) {
      return true;
    } else if (value > 1000) {
      return true;
    } else if (isEnabledOffert) {
      return true;
    } else if (isEnabledInstant) {
      return true;
    } else if (popular) {
      return true;
    } else if (userCategory.length > 0) {
      return true;
    } else if (minimoDay > 1) {
      return true;
    } else if (search) {
      return true;
    } else if (airPortPickup) {
      return true;
    } else {
      return false;
    }
  };

  const toggleSwitch = enabled => {
    setisEnabledInstant(enabled);
  };

  const toggleSwitchOffert = enabled => {
    setisEnabledOffert(enabled);
  };

  const toggleSwitchPopular = enabled => {
    setPopular(enabled);
  };

  const toggleSwitchAirPort = enabled => {
    setairPortPickup(enabled);
  };

  const {data, refetch, loading} = useQuery(queryCar.GET_FEED_CAR, {
    variables: {
      input: {
        marke: marke && marke.brand,
        year: seletecdItemsYear,
        models: seletedModel,
        price: Math.round(value),
        minimeDay: Math.round(minimoDay),
        instantBooking: isEnabledInstant,
        popular: popular,
        offert: isEnabledOffert,
        userCategory: userCategory,
        search: search,
        airPortPickup: airPortPickup,
      },
      page: isFilter() ? 1 : page,
      limit: isFilter() ? 100 : limit,
    },
  });

  const cars = data && data.getAdsFeed ? data.getAdsFeed.data : [];

  useEffect(() => {
    if (cars.length > 0) {
      cars.forEach(store => {
        var i = datos.findIndex((x: any) => x._id === store._id);
        if (i !== -1) {
          datos.splice(i, 1);
          setdatos(datos.concat(cars));
        } else {
          setdatos(datos.concat(cars));
        }
      });
    } else if (cars.length < 9) {
      setStopFetchMore(false);
    } else {
      setStopFetchMore(false);
    }
  }, [cars]);

  const clearFilter = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    setseletecdItemsYear([]);
    setSeletedModel([]);
    setMarke(null);
    setValue(1000);
    setisEnabledOffert(false);
    setisEnabledInstant(false);
    setairPortPickup(false);
    setPopular(false);
    setuserCategory([]);
    setMinimoDay(1);
    setStopFetchMore(true);
    setVisibleModal(false);
    refetch();
    setsearch(null);
  };

  const appleFilter = () => {
    setLoad(true);
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    setTimeout(() => {
      setLoad(false);
      setVisibleModal(false);
      setStopFetchMore(false);
      refetch();
    }, 300);
  };

  const handleKeyPress = ({nativeEvent: {key: keyValue}}) => {
    if (keyValue === 'Backspace') {
      clearFilter();
    }
  };

  return (
    <View style={styles.constainer}>
      <Header
        navigation={navigation}
        active={active}
        setActive={setActive}
        setVisibleModal={setVisibleModal}
        isFilter={isFilter}
        handleKeyPress={handleKeyPress}
        setSearch={setsearch}
        scrollY={scrollY}
        autoFocus={input.autoFocus}
      />
      {active === 1 ? (
        <MapViews
          navigation={navigation}
          ads={datos}
          stopFetchMore={stopFetchMore}
          Loading={loading}
          setpage={setpage}
          page={page}
          scrollY={scrollY}
          position={position}
        />
      ) : (
        <ListViews
          Loading={loading}
          ads={isFilter() ? cars : datos}
          setpage={setpage}
          page={page}
          stopFetchMore={stopFetchMore}
          navigation={navigation}
          setStopFetchMore={setStopFetchMore}
          refetch={refetch}
          scrollY={scrollY}
          languaje={languaje}
        />
      )}
      <Filter
        visibleModal={visibleModal}
        setvisibleModal={setVisibleModal}
        seletecdItems={seletedModel}
        setseletecdItems={setSeletedModel}
        seletecdItemsYear={seletecdItemsYear}
        setseletecdItemsYear={setseletecdItemsYear}
        value={value}
        setValue={setValue}
        toggleSwitch={toggleSwitch}
        toggleSwitchOffert={toggleSwitchOffert}
        toggleSwitchPopular={toggleSwitchPopular}
        popular={popular}
        isEnabled={isEnabledInstant}
        isEnabledOffert={isEnabledOffert}
        marke={marke}
        setMarke={setMarke}
        clearFilter={clearFilter}
        minimoDay={minimoDay}
        setMinimoDay={setMinimoDay}
        load={load}
        appleFilter={appleFilter}
        isFilter={isFilter}
        setStopFetchMore={setStopFetchMore}
        airPortPickup={airPortPickup}
        toggleSwitchAirPort={toggleSwitchAirPort}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  constainer: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
