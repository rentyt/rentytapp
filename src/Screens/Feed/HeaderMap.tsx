import React from 'react';
import {
  View,
  TextInput,
  TouchableOpacity,
  Platform,
  Animated,
} from 'react-native';
import {colors, dimensions} from '../../Themes';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import SegmentedControl from '@react-native-community/segmented-control';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function PriceMarket(props) {
  const {
    navigation,
    active,
    setActive,
    setVisibleModal,
    isFilter,
    search,
    setSearch,
    handleKeyPress,
    scrollY,
    autoFocus,
  } = props;
  const {t} = useTranslation();
  const styles = useDynamicStyleSheet(dynamicStyles);

  const mode = useColorSchemeContext();

  const ActAdd = {
    light: colors.main,
    dark: colors.main,
  };

  const PlaceHolded = {
    light: colors.black,
    dark: colors.white,
  };

  const placeHolded = PlaceHolded[mode];

  const mainColor = ActAdd[mode];

  const opacity = scrollY.interpolate({
    inputRange: [50, 200],
    outputRange: [100, 0],
    extrapolate: 'clamp',
  });

  return (
    <Animated.View style={[styles.container]}>
      <View style={styles.content}>
        <TouchableOpacity
          onPress={() => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(types, optiones);
            navigation.goBack();
          }}
          style={styles.icon}>
          <Icon
            name="arrow-left"
            type="Feather"
            size={24}
            style={styles.icons}
          />
        </TouchableOpacity>
        <View style={styles.input}>
          <TextInput
            style={styles.textInput}
            value={search}
            placeholder={t('search:placeholded')}
            placeholderTextColor={placeHolded}
            selectionColor={mainColor}
            returnKeyType="search"
            clearButtonMode="while-editing"
            onChangeText={value => setSearch(value)}
            onKeyPress={handleKeyPress}
            autoFocus={autoFocus}
          />
        </View>
        <TouchableOpacity
          style={styles.icon}
          onPress={() => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(types, optiones);
            setVisibleModal(true);
          }}>
          {isFilter() ? <View style={styles.badget} /> : null}
          <Icon name="sliders" type="Feather" size={20} style={styles.icons} />
        </TouchableOpacity>
      </View>
      <Animated.View
        style={{
          width: 180,
          alignSelf: 'center',
        }}>
        <SegmentedControl
          values={[t('feed:list'), t('feed:map')]}
          selectedIndex={active}
          onChange={event => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(types, optiones);
            setActive(event.nativeEvent.selectedSegmentIndex);
          }}
        />
      </Animated.View>
    </Animated.View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.ScreenWidth,
    backgroundColor: 'transparent',
    position: 'absolute',
    top: dimensions.IsIphoneX() ? 40 : Platform.select({ios: 20, android: 30}),
    zIndex: 100,
  },

  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 15,
  },

  icons: {
    color: new DynamicValue(colors.black, colors.white),
  },

  icon: {
    width: 40,
    height: 40,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 14,
    elevation: 23,
  },

  input: {
    height: 40,
    width: dimensions.Width(70),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 14,
    elevation: 23,
  },

  textInput: {
    height: 40,
    width: dimensions.Width(68),
    borderRadius: 15,
    fontSize: dimensions.FontSize(15),
    fontWeight: '400',
    paddingLeft: 15,
    fontFamily: 'Poppins',
    color: new DynamicValue(colors.black, colors.white),
  },

  badget: {
    width: 10,
    height: 10,
    backgroundColor: colors.ERROR,
    position: 'absolute',
    top: 7,
    right: 7,
    borderRadius: 100,
    zIndex: 100,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 14,
    elevation: 23,
  },
});
