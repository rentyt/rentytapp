import React from 'react';
import {View} from 'react-native';
import {colors, stylesText} from '../../../Themes';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {CustomText} from '../../../Components/CustomText';
import {Slider} from '@miblanchard/react-native-slider';
import {formaterPrice} from '../../../Utils/formaterPrice';

export default function Items({value, setValue}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const swichColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const SwichColor = swichColor[mode];

  return (
    <View style={styles.cont}>
      <CustomText
        style={stylesText.secondaryTextBold}
        light={colors.black}
        dark={colors.white}>
        {t('feed:offert:priceperday')}
      </CustomText>
      <View style={{marginTop: 10}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View />
          <CustomText
            style={stylesText.TitleCard}
            light={colors.black}
            dark={colors.white}>
            {t('feed:offert:max')} {formaterPrice(Math.round(value), '', '')} /{' '}
            {t('card:day')}
          </CustomText>
        </View>
        <Slider
          value={value}
          onValueChange={values => setValue(values)}
          animateTransitions={true}
          maximumTrackTintColor={SwichColor}
          maximumValue={20000}
          minimumTrackTintColor={colors.main}
          minimumValue={1000}
          thumbTintColor={colors.main}
        />
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    marginHorizontal: 15,
    marginTop: 30,
  },

  sliderContainer: {},
  titleContainer: {},
});
