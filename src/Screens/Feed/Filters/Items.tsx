import React, {useState} from 'react';
import {Switch, View, Platform} from 'react-native';
import {dimensions, colors, stylesText} from '../../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {CustomText} from '../../../Components/CustomText';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Items({
  toggleSwitch,
  toggleSwitchOffert,
  isEnabled,
  isEnabledOffert,
  toggleSwitchPopular,
  popular,
  airPortPickup,
  toggleSwitchAirPort,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const swichColor = {
    light: colors.white,
    dark: colors.white,
  };

  const swichColor1 = {
    light: '#767577',
    dark: colors.back_suave_dark,
  };

  const SwichColor = swichColor[mode];

  const SwichColor1 = swichColor1[mode];

  return (
    <View style={{marginTop: 10}}>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.icon}>
            <Icon type="Entypo" name="flash" size={24} color={colors.ERROR} />
          </View>
          <View style={{marginLeft: 10}}>
            <CustomText
              style={stylesText.TitleCard}
              light={colors.black}
              dark={colors.white}>
              {t('details:InstantDetail')}
            </CustomText>
            <CustomText
              style={stylesText.secondaryText}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('details:InstantDetail')}
            </CustomText>
          </View>
        </View>
        <View style={{marginRight: 10}}>
          <Switch
            trackColor={{false: SwichColor1, true: colors.ERROR}}
            thumbColor={isEnabled ? SwichColor : SwichColor}
            ios_backgroundColor={colors.rgb_235}
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
      </View>

      <View style={[styles.container, {marginTop: 15}]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.icon}>
            <Icon
              type="MaterialCommunityIcons"
              name="ticket-percent-outline"
              size={24}
              color={colors.green}
            />
          </View>
          <View style={{marginLeft: 10}}>
            <CustomText
              style={stylesText.TitleCard}
              light={colors.black}
              dark={colors.white}>
              {t('feed:offert:offert')}
            </CustomText>
            <CustomText
              style={stylesText.secondaryText}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('feed:offert:offert')}
            </CustomText>
          </View>
        </View>
        <View style={{marginRight: 10}}>
          <Switch
            trackColor={{false: SwichColor1, true: colors.green}}
            thumbColor={isEnabled ? SwichColor : SwichColor}
            ios_backgroundColor={colors.rgb_235}
            onValueChange={toggleSwitchOffert}
            value={isEnabledOffert}
          />
        </View>
      </View>

      <View style={[styles.container, {marginTop: 15}]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.icon}>
            <Icon
              type="MaterialCommunityIcons"
              name="fire"
              size={24}
              color={colors.orange1}
            />
          </View>
          <View style={{marginLeft: 10}}>
            <CustomText
              style={stylesText.TitleCard}
              light={colors.black}
              dark={colors.white}>
              {t('homeList:popular')}
            </CustomText>
            <CustomText
              style={stylesText.secondaryText}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('homeList:popular')}
            </CustomText>
          </View>
        </View>
        <View style={{marginRight: 10}}>
          <Switch
            trackColor={{false: SwichColor1, true: colors.orange1}}
            thumbColor={isEnabled ? SwichColor : SwichColor}
            ios_backgroundColor={colors.rgb_235}
            onValueChange={toggleSwitchPopular}
            value={popular}
          />
        </View>
      </View>

      <View style={[styles.container, {marginTop: 15}]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.icon}>
            <Icon
              type="MaterialCommunityIcons"
              name="airplane"
              size={24}
              color={colors.twitter_color}
            />
          </View>
          <View style={{marginLeft: 10}}>
            <CustomText
              style={stylesText.TitleCard}
              light={colors.black}
              dark={colors.white}>
              {t('card:airPort')}
            </CustomText>
            <CustomText
              style={stylesText.secondaryText}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('card:airPort')}
            </CustomText>
          </View>
        </View>
        <View style={{marginRight: 10}}>
          <Switch
            trackColor={{false: SwichColor1, true: colors.twitter_color}}
            thumbColor={isEnabled ? SwichColor : SwichColor}
            ios_backgroundColor={colors.rgb_235}
            onValueChange={toggleSwitchAirPort}
            value={airPortPickup}
          />
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  icon: {
    width: 40,
    height: 40,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginLeft: 10,
  },

  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },
});
