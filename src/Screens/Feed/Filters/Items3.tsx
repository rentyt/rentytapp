import React, {useState} from 'react';
import {View, Platform, TouchableOpacity} from 'react-native';
import {dimensions, colors, stylesText} from '../../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {CustomText} from '../../../Components/CustomText';
import data from '../../../data/year.json';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Items({seletecdItemsYear, setseletecdItemsYear}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  const SelectItems = (ids: string) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    setseletecdItemsYear(seletecdItemsYear.concat(ids));
  };

  const deletedItem = (ids: string) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    var i = seletecdItemsYear.findIndex((x: any) => x === ids);
    if (i !== -1) {
      seletecdItemsYear.splice(i, 1);
      setseletecdItemsYear(seletecdItemsYear.concat());
    }
  };

  const ActAdd = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  const _renderItem = (item, i) => {
    const adds =
      seletecdItemsYear.filter((x: string) => x === item.label).length === 1;
    return (
      <TouchableOpacity
        key={i}
        onPress={() => {
          if (adds) {
            deletedItem(item.label);
          } else {
            SelectItems(item.label);
          }
        }}>
        <View
          style={[
            styles.buttonView,
            {borderColor: adds ? colors.main : mainColor},
          ]}>
          {adds ? (
            <CustomText
              light={colors.main}
              dark={colors.main}
              style={[
                stylesText.miniTitle,
                styles.ActiveBTN,
                {fontWeight: '500'},
              ]}>
              {item.label}
            </CustomText>
          ) : (
            <View style={[styles.buttonViewIN]}>
              <CustomText
                light={colors.back_suave_dark}
                dark={colors.white}
                style={[stylesText.miniTitle, {fontWeight: '500'}]}>
                {item.label}
              </CustomText>
            </View>
          )}
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.cont}>
      <CustomText
        style={stylesText.secondaryTextBold}
        light={colors.black}
        dark={colors.white}>
        {t('feed:offert:year')}
      </CustomText>
      <View style={styles.container}>
        {data.map((item, i) => {
          return _renderItem(item, i);
        })}
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    marginHorizontal: 15,
    marginTop: 30,
  },

  container: {
    width: dimensions.Width(96),
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  buttonView: {
    borderRadius: dimensions.Width(98),
    paddingHorizontal: 2,
    paddingVertical: 2,
    marginHorizontal: 7,
    marginVertical: 7,
    borderWidth: 1,
  },

  buttonViewIN: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: dimensions.Width(98),
    paddingHorizontal: 15,
    paddingVertical: 6,
  },

  ActiveBTN: {
    borderRadius: dimensions.Width(98),
    paddingHorizontal: 15,
    paddingVertical: 6,
  },
});
