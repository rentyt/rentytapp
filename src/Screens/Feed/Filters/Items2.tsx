import React, {useState} from 'react';
import {
  FlatList,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Alert,
} from 'react-native';
import {dimensions, colors, stylesText} from '../../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import HeaderModal from '../../../Components/HeaderModal';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {CustomText} from '../../../Components/CustomText';
import data from '../../../data/preview.json';
import dataAll from '../../../data/model.json';
import {Button} from '../../../Components/Buttom';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Items({
  seletecdItems,
  setseletecdItems,
  marke,
  setMarke,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const [visibleModal, setvisibleModal] = useState(false);
  const [all, setAll] = useState(false);

  const datos = all ? dataAll : data;

  const SelectItems = (ids: string) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    setseletecdItems(seletecdItems.concat(ids));
  };

  const deletedItem = (ids: string) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    var i = seletecdItems.findIndex((x: any) => x === ids);
    if (i !== -1) {
      seletecdItems.splice(i, 1);
      setseletecdItems(seletecdItems.concat());
    }
  };

  const ActAdd = {
    light: colors.rgb_153,
    dark: colors.back_suave_dark,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  const _renderItem = (item, i) => {
    return (
      <TouchableOpacity
        key={i}
        onPress={() => {
          if (item.brand === 'all') {
            setAll(true);
          } else if (item.brand === 'menos') {
            setAll(false);
          } else {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(types, optiones);
            setMarke(item);
            setvisibleModal(true);
          }
        }}>
        <View
          style={[
            styles.buttonView,
            {
              borderColor:
                marke && marke.brand === item.brand ? colors.main : mainColor,
            },
          ]}>
          {item.brand === 'all' || item.brand === 'menos' ? (
            <CustomText
              light={colors.back_suave_dark}
              dark={colors.white}
              style={[
                stylesText.miniTitle,
                styles.ActiveBTN,
                {fontWeight: '500'},
              ]}>
              {all ? t('homeList:SeeMenos') : t('homeList:SeeAll')}
            </CustomText>
          ) : (
            <>
              {marke && marke.brand === item.brand ? (
                <CustomText
                  light={colors.main}
                  dark={colors.main}
                  style={[
                    stylesText.miniTitle,
                    styles.ActiveBTN,
                    {fontWeight: '500'},
                  ]}>
                  {item.brand}
                </CustomText>
              ) : (
                <View style={[styles.buttonViewIN]}>
                  <CustomText
                    light={colors.back_suave_dark}
                    dark={colors.white}
                    style={[stylesText.miniTitle, {fontWeight: '500'}]}>
                    {item.brand}
                  </CustomText>
                </View>
              )}
            </>
          )}
        </View>
      </TouchableOpacity>
    );
  };

  const renderItem = ({item}) => {
    const adds = seletecdItems.filter((x: string) => x === item).length === 1;
    return (
      <TouchableOpacity
        style={styles.items}
        onPress={() => {
          if (adds) {
            deletedItem(item);
          } else {
            SelectItems(item);
          }
        }}>
        <View style={{padding: 20}}>
          <CustomText
            light={colors.back_suave_dark}
            dark={colors.white}
            style={[stylesText.miniTitle, {fontWeight: '500'}]}>
            {item}
          </CustomText>
        </View>
        {adds ? (
          <Icon
            name="check"
            type="Feather"
            size={20}
            color={colors.green}
            style={{marginRight: 15}}
          />
        ) : null}
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.cont}>
      <CustomText
        style={stylesText.secondaryTextBold}
        light={colors.black}
        dark={colors.white}>
        {t('feed:offert:mark')}
      </CustomText>
      <View style={styles.container}>
        {datos.map((item, i) => {
          return _renderItem(item, i);
        })}
      </View>
      {marke ? (
        <Modal
          animationType="slide"
          visible={visibleModal}
          presentationStyle="formSheet"
          collapsable={true}
          statusBarTranslucent={true}
          onRequestClose={() => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(types, optiones);
            setvisibleModal(false);
            setseletecdItems([]);
            setMarke(null);
          }}>
          <HeaderModal
            onPress={() => {
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(types, optiones);
              setvisibleModal(false);
              setseletecdItems([]);
              setMarke(null);
            }}
            title={marke.brand}
            children={
              <View style={styles.contentModal}>
                <FlatList
                  data={marke.models}
                  renderItem={(item: any) => renderItem(item)}
                  keyExtractor={(item: any) => item}
                  showsVerticalScrollIndicator={false}
                />
              </View>
            }
          />
          <View style={styles.buttonContent}>
            <Button
              light={colors.white}
              dark={colors.white}
              loading={false}
              onPress={() => {
                if (seletecdItems.length > 0) {
                  //@ts-ignore
                  ReactNativeHapticFeedback.trigger(types, optiones);
                  setvisibleModal(false);
                } else {
                  Alert.alert(
                    t('feed:offert:NoItemAlertTitlw'),
                    t('feed:offert:NoItemAlert'),
                  );
                }
              }}
              title={t('feed:offert:apply')}
            />
          </View>
        </Modal>
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cont: {
    marginHorizontal: 15,
    marginTop: 30,
  },

  container: {
    width: dimensions.Width(96),
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  buttonView: {
    borderRadius: dimensions.Width(98),
    paddingHorizontal: 2,
    paddingVertical: 2,
    marginHorizontal: 7,
    marginVertical: 7,
    borderWidth: 1,
  },

  buttonViewIN: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: dimensions.Width(98),
    paddingHorizontal: 15,
    paddingVertical: 6,
  },

  ActiveBTN: {
    borderRadius: dimensions.Width(98),
    paddingHorizontal: 10,
    paddingVertical: 6,
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginBottom: dimensions.Height(20),
  },

  buttonContent: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(14),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 0,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 15}),
    zIndex: 100,
  },

  items: {
    borderBottomWidth: 0.4,
    borderBottomColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
