import React from 'react';
import {Modal, View, Platform} from 'react-native';
import {dimensions, colors} from '../../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Items() {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  return <View></View>;
}

const dynamicStyles = new DynamicStyleSheet({});
