import React from 'react';
import {Modal, View, Platform} from 'react-native';
import {useTranslation} from 'react-i18next';
import HeaderModal from '../../../Components/HeaderModal';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors} from '../../../Themes';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {Button} from '../../../Components/Buttom';
import Items from './Items';
import Items1 from './Items1';
import Items2 from './Items2';
import Items3 from './Items3';
import Item4 from './Items4';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Filters({
  visibleModal,
  setvisibleModal,
  seletecdItems,
  setseletecdItems,
  seletecdItemsYear,
  setseletecdItemsYear,
  value,
  setValue,
  toggleSwitch,
  toggleSwitchOffert,
  isEnabled,
  isEnabledOffert,
  marke,
  setMarke,
  toggleSwitchPopular,
  popular,
  clearFilter,
  minimoDay,
  setMinimoDay,
  load,
  appleFilter,
  isFilter,
  setStopFetchMore,
  airPortPickup,
  toggleSwitchAirPort,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();

  return (
    <Modal
      animationType="slide"
      visible={visibleModal}
      presentationStyle="formSheet"
      collapsable={true}
      statusBarTranslucent={true}
      onRequestClose={() => {
        //@ts-ignore
        ReactNativeHapticFeedback.trigger(types, optiones);
        setvisibleModal(false);
        setStopFetchMore(true);
      }}>
      <HeaderModal
        onPress={() => {
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(types, optiones);
          setvisibleModal(false);
          setStopFetchMore(true);
        }}
        clean={true}
        title2={t('feed:offert:clean')}
        title={t('feed:filtesr')}
        onPress1={clearFilter}
        children={
          <View style={styles.contentModal}>
            <Items
              toggleSwitch={toggleSwitch}
              toggleSwitchOffert={toggleSwitchOffert}
              isEnabled={isEnabled}
              isEnabledOffert={isEnabledOffert}
              toggleSwitchPopular={toggleSwitchPopular}
              popular={popular}
              airPortPickup={airPortPickup}
              toggleSwitchAirPort={toggleSwitchAirPort}
            />
            <Items1 value={value} setValue={setValue} />
            <Item4 value={minimoDay} setValue={setMinimoDay} />
            <Items2
              seletecdItems={seletecdItems}
              setseletecdItems={setseletecdItems}
              marke={marke}
              setMarke={setMarke}
            />
            <Items3
              seletecdItemsYear={seletecdItemsYear}
              setseletecdItemsYear={setseletecdItemsYear}
            />
          </View>
        }
      />
      <View style={styles.buttonContent}>
        <Button
          light={colors.white}
          dark={colors.white}
          loading={load}
          onPress={() => {
            if (isFilter()) {
              appleFilter();
            } else {
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(types, optiones);
              setvisibleModal(false);
              setStopFetchMore(true);
            }
          }}
          title={t('feed:offert:apply')}
        />
      </View>
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    marginBottom: dimensions.Height(20),
  },

  buttonContent: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(14),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 0,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 15}),
    zIndex: 100,
  },
});
