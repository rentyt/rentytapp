import React from 'react';
import {View, FlatList} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {customMaspStyles} from '../../Components/MapStyle';
import {dimensions} from '../../Themes';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useDynamicValue,
} from 'react-native-dynamic';
import CardStore from '../../Components/AdsCard/SmallCard';
import PriceMarket from './PriceMarket';
import {useTranslation} from 'react-i18next';
import LoadingAnimated from '../../Components/LoadingAnimated';

export default function MapViews({
  navigation,
  ads,
  stopFetchMore,
  Loading,
  setpage,
  page,
  scrollY,
  position,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const stylos = useDynamicValue(customMaspStyles);

  const {t} = useTranslation();

  const lat = position.lat;
  const lng = position.lgn;

  const renderItem = ({item, index}) => {
    return (
      <CardStore
        data={item}
        navigation={navigation}
        refetch={null}
        index={index}
        scrollY={scrollY}
      />
    );
  };

  const getItemLayout = (data, index) => ({
    length: 50,
    offset: 50 * index,
    index,
  });

  const loadMoreData = () => {
    if (stopFetchMore && !Loading) {
      setpage(page + 1);
    }
  };

  const renderFooster = () => {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 15,
        }}>
        {Loading && stopFetchMore && <LoadingAnimated name={`Cargando`} />}
      </View>
    );
  };

  return (
    <View style={styles.constainer}>
      <MapView
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        customMapStyle={stylos}
        style={{
          height: dimensions.IsIphoneX()
            ? dimensions.Height(100)
            : dimensions.Height(104),
          width: dimensions.ScreenWidth,
        }}
        region={{
          latitude: lat,
          longitude: lng,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}>
        {ads &&
          ads.map((item, i) => {
            return (
              <Marker
                key={i}
                onPress={() => navigation.navigate('Details', {id: item._id})}
                coordinate={{
                  latitude: item.location.geometry.location.lat,
                  longitude: item.location.geometry.location.lng,
                }}>
                <PriceMarket
                  amount={item.prices.value}
                  currency={item.prices.currency}
                  localcode={item.prices.localcode}
                  data={item}
                />
              </Marker>
            );
          })}
      </MapView>

      <View style={styles.overoll}>
        <FlatList
          data={ads}
          renderItem={(item: any) => renderItem(item)}
          keyExtractor={(item: any) => item.marker}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          getItemLayout={getItemLayout}
          onEndReached={loadMoreData}
          onEndReachedThreshold={0.1}
          ListFooterComponent={renderFooster}
        />
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  constainer: {
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
  },

  overoll: {
    width: dimensions.ScreenWidth,
    position: 'absolute',
    bottom: dimensions.IsIphoneX() ? 20 : -10,
  },
});
