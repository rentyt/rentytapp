import React, {useState, useRef} from 'react';
import {
  View,
  Animated,
  RefreshControl,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';
import {dimensions, colors, stylesText} from '../../Themes';
import {useTranslation} from 'react-i18next';
import CardAds from '../../Components/AdsCard/SmallCard';
import {CustomText} from '../../Components/CustomText';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import CardStore from '../../Components/AdsCard/Vertical';
import * as Animatable from 'react-native-animatable';
import LoadingAnimated from '../../Components/LoadingAnimated';
import {useQuery} from '@apollo/client';
import {query} from '../../GraphQL';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import GetCredit from '../../Components/GetCredit';
import LoadingComponent from '../../Components/PlaceHolded/ListHome';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

const screens = Dimensions.get('window').height;

export default function ListView({
  Loading,
  ads,
  setpage,
  page,
  stopFetchMore,
  navigation,
  setStopFetchMore,
  refetch,
  scrollY,
  languaje,
}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [refreshing, setRefreshing] = useState(false);

  const flatlistRef = useRef();

  const {t} = useTranslation();

  const {data, loading} = useQuery(query.GET_ALL_ADS, {
    variables: {input: {popular: true}},
  });

  const adsOffert = data && data.getAds ? data.getAds.data : [];

  const getItemLayout = (data, index) => ({
    length: 50,
    offset: 50 * index,
    index,
  });

  const scrollToIndex = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    let randomIndex = 0;
    if (flatlistRef) {
      //@ts-ignore
      flatlistRef.current.scrollToIndex({animated: true, index: randomIndex});
    }
  };

  const loadMoreData = () => {
    if (stopFetchMore && !Loading) {
      setpage(page + 1);
    }
  };

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
      setStopFetchMore(true);
    }, 2000);
  };

  const opacityText = scrollY.interpolate({
    inputRange: [300, 700],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  const _renderItem = ({item, index}) => {
    if (ads.length > 9) {
      return (
        <>
          {index === 9 ? (
            <>
              <CardAds
                data={item}
                navigation={navigation}
                refetch={refetch}
                scrollY={scrollY}
                index={index}
              />
              <GetCredit navigation={navigation} languaje={languaje} />
            </>
          ) : (
            <CardAds
              data={item}
              navigation={navigation}
              refetch={refetch}
              scrollY={scrollY}
              index={index}
            />
          )}
        </>
      );
    } else {
      return (
        <>
          {index === 6 ? (
            <>
              <CardAds
                data={item}
                navigation={navigation}
                refetch={refetch}
                scrollY={scrollY}
                index={index}
              />
              <GetCredit navigation={navigation} languaje={languaje} />
            </>
          ) : (
            <CardAds
              data={item}
              navigation={navigation}
              refetch={refetch}
              scrollY={scrollY}
              index={index}
            />
          )}
        </>
      );
    }
  };

  const renderFooster = () => {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: dimensions.Height(9),
          marginTop: 20,
        }}>
        {Loading ? (
          <LoadingAnimated name={t('feed:loading')} />
        ) : (
          <>
            {Loading && stopFetchMore && (
              <LoadingAnimated name={t('feed:loading')} />
            )}
          </>
        )}
      </View>
    );
  };

  const renderItem = (item: any, i: number) => {
    return (
      <CardStore
        data={item}
        navigation={navigation}
        key={i}
        refetch={refetch}
        scrollY={scrollY}
        index={i}
        feed={true}
        style={{marginTop: 15}}
      />
    );
  };

  const renderHeader = () => {
    return (
      <View style={styles.container}>
        <View
          style={{
            width: dimensions.Width(96),
            marginTop: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <CustomText
            style={stylesText.titleText}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('feed:featuredTitle')}
          </CustomText>
        </View>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {loading ? (
            <View style={styles.containers}>
              <LoadingComponent />
            </View>
          ) : (
            <View style={styles.containers}>
              {adsOffert &&
                adsOffert.map((item: any, i: number) => {
                  return renderItem(item, i);
                })}
            </View>
          )}
        </ScrollView>
        <View
          style={{
            width: dimensions.Width(96),
            marginTop: 5,
            marginBottom: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <CustomText
            style={stylesText.titleText}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('feed:recomendedTitle')}
          </CustomText>
        </View>
      </View>
    );
  };

  return (
    <View
      style={{
        marginTop:
          screens > 768 ? dimensions.Height(17) : dimensions.Height(20),
      }}>
      <Animated.FlatList
        data={ads}
        ref={flatlistRef}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: scrollY}}}],
          {useNativeDriver: true},
        )}
        renderItem={(item: any) => _renderItem(item)}
        keyExtractor={(item: any) => item._id}
        style={{marginLeft: 5}}
        showsVerticalScrollIndicator={false}
        getItemLayout={getItemLayout}
        onEndReached={loadMoreData}
        onEndReachedThreshold={0.1}
        initialScrollIndex={0}
        ListFooterComponent={renderFooster}
        ListHeaderComponent={renderHeader}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            enabled={true}
            title="Cargando"
            onRefresh={_onRefresh}
          />
        }
        ListEmptyComponent={
          <>{Loading ? null : <LoadingAnimated name={t('feed:loading')} />}</>
        }
      />

      <Animated.View style={[styles.top, {opacity: opacityText}]}>
        <Animatable.View animation="slideInUp" duration={100}>
          <TouchableOpacity
            onPress={scrollToIndex}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <CustomText
              style={[
                stylesText.secondaryTextBold,
                {fontSize: 14, colors: colors.black},
              ]}>
              {t('feed:offert:top')}
            </CustomText>
            <Icon
              name="chevron-up"
              type="Feather"
              size={20}
              color={colors.black}
            />
          </TouchableOpacity>
        </Animatable.View>
      </Animated.View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: dimensions.ScreenWidth,
  },

  containers: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: 5,
    minWidth: dimensions.ScreenWidth,
  },

  top: {
    position: 'absolute',
    zIndex: 100,
    width: dimensions.Width(20),
    borderRadius: 100,
    backgroundColor: colors.white,
    padding: 10,
    bottom: 30,
    left: '40%',
    shadowColor: new DynamicValue(colors.light_grey, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },

  buttonContent: {
    marginTop: 20,
  },
});
