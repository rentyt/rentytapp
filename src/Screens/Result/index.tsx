import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';

import {CustomText} from '../../Components/CustomText';
import {Button} from '../../Components/Buttom';
import {stylesText, dimensions, colors} from '../../Themes';
import Confetti from 'react-native-confetti';
import LottieView from 'lottie-react-native';
import InAppReview from 'react-native-in-app-review';
import {useTranslation} from 'react-i18next';

function Result({route, navigation}) {
  const {id} = route.params;
  const [_confettiView, set_confettiView] = useState(null);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const {t} = useTranslation();

  useEffect(() => {
    if (_confettiView) {
      _confettiView.startConfetti();
    }
  }, [_confettiView]);

  useEffect(() => {
    if (InAppReview.isAvailable()) {
      InAppReview.RequestInAppReview();
    }
  }, []);

  return (
    <View style={styles.container}>
      <Confetti ref={node => set_confettiView(node)} />
      <View
        style={{
          alignSelf: 'center',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: dimensions.Height(15),
        }}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <LottieView
            source={require('../../Assets/animated/success.json')}
            autoPlay
            loop
            style={{width: 300}}
          />
        </View>
        <CustomText
          dark={colors.white}
          light={colors.black}
          style={[
            stylesText.secondaryTextBold,
            {textAlign: 'center', paddingHorizontal: 30, marginBottom: 20},
          ]}>
          {t('result:title')}
        </CustomText>
        <CustomText
          dark={colors.white}
          light={colors.black}
          style={[
            stylesText.secondaryText,
            {textAlign: 'center', paddingHorizontal: 30},
          ]}>
          {t('result:description')}
        </CustomText>
        <View style={{marginTop: 50}}>
          <Button
            dark={colors.white}
            light={colors.white}
            onPress={async () => navigation.navigate('DetailsOrders', {id: id})}
            title={t('result:btn')}
          />
        </View>
        <View style={{marginTop: 50}}>
          <TouchableOpacity onPress={async () => navigation.navigate('Home')}>
            <CustomText
              dark={colors.green}
              light={colors.green}
              style={[
                stylesText.secondaryTextBold,
                {textAlign: 'center', paddingHorizontal: 30, marginBottom: 20},
              ]}>
              {t('result:home')}
            </CustomText>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  signupButtonContainer: {
    alignSelf: 'center',
    width: dimensions.Width(90),
  },
});

export default Result;
