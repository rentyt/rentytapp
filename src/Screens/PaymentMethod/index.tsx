import React, {useContext} from 'react';
import {
  View,
  FlatList,
  ScrollView,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {dimensions, colors, image, stylesText} from '../../Themes';
import Header from '../../Components/Header';
import {useTranslation} from 'react-i18next';
import Items from './Items';
import {MainContext} from '../../store/MainProvider';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../Components/CustomText';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import ItemsSecundary from './ItemsSecudanry';
import LottieView from 'lottie-react-native';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function PaymentMethod({navigation}) {
  const {cards} = useContext(MainContext);
  const styles = useDynamicValue(dynamicStyles);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const main = {
    light: colors.green,
    dark: colors.green,
  };

  const mainColor = main[mode];

  const types: string = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const renderItems = ({item, index}) => {
    return <Items item={item} isDelete={true} index={index} />;
  };

  return (
    <View style={styles.container}>
      <Header
        navigation={navigation}
        title={t('addPaymentMethod:headerTitle')}
        leftIcon={true}
        name="plus"
        type="Feather"
        onPress={() => {
          navigation.navigate('AddPaymentMethod');
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(types, optiones);
        }}
      />

      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{
          height: dimensions.ScreenHeight,
        }}>
        <View style={{paddingBottom: dimensions.Height(15), marginTop: 30}}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={[
              stylesText.titleText,
              {marginVertical: 20, marginHorizontal: 15},
            ]}>
            {t('addPaymentMethod:headerTitle')}
          </CustomText>

          {Platform.OS === 'ios' ? (
            <ItemsSecundary
              imagen={image.ApplePayLogo}
              title={t('addPaymentMethod:item_Apple_title')}
              description={t('addPaymentMethod:item_Apple_description')}
            />
          ) : (
            <ItemsSecundary
              imagen={image.GooglePayLogo}
              title={t('addPaymentMethod:item_Google_title')}
              description={t('addPaymentMethod:item_Google_description')}
            />
          )}
          <ItemsSecundary
            imagen={image.PaypalLogo}
            title={t('addPaymentMethod:item_Paypal_title')}
            description={t('addPaymentMethod:item_Paypal_description')}
          />

          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={[
              stylesText.titleText,
              {marginVertical: 15, marginHorizontal: 15},
            ]}>
            {t('addPaymentMethod:card_add')}
          </CustomText>

          <TouchableOpacity
            style={[styles.addCont, {borderColor: mainColor}]}
            onPress={() => {
              navigation.navigate('AddPaymentMethod');
              //@ts-ignore
              ReactNativeHapticFeedback.trigger(types, optiones);
            }}>
            <Icon
              name="plus-circle"
              type="Feather"
              size={24}
              color={mainColor}
            />
            <CustomText
              light={mainColor}
              dark={mainColor}
              style={[
                stylesText.secondaryText,
                {paddingBottom: 5, marginTop: 5},
              ]}>
              {t('addPaymentMethod:title')}
            </CustomText>
          </TouchableOpacity>
          <FlatList
            data={cards}
            renderItem={(item: any) => renderItems(item)}
            keyExtractor={(item: any) => item._id}
            showsVerticalScrollIndicator={false}
            scrollEnabled={false}
            style={{paddingTop: 20}}
            ListEmptyComponent={
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <LottieView
                  source={require('../../Assets/animated/credit-card-blue.json')}
                  autoPlay
                  loop
                  style={{width: 250}}
                />
                <CustomText
                  light={colors.back_suave_dark}
                  dark={colors.white}
                  style={[stylesText.secondaryText, {paddingBottom: 5}]}>
                  {t('addPaymentMethod:emptyText')}
                </CustomText>
              </View>
            }
          />
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  addCont: {
    padding: 10,
    margin: 15,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderWidth: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
});
