import React, {useState, useEffect} from 'react';
import {GooglePayButton, useGooglePay} from '@stripe/stripe-react-native';
import {MAIN_URL} from '../../Utils/Urls';
import {Alert, StyleSheet, View} from 'react-native';

export default function GooglePayScreen() {
  const {
    initGooglePay,
    presentGooglePay,
    loading,
    createGooglePayPaymentMethod,
  } = useGooglePay();
  const [initialized, setInitialized] = useState(false);

  // 1. Initialize Google Pay
  const initialize = async () => {
    const {error} = await initGooglePay({
      testEnv: true,
      merchantName: 'Test',
      countryCode: 'US',
      billingAddressConfig: {
        format: 'FULL',
        isPhoneNumberRequired: false,
        isRequired: false,
      },
      existingPaymentMethodRequired: false,
      isEmailRequired: false,
    });

    if (error) {
      Alert.alert(error.code, error.message);
      return;
    }
    setInitialized(true);
  };

  useEffect(() => {
    initialize();
  }, [initialized]);

  const fetchPaymentIntentClientSecret = async () => {
    const response = await fetch(`${MAIN_URL}/create-payment-intent`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        amount: 2700,
        currency: 'usd',
        force3dSecure: true,
      }),
    });
    const {clientSecret} = await response.json();

    return clientSecret;
  };

  const pay = async () => {
    // 2. Fetch payment intent client secret
    const clientSecret = await fetchPaymentIntentClientSecret();

    // 3. Open Google Pay sheet and proceed a payment
    const {error} = await presentGooglePay({
      clientSecret,
      forSetupIntent: false,
    });

    if (error) {
      Alert.alert(error.code, error.message);
      return;
    }
    Alert.alert('Success', 'The payment was confirmed successfully.');
    setInitialized(false);
  };

  /* const createPaymentMethod = async () => {
    const {error, paymentMethod} = await createGooglePayPaymentMethod({
      amount: 12,
      currencyCode: 'USD',
    });

    if (error) {
      Alert.alert(error.code, error.message);
      return;
    } else if (paymentMethod) {
      Alert.alert(
        'Success',
        `The payment method was created successfully. paymentMethodId: ${paymentMethod.id}`,
      );
    }
    setInitialized(false);
  }; */

  return (
    <View>
      <GooglePayButton
        disabled={!initialized || loading}
        style={styles.payButton}
        type="pay"
        onPress={pay}
      />

      {/* <View style={styles.row}>
        <GooglePayButton
          disabled={!initialized || loading}
          style={styles.standardButton}
          type="pay"
          onPress={createPaymentMethod}
        />
      </View> */}
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    marginTop: 30,
  },
  payButton: {
    width: '100%',
    height: 50,
  },
  standardButton: {
    width: '100%',
    height: 40,
  },
});
