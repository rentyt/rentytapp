import React, {useContext} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Alert,
  Platform,
  ActionSheetIOS,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {dimensions, colors, image, stylesText} from '../../Themes';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../Components/CustomText';
import CheckBox from '@react-native-community/checkbox';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Icon from 'react-native-dynamic-vector-icons';
import {MAIN_URL} from '../../Utils/Urls';
import {MainContext} from '../../store/MainProvider';
import Toast from 'react-native-simple-toast';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function Items({item, isDelete, index}) {
  const styles = useDynamicValue(dynamicStyles);
  const {getCard} = useContext(MainContext);

  const {t} = useTranslation();

  const mode = useColorSchemeContext();

  const main = {
    light: colors.main,
    dark: colors.main,
  };

  const types: string = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const mainColor = main[mode];

  const renderLogoBrand = () => {
    switch (item.card.brand) {
      case 'visa':
        return image.VisaLogo;

      case 'mastercard':
        return image.MasterCardLogo;

      case 'amex':
        return image.AmericamLogo;

      case 'dicover':
        return image.DiscoverLogo;

      default:
        return image.VisaLogo;
    }
  };

  const deleteCard = async id => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    const response = await fetch(`${MAIN_URL}/delete-card`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({cardID: id}),
    });

    const {success, message} = await response.json();

    if (success) {
      Toast.show(t(`addPaymentMethod:${message}`), Toast.LONG, [
        'UIAlertController',
      ]);
      getCard();
      //@ts-ignore
      ReactNativeHapticFeedback.trigger(types, optiones);
    } else {
      Toast.show(t(`addPaymentMethod:${message}`), Toast.LONG, [
        'UIAlertController',
      ]);
      //@ts-ignore
      ReactNativeHapticFeedback.trigger(types, optiones);
    }
  };

  const onPress = id => {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: t('addPaymentMethod:deletetitle'),
          options: [
            t('addPaymentMethod:options_candel'),
            t('addPaymentMethod:options_delete'),
          ],
          destructiveButtonIndex: 1,
          cancelButtonIndex: 0,
        },
        buttonIndex => {
          if (buttonIndex === 0) {
            // cancel action
          } else if (buttonIndex === 1) {
            deleteCard(id);
          }
        },
      );
    } else {
      Alert.alert(
        t('addPaymentMethod:deletetitleAlert'),
        t('addPaymentMethod:deletetitle'),
        [
          {
            text: t('addPaymentMethod:options_candel'),
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: t('addPaymentMethod:options_delete'),
            onPress: () => deleteCard(id),
          },
        ],
      );
    }
  };

  return (
    <View style={[styles.container]}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}>
        <View>
          <Image
            source={renderLogoBrand()}
            style={{width: 60, height: 40, resizeMode: 'contain'}}
          />
        </View>
        <View style={{marginLeft: 10}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              {width: dimensions.Width(50)},
            ]}>
            {item.billing_details.name}
          </CustomText>
          <View style={{marginTop: 5}}>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={[stylesText.secondaryText, {fontWeight: 'bold'}]}>
              **** **** {item.card.last4}
            </CustomText>
            <CustomText
              style={[stylesText.terciaryText, {color: colors.rgb_153}]}>
              {t('addPaymentMethod:expire')}: {item.card.exp_month}/
              {item.card.exp_year}
            </CustomText>
          </View>
        </View>
      </View>
      {!isDelete ? (
        <CheckBox
          disabled={false}
          onValueChange={value => {}}
          onTintColor={mainColor}
          tintColors={{true: mainColor, false: colors.rgb_153}}
          onCheckColor={mainColor}
          onAnimationType="fill"
          offAnimationType="fill"
          style={{marginRight: 10}}
          lineWidth={1}
        />
      ) : (
        <TouchableOpacity
          style={[styles.trash]}
          onPress={() => onPress(item.id)}>
          <Icon name="trash-2" type="Feather" size={24} color={colors.ERROR} />
        </TouchableOpacity>
      )}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    shadowColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: Platform.select({ios: 10, android: 5}),
    elevation: Platform.select({ios: 23, android: 8}),
    padding: 10,
    marginBottom: 20,
    marginHorizontal: 15,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  trash: {
    backgroundColor: new DynamicValue(
      colors.slight_grey,
      colors.back_suave_dark,
    ),
    padding: 10,
    borderRadius: 100,
  },
});
