import React from 'react';
import {
  ApplePayButton,
  useApplePay,
  presentApplePay,
  confirmApplePayPayment,
} from '@stripe/stripe-react-native';
import {View, Alert} from 'react-native';
import {MAIN_URL} from '../../Utils/Urls';

export default function PaymentScreen() {
  const {isApplePaySupported} = useApplePay();

  const fetchPaymentIntentClientSecret = async () => {
    const response = await fetch(`${MAIN_URL}/create-payment-intent`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        amount: 2500,
        currency: 'usd',
        force3dSecure: true,
      }),
    });
    const {clientSecret} = await response.json();

    return clientSecret;
  };

  const pay = async () => {
    if (!isApplePaySupported) return;
    const {error} = await presentApplePay({
      cartItems: [{label: 'Example item name', amount: '14.00'}],
      country: 'US',
      currency: 'USD',
    });
    if (error) {
      Alert.alert('Algo Salio mal');
    } else {
      const clientSecret = await fetchPaymentIntentClientSecret();

      const {error: confirmError} = await confirmApplePayPayment(clientSecret);

      if (confirmError) {
        Alert.alert('Algo Salio mal despues de cobrar');
      } else {
        Alert.alert('Pago capturado');
      }
    }
  };

  return (
    <View>
      {isApplePaySupported && (
        <ApplePayButton
          onPress={pay}
          type="plain"
          buttonStyle="black"
          borderRadius={4}
          style={{
            width: '100%',
            height: 50,
          }}
        />
      )}
    </View>
  );
}
