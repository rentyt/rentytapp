import React from 'react';
import {View, Image, TouchableOpacity, Alert, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../Themes';
import {CustomText} from '../../Components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ItemsSecundary({imagen, title, description}) {
  const styles = useDynamicValue(dynamicStyles);

  return (
    <View style={[styles.container]}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}>
        <View>
          <Image
            source={imagen}
            style={{width: 60, height: 40, resizeMode: 'contain'}}
          />
        </View>
        <View style={{marginLeft: 10}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.white}
            style={[
              stylesText.secondaryTextBold,
              {width: dimensions.Width(50)},
            ]}>
            {title}
          </CustomText>
          <View style={{marginTop: 5}}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText]}>
              {description}
            </CustomText>
          </View>
        </View>
      </View>
      <View>
        <Icon name="check" type="Feather" size={24} color={colors.green} />
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    shadowColor: new DynamicValue(colors.rgb_235, colors.back_dark),
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: Platform.select({ios: 10, android: 5}),
    elevation: Platform.select({ios: 23, android: 8}),
    padding: 10,
    marginBottom: 20,
    marginHorizontal: 15,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  trash: {
    backgroundColor: new DynamicValue(
      colors.slight_grey,
      colors.back_suave_dark,
    ),
    padding: 10,
    borderRadius: 100,
  },
});
