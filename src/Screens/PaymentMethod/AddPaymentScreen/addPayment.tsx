import React, {useState, useContext} from 'react';
import {CardField, createPaymentMethod} from '@stripe/stripe-react-native';
import {
  View,
  Keyboard,
  Alert,
  TextInput,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {MAIN_URL} from '../../../Utils/Urls';
import {colors, dimensions} from '../../../Themes';
import {MainContext} from '../../../store/MainProvider';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {CustomText} from '../../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import Toast from 'react-native-simple-toast';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function PaymentScreen({
  navigation,
  setLoading,
  onCloset,
  isPayment,
  setcardSeleted,
  setpaymentType,
}) {
  const styles = useDynamicValue(dynamicStyles);
  const {users, getCard} = useContext(MainContext);
  const [error, seterror] = useState(false);
  const [name, setname] = useState('');

  const {t} = useTranslation();

  const types: string = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const handlePayPress = async () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    if (name) {
      if (setLoading) {
        setLoading(true);
      }
      Keyboard.dismiss();
      const {error, paymentMethod} = await createPaymentMethod({
        type: 'Card',
        billingDetails: {name},
      });

      if (error) {
        if (setLoading) {
          setLoading(false);
        }
        Alert.alert(error.code, error.message);
      } else {
        const response = await fetch(`${MAIN_URL}/create-card`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            customer: users.StripeID,
            paymentMethod: paymentMethod.id,
          }),
        });

        const {success, message, data} = await response.json();

        getCard();

        if (isPayment) {
          setcardSeleted(data);
          setpaymentType('Card');
        }

        if (success) {
          if (setLoading) {
            setLoading(false);
          }

          //@ts-ignore
          ReactNativeHapticFeedback.trigger(types, optiones);

          setTimeout(() => {
            Toast.show(t(`addPaymentMethod:${message}`), Toast.LONG, [
              'UIAlertController',
            ]);
          }, 1000);

          setTimeout(() => {
            if (isPayment) {
              onCloset();
            } else {
              navigation.goBack();
            }
          }, 2000);
        } else {
          if (setLoading) {
            setLoading(false);
          }
          Toast.show(t(`addPaymentMethod:${message}`), Toast.LONG, [
            'UIAlertController',
          ]);
        }
      }
    } else {
      seterror(true);
    }
  };

  const mode = useColorSchemeContext();

  const main = {
    light: colors.colorInput,
    dark: colors.back_dark,
  };

  const textcolor = {
    light: colors.back_dark,
    dark: colors.rgb_153,
  };

  const mainColor = main[mode];

  const textColor = textcolor[mode];

  return (
    <View style={styles.container}>
      <TextInput
        autoCorrect={false}
        placeholder={t('addPaymentMethod:placeholder')}
        enablesReturnKeyAutomatically={true}
        autoCompleteType="name"
        numberOfLines={1}
        multiline={false}
        onFocus={() => seterror(false)}
        selectionColor={colors.light_sky}
        placeholderTextColor={error ? colors.white : '#a0a0a0'}
        onChangeText={setname}
        style={[
          styles.input,
          {
            backgroundColor: error ? colors.ERROR : mainColor,
            color: error ? colors.white : textColor,
            fontFamily: 'Poppins',
          },
        ]}
      />
      {error ? (
        <CustomText
          style={{
            color: colors.ERROR,
            width: dimensions.Width(86),
            marginLeft: 15,
            marginTop: 10,
          }}>
          {t('addPaymentMethod:messages_error')}
        </CustomText>
      ) : null}
      <CardField
        postalCodeEnabled={false}
        placeholder={{
          number: '4242 4242 4242 4242',
        }}
        cardStyle={styles.cardStyle}
        style={styles.card}
      />
      <TouchableOpacity onPress={handlePayPress} style={styles.buttonView}>
        <CustomText style={styles.buttonTitle}>
          {t('addPaymentMethod:btn_add')}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
    marginHorizontal: 15,
    borderRadius: 10,
    /* backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23, */
  },
  cardStyle: {
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
  },

  card: {
    width: '100%',
    height: 50,
    marginVertical: 20,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderWidth: 1,
    borderRadius: 5,
  },
  input: {
    width: '100%',
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderWidth: 1,
    height: 50,
    borderRadius: 5,
    paddingHorizontal: 10,
    fontSize: 16,
    fontWeight: '300',
  },

  buttonView: {
    backgroundColor: new DynamicValue(colors.black, colors.white),
    width: '100%',
    borderRadius: dimensions.Width(100),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.IsIphoneX()
      ? dimensions.Width(4)
      : dimensions.Width(5),
    color: new DynamicValue(colors.white, colors.black),
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(16),
    textAlign: 'center',
    fontFamily: 'Poppins',
  },
});
