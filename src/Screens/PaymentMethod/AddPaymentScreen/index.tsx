import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  SafeAreaView,
  View,
  Animated,
  ImageBackground,
  Platform,
  TouchableOpacity,
  Linking,
} from 'react-native';
import AddPayment from './addPayment';
import {dimensions, colors, image, stylesText} from '../../../Themes';
import Loader from '../../../Components/ModalLoading';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useTranslation} from 'react-i18next';
import {CustomText} from '../../../Components/CustomText';
import SafariView from 'react-native-safari-view';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import AnimatedIcon from '../../../Components/AnimatedIcons';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function AddPaymentScreen({navigation}) {
  const styles = useDynamicValue(dynamicStyles);
  const [Loading, setLoading] = useState(false);
  const [scrollY] = useState(new Animated.Value(0));
  const [scrollTop, setscrollTop] = useState(0);

  const types: string = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const mode = useColorSchemeContext();

  const header = {
    light: colors.white,
    dark: colors.black,
  };

  const ActAdd = {
    light: colors.main,
    dark: colors.main,
  };

  const text = {
    light: colors.back_dark,
    dark: colors.white,
  };

  const icons1 = {
    light: colors.white,
    dark: colors.white,
  };

  const icons2 = {
    light: colors.back_dark,
    dark: colors.white,
  };

  const bgHeader = header[mode];

  const mainColor = ActAdd[mode];

  const textColor = text[mode];

  const IconsColor1 = icons1[mode];
  const IconsColor2 = icons2[mode];

  const imageContainerHeight = scrollY.interpolate({
    inputRange: [-200, 0],
    outputRange: [450, 250],
    extrapolate: 'extend',
  });

  const opacity = scrollY.interpolate({
    inputRange: [0, 75],
    outputRange: [100, 0],
    extrapolate: 'extend',
  });

  const opacity1 = scrollY.interpolate({
    inputRange: [110, 300],
    outputRange: [0, 100],
    extrapolate: 'extend',
  });

  const {t} = useTranslation();

  const openWeb = url => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    if (Platform.OS === 'ios') {
      SafariView.show({
        url: url,
        tintColor: mainColor, // optional
        readerMode: true,
        fromBottom: true,
      });
    } else {
      Linking.openURL(url);
    }
  };

  const setTop = event => {
    setscrollTop(event.nativeEvent.contentOffset.y);
  };

  return (
    <View style={styles.container}>
      <Loader loading={Loading} color={colors.main} />
      <Animated.View
        style={[
          styles.containerIcon,
          {backgroundColor: scrollTop > 110 ? bgHeader : 'transparent'},
        ]}>
        <SafeAreaView style={styles.containerIcons}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack(); //@ts-ignore
                ReactNativeHapticFeedback.trigger(types, optiones);
              }}
              style={styles.icon}>
              <AnimatedIcon
                name="arrow-left"
                type="Feather"
                size={32}
                color={scrollTop > 110 ? IconsColor2 : IconsColor1}
              />
            </TouchableOpacity>
            <Animated.Text
              numberOfLines={1}
              style={{
                opacity: opacity1,
                fontWeight: 'bold',
                textAlign: 'center',
                width: dimensions.Width(50),
                color: textColor,
                fontSize: 18,
                fontFamily: 'Poppins',
              }}>
              {t('addPaymentMethod:title')}
            </Animated.Text>
            <TouchableOpacity
              style={styles.icon}
              onPress={() => openWeb('https://rentytapp.com/privacity')}>
              <Icon
                type="Feather"
                name="help-circle"
                size={32}
                color={scrollTop > 110 ? IconsColor2 : IconsColor1}
              />
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </Animated.View>

      <Animated.View
        style={[styles.gradContainer, {height: imageContainerHeight}]}>
        <ImageBackground
          style={styles.image}
          resizeMode="cover"
          source={image.Header}>
          <Image style={styles.image} source={image.Header} />
          <Animated.Text
            style={[styles.title, {opacity: opacity, fontFamily: 'Poppins'}]}>
            {t('addPaymentMethod:title')}
          </Animated.Text>
        </ImageBackground>
      </Animated.View>

      <ScrollView
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: scrollY}}}],
          //@ts-ignore
          {listener: event => setTop(event)},
        )}
        scrollEventThrottle={16}
        stickyHeaderIndices={[1]}
        keyboardShouldPersistTaps="handled">
        <View style={{marginTop: 250}}>
          <View
            style={{
              width: dimensions.ScreenWidth,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={image.LogoCard}
              style={{width: 300, height: 140, resizeMode: 'contain'}}
            />
          </View>
          <AddPayment
            navigation={navigation}
            setLoading={setLoading}
            onCloset={null}
            setcardSeleted={null}
            setpaymentType={null}
            isPayment={false}
          />

          <View style={styles.secure}>
            <View>
              <Icon name="lock" type="Feather" size={24} color={colors.green} />
            </View>

            <View style={{marginLeft: 15}}>
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {color: colors.green, fontWeight: '500'},
                ]}>
                {t('addPaymentMethod:secure')}
              </CustomText>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.colorInput, colors.black),
  },

  containerIcon: {
    width: dimensions.Width(100),
    position: 'absolute',
    paddingTop: Platform.select({
      ios: dimensions.IsIphoneX() ? dimensions.Height(0) : dimensions.Height(4),
      android: dimensions.Height(5),
    }),
    zIndex: 1,
    paddingBottom: 10,
  },

  containerIcons: {
    width: dimensions.Width(96),
    marginHorizontal: 10,
  },

  gradContainer: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: 200,
  },

  image: {
    flex: 1,
  },

  icon: {
    width: 36,
    height: 36,
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  title: {
    position: 'absolute',
    top: 120,
    paddingHorizontal: 20,
    color: colors.white,
    fontSize: 30,
    width: 300,
    fontWeight: 'bold',
  },

  secure: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 20,
    backgroundColor: 'rgba(41, 216, 131, 0.205)',
    marginHorizontal: 20,
    marginTop: dimensions.Height(10),
    borderRadius: 10,
    marginBottom: dimensions.Height(5),
  },
});
