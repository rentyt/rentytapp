import React, {useState} from 'react';
import {View, Platform} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import {colors} from '../../Themes';
import Header from '../../Components/Header';
import SegmentedControl from '@react-native-community/segmented-control';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import RatingCar from '../../Components/RatingCar/ratingView';
import RatingUser from '../../Components/RatingUser/ratingView';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Rating({route, navigation}) {
  const {id} = route.params;
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const [active, setActive] = useState(0);

  return (
    <View style={styles.container}>
      <Header
        navigation={navigation}
        title={t('rating:titleHeader')}
        leftIcon={false}
      />
      <View
        style={{
          width: '96%',
          alignSelf: 'center',
          marginTop: 20,
          marginBottom: 30,
          marginHorizontal: 15,
        }}>
        <SegmentedControl
          values={[t('rating:UserTitle'), t('rating:carTitle')]}
          selectedIndex={active}
          onChange={event => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
            setActive(event.nativeEvent.selectedSegmentIndex);
          }}
        />
      </View>

      <View style={styles.container}>
        {active === 0 ? <RatingUser id={id} /> : <RatingCar id={id} />}
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
