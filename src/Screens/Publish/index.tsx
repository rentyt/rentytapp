import React, {useState} from 'react';
import {View, TouchableOpacity, Platform, Dimensions} from 'react-native';
import {stylesText, colors} from '../../Themes';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import AddComponent from '../../Components/AddCar';
import LottieView from 'lottie-react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const screens = Dimensions.get('window').height;

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Favourites({navigation, user}) {
  const [visibleModal, setvisibleModal] = useState(false);

  const {t} = useTranslation();

  const setstart = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    setvisibleModal(true);
  };
  return (
    <View>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: screens > 768 ? 80 : 20,
        }}>
        <LottieView
          source={require('../../Assets/animated/Publish.json')}
          autoPlay
          loop
          style={{width: screens > 768 ? 300 : 250}}
        />
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.secondaryTextBold,
            {textAlign: 'center', marginHorizontal: 30},
          ]}>
          {t('publish:title')}
        </CustomText>
        <CustomText
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.secondaryText,
            {textAlign: 'center', marginTop: 15, marginHorizontal: 30},
          ]}>
          {t('publish:description')}
        </CustomText>

        <TouchableOpacity
          onPress={setstart}
          style={{
            marginTop: 30,
            backgroundColor: colors.green,
            paddingVertical: 15,
            paddingHorizontal: 50,
            borderRadius: 100,
          }}>
          <CustomText
            light={colors.white}
            dark={colors.white}
            style={[stylesText.secondaryTextBold, {textAlign: 'center'}]}>
            {t('publish:btn')}
          </CustomText>
        </TouchableOpacity>
      </View>
      <AddComponent
        visibleModal={visibleModal}
        setvisibleModal={setvisibleModal}
        edit={false}
        refetch={null}
        data={null}
      />
    </View>
  );
}
