import React from 'react';
import {View, TextInput, TouchableOpacity, Platform} from 'react-native';
import {CustomText} from '../../Components/CustomText';
import {colors, stylesText, dimensions} from '../../Themes';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-dynamic-vector-icons';
import Textarea from 'react-native-textarea';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import {NavPhone} from '../../interfaces/navigation';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Content({
  data,
  name,
  setName,
  latname,
  setlatName,
  description,
  setDescription,
  navigation,
  openModal,
  city,
}) {
  const {t} = useTranslation();
  const styles = useDynamicStyleSheet(dynamicStyles);

  const navPhone: NavPhone = {
    id: data._id,
    token: '',
    Register: false,
    user: data,
    password: '',
    avatar: data.avatar,
    StripeID: data.StripeID,
  };
  const chageNumber = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    navigation.navigate('VerifyPhone', navPhone);
  };

  return (
    <View style={styles.container}>
      <View>
        <CustomText
          numberOfLines={2}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold]}>
          {t('updateProfile:title')}
        </CustomText>
        <TextInput
          autoCorrect={false}
          placeholder={t('updateProfile:name')}
          selectionColor={colors.green}
          placeholderTextColor={colors.rgb_153}
          enablesReturnKeyAutomatically={true}
          value={name ? name : data.name}
          onChangeText={value => setName(value)}
          style={styles.textInput}
        />

        <TextInput
          autoCorrect={false}
          placeholder={t('updateProfile:lastName')}
          selectionColor={colors.green}
          value={latname}
          placeholderTextColor={colors.rgb_153}
          enablesReturnKeyAutomatically={true}
          onChangeText={value => setlatName(value)}
          style={styles.textInput}
        />
      </View>

      <View style={{marginTop: 20}}>
        <CustomText
          numberOfLines={2}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold]}>
          {t('updateProfile:title2')}
        </CustomText>
        <Textarea
          containerStyle={styles.textareaContainer}
          //@ts-ignore
          style={styles.textarea}
          onChangeText={value => setDescription(value)}
          defaultValue={description}
          maxLength={250}
          minLength={100}
          selectionColor={colors.green}
          placeholder={t('updateProfile:title2')}
          placeholderTextColor={'#c7c7c7'}
          underlineColorAndroid={'transparent'}
        />
      </View>
      <View style={{marginTop: 20}}>
        <CustomText
          numberOfLines={2}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold]}>
          {t('updateProfile:title3')}
        </CustomText>
        <TouchableOpacity style={styles.textInput} onPress={chageNumber}>
          <CustomText
            numberOfLines={2}
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              {fontSize: 18, fontWeight: '300'},
            ]}>
            {data.phone ? `+${data.phone}` : t('updateProfile:phone')}
          </CustomText>
        </TouchableOpacity>
      </View>

      <View style={{marginTop: 20, marginBottom: dimensions.Height(35)}}>
        <CustomText
          numberOfLines={2}
          light={colors.black}
          dark={colors.white}
          style={[stylesText.secondaryTextBold]}>
          {t('updateProfile:title4')}
        </CustomText>
        <TouchableOpacity style={styles.textInput} onPress={openModal}>
          <CustomText
            numberOfLines={2}
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.secondaryText,
              {fontSize: 18, fontWeight: '300'},
            ]}>
            {city ? `${city}` : t('updateProfile:city')}
          </CustomText>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    alignSelf: 'center',
    width: dimensions.Width(92),
    marginTop: 0,
  },

  textInput: {
    marginTop: 10,
    padding: 15,
    borderRadius: 10,
    marginBottom: 5,
    fontSize: 18,
    fontFamily: 'Poppins',
    fontWeight: '300',
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    color: new DynamicValue(colors.black, colors.white),
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },

  textareaContainer: {
    height: 'auto',
    minHeight: 100,
    padding: 10,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    borderRadius: 10,
    marginTop: 10,
    width: '100%',
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
  },
  textarea: {
    textAlignVertical: 'top', // hack android
    height: 'auto',
    minHeight: 100,
    color: new DynamicValue(colors.black, colors.white),
    fontSize: 18,
    fontFamily: 'Poppins',
    fontWeight: '300',
  },
});
