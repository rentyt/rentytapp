import React, {useContext, useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  Platform,
  Alert,
  Modal,
} from 'react-native';
import {useMutation} from '@apollo/client';
import {mutations} from '../../GraphQL';
//@ts-ignore
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import {CustomText} from '../../Components/CustomText';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText, image} from '../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import Star from '../../Components/star';
import {MainContext} from '../../store/MainProvider';
import {Button} from '../../Components/Buttom';
import {useTranslation} from 'react-i18next';
import Content from './content';
import Loader from '../../Components/ModalLoading';
import Toast from 'react-native-simple-toast';
import {setItem} from '../../helpers/AsyncStorage';
import ImagePicker from 'react-native-image-crop-picker';
import Location from './Location';
import HeaderModal from '../../Components/HeaderModal';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';

const window = Dimensions.get('window');

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function UserPorfile({navigation}) {
  const {users, getUser} = useContext(MainContext);
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const [updateUser] = useMutation(mutations.UPDATE_USUARIO);
  const [singleUploadToAws] = useMutation(mutations.UPLOAD_FILE);

  const [name, setName] = useState(users.name);
  const [latname, setlatName] = useState(users.lastName);
  const [description, setDescription] = useState(users.description);
  const [avatar, setAvatar] = useState(users.avatar);
  const [Loading, setLoading] = useState(false);
  const [location, setLocation] = useState(users.location);
  const [visibleModal, setvisibleModal] = useState(false);
  const [city, setCity] = useState(users.city);

  useEffect(() => {
    getUser();
  }, []);

  useEffect(() => {
    if (Platform.OS === 'ios') {
      requestMultiple([PERMISSIONS.IOS.CAMERA]).then(statuses => {});
    } else {
      requestMultiple([PERMISSIONS.ANDROID.CAMERA]).then(statuses => {});
    }
  }, []);

  const updateUSer = async () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    const input = {
      _id: users._id,
      name: name,
      lastName: latname,
      description: description,
      location: location,
      city: city,
    };
    setLoading(true);
    updateUser({variables: {input: input}})
      .then(async res => {
        if (res.data.updateUser.success) {
          await setItem('user', JSON.stringify(res.data.updateUser.data));
          setLoading(false);
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(type, optiones);
          getUser();
          setTimeout(() => {
            Toast.showWithGravity(
              res.data.updateUser.message,
              Toast.LONG,
              Toast.TOP,
            );
            getUser();
          }, 1000);
        } else {
          getUser();
          setLoading(false);
          setTimeout(() => {
            Toast.showWithGravity(
              res.data.updateUser.message,
              Toast.LONG,
              Toast.TOP,
            );
          }, 1000);
        }
      })
      .catch(e => {
        console.log(e);
        setLoading(false);
        Alert.alert('Algo salio mal intentalo de nuevo');
      })
      .finally(() => {
        getUser();
        setLoading(false);
      });
  };

  const ChangeAvatar = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    ImagePicker.openPicker({
      mediaType: 'photo',
      width: 512,
      height: 512,
      cropping: true,
      includeBase64: true,
      useFrontCamera: true,
      cropperCircleOverlay: true,
      cropperChooseText: t('updateProfile:Seleccionar'),
      cropperCancelText: t('updateProfile:Cancelar'),
      cropperToolbarTitle: t('updateProfile:titlePhoto'),
    })
      .then(image => {
        setLoading(true);
        const file = 'data:image/jpeg;base64,' + image.data;
        singleUploadToAws({variables: {file: file}}).then(res => {
          const result = res.data.singleUploadToAws.data;
          if (result.Location) {
            setAvatar(result.Location);
            const inputAvat = {
              _id: users._id,
              avatar: result.Location,
            };
            updateUser({variables: {input: inputAvat}})
              .then(async res => {
                if (res.data.updateUser.success) {
                  //@ts-ignore
                  ReactNativeHapticFeedback.trigger(type, optiones);
                  await setItem(
                    'user',
                    JSON.stringify(res.data.updateUser.data),
                  );
                  setLoading(false);
                  getUser();
                  setTimeout(() => {
                    Toast.showWithGravity(
                      'Foto del perfil actualizada con éxito',
                      Toast.LONG,
                      Toast.TOP,
                    );
                    getUser();
                  }, 1000);
                } else {
                  getUser();
                  setLoading(false);
                  setTimeout(() => {
                    Toast.showWithGravity(
                      'Algo salio mal intentalo de nuevo',
                      Toast.LONG,
                      Toast.TOP,
                    );
                  }, 1000);
                }
              })
              .catch(e => {
                console.log(e);
                setLoading(false);
                Alert.alert('Algo salio mal intentalo de nuevo');
              })
              .finally(() => {
                getUser();
                setLoading(false);
              });
          } else {
            Alert.alert('Algo salio mal intentalo de nuevo');
          }
        });
      })
      .catch(() => {
        setLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <Loader loading={Loading} />
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor="transparent"
        stickyHeaderHeight={100}
        parallaxHeaderHeight={200}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background" style={{backgroundColor: colors.white}}>
            <ImageBackground
              style={styles.imagenbg}
              source={image.HeaderUser}
            />
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <TouchableOpacity
              style={styles.left}
              onPress={() => {
                navigation.goBack();
                getUser();
              }}>
              <Icon
                name="arrow-left"
                type="Feather"
                size={24}
                style={styles.close}
              />
            </TouchableOpacity>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[
                stylesText.secondaryTextBold,
                {marginLeft: 65, width: dimensions.Width(75)},
              ]}>
              {users.name} {users.lastName}{' '}
              {users.verified ? (
                <Icon
                  type="MaterialIcons"
                  name="verified"
                  size={16}
                  color={colors.twitter_color}
                  style={{marginLeft: 5}}
                />
              ) : null}
            </CustomText>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View
            style={{
              position: 'absolute',
              top: -50,
              width: window.width,
              justifyContent: 'center',
              alignItems: 'center',
              zIndex: 1001,
            }}>
            <TouchableOpacity
              style={styles.avatarBorder}
              onPress={ChangeAvatar}>
              <Image source={{uri: avatar}} style={styles.avatar} />
              <View style={styles.edit}>
                <Icon
                  type="Feather"
                  name="edit"
                  size={16}
                  color={colors.green}
                />
              </View>
            </TouchableOpacity>

            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 5,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <CustomText
                  numberOfLines={1}
                  light={colors.black}
                  dark={colors.white}
                  style={[stylesText.titleText]}>
                  {users.name} {users.lastName}
                </CustomText>
                {users.verified ? (
                  <Icon
                    type="MaterialIcons"
                    name="verified"
                    size={16}
                    color={colors.twitter_color}
                    style={{marginLeft: 5}}
                  />
                ) : null}
              </View>
              <View style={{flexDirection: 'row', marginTop: 3}}>
                <Star star={Number.parseFloat(users.rating)} />
                <CustomText
                  numberOfLines={1}
                  light={colors.orange}
                  dark={colors.orange}
                  style={[stylesText.secondaryTextBold, {marginLeft: 10}]}>
                  {users.rating}
                </CustomText>
              </View>
            </View>
          </View>

          <View style={{marginTop: 150}}>
            <Content
              data={users}
              name={name}
              setName={setName}
              latname={latname}
              setlatName={setlatName}
              description={description}
              setDescription={setDescription}
              navigation={navigation}
              city={city}
              openModal={() => {
                setvisibleModal(true);
                //@ts-ignore
                ReactNativeHapticFeedback.trigger(type, optiones);
              }}
            />
          </View>
        </View>
      </ParallaxScrollView>
      <View style={styles.butonModal}>
        <View style={{alignSelf: 'center', marginBottom: 30}}>
          <Button
            title={t('updateProfile:btn')}
            light={colors.white}
            dark={colors.white}
            loading={false}
            onPress={updateUSer}
          />
        </View>
      </View>
      <Modal
        animationType="slide"
        visible={visibleModal}
        presentationStyle="formSheet"
        collapsable={true}
        statusBarTranslucent={true}
        onRequestClose={() => {
          setvisibleModal(false);
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(type, optiones);
        }}>
        <HeaderModal
          clean={false}
          onPress={() => {
            setvisibleModal(false);
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
          }}
          title={t('updateProfile:modalTitle')}
          children={
            <View style={styles.contentModal}>
              <Location
                location={location}
                setLocation={setLocation}
                city={city}
                setCity={setCity}
              />
            </View>
          }
        />

        <View style={styles.butonModal}>
          <View style={{alignSelf: 'center', marginBottom: 30}}>
            <Button
              title={t('updateProfile:btnAdrees')}
              light={colors.white}
              dark={colors.white}
              loading={false}
              onPress={() => {
                setvisibleModal(false);
                //@ts-ignore
                ReactNativeHapticFeedback.trigger(type, optiones);
              }}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  contenedor: {
    flex: 1,
    width: dimensions.ScreenWidth,
    height: 'auto',
    minHeight: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  imagenbg: {
    width: dimensions.ScreenWidth,
    height: 200,
  },

  stickySection: {
    height: 100,
    width: '100%',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: 40,
  },

  fixedSection: {
    position: 'absolute',
    left: 15,
    flexDirection: 'row',
    top: 50,
  },

  left: {
    marginRight: dimensions.Width(4),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
  },

  close: {
    color: new DynamicValue(colors.black, colors.white),
  },

  butonModal: {
    width: dimensions.Width(100),
    height: 100,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    bottom: -5,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 23,
  },

  avatarBorder: {
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_suave_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
  },

  avatar: {
    width: 100,
    height: 100,
    borderRadius: 100,
    borderColor: new DynamicValue(colors.white, colors.back_dark),
    borderWidth: 4,
  },

  edit: {
    width: 35,
    height: 35,
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: 0,
    bottom: 0,
  },

  contentModal: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
