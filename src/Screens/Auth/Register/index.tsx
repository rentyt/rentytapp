import React, {useState, useContext} from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  Animated,
  Keyboard,
} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../../Components/CustomText';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Button} from '../../../Components/Buttom';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Toast from 'react-native-toast-message';
import {validateEmail} from '../../../Utils/validateEmail';
import SocialLogin from '../Login/SocialLogin';
import {useMutation} from '@apollo/client';
import {mutations} from '../../../GraphQL';
import ModalLoading from '../../../Components/ModalLoading';
import {NavPhone} from '../../../interfaces/navigation';
import {useTranslation} from 'react-i18next';
import {MainContext} from '../../../store/MainProvider';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function RegisterScreens({navigation}) {
  const [error, seterror] = useState(false);
  const [Loading, setLoading] = useState(false);
  const [email, setemail] = useState('');
  const [name, setname] = useState('');
  const [lastName, setlastName] = useState('');
  const [password, setpassword] = useState('');
  const [widthImage] = useState(new Animated.Value(dimensions.Height(30)));
  const [top] = useState(new Animated.Value(dimensions.Height(5)));
  const styles = useDynamicValue(dynamicStyles);

  const {getUser, getCard} = useContext(MainContext);

  const {t} = useTranslation();

  const [crearUsuario] = useMutation(mutations.CREATE_USUARIO);

  const animatedIcon = () => {
    seterror(false);
    Animated.sequence([
      //@ts-ignore
      Animated.timing(widthImage, {
        toValue: dimensions.Height(15),
        duration: 500,
      }),
      //@ts-ignore
      Animated.timing(top, {
        toValue: dimensions.Height(0),
        duration: 500,
      }),
    ]).start();
  };

  const animatedImagen = () => {
    Keyboard.dismiss();
    Animated.sequence([
      //@ts-ignore
      Animated.timing(widthImage, {
        toValue: dimensions.Height(30),
        duration: 200,
      }),
      //@ts-ignore
      Animated.timing(top, {
        toValue: dimensions.Height(5),
        duration: 200,
      }),
    ]).start();
  };

  const input = {
    name,
    lastName,
    email,
    password,
  };

  const resetPassword = () => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    setLoading(true);
    Keyboard.dismiss();
    animatedImagen();
    setLoading(!Loading);
    if (!validateEmail(email.trim())) {
      setLoading(false);
      seterror(true);
      ReactNativeHapticFeedback.trigger('notificationError', optiones);
      Toast.show({
        text1: t('forgot:noEmail'),
        text2: t('forgot:BagError'),
        position: 'top',
        type: 'error',
        topOffset: 50,
      });
    } else {
      crearUsuario({variables: {input: input}})
        .then(res => {
          const data = res.data.crearUsuario;
          if (data.success) {
            setLoading(false);
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
            Toast.show({
              text1: t('forgot:genial'),
              text2: data.message,
              position: 'top',
              type: 'success',
              topOffset: 50,
            });

            const navPhone: NavPhone = {
              id: data.data._id,
              token: '',
              Register: true,
              user: data.data,
              password: password,
              avatar: data.data.avatar,
              StripeID: data.StripeID,
            };
            getUser();
            getCard();
            if (data.data.verifyPhone) {
              navigation.navigate('Login');
            } else {
              setTimeout(() => {
                navigation.navigate('VerifyPhone', navPhone);
              }, 2000);
            }
          } else {
            setLoading(false);
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
            Toast.show({
              text1: t('forgot:genial'),
              text2: data.message,
              position: 'top',
              type: 'error',
              topOffset: 50,
            });
          }
        })
        .catch(() => {
          setLoading(false);
          Toast.show({
            text1: t('forgot:genial'),
            text2: t('forgot:error'),
            position: 'top',
            type: 'error',
            topOffset: 50,
          });
        });
    }
  };

  const goToBack = () => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    navigation.goBack();
  };

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];

  const TextColor = {
    light: colors.black,
    dark: colors.white,
  };

  const TextsColor = TextColor[mode];

  return (
    <View style={styles.container}>
      <Toast ref={ref => Toast.setRef(ref)} style={{zIndex: 100}} />
      <ModalLoading loading={Loading} />
      <TouchableOpacity style={styles.back} onPress={goToBack}>
        <Icon
          type="Feather"
          name="arrow-left"
          size={26}
          color={colors.rgb_153}
        />
      </TouchableOpacity>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: dimensions.Height(20),
          }}>
          <View style={styles.content}>
            <CustomText
              style={[stylesText.titleText, {textAlign: 'center'}]}
              light={colors.black}
              dark={colors.white}>
              {t('login:successMessage')}
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {
                  marginTop: dimensions.Height(3),
                  textAlign: 'center',
                  marginBottom: dimensions.Height(3),
                },
              ]}>
              {t('register:subtitle')}
            </CustomText>

            <SocialLogin
              navigation={navigation}
              setLoading={setLoading}
              register={true}
            />

            <View
              style={{
                marginBottom: dimensions.Height(2),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: dimensions.Width(40),
                  height: 1,
                  backgroundColor: backgroundColor,
                }}
              />
              <CustomText
                style={[stylesText.secondaryText, {textAlign: 'center'}]}
                light={colors.rgb_153}
                dark={colors.rgb_153}>
                {t('register:or')}
              </CustomText>
              <View
                style={{
                  width: dimensions.Width(40),
                  height: 1,
                  backgroundColor: backgroundColor,
                }}
              />
            </View>

            <TextInput
              autoCorrect={false}
              placeholder={t('register:name')}
              onFocus={animatedIcon}
              selectionColor={colors.green}
              placeholderTextColor={colors.rgb_153}
              enablesReturnKeyAutomatically={true}
              onChangeText={setname}
              style={[
                styles.textInput,
                {backgroundColor: backgroundColor, color: TextsColor},
              ]}
            />

            <TextInput
              autoCorrect={false}
              placeholder={t('register:lastName')}
              onFocus={animatedIcon}
              selectionColor={colors.green}
              placeholderTextColor={colors.rgb_153}
              enablesReturnKeyAutomatically={true}
              onChangeText={setlastName}
              style={[
                styles.textInput,
                {
                  backgroundColor: backgroundColor,
                  color: TextsColor,
                },
              ]}
            />

            <TextInput
              autoCorrect={false}
              placeholder="Email"
              autoCapitalize="none"
              onFocus={animatedIcon}
              selectionColor={colors.green}
              placeholderTextColor={error ? colors.white : colors.rgb_153}
              enablesReturnKeyAutomatically={true}
              onChangeText={value => setemail(value.trim())}
              style={[
                styles.textInput,
                {
                  backgroundColor: error ? colors.ERROR : backgroundColor,
                  color: error ? colors.white : TextsColor,
                },
              ]}
            />

            <TextInput
              autoCorrect={false}
              placeholder={t('register:password')}
              secureTextEntry={true}
              autoCapitalize="none"
              passwordRules="minlength: 6; required: lower; required: upper; required: digit; required: [-];"
              onFocus={animatedIcon}
              selectionColor={colors.green}
              placeholderTextColor={colors.rgb_153}
              enablesReturnKeyAutomatically={true}
              onChangeText={setpassword}
              style={[
                styles.textInput,
                {backgroundColor: backgroundColor, color: TextsColor},
              ]}
            />

            <View style={{marginTop: dimensions.Height(3)}}>
              <Button
                light={colors.white}
                dark={colors.white}
                loading={Loading}
                onPress={resetPassword}
                title={t('register:btn')}
              />
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
  },

  content: {
    marginHorizontal: 20,
    marginTop: 0,
  },

  back: {
    width: 50,
    height: 50,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 15,
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 0,
  },

  textInput: {
    marginTop: 7,
    padding: 20,
    borderRadius: 10,
    marginBottom: 5,
    fontSize: 18,
    fontFamily: 'Poppins',
    fontWeight: '300',
  },
});
