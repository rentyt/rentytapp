import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  Animated,
  Keyboard,
} from 'react-native';
import {colors, dimensions, image, stylesText} from '../../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../../Components/CustomText';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Button} from '../../../Components/Buttom';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Toast from 'react-native-toast-message';
import {validateEmail} from '../../../Utils/validateEmail';
import {MAIN_URL} from '../../../Utils/Urls';
import ModalLoading from '../../../Components/ModalLoading';
import {useTranslation} from 'react-i18next';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export default function ForgotPassWord({navigation}) {
  const [error, seterror] = useState(false);
  const [Loading, setLoading] = useState(false);
  const [email, setemail] = useState('');
  const [widthImage] = useState(new Animated.Value(dimensions.Height(30)));
  const [top] = useState(new Animated.Value(dimensions.Height(5)));
  const styles = useDynamicValue(dynamicStyles);

  const {t} = useTranslation();

  const animatedIcon = () => {
    seterror(false);
    Animated.sequence([
      //@ts-ignore
      Animated.timing(widthImage, {
        toValue: dimensions.Height(15),
        duration: 500,
      }),
      //@ts-ignore
      Animated.timing(top, {
        toValue: dimensions.Height(0),
        duration: 500,
      }),
    ]).start();
  };

  const animatedImagen = () => {
    Keyboard.dismiss();
    Animated.sequence([
      //@ts-ignore
      Animated.timing(widthImage, {
        toValue: dimensions.Height(30),
        duration: 200,
      }),
      //@ts-ignore
      Animated.timing(top, {
        toValue: dimensions.Height(5),
        duration: 200,
      }),
    ]).start();
  };

  const resetPassword = async () => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    Keyboard.dismiss();
    animatedImagen();
    setLoading(!Loading);
    if (!validateEmail(email)) {
      setLoading(false);
      seterror(true);
      ReactNativeHapticFeedback.trigger('notificationError', optiones);
      Toast.show({
        text1: t('forgot:noEmail'),
        text2: t('forgot:BagError'),
        position: 'top',
        type: 'error',
        topOffset: 50,
      });
    } else {
      let res = await fetch(`${MAIN_URL}/forgotpassword?email=${email}`);
      if (res) {
        const user = await res.json();
        if (!user.success) {
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          setLoading(false);
          Toast.show({
            text1: t('login:bag'),
            text2: user.message,
            position: 'top',
            type: 'error',
            topOffset: 50,
          });
        } else {
          ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
          Toast.show({
            text1: t('forgot:genial'),
            text2: user.message,
            position: 'top',
            type: 'success',
            topOffset: 50,
          });
          setLoading(false);
          setTimeout(() => {
            navigation.goBack();
          }, 2000);
        }
      } else {
        ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
        Toast.show({
          text1: t('login:bag'),
          text2: t('forgot:messageErro'),
          position: 'top',
          type: 'error',
          topOffset: 50,
        });
        setLoading(false);
      }
    }
  };

  const goToBack = () => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    navigation.goBack();
  };

  const backgroundColors = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };
  const mode = useColorSchemeContext();
  const backgroundColor = backgroundColors[mode];

  const TextColor = {
    light: colors.black,
    dark: colors.white,
  };

  const TextsColor = TextColor[mode];

  return (
    <View style={styles.container}>
      <ModalLoading loading={Loading} />
      <Toast ref={ref => Toast.setRef(ref)} style={{zIndex: 100}} />
      <TouchableOpacity style={styles.back} onPress={goToBack}>
        <Icon
          type="Feather"
          name="arrow-left"
          size={26}
          color={colors.rgb_153}
        />
      </TouchableOpacity>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Animated.View
            style={{
              alignItems: 'center',
              zIndex: 0,
              marginTop: 50,
            }}>
            <Icon type="Feather" name="lock" size={56} color={colors.main} />
          </Animated.View>

          <View style={styles.content}>
            <CustomText
              style={[stylesText.titleText, {textAlign: 'center'}]}
              light={colors.black}
              dark={colors.white}>
              {t('forgot:title')}
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {marginTop: dimensions.Height(3), textAlign: 'center'},
              ]}>
              {t('forgot:message')}
            </CustomText>
            <TextInput
              autoCorrect={false}
              placeholder="Email"
              autoCapitalize="none"
              onFocus={animatedIcon}
              selectionColor={colors.green}
              placeholderTextColor={error ? colors.white : colors.rgb_153}
              enablesReturnKeyAutomatically={true}
              onChangeText={setemail}
              style={[
                styles.textInput,
                {
                  backgroundColor: error ? colors.ERROR : backgroundColor,
                  color: error ? colors.white : TextsColor,
                },
              ]}
            />
            <Button
              light={colors.white}
              dark={colors.white}
              loading={Loading}
              onPress={resetPassword}
              title={t('forgot:btn')}
            />
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
  },

  content: {
    marginHorizontal: 20,
    marginTop: 30,
  },

  back: {
    width: 50,
    height: 50,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 15,
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 0,
  },

  textInput: {
    marginTop: 30,
    padding: 20,
    borderRadius: 10,
    marginBottom: 25,
    fontSize: 18,
    fontFamily: 'Poppins',
    fontWeight: '300',
  },
});
