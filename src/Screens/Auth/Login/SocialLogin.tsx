import React, {useEffect, useContext} from 'react';
import {View, TouchableOpacity, Platform, Image, Alert} from 'react-native';
import {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import {DynamicStyleSheet, useDynamicValue} from 'react-native-dynamic';
import {CustomText} from '../../../Components/CustomText';
import {colors, dimensions, image, stylesText} from '../../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import Toast from 'react-native-toast-message';
import {ILogin} from '../../../interfaces';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {appleAuth} from '@invertase/react-native-apple-authentication';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {MAIN_URL} from '../../../Utils/Urls';
import {useMutation} from '@apollo/client';
import {mutations} from '../../../GraphQL';
import {MainContext} from '../../../store/MainProvider';
import {setItem} from '../../../helpers/AsyncStorage';
import {NavPhone} from '../../../interfaces/navigation';
import {useTranslation} from 'react-i18next';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

GoogleSignin.configure({
  iosClientId:
    '944342204268-k2uipa6gin9k0oacvh8u04hi73dnq44p.apps.googleusercontent.com',
});

export default function SocialLogin({navigation, setLoading, register}) {
  const styles = useDynamicValue(dynamicStyles);
  const {setId, getUser, getCard} = useContext(MainContext);
  const [LoginUser] = useMutation(mutations.AUTENTICAR_USUARIO);

  const {t} = useTranslation();

  useEffect(() => {
    if (Platform.OS === 'ios') {
      return appleAuth.onCredentialRevoked(async () => {
        console.warn(
          'If this function executes, User Credentials have been Revoked',
        );
      });
    }
  }, []);

  const onLogin = (email: string, password: string) => {
    setLoading(true);
    LoginUser({variables: {email: email, password: password}})
      .then(async res => {
        if (res.data.LoginUser.success) {
          const user = res.data.LoginUser.data;
          if (user.verifyPhone) {
            await setItem('token', user.token);
            await setItem('id', user.id);
            await setItem('user', JSON.stringify(user.user));
            setId(user.id);
            Toast.show({
              text1: t('login:hello'),
              text2: t(`login:${res.data.LoginUser.message}`),
              position: 'top',
              type: 'success',
              topOffset: 50,
            });
            getUser();
            getCard();
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
            setTimeout(() => {
              navigation.navigate('Home');
            }, 1500);
          } else {
            getUser();
            getCard();
            const navPhone: NavPhone = {
              id: user.id,
              token: user.token,
              Register: false,
              user: user.user,
              password: '',
              avatar: user.avatar,
              StripeID: user.StripeID,
            };
            navigation.navigate('VerifyPhone', navPhone);
          }
        } else {
          setLoading(false);
          ReactNativeHapticFeedback.trigger('notificationError', optiones);
          Toast.show({
            text1: t('login:bag'),
            text2: t(`login:${res.data.LoginUser.message}`),
            position: 'top',
            type: 'error',
            topOffset: 50,
          });
        }
      })
      .catch(() => {
        ReactNativeHapticFeedback.trigger('notificationError', optiones);
        Toast.show({
          text1: t('login:bag'),
          text2: t('login:bag'),
          position: 'top',
          type: 'error',
          topOffset: 50,
        });
        setLoading(false);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const socialLogin = async (data: ILogin) => {
    await fetch(`${MAIN_URL}/api/v1/auth/social/mobile`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(async (res: any) => {
        const data = await res.json();
        onLogin(data.nuevoUsuario.email, data.token);
      })
      .catch((err: any) => {
        Toast.show({
          text1: t('login:bag'),
          text2: t('login:bag'),
          position: 'top',
          type: 'error',
          topOffset: 50,
        });
        ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
      });
  };

  async function onAppleButtonPress() {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    if (Platform.OS === 'ios') {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });

      const credentialState = await appleAuth.getCredentialStateForUser(
        appleAuthRequestResponse.user,
      );

      if (credentialState === appleAuth.State.AUTHORIZED) {
        let email = appleAuthRequestResponse.email;
        let token = appleAuthRequestResponse.user;
        let firstName = appleAuthRequestResponse.fullName.givenName;
        let lastName = appleAuthRequestResponse.fullName.familyName;
        let provide = 'Apple App';

        socialLogin({firstName, lastName, email, token, provide});
      }
    }
  }

  const googleSignIn = async () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      let firstName = userInfo.user.name;
      let lastName = userInfo.user.givenName;
      let email = userInfo.user.email;
      let token = userInfo.user.id;
      let provide = 'Google App';
      socialLogin({firstName, lastName, email, token, provide});
    } catch (error) {
      console.log(error);
    }
  };

  const getFacebookUserInfo = (result: any) => {
    let name = result.name.split(' ');
    let firstName = name[0];
    let lastName = name[1] || '';
    let email = result.email;
    let token = result.id;
    let provide = 'Facebook App';
    socialLogin({firstName, lastName, email, token, provide});
  };

  const facebookLogin = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function (result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log(
            'Login success with permissions: ' +
              result.grantedPermissions.toString(),
          );

          const responseInfoCallback = (error: any, result: any) => {
            if (error) {
              console.log(error);
              Alert.alert('Facebook request failed. Try Again.');
            } else {
              getFacebookUserInfo(result);
            }
          };

          const infoRequest = new GraphRequest(
            '/me?fields=email,name,picture.height(480)',
            null,
            responseInfoCallback,
          );
          // Start the graph request.
          console.log('start  graph request');
          new GraphRequestManager().addRequest(infoRequest).start();
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  return (
    <>
      {register ? (
        <View>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              width: dimensions.Width(55),
              justifyContent: 'space-between',
            }}>
            {Platform.OS === 'ios' ? (
              <TouchableOpacity
                onPress={onAppleButtonPress}
                style={[
                  styles.btn,
                  {
                    width: 60,
                    height: 60,
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                ]}>
                <Icon
                  type="AntDesign"
                  name="apple1"
                  size={30}
                  color={colors.back_suave_dark}
                />
              </TouchableOpacity>
            ) : null}

            <TouchableOpacity
              onPress={googleSignIn}
              style={[
                styles.btn,
                {
                  width: 60,
                  height: 60,
                  justifyContent: 'center',
                  alignItems: 'center',
                },
              ]}>
              <Image
                source={image.Google_Icon}
                style={{width: 30, height: 30}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={facebookLogin}
              style={[
                styles.btn,
                {
                  width: 60,
                  height: 60,
                  justifyContent: 'center',
                  alignItems: 'center',
                },
              ]}>
              <Icon
                type="MaterialIcons"
                name="facebook"
                size={30}
                color="#4267B2"
              />
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <View>
          {Platform.OS === 'ios' ? (
            <TouchableOpacity
              onPress={onAppleButtonPress}
              style={[styles.btn, {marginVertical: 5, marginTop: 40}]}>
              <Icon
                type="AntDesign"
                name="apple1"
                size={20}
                style={{marginRight: 10}}
                color={colors.back_suave_dark}
              />
              <CustomText
                style={stylesText.secondaryTextBold}
                light={colors.black}
                dark={colors.black}>
                {t('socialLogin:apple')}
              </CustomText>
            </TouchableOpacity>
          ) : null}

          <TouchableOpacity
            onPress={googleSignIn}
            style={[
              styles.btn,
              {marginVertical: 5, marginTop: Platform.OS === 'ios' ? 5 : 40},
            ]}>
            <Image
              source={image.Google_Icon}
              style={{width: 20, height: 20, marginRight: 10}}
            />
            <CustomText
              style={stylesText.secondaryTextBold}
              light={colors.black}
              dark={colors.black}>
              {t('socialLogin:google')}
            </CustomText>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={facebookLogin}
            style={[styles.btn, {marginVertical: 5}]}>
            <Icon
              type="MaterialIcons"
              name="facebook"
              size={24}
              style={{marginRight: 10}}
              color="#4267B2"
            />
            <CustomText
              style={stylesText.secondaryTextBold}
              light={colors.black}
              dark={colors.black}>
              {t('socialLogin:facebook')}
            </CustomText>
          </TouchableOpacity>
        </View>
      )}
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  btn: {
    padding: 15,
    marginVertical: 20,
    width: dimensions.Width(90),
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    flexDirection: 'row',
  },
});
