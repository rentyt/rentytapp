import React, {useContext, useState} from 'react';
import {View, TextInput, Animated, TouchableOpacity} from 'react-native';
import {DynamicStyleSheet, useDynamicValue} from 'react-native-dynamic';
import {colors, dimensions, image} from '../../../Themes';
import {CustomText} from '../../../Components/CustomText';
import {
  HomeScreenNavigationProp,
  NavPhone,
} from '../../../interfaces/navigation';
import {MainContext} from '../../../store/MainProvider';
import {setItem} from '../../../helpers/AsyncStorage';
import {useMutation} from '@apollo/client';
import {mutations} from '../../../GraphQL';
import {validateEmail} from '../../../Utils/validateEmail';
import LinearGradient from 'react-native-linear-gradient';
import {useEffect} from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {stylesText} from '../../../Themes/TextStyle';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Toast from 'react-native-toast-message';
import SocialLogin from './SocialLogin';
import ModalLoading from '../../../Components/ModalLoading';
import {useTranslation} from 'react-i18next';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

interface Props {
  navigation: HomeScreenNavigationProp;
}

export default function LoginScreen({navigation}: Props) {
  const styles = useDynamicValue(dynamicStyles);
  const [password, setpassword] = useState('');
  const [email, setemail] = useState('');
  const [Loading, setLoading] = useState(false);
  const [error, seterror] = useState(false);
  const [widthImage] = useState(new Animated.Value(0));
  const [HeigthImage] = useState(new Animated.Value(0));
  const [top] = useState(new Animated.Value(0));
  const [opacity] = useState(new Animated.Value(0));

  const {t} = useTranslation();

  const animatedIcon = () => {
    Animated.sequence([
      //@ts-ignore
      Animated.timing(widthImage, {
        toValue: 160,
        duration: 1000,
      }),

      //@ts-ignore
      Animated.timing(HeigthImage, {
        toValue: 80,
        duration: 1000,
      }),

      //@ts-ignore
      Animated.timing(opacity, {
        toValue: 100,
        duration: 300,
      }),

      //@ts-ignore
      Animated.timing(top, {
        toValue: dimensions.Height(0),
        duration: 100,
      }),
    ]).start();
  };

  const {setId, getUser, getCard} = useContext(MainContext);

  useEffect(() => {
    animatedIcon();
  }, []);

  const [LoginUser] = useMutation(mutations.AUTENTICAR_USUARIO);

  const onEnter = () => {
    ReactNativeHapticFeedback.trigger('selection', optiones);
    setLoading(true);
    if (!validateEmail(email)) {
      setLoading(false);
      seterror(true);
      ReactNativeHapticFeedback.trigger('notificationError', optiones);
      Toast.show({
        text1: t('login:Emailincorrecto'),
        text2: t('login:Emailincorrecto'),
        position: 'top',
        type: 'error',
        topOffset: 50,
      });
    } else {
      LoginUser({variables: {email: email, password: password}})
        .then(async res => {
          if (res.data.LoginUser.success) {
            const user = res.data.LoginUser.data;
            if (user.verifyPhone) {
              await setItem('token', user.token);
              await setItem('id', user.id);
              await setItem('user', JSON.stringify(user.user));
              setId(user.id);
              Toast.show({
                text1: t('login:hello'),
                text2: t(`login:${res.data.LoginUser.message}`),
                position: 'top',
                type: 'success',
                topOffset: 50,
              });
              ReactNativeHapticFeedback.trigger(
                'notificationSuccess',
                optiones,
              );

              getUser();
              getCard();
              setTimeout(() => {
                navigation.navigate('Home');
              }, 1500);
            } else {
              getUser();
              getCard();
              const navPhone: NavPhone = {
                id: user.id,
                token: user.token,
                Register: false,
                password: '',
                user: user.user,
                avatar: user.avatar,
                StripeID: user.StripeID,
              };
              navigation.navigate('VerifyPhone', navPhone);
            }
          } else {
            setLoading(false);
            ReactNativeHapticFeedback.trigger('notificationError', optiones);
            Toast.show({
              text1: t('login:bag'),
              text2: t(`login:${res.data.LoginUser.message}`),
              position: 'top',
              type: 'error',
              topOffset: 50,
            });
          }
        })
        .catch(() => {
          ReactNativeHapticFeedback.trigger('notificationError', optiones);
          Toast.show({
            text1: t('login:bag'),
            text2: t('login:bag'),
            position: 'top',
            type: 'error',
            topOffset: 50,
          });
          setLoading(false);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  };

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 0, y: 1}}
      colors={[colors.secundary, colors.main]}
      style={styles.container}>
      <ModalLoading loading={Loading} />
      <Animated.View
        style={{
          alignItems: 'center',
          marginTop: dimensions.Height(5),
          zIndex: 0,
        }}>
        <Animated.Image
          source={image.Logo}
          resizeMode="contain"
          style={{
            width: widthImage,
            height: HeigthImage,
            top: top,
          }}
        />
      </Animated.View>
      <Toast ref={ref => Toast.setRef(ref)} />
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="never"
        showsVerticalScrollIndicator={false}>
        <Animated.View
          style={{
            opacity: opacity,
            alignItems: 'center',
            marginTop: dimensions.IsIphoneX()
              ? dimensions.Height(10)
              : dimensions.Height(7),
          }}>
          <View
            style={[
              styles.input,
              {
                borderTopEndRadius: 10,
                borderTopLeftRadius: 10,
                borderBottomWidth: 0.2,
                borderBottomColor: colors.rgb_235,
                backgroundColor: error ? colors.ERROR : colors.white,
              },
            ]}>
            <CustomText
              style={stylesText.miniTitle}
              light={error ? colors.white : colors.black}
              dark={error ? colors.white : colors.black}>
              {t('login:Email')}
            </CustomText>
            <TextInput
              autoCorrect={false}
              placeholder="jhon@rentytapp.com"
              enablesReturnKeyAutomatically={true}
              textAlign="right"
              autoCompleteType="email"
              numberOfLines={1}
              multiline={false}
              autoCapitalize="none"
              onFocus={() => seterror(false)}
              selectionColor={colors.green}
              placeholderTextColor={error ? colors.white : colors.rgb_153}
              onChangeText={setemail}
              style={[
                styles.textInput,
                {
                  color: error ? colors.white : colors.black,
                  fontSize: 16,
                  fontFamily: 'Poppins',
                  fontWeight: '300',
                },
              ]}
            />
          </View>
          <View
            style={[
              styles.input,
              {borderBottomLeftRadius: 10, borderBottomRightRadius: 10},
            ]}>
            <CustomText
              style={stylesText.miniTitle}
              light={colors.black}
              dark={colors.black}>
              {t('login:password')}
            </CustomText>
            <TextInput
              autoCorrect={false}
              placeholder="••••••••••••••"
              autoCapitalize="none"
              selectionColor={colors.green}
              placeholderTextColor={colors.rgb_153}
              secureTextEntry={true}
              enablesReturnKeyAutomatically={true}
              onChangeText={setpassword}
              style={[
                styles.textInput,
                {
                  fontSize: 20,
                  fontFamily: 'Poppins',
                  fontWeight: '300',
                },
              ]}
            />
          </View>
          <TouchableOpacity
            onPress={() => onEnter()}
            style={[styles.btn, {backgroundColor: colors.black}]}>
            <CustomText
              style={stylesText.secondaryTextBold}
              light={colors.white}
              dark={colors.white}>
              {t('login:login')}
            </CustomText>
          </TouchableOpacity>

          <TouchableOpacity
            style={{marginVertical: dimensions.IsIphoneX() ? 20 : 10}}
            onPress={() => {
              navigation.navigate('Home');
              ReactNativeHapticFeedback.trigger('selection', optiones);
            }}>
            <CustomText
              style={stylesText.secondaryTextBold}
              light={colors.white}
              dark={colors.white}>
              {t('login:Explore')}
            </CustomText>
          </TouchableOpacity>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <CustomText
              style={stylesText.secondaryText}
              light={colors.white}
              dark={colors.white}>
              {t('login:forgot')}
            </CustomText>
            <TouchableOpacity
              style={{marginLeft: 8}}
              onPress={() => {
                navigation.navigate('Forgot');
                ReactNativeHapticFeedback.trigger('selection', optiones);
              }}>
              <CustomText
                style={stylesText.secondaryTextBold}
                light={colors.white}
                dark={colors.white}>
                {t('login:FogotBtn')}
              </CustomText>
            </TouchableOpacity>
          </View>

          <View
            style={{
              marginTop: 10,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <CustomText
              style={stylesText.secondaryText}
              light={colors.white}
              dark={colors.white}>
              {t('login:noAccount')}
            </CustomText>
            <TouchableOpacity
              style={{marginLeft: 8}}
              onPress={() => {
                navigation.navigate('Register');
                ReactNativeHapticFeedback.trigger('selection', optiones);
              }}>
              <CustomText
                style={stylesText.secondaryTextBold}
                light={colors.white}
                dark={colors.white}>
                {t('login:Register')}
              </CustomText>
            </TouchableOpacity>
          </View>
          <SocialLogin
            navigation={navigation}
            setLoading={setLoading}
            register={false}
          />
        </Animated.View>
      </KeyboardAwareScrollView>
    </LinearGradient>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },

  icons: {},

  textInput: {
    width: dimensions.IsIphoneX() ? dimensions.Width(58) : dimensions.Width(55),
    height: dimensions.Height(6),
    textAlign: 'right',
    fontFamily: 'Poppins',
  },

  input: {
    flexDirection: 'row',
    width: dimensions.Width(90),
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.white,
    paddingHorizontal: 20,
  },

  btn: {
    padding: 15,
    marginVertical: 15,
    width: dimensions.Width(90),
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    flexDirection: 'row',
  },
});
