import React from 'react';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {View, TouchableOpacity} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../../Components/CustomText';
import {User} from '../../../interfaces/index';
import {Badget} from '../../../Components/Badget';

export interface Item {
  title: string;
  type:
    | 'Feather'
    | 'AntDesign'
    | 'MaterialCommunityIcons'
    | 'Ionicons'
    | 'Fontisto';
  name: string;
  colorIcon?: string;
  typeIcon: 'link' | 'navigation';
  badget?: boolean;
  badgetNumber?: number;
  onPress: () => void;
}

export interface IAccount {
  user: User;
  item: Item[];
}

export default function Profile(props: IAccount) {
  const {user, item} = props;
  const styles = useDynamicValue(dynamicStyles);

  const ActAdd = {
    light: colors.green,
    dark: colors.green,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  return (
    <View style={styles.content}>
      {item.map((x, i) => {
        return (
          <TouchableOpacity
            style={styles.container}
            key={i}
            onPress={x.onPress}>
            <View>
              <Icon
                type={x.type}
                name={x.name}
                size={26}
                color={x.colorIcon ? x.colorIcon : mainColor}
              />
            </View>
            <View style={styles.names}>
              <CustomText
                style={[stylesText.titleText200, {fontWeight: '300'}]}
                numberOfLines={1}
                light={colors.black}
                dark={colors.white}>
                {x.title}
              </CustomText>
            </View>
            {x.typeIcon === 'link' ? (
              <Icon
                type="Feather"
                name="external-link"
                size={20}
                color={colors.rgb_153}
              />
            ) : (
              <View>
                {x.badget ? (
                  <Badget
                    number={x.badgetNumber}
                    left={-25}
                    bottom={0}
                    right={0}
                  />
                ) : null}
                <Icon
                  type="Feather"
                  name="chevron-right"
                  size={20}
                  color={colors.rgb_153}
                />
              </View>
            )}
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  content: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
  },
  container: {
    width: dimensions.Width(90),
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  names: {
    width: dimensions.IsIphoneX() ? dimensions.Width(69) : dimensions.Width(65),
  },

  avatar: {
    width: 60,
    height: 60,
    borderRadius: 100,
    resizeMode: 'cover',
  },
});
