import React, {useContext} from 'react';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {View, TouchableOpacity} from 'react-native';
import {colors, dimensions, stylesText} from '../../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../../../Components/CustomText';
import {User} from '../../../interfaces/index';
import {useTranslation} from 'react-i18next';
import {Avatar} from '../../../Components/Avatar';
import {MainContext} from '../../../store/MainProvider';

export interface IAccount {
  user: User;
  navigation: any;
}

export default function Profile(props: IAccount) {
  const {user, navigation} = props;
  const styles = useDynamicValue(dynamicStyles);
  const {getUser} = useContext(MainContext);

  const {t} = useTranslation();

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        navigation.navigate('UpdateProfile');
        getUser();
      }}>
      <View>
        <Avatar uri={user.avatar} width={60} height={60} />
      </View>
      <View style={styles.names}>
        <CustomText
          style={[stylesText.secondaryTextBold, {fontSize: 24}]}
          numberOfLines={1}
          light={colors.black}
          dark={colors.white}>
          {user.name} {user.lastName}{' '}
          {user.verified ? (
            <Icon
              type="MaterialIcons"
              name="verified"
              size={16}
              color={colors.twitter_color}
            />
          ) : null}
        </CustomText>
        <CustomText
          style={[stylesText.secondaryText, {color: colors.rgb_153}]}
          numberOfLines={1}
          light={colors.rgb_235}
          dark={colors.rgb_235}>
          {t('profile:myAccountItem')}
        </CustomText>
      </View>
      <Icon
        type="Feather"
        name="chevron-right"
        size={26}
        color={colors.rgb_153}
      />
    </TouchableOpacity>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: dimensions.Width(90),
    padding: 10,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  names: {
    width: dimensions.IsIphoneX() ? dimensions.Width(60) : dimensions.Width(55),
  },
});
