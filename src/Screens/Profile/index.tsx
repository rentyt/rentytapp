import React, {useContext, useEffect} from 'react';
import {View, Alert, Linking, Platform, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicValue,
  useColorSchemeContext,
  DynamicValue,
} from 'react-native-dynamic';
import {MainContext} from '../../store/MainProvider';
import AsyncStorage from '@react-native-community/async-storage';
import SafariView from 'react-native-safari-view';
import {openSettings} from 'react-native-permissions';
import {getVersion} from 'react-native-device-info';
import {useQuery} from '@apollo/client';
import {queryOrder} from '../../GraphQL';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import ItemProfile from './items/profile';
import Items from './items/Items';
import {CustomText} from '../../Components/CustomText';
import {useTranslation} from 'react-i18next';
import {stylesText, colors, dimensions} from '../../Themes';
import Icon from 'react-native-dynamic-vector-icons';
import Mailer from 'react-native-mail';
import RNRestart from 'react-native-restart';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const ProfileScreen = ({navigation, setruteName}) => {
  const {users, getCard, getUser} = useContext(MainContext);
  const styles = useDynamicValue(dynamicStyles);

  useEffect(() => {
    getCard();
    getUser();
  }, []);

  const {t} = useTranslation();

  const ActAdd = {
    light: colors.green,
    dark: colors.green,
  };

  const mode = useColorSchemeContext();
  const mainColor = ActAdd[mode];

  const version = getVersion();

  const type: string = Platform.select({
    ios: 'selection',
    android: 'notificationSuccess',
  });

  const logOut = () => {
    ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
    Alert.alert(
      t('profile:logoutAlert'),
      t('profile:logoutmessage'),
      [
        {
          text: t('profile:cancel'),
          onPress: () => console.log('OK Pressed'),
          style: 'cancel',
        },
        {
          text: t('profile:okLogout'),
          style: 'destructive',
          onPress: async () => {
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
            await AsyncStorage.removeItem('id');
            await AsyncStorage.removeItem('token');
            await AsyncStorage.removeItem('user');
            setruteName('Home');
            RNRestart.Restart();
            //navigation.navigate('Login');
          },
        },
        ,
      ],
      {cancelable: false},
    );
  };

  const OpenURLButton = async (url: any) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    Linking.openURL(url);
  };

  const openWeb = url => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      SafariView.show({
        url: url,
        tintColor: mainColor, // optional
      });
    } else {
      OpenURLButton(url);
    }
  };

  const ope = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    openSettings().catch(() => console.warn('cannot open settings'));
  };

  const review = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      OpenURLButton(
        'https://itunes.apple.com/es/app/rentyt-alquiler-de-vehículos/id1617442948?mt=8&action=write-review',
      );
    } else {
      OpenURLButton(
        'https://play.google.com/store/apps/details?id=com.rentytapp&hl=es_PY&gl=ES&showAllReviews=true',
      );
    }
  };

  const status = ['Nueva', 'Confirmada', 'Entregado'];

  const {data, refetch} = useQuery(queryOrder.GET_ALL_REQUEST, {
    variables: {
      owner: users._id,
      status: status,
      page: 1,
      limit: 10,
    },
  });

  const order = data && data.getAllRequest ? data.getAllRequest.data : [];

  setTimeout(() => {
    refetch();
  }, 2000);

  const goToEmail = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      Mailer.mail(
        {
          subject: 'Vefificación de cuenta',
          recipients: ['verify@rentytapp.com'],
          body: `<b>Hola solicito verificar mi cuenta en Rentyt Datos: Teléfono: ${users.phone}, Email: ${users.email}, ID: ${users._id}</b>`,
          isHTML: true,
        },
        (error, event) => {
          if (event === 'send') {
            Alert.alert(t(`verified:send`));
          }
        },
      );
    } else {
      OpenURLButton(
        `mailto:"verify@rentytapp.com"?subject=Vefificación de cuenta&body=Hola solicito verificar mi cuenta en Rentyt`,
      );
    }
  };

  const VerifyAccount = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    Alert.alert(t('verified:alertTitle'), t('verified:AlertMEssage'), [
      {
        text: t('verified:btnAert'),
        onPress: () => goToEmail(),
      },
    ]);
  };

  return (
    <View>
      {!users.verified ? (
        <View style={styles.verify}>
          <Icon
            type="MaterialIcons"
            name="verified"
            size={30}
            color={colors.twitter_color}
          />
          <View style={{width: dimensions.Height(35), marginLeft: 10}}>
            <CustomText
              numberOfLines={1}
              style={[stylesText.secondaryTextBold]}
              light={colors.twitter_color}
              dark={colors.twitter_color}>
              {t('verified:title')}
            </CustomText>
            <CustomText
              numberOfLines={3}
              style={[stylesText.secondaryText]}
              light={colors.twitter_color}
              dark={colors.twitter_color}>
              {t('verified:message')}
            </CustomText>
            <TouchableOpacity
              onPress={VerifyAccount}
              style={{
                marginTop: 10,
                borderWidth: 1,
                borderColor: colors.twitter_color,
                width: 100,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
                borderRadius: 100,
              }}>
              <CustomText
                numberOfLines={3}
                style={[stylesText.TitleCard]}
                light={colors.twitter_color}
                dark={colors.twitter_color}>
                {t('verified:Empezar')}
              </CustomText>
            </TouchableOpacity>
          </View>
        </View>
      ) : null}

      <View style={styles.container}>
        <ItemProfile user={users} navigation={navigation} />
        <View
          style={{
            alignItems: 'flex-start',
            marginHorizontal: 20,
            marginTop: 30,
          }}>
          <CustomText
            style={[stylesText.secondaryTextBold, {marginBottom: 15}]}
            numberOfLines={1}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('profile:subtitleMyAdd')}
          </CustomText>
          <Items
            user={users}
            item={[
              {
                title: t('profile:myBook'),
                type: 'Feather',
                name: 'calendar',
                typeIcon: 'navigation',
                onPress: () => navigation.navigate('MyOrders'),
              },
              {
                title: t('profile:myCar'),
                type: 'Ionicons',
                name: 'car-sport-outline',
                typeIcon: 'navigation',
                onPress: () => navigation.navigate('MyAds'),
              },

              {
                title: t('profile:myRequests'),
                type: 'Feather',
                name: 'clock',
                typeIcon: 'navigation',
                badget: true,
                badgetNumber: order && order.length,
                onPress: () => navigation.navigate('MyRequest'),
              },
              {
                title: t('profile:myvaloration'),
                type: 'Feather',
                name: 'star',
                colorIcon: colors.orange1,
                typeIcon: 'navigation',
                onPress: () => navigation.navigate('MyRating'),
              },
            ]}
          />
        </View>
        <View
          style={{
            alignItems: 'flex-start',
            marginHorizontal: 20,
            marginTop: 30,
          }}>
          <CustomText
            style={[stylesText.secondaryTextBold, {marginBottom: 15}]}
            numberOfLines={1}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('profile:subTitlePayments')}
          </CustomText>
          <Items
            user={users}
            item={[
              {
                title: t('profile:paymentMethod'),
                type: 'Feather',
                name: 'credit-card',
                onPress: () => {
                  getCard();
                  navigation.navigate('PaymentMethod'); //@ts-ignore
                  ReactNativeHapticFeedback.trigger(type, optiones);
                },
                typeIcon: 'navigation',
              },
              {
                title: t('profile:cuponCode'),
                type: 'MaterialCommunityIcons',
                name: 'ticket-percent-outline',
                typeIcon: 'navigation',
                onPress: () => {
                  navigation.navigate('PromoCode');
                  //@ts-ignore
                  ReactNativeHapticFeedback.trigger(type, optiones);
                },
              },
              {
                title: t('profile:gift'),
                type: 'Feather',
                name: 'gift',
                typeIcon: 'navigation',
                onPress: () => navigation.navigate('Share'),
              },
            ]}
          />
        </View>

        <View
          style={{
            alignItems: 'flex-start',
            marginHorizontal: 20,
            marginTop: 30,
          }}>
          <CustomText
            style={[stylesText.secondaryTextBold, {marginBottom: 15}]}
            numberOfLines={1}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('profile:helpTitle')}
          </CustomText>
          <Items
            user={users}
            item={[
              {
                title: t('profile:how'),
                type: 'Feather',
                name: 'help-circle',
                onPress: () => openWeb('https://rentytapp.com'),
                typeIcon: 'link',
              },
              {
                title: t('profile:help'),
                type: 'Feather',
                name: 'mail',
                typeIcon: 'link',
                onPress: () => openWeb('https://rentytapp.com/contact'),
              },
            ]}
          />
        </View>

        <View
          style={{
            alignItems: 'flex-start',
            marginHorizontal: 20,
            marginTop: 30,
          }}>
          <CustomText
            style={[stylesText.secondaryTextBold, {marginBottom: 15}]}
            numberOfLines={1}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('profile:legalTitle')}
          </CustomText>
          <Items
            user={users}
            item={[
              {
                title: t('profile:privacity'),
                type: 'Feather',
                name: 'lock',
                onPress: () => openWeb('https://rentytapp.com/privacity'),
                typeIcon: 'link',
              },
              {
                title: t('profile:cookie'),
                type: 'MaterialCommunityIcons',
                name: 'cookie',
                typeIcon: 'link',
                onPress: () => openWeb('https://rentytapp.com/cookie'),
              },

              {
                title: t('profile:condition'),
                type: 'Feather',
                name: 'book',
                typeIcon: 'link',
                onPress: () => openWeb('https://rentytapp.com/use-condition'),
              },
            ]}
          />
        </View>

        <View
          style={{
            alignItems: 'flex-start',
            marginHorizontal: 20,
            marginTop: 30,
            marginBottom: dimensions.Height(15),
          }}>
          <CustomText
            style={[stylesText.secondaryTextBold, {marginBottom: 15}]}
            numberOfLines={1}
            light={colors.back_suave_dark}
            dark={colors.white}>
            {t('profile:logoutSubTitle')}
          </CustomText>
          <Items
            user={users}
            item={[
              {
                title: t('profile:setting'),
                type: 'Feather',
                name: 'settings',
                onPress: () => ope(),
                typeIcon: 'link',
              },
              {
                title: t('profile:ratingapp'),
                type: 'Fontisto',
                name: Platform.OS == 'ios' ? 'app-store' : 'google-play',
                typeIcon: 'link',
                onPress: () => review(),
              },

              {
                title: t('profile:logout'),
                type: 'Feather',
                name: 'log-out',
                colorIcon: colors.ERROR,
                typeIcon: 'navigation',
                onPress: () => logOut(),
              },
            ]}
          />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 50,
              width: dimensions.Width(90),
            }}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginHorizontal: 15,
              }}>
              <TouchableOpacity
                style={[styles.btnSocial]}
                onPress={() =>
                  OpenURLButton('https://www.instagram.com/rentyt.do')
                }>
                <Icon
                  name="instagram"
                  type="Feather"
                  size={24}
                  color={colors.rgb_153}
                />
              </TouchableOpacity>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryText, {marginTop: 5}]}>
                Instagram
              </CustomText>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginHorizontal: 15,
              }}>
              <TouchableOpacity
                style={styles.btnSocial}
                onPress={() =>
                  OpenURLButton('https://www.facebook.com/rentyt.do')
                }>
                <Icon
                  name="facebook"
                  type="FontAwesome"
                  size={24}
                  color={colors.rgb_153}
                />
              </TouchableOpacity>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryText, {marginTop: 5}]}>
                Facebook
              </CustomText>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: 15,
                marginLeft: 20,
              }}>
              <TouchableOpacity
                style={styles.btnSocial}
                onPress={() => OpenURLButton('https://twitter.com/rentyt_DO')}>
                <Icon
                  name="twitter"
                  type="AntDesign"
                  size={24}
                  color={colors.rgb_153}
                />
              </TouchableOpacity>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[stylesText.secondaryText, {marginTop: 5}]}>
                Twitter
              </CustomText>
            </View>
          </View>
          <View
            style={{
              width: dimensions.Width(90),
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 20,
            }}>
            <CustomText
              style={[stylesText.secondaryText, {marginTop: 20}]}
              numberOfLines={1}
              light={colors.rgb_153}
              dark={colors.rgb_153}>
              {t('profile:version')} {version}
            </CustomText>
          </View>
        </View>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  verify: {
    backgroundColor: '#1b94e029',
    marginTop: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 15,
    borderRadius: 15,
  },

  btnSocial: {
    width: 40,
    height: 40,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_dark),
  },
});
export default ProfileScreen;
