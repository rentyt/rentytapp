import React, {useState, useRef} from 'react';
import {View, Platform, RefreshControl, Animated} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import Header from '../../Components/Header';
import {useQuery} from '@apollo/client';
import {query} from '../../GraphQL';
import {dimensions, stylesText, colors} from '../../Themes';
import CardAds from '../../Components/AdsCard/SmallCard';
import {CustomText} from '../../Components/CustomText';
import Loading from '../../Components/PlaceHolded/SmallCard';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function OwnerCar({route, navigation}) {
  const {id} = route.params;
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const [refreshing, setRefreshing] = useState(false);
  const flatlistRef = useRef();
  const scrollY = useRef(new Animated.Value(0)).current;

  const {data, loading, refetch} = useQuery(query.GET_ALL_ADS, {
    variables: {
      input: {
        owner: id,
        status: 'Succees',
        visible: true,
      },
    },
  });

  const ads = data && data.getAds ? data.getAds.data : [];

  setTimeout(() => {
    refetch();
  }, 1500);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const _renderItem = ({item, index}) => {
    return (
      <CardAds
        data={item}
        navigation={navigation}
        refetch={refetch}
        scrollY={scrollY}
        index={index}
      />
    );
  };

  const RenderHeader = () => {
    return (
      <View style={{marginHorizontal: 10, marginVertical: 30}}>
        <CustomText
          style={stylesText.h1}
          light={colors.black}
          dark={colors.white}>
          {t('ownerCar:titleHeader')}
        </CustomText>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header
        navigation={navigation}
        title={t('ownerCar:titleHeader')}
        leftIcon={false}
      />
      {loading ? (
        <>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 30,
            }}>
            <Loading />
            <Loading />
            <Loading />
            <Loading />
            <Loading />
            <Loading />
          </View>
        </>
      ) : (
        <View>
          <Animated.FlatList
            data={ads}
            ref={flatlistRef}
            onScroll={Animated.event(
              [{nativeEvent: {contentOffset: {y: scrollY}}}],
              {useNativeDriver: true},
            )}
            renderItem={(item: any) => _renderItem(item)}
            keyExtractor={(item: any) => item._id}
            style={{marginLeft: 5}}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={RenderHeader}
            ListFooterComponent={
              <View style={{marginBottom: dimensions.Height(20)}} />
            }
            onEndReachedThreshold={0.1}
            initialScrollIndex={0}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                enabled={true}
                title="Cargando"
                onRefresh={_onRefresh}
              />
            }
          />
        </View>
      )}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
