import React, {useContext, useState, useRef, useEffect} from 'react';
import Header from '../../Components/Header';
import {
  View,
  Platform,
  FlatList,
  Animated,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {dimensions, colors, stylesText} from '../../Themes';
import {useTranslation} from 'react-i18next';
import {MainContext} from '../../store/MainProvider';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import SegmentedControl from '@react-native-community/segmented-control';
import {useQuery} from '@apollo/client';
import {queryOrder} from '../../GraphQL';
import LottieView from 'lottie-react-native';
import {CustomText} from '../../Components/CustomText';
import Items from '../MyOrder/Items';
import LoadingComponent from '../../Components/PlaceHolded/List';
import LoadingAnimated from '../../Components/LoadingAnimated';
import Icon from 'react-native-dynamic-vector-icons';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function MyOrder({navigation}) {
  const styles = useDynamicValue(dynamicStyles);
  const {t} = useTranslation();
  const {users} = useContext(MainContext);
  const [active, setActive] = useState(0);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [stopFetchMore, setStopFetchMore] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const flatlistRef = useRef();
  const [datos, setdatos] = useState([]);
  const scrollY = useRef(new Animated.Value(0)).current;

  const opacityText = scrollY.interpolate({
    inputRange: [300, 700],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  const status = ['Nueva', 'Confirmada', 'Entregado'];

  const statusLst = ['Rechazado', 'Recogido', 'Finalizada', 'Devuelto'];

  const response = useQuery(queryOrder.GET_ALL_REQUEST, {
    variables: {
      owner: users._id,
      status: status,
      page: 1,
      limit: 100,
    },
  });

  const orderNew =
    response && response.data && response.data.getAllRequest
      ? response.data.getAllRequest.data
      : [];

  const {data, refetch, loading} = useQuery(queryOrder.GET_ALL_REQUEST, {
    variables: {
      owner: users._id,
      status: statusLst,
      page: page,
      limit: limit,
    },
  });

  const order = data && data.getAllRequest ? data.getAllRequest.data : [];

  useEffect(() => {
    if (order.length > 0) {
      order.forEach(items => {
        var i = datos.findIndex((x: any) => x._id === items._id);
        if (i !== -1) {
          datos.splice(i, 1);
          setdatos(datos.concat(order));
        } else {
          setdatos(datos.concat(order));
        }
      });
    } else if (order.length < 9) {
      setStopFetchMore(false);
    } else {
      setStopFetchMore(false);
    }
  }, [order]);

  setTimeout(() => {
    refetch();
    response.refetch();
  }, 1000);

  const getItemLayout = (data, index) => ({
    length: 50,
    offset: 50 * index,
    index,
  });

  const scrollToIndex = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    let randomIndex = 0;
    if (flatlistRef) {
      //@ts-ignore
      flatlistRef.current.scrollToIndex({animated: true, index: randomIndex});
    }
  };

  const loadMoreData = () => {
    if (stopFetchMore && !loading) {
      setPage(page + 1);
    }
  };

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
      setStopFetchMore(true);
    }, 2000);
  };

  const renderItem = ({item}) => {
    return (
      <Items
        data={item}
        navigation={navigation}
        refetch={refetch}
        request={true}
      />
    );
  };

  const renderFooster = () => {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: dimensions.Height(12),
          marginTop: 20,
        }}>
        {loading ? (
          <LoadingAnimated name={t('feed:loading')} />
        ) : (
          <>
            {loading && stopFetchMore && (
              <LoadingAnimated name={t('feed:loading')} />
            )}
          </>
        )}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header
        navigation={navigation}
        title={t('request:titleHeader')}
        leftIcon={false}
      />
      <View
        style={{
          width: '96%',
          alignSelf: 'center',
          marginTop: 20,
          marginBottom: 30,
          marginHorizontal: 15,
        }}>
        <SegmentedControl
          values={[t('myOrder:incourse'), t('myOrder:last')]}
          selectedIndex={active}
          onChange={event => {
            //@ts-ignore
            ReactNativeHapticFeedback.trigger(type, optiones);
            setActive(event.nativeEvent.selectedSegmentIndex);
            setPage(page);
          }}
        />
      </View>

      {loading ? (
        <View style={{height: dimensions.ScreenHeight}}>
          <LoadingComponent />
        </View>
      ) : (
        <FlatList
          data={active === 1 ? datos : orderNew}
          renderItem={(item: any) => renderItem(item)}
          keyExtractor={(item: any) => item._id}
          ref={flatlistRef}
          onScroll={Animated.event([
            {nativeEvent: {contentOffset: {y: scrollY}}},
          ])}
          showsVerticalScrollIndicator={false}
          getItemLayout={getItemLayout}
          onEndReached={active === 1 ? loadMoreData : null}
          onEndReachedThreshold={0.1}
          initialScrollIndex={0}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              enabled={true}
              title="Cargando"
              onRefresh={_onRefresh}
            />
          }
          ListEmptyComponent={
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <LottieView
                source={require('../../Assets/animated/Nodata.json')}
                autoPlay
                loop
                style={{width: 200}}
              />
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={[
                  stylesText.secondaryText,
                  {textAlign: 'center', marginHorizontal: 30},
                ]}>
                {t('request:noData')}
              </CustomText>
            </View>
          }
          ListFooterComponent={
            active === 1 ? (
              renderFooster
            ) : (
              <View style={{marginBottom: dimensions.Height(12)}} />
            )
          }
        />
      )}
      {active === 1 ? (
        <Animated.View style={[styles.top, {opacity: opacityText}]}>
          <TouchableOpacity
            onPress={scrollToIndex}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <CustomText
              style={[
                stylesText.secondaryTextBold,
                {fontSize: 14, colors: colors.black},
              ]}>
              {t('feed:offert:top')}
            </CustomText>
            <Icon
              name="chevron-up"
              type="Feather"
              size={20}
              color={colors.black}
            />
          </TouchableOpacity>
        </Animated.View>
      ) : null}
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  top: {
    position: 'absolute',
    zIndex: 100,
    width: dimensions.Width(20),
    borderRadius: 100,
    backgroundColor: colors.white,
    padding: 10,
    bottom: 30,
    left: '40%',
    shadowColor: new DynamicValue(colors.light_grey, colors.black),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
});
