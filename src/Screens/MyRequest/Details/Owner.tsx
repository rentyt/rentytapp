import React from 'react';
import {View, Platform, TouchableOpacity, Linking} from 'react-native';
import {CustomText} from '../../../Components/CustomText';
import {dimensions, colors, stylesText} from '../../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useQuery} from '@apollo/client';
import {query} from '../../../GraphQL';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {useTranslation} from 'react-i18next';
import {Avatar} from '../../../Components/Avatar';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const type = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

export default function Car({data, users, navigation}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {t} = useTranslation();

  const reponse = useQuery(query.GET_CONVERSATION_ONE, {
    variables: {userID: users._id, adsID: data.car},
  });

  const chast =
    reponse && reponse.data && reponse.data.getConversationOne
      ? reponse.data.getConversationOne.data
      : {data: ''};

  const dataNAv = chast
    ? chast
    : {
        ads: {
          _id: data.Car._id,
          car: data.Car.car,
          imageCar: data.Car.images[0].uri,
          owner: data.Car.owner,
        },
        user: data.Customer,
        Owner: data.Owner,
        messages: [],
        userID: data.Customer._id,
        adsID: data.car,
        owner: data.wner,
        userRoomID: `${Number.parseInt(getRandomArbitrary(10000, 100000000))}`,
        ownerRoomID: `${Number.parseInt(getRandomArbitrary(10000, 100000000))}`,
        created_at: new Date(),
      };

  const onPress = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    if (Platform.OS === 'ios') {
      Linking.openURL(`tel:${data.Customer.phone}`);
    }
  };

  const initChat = () => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(type, optiones);
    reponse.refetch();
    dataNAv.refetch = reponse.refetch;
    setTimeout(() => {
      navigation.navigate('ChatScreen', dataNAv);
    }, 100);
  };

  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
      }}>
      <View style={[styles.items]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            width: dimensions.Width(50),
          }}>
          <View style={{width: 65}}>
            <Avatar uri={data.Customer.avatar} width={60} height={60} />
          </View>
          <View style={{marginLeft: 5}}>
            <CustomText
              numberOfLines={1}
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {data.Customer.name} {data.Customer.lastName}{' '}
            </CustomText>
            <CustomText
              numberOfLines={2}
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText]}>
              Teléfono: +{data.Customer.phone}
            </CustomText>
          </View>
        </View>
        <View style={styles.line}>
          <TouchableOpacity onPress={onPress}>
            <Icon name="phone" type="Feather" size={30} color={colors.green} />
          </TouchableOpacity>
        </View>
      </View>

      <TouchableOpacity style={styles.btn} onPress={initChat}>
        <Icon
          name="message-square"
          type="Feather"
          size={20}
          color={colors.green}
          style={{marginTop: 3, marginRight: 7}}
        />
        <CustomText
          numberOfLines={1}
          light={colors.green}
          dark={colors.green}
          style={stylesText.secondaryTextBold}>
          {t('myOrder:details:chatBtn')} {data.Customer.name.toUpperCase()}
        </CustomText>
      </TouchableOpacity>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  mainImages: {
    width: 60,
    height: 60,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
  },

  mainImage: {
    width: 56,
    height: 56,
    borderRadius: 100,
  },

  iconContent: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: 7,
  },

  icon: {
    width: 25,
    height: 25,
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4,
    elevation: Platform.select({ios: 7, android: 5}),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  mapContent: {
    overflow: 'hidden',
    height: dimensions.Height(20),
    width: dimensions.Width(94),
    borderRadius: 10,
  },

  items: {
    height: 'auto',
    width: dimensions.Width(90),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    padding: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    shadowColor: new DynamicValue(colors.rgb_153, colors.back_dark),
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: Platform.select({ios: 18, android: 13}),
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.white, colors.back_suave_dark),
  },

  btn: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
    borderColor: colors.green,
    borderWidth: 1,
    width: dimensions.Width(94),
    marginTop: 30,
    padding: 16,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  line: {
    borderLeftWidth: 0.5,
    borderLeftColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
