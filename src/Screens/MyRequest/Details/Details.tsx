import React, {useContext, useState} from 'react';
import {View, Modal, TouchableOpacity, Platform, Alert} from 'react-native';
import {CustomText} from '../../../Components/CustomText';
import {dimensions, colors, stylesText} from '../../../Themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useColorSchemeContext,
} from 'react-native-dynamic';
import {useTranslation} from 'react-i18next';
import HeaderScrollView from 'react-native-header-scroll-view';
import {useQuery, useMutation} from '@apollo/client';
import {queryOrder, mutations} from '../../../GraphQL';
import {MainContext} from '../../../store/MainProvider';
import Car from '../../MyOrder/Details/Car';
import Owner from './Owner';
import Items from '../../MyOrder/Details/Items';
import DaySeleted from '../../MyOrder/Details/DaySelected';
import Proccess from '../../MyOrder/Details/proccess';
import HeaderModal from '../../../Components/HeaderModal';
import Note from '../../MyOrder/Details/Note';
import Resumen from '../../MyOrder/Details/Resumen';
import ItemBaby from '../../MyOrder/Details/Itemsbaby';
import ItemExtra from '../../MyOrder/Details/ItemsExtra';
import Icon from 'react-native-dynamic-vector-icons';
import ModalLoading from '../../../Components/ModalLoading';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import LoadingComponent from '../../../Components/PlaceHolded/Details';

const optiones = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

const types: string = Platform.select({
  ios: 'selection',
  android: 'notificationSuccess',
});

export default function Details({route, navigation}) {
  const {id} = route.params;
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {t} = useTranslation();
  const modo = useColorSchemeContext();
  const {users} = useContext(MainContext);
  const [visible, setVisible] = useState(false);
  const [Loading, setLoading] = useState(false);

  const [NewOrdenProceed] = useMutation(mutations.PROCESS_ORDER);

  const {data, loading, refetch} = useQuery(queryOrder.GET_ORDER_ID, {
    variables: {id: id},
  });

  const order = data && data.getOrder ? data.getOrder.data : null;

  setTimeout(() => {
    refetch();
  }, 1500);

  const titleColor = {
    light: colors.black,
    dark: colors.white,
  };

  const textColor = titleColor[modo];

  const bgColor = {
    light: colors.colorInput,
    dark: colors.back_dark,
  };

  const bg = bgColor[modo];

  const borderColor = {
    light: colors.rgb_235,
    dark: colors.back_suave_dark,
  };

  const border = borderColor[modo];

  if (loading) {
    return (
      <View style={{height: dimensions.ScreenHeight}}>
        <LoadingComponent />
      </View>
    );
  }

  const proccessOrder = (ordenId, status, IntegerValue) => {
    //@ts-ignore
    ReactNativeHapticFeedback.trigger(types, optiones);
    setLoading(true);
    NewOrdenProceed({variables: {ordenId, status, IntegerValue}})
      .then(res => {
        if (res.data.NewOrdenProceed.success) {
          setLoading(false);
          refetch();
          //@ts-ignore
          ReactNativeHapticFeedback.trigger(types, optiones);
        } else {
          setLoading(false);
          refetch();
          Alert.alert(res.data.NewOrdenProceed.messages);
        }
      })
      .catch(e => {
        Alert.alert('Algo salio mal intentalo de nuevo');
        console.log(e);
        setLoading(false);
      });
  };

  const cancelOrder = () => {
    Alert.alert(
      t('myOrder:details:alertTitle'),
      t('myOrder:details:alertMessage'),
      [
        {
          text: t('myOrder:details:cancel'),
          onPress: () => console.log('OK Pressed'),
          style: 'cancel',
        },
        {
          text: t('myOrder:details:okPreset'),
          style: 'destructive',
          onPress: async () => {
            ReactNativeHapticFeedback.trigger('notificationSuccess', optiones);
            proccessOrder(order._id, 'Rechazado', 0);
          },
        },
        ,
      ],
      {cancelable: false},
    );
  };
  const onPressBTN = () => {
    if (order.status === 'Nueva') {
      proccessOrder(order._id, 'Confirmada', 50);
    } else if (order.status === 'Confirmada') {
      proccessOrder(order._id, 'Entregado', 60);
    } else if (order.status === 'Entregado') {
      proccessOrder(order._id, 'Recogido', 70);
    }
  };

  const btnTitle = () => {
    if (order.status === 'Nueva') {
      return t(`status:Confirmada`);
    } else if (order.status === 'Confirmada') {
      return t(`status:Entregado`);
    } else if (order.status === 'Entregado') {
      return t(`status:Recogido`);
    }
  };

  const isBTN = () => {
    if (
      order.status === 'Rechazado' ||
      order.status === 'Devuelto' ||
      order.status === 'Finalizada' ||
      order.status === 'Recogido'
    ) {
      return false;
    } else {
      return true;
    }
  };

  const colorsStatus = () => {
    if (order.status === 'Rechazado') {
      return colors.ERROR;
    } else if (order.status === 'Devuelto') {
      return colors.ERROR;
    } else if (order.status === 'Pendiente de pago') {
      return colors.orange1;
    } else if (order.status === 'Nueva') {
      return colors.orange1;
    } else {
      return colors.green;
    }
  };

  const displayStatus = t(`status:${order.status}`);

  return (
    <View style={styles.container}>
      <ModalLoading loading={Loading} />
      <HeaderScrollView
        title={`ID #${order.channelOrderDisplayId}`}
        titleStyle={{color: textColor}}
        containerStyle={{
          backgroundColor: 'transparent',
        }}
        headerContainerStyle={{
          backgroundColor: bg,
          borderBottomColor: border,
        }}
        headlineStyle={{
          color: textColor,
          marginTop: 30,
          fontSize: 20,
          fontWeight: 'bold',
        }}
        scrollViewProps={{
          showsVerticalScrollIndicator: false,
          keyboardShouldPersistTaps: 'handled',
        }}>
        <View style={{marginTop: 20, marginBottom: dimensions.Height(20)}}>
          <TouchableOpacity
            style={[styles.status, {backgroundColor: colorsStatus()}]}
            onPress={() => setVisible(true)}>
            <CustomText
              numberOfLines={1}
              style={[stylesText.TitleCard]}
              light={colors.white}
              dark={colors.white}>
              {displayStatus.toUpperCase()}
            </CustomText>
          </TouchableOpacity>
          <View style={{margin: 20}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {t('myOrder:details:ownerTitle')}
            </CustomText>
            <Owner data={order} users={users} navigation={navigation} />
          </View>

          <View style={{marginHorizontal: 20, marginTop: 20}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {t('myOrder:details:carTitle')}
            </CustomText>
          </View>
          <Car data={order} navigation={navigation} request={true} />
          <Items data={order} />
          {order.babyChair.length > 0 ? <ItemBaby data={order} /> : null}
          {order.extraEquipament.length > 0 ? <ItemExtra data={order} /> : null}

          <DaySeleted
            arrayDates={order.selectdDay}
            deliveryTime={order.deliveryDate.time}
            timePickup={order.pickupDate.time}
          />
          <View style={{marginHorizontal: 20, marginTop: 20}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {t('myOrder:details:noteTitle')}
            </CustomText>
          </View>
          <Note data={order} />

          <View style={{marginHorizontal: 20, marginTop: 30}}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={[stylesText.secondaryTextBold]}>
              {t('myOrder:details:resumen')}
            </CustomText>
          </View>
          <Resumen data={order} request={true} />
        </View>
      </HeaderScrollView>
      <View style={styles.contentIcon}>
        <TouchableOpacity
          activeOpacity={100}
          onPress={() => navigation.goBack()}
          style={[styles.btnBack]}>
          <CustomText
            light={colors.white}
            dark={colors.black}
            style={[stylesText.secondaryTextBold]}>
            <Icon
              type="Feather"
              name="arrow-left"
              size={28}
              style={styles.iconColor}
            />
          </CustomText>
        </TouchableOpacity>
        {isBTN() ? (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {order.status === 'Nueva' ? (
              <TouchableOpacity
                onPress={cancelOrder}
                style={[
                  styles.btnBackLargue,
                  {
                    backgroundColor: colors.ERROR,
                    width: '40%',
                    marginRight: 20,
                  },
                ]}
                activeOpacity={100}>
                <CustomText
                  light={colors.white}
                  dark={colors.white}
                  style={[stylesText.secondaryTextBold]}>
                  {t('myOrder:details:rechazoBtn')}
                </CustomText>
              </TouchableOpacity>
            ) : null}

            <TouchableOpacity
              onPress={onPressBTN}
              style={[
                styles.btnBackLargue,
                {width: order.status === 'Nueva' ? '43%' : '80%'},
              ]}
              activeOpacity={100}>
              <CustomText
                light={colors.white}
                dark={colors.white}
                style={[stylesText.secondaryTextBold]}>
                {btnTitle()}
              </CustomText>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
      <Modal
        animationType="slide"
        visible={visible}
        presentationStyle="formSheet"
        statusBarTranslucent={true}
        onRequestClose={() => setVisible(false)}>
        <HeaderModal
          onPress={() => setVisible(false)}
          title={t('myOrder:details:status')}
          children={
            <View style={styles.centeredView}>
              <Proccess data={order.statusProcess} />
            </View>
          }
        />
      </Modal>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  status: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 100,
    maxWidth: 150,
    marginLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },

  centeredView: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
  },

  contentIcon: {
    position: 'absolute',
    bottom: 25,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    width: dimensions.Width(94),
  },

  btnBack: {
    padding: 10,
    width: 55,
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 100,
    backgroundColor: new DynamicValue(colors.black, colors.white),
  },

  btnBackLargue: {
    padding: 10,
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 100,
    backgroundColor: colors.green,
  },
  iconColor: {
    color: new DynamicValue(colors.white, colors.black),
  },
});
