/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState, useEffect} from 'react';
import {View, Text, LogBox} from 'react-native';
import Router from './src/Router';
import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';
import OneSignal from 'react-native-onesignal';
import NoInternet from './src/Components/ModalPermision/NoInternet';
import NetInfo from '@react-native-community/netinfo';

import './src/Language';

LogBox.ignoreAllLogs();

const App = ({navigation}) => {
  const [token, settoken] = useState(null);
  const [Loading, setLoading] = useState(true);
  const [ModalInternet, setModalInternet] = useState(false);
  const [conectedInterner, setconectedInterner] = useState(true);

  let conexión = true;

  OneSignal.setLogLevel(6, 0);
  OneSignal.setAppId('0bd7fc89-c247-4972-8aeb-cc65af8f7fb0');

  OneSignal.setNotificationWillShowInForegroundHandler(
    notificationReceivedEvent => {
      let notification = notificationReceivedEvent.getNotification();
      notificationReceivedEvent.complete(notification);
    },
  );

  const getToken = async () => {
    AsyncStorage.getItem('token')
      .then(res => {
        const data = JSON.parse(res);
        settoken(data);
      })
      .catch(e => {
        console.error(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getToken();
  }, [token]);

  useEffect(() => {
    const country = RNLocalize.getLocales();
    const currecy = RNLocalize.getCurrencies();
    AsyncStorage.setItem('countryCode', country[0].countryCode);
    AsyncStorage.setItem('localeCode', country[0].languageTag);
    AsyncStorage.setItem('languageCode', country[0].languageCode);
    AsyncStorage.setItem('currecy', currecy[0]);
  }, [conectedInterner]);

  useEffect(() => {
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
        setModalInternet(true);
        setconectedInterner(state.isConnected);
      } else {
        setconectedInterner(state.isConnected);
        setModalInternet(false);
      }
    });
  }, [conexión]);

  NetInfo.addEventListener(state => {
    conexión = state.isConnected;
  });

  return (
    <>
      {Loading ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgb(41, 216, 132)',
          }}>
          <Text style={{fontFamily: 'Poppins', color: 'white'}}>
            Loading...
          </Text>
        </View>
      ) : (
        <Router token={token} />
      )}
      <NoInternet
        visibleModal={ModalInternet}
        setModalVisible={setModalInternet}
        isINternet={conectedInterner}
        setconectedInterner={setconectedInterner}
      />
    </>
  );
};

export default App;

//paypal sb-jcdov8894988@personal.example.com

//ar:l5<=O
